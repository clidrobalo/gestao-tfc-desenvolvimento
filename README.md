# Gestão de TFCs

Plataforma web de suporte à unidade curricular de Trabalho Final de Curso (TFC), 
das licenciaturas em Engenharia Informática, Informática de Gestão e 
Engenharia Informática, Redes e Telecomunicações.

O desenvolvimento desta plataforma foi ele próprio um TFC do aluno Francisco Santos, 
implementado no ano lectivo de 2017/18.

A plataforma está disponível em https://deisi.ulusofona.pt/tfc

## Instalação

É necessário ter o MySQL instalado. A respectiva base de dados pode ser criada da seguinte forma:

```
create database gestaotfc;

create user 'gestaotfc'@'localhost' identified by 'tfc';

grant all privileges on gestaotfc.* to 'gestaotfc'@'localhost';
```