package pt.ulusofona.deisi.gestaotfcs.controller;

import com.sun.security.auth.UserPrincipal;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import pt.ulusofona.deisi.gestaotfcs.data.TFC;
import pt.ulusofona.deisi.gestaotfcs.data.Utilizador;
import pt.ulusofona.deisi.gestaotfcs.services.ServiceInformacao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"file:src/main/webapp/WEB-INF/dispatcher-servlet-test.xml",
                       "file:src/main/webapp/WEB-INF/security-test.xml"})
@WebAppConfiguration
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class TestFormController {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private ServiceInformacao serviceInformacao;

    @PersistenceContext
    private EntityManager em;

    private MockMvc mvc;

    @Before
    public void setup() {
        mvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();

        serviceInformacao.criaUtilizadorProfessor(em, "Professor 1", "p111");
        serviceInformacao.criaUtilizadorProfessor(em, "Coordenador", "p1");
    }

    @Test
    public void testUnauthenticatedHomePageShouldRedirectToLogin() throws Exception {

        mvc.perform(get("/"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithMockUser(username = "a111", password = "password", roles = "STUDENT")
    public void testHomePage() throws Exception {

        mvc.perform(get("/"))
                .andExpect(status().isOk());
    }


    @Test
    @WithMockUser(username = "a111", password = "password", roles = "STUDENT")
    public void testListarVazio() throws Exception {

        mvc.perform(get("/listar"))
                .andExpect(status().isOk())
                .andExpect(view().name("listar"))
                .andExpect(model().attribute("listaPreponente", new ArrayList<>()));

    }

    @Test
    @WithMockUser(username = "a111", password = "password", roles = "STUDENT")
    public void testAlunoInserePropostaEListar() throws Exception {

        mvc.perform(post("/formaluno")
                .param("titulo", "Titulo do TFC")
                .param("descricao", "Descrição do TFC")
                .param("cursoAssociado", "1")  // 1 = LEI
                .param("disciplinasTFC", "1", "2", "6")
                .param("tecnologiasTFC", "2", "4"))
                .andExpect(status().isFound())
                .andExpect(header().string("Location", "/listar"))
                .andExpect(flash().attribute("flashInfo","O TFC com id Aluno1 foi criado com sucesso"));

        MvcResult result = mvc.perform(get("/listar"))
                .andExpect(status().isOk())
                .andExpect(view().name("listar"))
                .andReturn();
        List<TFC> listaTFCs = (List<TFC>) result.getModelAndView().getModelMap().get("listaPreponente");
        Assert.assertEquals(1, listaTFCs.size());
        Assert.assertEquals("Titulo do TFC", listaTFCs.get(0).getTitulo());
    }

    @Test
    @WithMockUser(username = "p111", password = "password", roles = "TEACHER")
    public void testProfessorInserePropostaEListar() throws Exception {

        mvc.perform(post("/formprofessor")
                .param("titulo", "Titulo do TFC")
                .param("descricao", "Descrição do TFC")
                .param("cursoAssociado", "1")  // 1 = LEI
                .param("disciplinasTFC", "")    // ??
                .param("disciplinas", "2", "6")
                .param("tecnologiasTFC", "")  // ??
                .param("tecnologias", "2"))
                .andExpect(status().isFound())
                .andExpect(header().string("Location", "/listar"))
                .andExpect(flash().attribute("flashInfo","O TFC com id DEISI1 foi criado com sucesso"));

        MvcResult result = mvc.perform(get("/listar"))
                .andExpect(status().isOk())
                .andExpect(view().name("listar"))
                .andReturn();
        List<TFC> listaTFCs = (List<TFC>) result.getModelAndView().getModelMap().get("listaPreponente");
        Assert.assertEquals(1, listaTFCs.size());
        Assert.assertEquals("Titulo do TFC", listaTFCs.get(0).getTitulo());
        Assert.assertEquals("A Aguardar Atribuição", listaTFCs.get(0).getEstado());

        MvcResult result2 = mvc.perform(get("/ver/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("ver"))
                .andReturn();

        TFC tfc = (TFC) result2.getModelAndView().getModelMap().get("tfc");
        Assert.assertEquals("Titulo do TFC", tfc.getTitulo());
    }

    @Test
    public void testProfessorInserePropostaEAlunoCandidatase() throws Exception {

        // professor insere proposta
        mvc.perform(post("/formprofessor")
                .with(user(new User("p111", "password", Arrays.asList(new SimpleGrantedAuthority("ROLE_TEACHER")))))
                .param("titulo", "Titulo do TFC")
                .param("descricao", "Descrição do TFC")
                .param("cursoAssociado", "1")  // 1 = LEI
                .param("disciplinasTFC", "")    // ??
                .param("disciplinas", "2", "6")
                .param("tecnologiasTFC", "")  // ??
                .param("tecnologias", "2"))
                .andExpect(status().isFound())
                .andExpect(header().string("Location", "/listar"))
                .andExpect(flash().attribute("flashInfo","O TFC com id DEISI1 foi criado com sucesso"));


        // aluno candidata-se a proposta
        mvc.perform(post("/verinfotfc")
                .with(user(new User("a111", "password", Arrays.asList(new SimpleGrantedAuthority("ROLE_STUDENT")))))
                .param("idTFC","DEISI1")
                .param("ordemEscolha","1"))
                .andExpect(status().isFound())
                .andExpect(header().string("Location", "/listatfc"))
                .andExpect(flash().attribute("flashInfo","Inscrição no TFC feita com sucesso."));

        // coordenação aprova proposta
        mvc.perform(post("/atribuirtfc")
                .with(user(new User("p1", "password", Arrays.asList(new SimpleGrantedAuthority("ROLE_TFC_COORDINATION")))))
                .param("idTFC","DEISI1")
                .param("numeroAlunoAtribuido", "1"))
                .andExpect(status().isFound())
                .andExpect(header().string("Location", "/listarTFCInsc"));

        // professor vê que o TFC foi atribuído
        MvcResult result = mvc.perform(get("/listar")
                .with(user(new User("p111", "password", Arrays.asList(new SimpleGrantedAuthority("ROLE_TEACHER"))))))
                .andExpect(status().isOk())
                .andExpect(view().name("listar"))
                .andReturn();
        List<TFC> listaTFCs = (List<TFC>) result.getModelAndView().getModelMap().get("listaPreponente");
        Assert.assertEquals(1, listaTFCs.size());
        Assert.assertEquals("Titulo do TFC", listaTFCs.get(0).getTitulo());
        Assert.assertEquals("Atribuido", listaTFCs.get(0).getEstado());

        // professor consulta o TFC
        MvcResult result2 = mvc.perform(get("/ver/1")
                .with(user(new User("p111", "password", Arrays.asList(new SimpleGrantedAuthority("ROLE_TEACHER"))))))
                .andExpect(status().isOk())
                .andExpect(view().name("ver"))
                .andReturn();

        TFC tfc = (TFC) result2.getModelAndView().getModelMap().get("tfc");
        Assert.assertEquals("Titulo do TFC", tfc.getTitulo());

    }

    @Test
    public void testProfessorInserePropostaEGrupoCandidatase() throws Exception {

        // professor insere proposta
        mvc.perform(post("/formprofessor")
                .with(user(new User("p111", "password", Arrays.asList(new SimpleGrantedAuthority("ROLE_TEACHER")))))
                .param("titulo", "Titulo do TFC")
                .param("descricao", "Descrição do TFC")
                .param("cursoAssociado", "1")  // 1 = LEI
                .param("disciplinasTFC", "")    // ??
                .param("disciplinas", "2", "6")
                .param("tecnologiasTFC", "")  // ??
                .param("tecnologias", "2"))
                .andExpect(status().isFound())
                .andExpect(header().string("Location", "/listar"))
                .andExpect(flash().attribute("flashInfo","O TFC com id DEISI1 foi criado com sucesso"));


        // grupo candidata-se a proposta
        mvc.perform(post("/verinfotfc")
                .with(user(new User("a111", "password", Arrays.asList(new SimpleGrantedAuthority("ROLE_STUDENT")))))
                .param("idTFC","DEISI1")
                .param("ordemEscolha","1")
                .param("numero2Aluno","a21700000"))
                .andExpect(status().isFound())
                .andExpect(header().string("Location", "/listatfc"))
                .andExpect(flash().attribute("flashInfo","Inscrição no TFC feita com sucesso."));

        // coordenação aprova proposta
        mvc.perform(post("/atribuirtfc")
                .with(user(new User("p1", "password", Arrays.asList(new SimpleGrantedAuthority("ROLE_TFC_COORDINATION")))))
                .param("idTFC","DEISI1")
                .param("numeroAlunoAtribuido", "1"))
                .andExpect(status().isFound())
                .andExpect(header().string("Location", "/listarTFCInsc"));

        // professor vê que o TFC foi atribuído
        MvcResult result = mvc.perform(get("/listar")
                .with(user(new User("p111", "password", Arrays.asList(new SimpleGrantedAuthority("ROLE_TEACHER"))))))
                .andExpect(status().isOk())
                .andExpect(view().name("listar"))
                .andReturn();
        List<TFC> listaTFCs = (List<TFC>) result.getModelAndView().getModelMap().get("listaPreponente");
        Assert.assertEquals(1, listaTFCs.size());
        Assert.assertEquals("Titulo do TFC", listaTFCs.get(0).getTitulo());
        Assert.assertEquals("Atribuido", listaTFCs.get(0).getEstado());

        // professor consulta o TFC
        MvcResult result2 = mvc.perform(get("/ver/1")
                .with(user(new User("p111", "password", Arrays.asList(new SimpleGrantedAuthority("ROLE_TEACHER"))))))
                .andExpect(status().isOk())
                .andExpect(view().name("ver"))
                .andReturn();

        TFC tfc = (TFC) result2.getModelAndView().getModelMap().get("tfc");
        Assert.assertEquals("Titulo do TFC", tfc.getTitulo());

    }

}
