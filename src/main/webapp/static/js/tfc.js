var el = document.getElementById('adicionaDisciplina');

if (el != null) {
    el.onclick = function() {
        var disciplina = document.getElementById("tecno").value;
        var disciplina_sem_espaco = disciplina.replace(/\s+/g, '');
        var node = document.getElementById("conjuntoTecnologogias");
        var butao = document.createElement("form:button");
        butao.setAttribute("id", "butaoDisciplina");
        butao.setAttribute("class", "btn btn-success btn-sm");
        butao.setAttribute("value", disciplina);
        butao.setAttribute("path", "tecnologias");
        butao.setAttribute("onclick", "deleteElement(this, " + disciplina_sem_espaco+")");
        butao.innerHTML = disciplina;
        node.append(butao);
        var inputElem = document.createElement('input');
        inputElem.setAttribute("id", disciplina_sem_espaco);
        inputElem.setAttribute("type", "hidden");
        inputElem.setAttribute("name", "tecnologias");
        inputElem.setAttribute("value", disciplina);
        node.append(inputElem);
        document.getElementById("tecno").value = '';
        document.getElementById('adicionaDisciplina').disabled = true;
    }
}


function deleteElement(id, elem){
    elem.remove();
    id.remove();
}

function deleteElementBD(id, elem){
    /* var elementos = document.getElementsByName("butaoDisciplina");
     $(".classe").remove();*/
    var elementoHidden = document.getElementById(elem);
    elementoHidden.remove();
    /* var elementos = document.getElementById("butaoDisciplina");
     elementos.remove();*/
    id.remove();
}


function toogle() {
    var input = document.getElementById("entidade");
    if(input.disabled){
        input.disabled = false;
    } else{
        input.disabled = true;
    }
}

function preencheModal(disciplinas) {
    var titulo = document.getElementById("tituloForm").value;
    var tituloModal = document.getElementById("tituloModal");
    tituloModal.value = titulo;

    var cursoSelect = document.getElementById("cursoForm");
    var cS = cursoSelect.options[cursoSelect.selectedIndex].text;
    var cursoModal = document.getElementById("cursoModal");
    cursoModal.value = cS;

    /*var disciplinaModal = document.getElementById("disciplinaModal")
    for(var x = 0; x<disciplinas;x++){
        disciplinaModal.value = disciplinas[x].value;

    }*/
}

if (document.getElementById('descricaoForm') != null) {
    document.getElementById('descricaoForm').onkeyup = function () {
        var conta = 500 - this.value.length;
        if (conta > 0) {
            document.getElementById('count').innerHTML = "Ainda tem: " + (500 - this.value.length + " caractéres.");
        } else {
            document.getElementById('count').innerHTML = "Já não tem mais caractéres para escrever";
        }

    }
}

/*Funcao que nao permite a introdução de uma tecnologia vazia*/
if (document.getElementById('tecno') != null) {
    document.getElementById('tecno').onkeyup = function () {
        if (document.getElementById('tecno').value == "") {
            document.getElementById('adicionaDisciplina').disabled = true;
        } else {
            document.getElementById('adicionaDisciplina').disabled = false;
        }

    }
}

function toogleGrupo(){
    var nome2Aluno = document.getElementById("nome2Aluno");
    var numero2Aluno = document.getElementById("numero2Aluno");
    if(nome2Aluno.disabled){
        nome2Aluno.disabled = false;
        numero2Aluno.disabled = false;
    } else{
        nome2Aluno.disabled = true;
        numero2Aluno.disabled = true;
    }
}

function funcaoProcuraID() {
    // Declare variables
    var input, filter, table, tr, td, i;
    input = document.getElementById("inputdeProcuraID");
    filter = input.value.toUpperCase();
    table = document.getElementById("tabelaTFCAvaliar");
    tr = table.getElementsByTagName("tr");

    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function funcaoProcuraEstado() {
    // Declare variables
    var input, filter, table, tr, td, i;
    input = document.getElementById("inputdeProcuraEstado");
    filter = input.value.toUpperCase();
    table = document.getElementById("tabelaTFCAvaliar");
    tr = table.getElementsByTagName("tr");

    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[3];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function validate()
{
    var dropbox = document.getElementById("Avaliacao");
    var selectedValue = dropbox.options[dropbox.selectedIndex].value;
    var textRecusa = document.getElementById("recusa");
    if (document.getElementById("Avaliacao").value == "Recusado" ||
        document.getElementById("Avaliacao").value == "Aceite Com Modificações"){
        textRecusa.disabled = false
    }else{
        textRecusa.disabled = true
    }
}

$("#tecno").select(function() {
    document.getElementById('adicionaDisciplina').disabled = false;
});

//Variaveis globais que iram se enviados para Controller
var notasEscala = new Map();
var justificacoes = new Map();

function saveNotaEscala(idBtn, groupIdBtn, idCriterio, green, blue) {
    var nota = idBtn;
    var key = groupIdBtn;
    var id = idBtn.toString().concat("_").concat(groupIdBtn);
    //Testar se a notaEscala esta selecionada
    if(document.getElementById(id).style.backgroundColor == green) {
        //Ex: key, 1_7 -> criterio 1 e nota 7
        notasEscala.set(key, idCriterio.toString().concat("_").concat(nota.toString()));
    } else {
        console.log("Nota delete.")
        notasEscala.delete(key);
    }

    console.log("-------------------------------------");
    for (var value of notasEscala.values()) {
      console.log("Nota: ", value);
    }
    //console.log(Array.from(notasEscala).join(";"));
}

function toogleColorBtnNota(elemento, idBtn, groupIdBtn, idCriterio) {

     var green = "rgb(21, 142, 18)";
     var blue = "rgb(0, 108, 255)";

        //comparar cor
        var n = elemento.style.backgroundColor.localeCompare(blue);

        //toggle cor
        elemento.style.backgroundColor = n == 0 ? green : blue;

        console.log(idBtn + "_" + groupIdBtn);
        //setar a blue todos os botoes do grupo selecionado
        for(var idx = 1; idx <= 7; idx++) {
             document.getElementById(idx.toString().concat("_").concat(groupIdBtn)).style.backgroundColor = blue;
        }
       //toogle Cor
       document.getElementById(idBtn.toString().concat("_").concat(groupIdBtn)).style.backgroundColor = n == 0 ? green : blue;

       saveNotaEscala(idBtn, groupIdBtn, idCriterio, green, blue);
}

function loadDados(escalaAvaliacao) {
    //converter Map to Array, Array to String
    var strNotasEscala = Array.from(notasEscala).join(";");

    var hiddenNotas = document.getElementById("hiddenNotas");
    hiddenNotas.value = strNotasEscala;

    var strJustificacoes = Array.from(justificacoes).join(";");
    document.getElementById("hiddenJustificacoes").value = strJustificacoes;
}

function validateForm(qtdCriteriosAvaliacao) {
    if(notasEscala.size == qtdCriteriosAvaliacao) {
        alert("Notas Submetidas com sucesso!")
        return true;
    } else {
        alert("Falta notas por preencher!")
        return false;
    }
}

function obterValueSessao(id){
    var selector = document.getElementById('select_'.concat(id));
    var value = selector.options[selector.selectedIndex].value;

    var h2 = document.getElementById("hiddenS".concat(id));
    h2.value = parseFloat(value);

    console.log(id, " - ", value);
}

function validateOptionSelected(id) {
     var selector = document.getElementById('select_'.concat(id));
     var value = selector.options[selector.selectedIndex].value;

    if(value != 0) {
        alert("Agenda criada com sucesso!")
        return true;
    } else {
        alert("Tem que selecionar uma sessão para o agendamento!")
        return false;
    }
}

function destacarNotaCriterio(elemento, idBtn, notaCriterio) {
    console.log("Indide JS");
    var blue = "rgb(0, 108, 255)";

    if(idBtn == notaCriterio) {
        //set cor
        elemento.style.backgroundColor = blue;
    }
}

function salvarJustificacao(idCriterio) {
    var justificacao = document.getElementById(idCriterio).value;

    if(justificacao == "") {
        alert("Justificação não pode ser vazio!");
    } else {
        justificacoes.set(idCriterio, justificacao);
        console.log(idCriterio, " - ", justificacao);
    }
}
