    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    <tiles:insertDefinition name="defaultTemplate">

        <tiles:putAttribute name="body">
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                    <a href="<c:url value=""/>">Apresentação Intermédia</a>
              </li>
              <li class="breadcrumb-item active">
                    <a href="<c:url value=""/>">Apresentação 1º Época</a>
              </li>
            </ol>
            <c:choose>
               <c:when test="${not empty listaTFCsparaavaliar}">

                   <table class="table table-striped table-hover table-responsive-sm" id="tabelaListaTFCsAvaliados">

                    <thead  class="thead-dark">
                         <tr>
                            <th class="text-center">ID TFC</th>
                            <th>Descrição TFC</th>
                            <th class="text-center">---</th>
                         </tr>
                    </thead>
                       <tbody class="p-3 mb-2 bg-light text-dark ">


                            <c:forEach var="tfc" items="${listaTFCsparaavaliar}">
                               <tr>
                                   <td class="text-center">${tfc.getId()}</td>
                                   <td>${tfc.getTitulo()}</td>

                                   <td class="text-center">
                                         <a href="<c:url value="/formAvaliacao/${tfc.getId()}"/>" >
                                         <button class="btn btn btn-primary "> Avaliar</button>
                                   </td>
                               </tr>
                           </c:forEach>
                       </tbody>
                   </table>
               </c:when>
               <c:otherwise>
               <h2 id="titulo2">Vazio</h2>
               </c:otherwise>
            </c:choose>
           </div>
        </tiles:putAttribute>
    </tiles:insertDefinition>
