<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">
        <div class="container">
            <h1>TFCs para escolha</h1>
        </div>
        <div class="container">
            <c:choose>
                <c:when test="${fn: length(listainscricoes) < 3}">
                    <div class="alert alert-danger" role="alert">
                        <strong>Deve-se inscrever no mínimo em 3 TFCs e no máximo em 5 TFCs. Neste momento está inscrito em ${fn: length(listainscricoes)} TFCs</strong>
                    </div>
                </c:when>
            </c:choose>
        </div>


        <div class="container">
            <div class="row">
                <div class="col-12">
                    <input type="text" id="procuraID" onkeyup="procuraOrientador()" placeholder="Introduza o nome do Orientador a pesquisar...">
                    <input type="text" id="procuraInscricao" onkeyup="procuraInscricao()" placeholder="Introduza a ordem da sua Escolha para pesquisar...">
                </div>
            </div>

        </div>

        <div class="container">
            <c:choose>
                <c:when test="${not empty listatfc}">
                    <table class="table table-striped table-hover table-responsive-sm" id="tabelaInscricao">
                        <thead>
                        <tr>
                            <th>ID TFC</th>
                            <th>Titulo TFC</th>
                            <th>Orientador</th>
                            <th>Prioridade</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="tfc" items="${listatfc}">
                            <tr>
                                <td>${tfc.idtfc}</td>
                                <td>${tfc.titulo}</td>
                                <c:choose>
                                    <c:when test="${not empty orientadores}">
                                        <c:forEach items="${orientadores}" var="orientador">
                                            <c:if test="${orientador.numeroProfessor == tfc.orientador}">
                                                <td>${orientador.nome}</td>
                                            </c:if>
                                        </c:forEach>
                                    </c:when>
                                    <c:otherwise>
                                        <td>${tfc.orientador}</td>
                                    </c:otherwise>
                                </c:choose>
                                <c:choose>

                                    <c:when test="${not empty listainscricoes}">
                                        <td>
                                        <c:forEach items="${listainscricoes}" var="inscricao">

                                            <c:if test="${inscricao.idTFC == tfc.idtfc}">
                                                ${inscricao.ordemEscolha}ª Escolha
                                            </c:if>

                                        </c:forEach>
                                        </td>
                                    </c:when>
                                    <c:otherwise>
                                        <td></td>
                                    </c:otherwise>
                                </c:choose>
                                <td><a href="<c:url value="/vertfc/${tfc.id}"/>"><i class="fas fa-eye"></i></a> </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise>
                    <h2 id="titulo2">Não existem TFCs para escolher</h2>
                </c:otherwise>
            </c:choose>
        </div>

        <script>
            function procuraOrientador() {
                // Declare variables
                var input, filter, table, tr, td, i;
                input = document.getElementById("procuraID");
                filter = input.value.toUpperCase();
                table = document.getElementById("tabelaInscricao");
                tr = table.getElementsByTagName("tr");

                // Loop through all table rows, and hide those who don't match the search query
                for (i = 0; i < tr.length; i++) {
                    td = tr[i].getElementsByTagName("td")[2];
                    if (td) {
                        if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                            tr[i].style.display = "";
                        } else {
                            tr[i].style.display = "none";
                        }
                    }
                }
            }

            function procuraInscricao() {
                // Declare variables
                var input, filter, table, tr, td, i;
                input = document.getElementById("procuraInscricao");
                filter = input.value.toUpperCase();
                table = document.getElementById("tabelaInscricao");
                tr = table.getElementsByTagName("tr");

                // Loop through all table rows, and hide those who don't match the search query
                for (i = 0; i < tr.length; i++) {
                    td = tr[i].getElementsByTagName("td")[3];
                    if (td) {
                        if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                            tr[i].style.display = "";
                        } else {
                            tr[i].style.display = "none";
                        }
                    }
                }
            }
        </script>

    </tiles:putAttribute>
</tiles:insertDefinition>
</html>
