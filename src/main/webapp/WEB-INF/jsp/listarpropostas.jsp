<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">
        <div class="container">
            <h1>Listagem de todas as propostas apresentadas</h1>
        </div>
        <div class="container">
            <label>Filtros de Pesquisa</label>
            <div class="row">
                <div class="col-12">
                    <input type="text" id="inputdeProcuraID" onkeyup="funcaoProcuraID()" placeholder="Introduza o tipo de ID TFC que pretende pesquisar...">
                    <input type="text" id="inputdeProcuraEstado" onkeyup="funcaoProcuraEstado()" placeholder="Introduza o tipo de Avaliação que pretende pesquisar...">
                </div>
            </div>

        </div>

        <div class="container">
            <c:choose>
                <c:when test="${not empty listatfc}">
                    <table class="table table-striped table-hover table-responsive-sm" id="tabelaTFCAvaliar">
                        <thead>
                        <tr>
                            <th>ID TFC</th>
                            <th>Estado</th>
                            <th>Avaliação</th>
                            <th>Titulo TFC</th>
                            <th>Data Estado</th>
                            <th>Data da Proposta</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="tfc" items="${listatfc}">
                            <tr>
                                <td>${tfc.idtfc}</td>
                                <td>${tfc.estado}</td>
                                <c:choose>
                                    <c:when test="${not empty tfc.avaliacaoProposta}">
                                        <td>${tfc.avaliacaoProposta}</td>
                                    </c:when>
                                    <c:otherwise>
                                        <td>Não Avaliado</td>
                                    </c:otherwise>
                                </c:choose>
                                <td>${tfc.titulo}</td>
                                <td>${tfc.dataEstado}</td>
                                <td>${tfc.dataProposta}</td>
                                <td><a href="<c:url value="/avaliaproposta/${tfc.id}"/>" ><i class="fas fa-eye"></i></a> </td>
                            </tr>

                        </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise>
                    <h2 id="titulo2">Não existem TFCs para aprovar</h2>
                </c:otherwise>
            </c:choose>
        </div>


    </tiles:putAttribute>
</tiles:insertDefinition>

