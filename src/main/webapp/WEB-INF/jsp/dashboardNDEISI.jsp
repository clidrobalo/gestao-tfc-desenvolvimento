<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">

<c:if test="${not empty flashInfo}">
    <div class="alert alert-success alert-dismissible fade show" role="alert">
            ${flashInfo}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</c:if>

<div class="container">
    <h1>Gestão de TFCs</h1>

    <c:url var="post_url"  value="/editNDEISI" />
    <form action="${post_url}">
        <div class="form-group">
            <label>Introduza o email de Contato e o ID do TFC a Editar</label>
        </div>
        <div class="form-group">
            <input type="text" name="email" class="form-control" id="emailEmpresa" aria-describedby="textHelp"
                   placeholder="Introduza o email de contato"/>
        </div>
        <div class="form-group">
            <input type="text" name="id" class="form-control" id="idTFC" aria-describedby="textHelp"
                   placeholder="Introduza o ID do TFC a editar"/>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-success" id="btnNDEISI" value="Editar TFC"/>
        </div>
        <hr/>
        <div class="form-group">
            <label>Ou pode adicionar um novo TFC</label>
        </div>
        <div class="form-group">
            <a href="<c:url value='/formNDEISI'/>" class="btn btn-success">Adicionar novo TFC</a>
        </div>
    </form>
</div>

    </tiles:putAttribute>
</tiles:insertDefinition>


