    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    <tiles:insertDefinition name="defaultTemplate">

        <tiles:putAttribute name="body">
           <div class="container">
                           <h1 id="titulo">Agendamento Apresentação</h1>

                            <div class="text-right">
                                <button class="btn btn btn-primary mb-2"><i class="fas fa-check-circle"></i>    Confirmar Aprentações</button>
                            </div>

                            <c:choose>
                                <c:when test="${listaSessao.size() == 0}">
                                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                                        <h4 class="alert-heading">Aviso de Sessão agendamento:</h4>
                                        <p>Todas as sessões de agendamento já estão preenchidas ou ainda não foi criada nenhuma sessão.</p>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                </c:when>
                            </c:choose>

                           <c:choose>
                               <c:when test="${not empty listaJuris}">
                                   <table class="table table-striped table-hover table-responsive-sm" id="tabelaListaAgenda">
                                       <thead  class="thead-dark">
                                           <tr>
                                                <th class="text-center">ID</th>
                                                <th class="text-center">Nome</th>
                                                <th class="text-center">Sessão</th>
                                                 <th class="text-center">---</th>
                                           </tr>
                                       </thead>
                                       <tbody class="p-3 mb-2 bg-light text-dark ">
                                            <c:forEach var="juri" items="${listaJuris}">
                                               <tr>
                                                   <td class="text-center">${juri.getNumeroProfessor()}</td>
                                                   <td class="text-center">${juri.getNome()}</td>
                                                   <c:choose>
                                                       <c:when test="${!juri.temSessao(agendamentos)}">
                                                           <td class="text-center">
                                                                <form:select id="select_${juri.getId()}" path="listaSessao" name="sessao" class="form-control">
                                                                    <form:option value="0">Selecione uma sessão</form:option>
                                                                    <c:forEach var="sessao" items="${listaSessao}">
                                                                        <form:option value="${sessao.getId()}">${sessao.toString()}</form:option>
                                                                    </c:forEach>
                                                                </form:select>
                                                           </td>
                                                           <td class="text-center">
                                                                 <c:url var="post_url"  value="/formAtribuirJuri" />
                                                                 <form:form method="POST" action="${post_url}" onsubmit="return validateOptionSelected(${juri.getId()})">
                                                                    <input type="hidden" name="numeroProfessorDEISI" value="${juri.getNumeroProfessor()}" id="hiddenP${juri.getId()}"/>
                                                                    <input type="hidden" name="idSessao" id="hiddenS${juri.getId()}"/>
                                                                    <button type="submit" id="${juri.getNumeroProfessor()}" class="btn btn btn-primary btn_confirm_agenda" onclick="obterValueSessao(${juri.getId()})"><i class="fas fa-check-circle"></i> Confirmar</button>
                                                                </form:form>
                                                           </td>
                                                       </c:when>
                                                       <c:when test="${juri.temSessao(agendamentos)}">
                                                            <td class="text-center">
                                                                <form:select path="listaSessao" name="sessao" class="form-control">
                                                                    <form:option value="0" disabled="disabled">${listaSessaoOriginal.get(juri.getIdSessao(agendamentos)-1).toString()}</form:option>
                                                                </form:select>
                                                           </td>
                                                           <td class="text-center">
                                                                 <c:url var="post_url"  value="/deleteAgenda" />
                                                                 <form:form method="POST" action="${post_url}">
                                                                    <input type="hidden" value="${juri.getNumeroProfessor()}" name="numeroProfessorDEISI" id="hiddenP${juri.getId()}"/>
                                                                    <button type="submit" id="${juri.getId()}" class="btn btn btn-primary btn_confirm_agenda"><i class="fas fa-edit"></i> Editar</button>
                                                                </form:form>
                                                           </td>
                                                       </c:when>
                                                   </c:choose>
                                               </tr>
                                           </c:forEach>
                                       </tbody>
                                   </table>
                               </c:when>
                               <c:otherwise>
                               <h2 id="titulo2">Vazio</h2>
                               </c:otherwise>
                           </c:choose>
                       </div>
        </tiles:putAttribute>
    </tiles:insertDefinition>
