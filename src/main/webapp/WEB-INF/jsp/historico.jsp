<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">
        <h1 id="titulo">Lista de Histórico de TFCs</h1>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <label>Filtros de Pesquisa</label>
                    <input type="text" id="inputdeProcuraID" onkeyup="funcaoProcuraID()" placeholder="Introduza o tipo de ID TFC que pretende pesquisar...">
                    <input type="text" id="inputdeProcuraEstado" onkeyup="funcaoProcuraEstado()" placeholder="Introduza o número de Utilizador que pretende pesquisar...">
                </div>
            </div>

        </div>
        <div class="container">
            <c:choose>
                <c:when test="${not empty listahistorico}">
                    <table class="table table-striped table-hover table-responsive-sm" id="tabelaTFCAvaliar">
                        <thead>
                        <tr>
                            <th>ID TFC</th>
                            <th>Estado</th>
                            <th>Data Mudança de Estado</th>
                            <th>Utilizador</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="historico" items="${listahistorico}">
                            <tr>
                                <td>${historico.idTFC}</td>
                                <td>${historico.estado}</td>
                                <td>${historico.dataMudancaEstado}</td>
                                <td>${historico.utilizador}</td>
                                <td><a href="<c:url value="/ver/${historico.idTFCNumerico}"/>" ><i class="fas fa-eye"></i></a> </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise>
                    <h2 id="titulo2">Ainda não submeteu nenhum tema para TFC</h2>
                </c:otherwise>
            </c:choose>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>

