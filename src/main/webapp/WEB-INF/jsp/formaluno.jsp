<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">
        <div class="container">
            <h1 style="margin-top: 100px;">Submissão de Proposta de TFC</h1>
        </div>
        <c:url var="post_url"  value="/formaluno" />
        <form:form method="POST" modelAttribute="alunoForm" action="${post_url}">
            <form:hidden path="id"/>
            <form:hidden path="cursoTFC"/>
            <form:hidden path="disciplinasTFC"/>
            <form:hidden path="tecnologiasTFC"/>
            <form:hidden path="preponente"/>
            <form:hidden path="entidadeTFC"/>
            <div class="container">
                <div class="content">
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        <h4 class="alert-heading">Instruções para submissão:</h4>
                        Cara(o) aluna(o),<br>
                            O DEISI - Departamento de Engenharia Informática e Sistemas de Informação da Universidade Lusófona, com o objetivo de fomentar o desenvolvimento de novas ideias pelos alunos, disponibiliza a possibilidade dos próprios alunos proporem o seu tema de Trabalho Final de Curso.
                        Caso pretenda realizar o TFC em grupo, colocar o número e nome do segundo aluno. Este deverá depois confirmar
                        que pretende realizar o TFC em grupo.<br>
                        Pretende-se que os Trabalhos Finais de Curso (TFC) tenham por âmbito uma das seguintes características:
                        <ul>
                            <li>criação de demonstração de tecnologias;</li>
                            <li>desenvolvimento de protótipos;</li>
                            <li>análise comparativa de soluções;</li>
                        </ul>

                            A Direção<br>
                            Departamento de Engenharia Informática e Sistemas de Informação<br>
                            Universidade Lusófona de Humanidades e Tecnologias

                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-12">
                        <form>

                            <div class="form-group">
                                <form>
                                    <label class="form-check-label" for="checkboxGrupo">Pretende fazer o TFC em Grupo?</label>
                                    <c:choose>
                                        <c:when test="${not empty alunoForm.nome2Aluno}">
                                            <input type="checkbox" id="checkboxGrupo" onclick="toogleGrupo()" checked><br>
                                            <div class="row">
                                                <div class="col-4">
                                                    <form:input path="numero2Aluno" type="text"  id="numero2Aluno" placeholder="Introduza o numero do segundo aluno"  class="form-control" disabled = "false"/>
                                                </div>
                                                <div class="col">
                                                    <form:input path="nome2Aluno" type="text"  id="nome2Aluno" placeholder="Introduza o nome do segundo aluno"  class="form-control" disabled = "false" />
                                                </div>
                                            </div>
                                        </c:when>
                                        <c:otherwise>
                                            <input type="checkbox" id="checkboxGrupo" onclick="toogleGrupo()"><br>
                                            <div class="row">
                                                <div class="col-4">
                                                    <form:input path="numero2Aluno" type="text"  id="numero2Aluno" placeholder="Introduza o numero do segundo aluno"  class="form-control" disabled = "true"/>
                                                </div>
                                                <div class="col">
                                                    <form:input path="nome2Aluno" type="text"  id="nome2Aluno" placeholder="Introduza o nome do segundo aluno"  class="form-control" disabled = "true" />
                                                </div>
                                            </div>
                                        </c:otherwise>
                                    </c:choose>
                                </form>
                            </div>

                            <div class="form-group">
                                <label id="labelCostumizada" for="profOrientador">Existe preferência de Docente Orientador:</label>
                                <form:select id="profOriForm" path="orientador" name="profOrientador">
                                    <c:choose>
                                        <c:when test="${not empty orientadores}">
                                            <form:option value="0">Sem preferência</form:option>
                                            <c:forEach var="orienta" items="${orientadores}">
                                                <form:option value="${orienta.numeroProfessor}">${orienta.nome}</form:option>
                                            </c:forEach>
                                        </c:when>
                                    </c:choose>
                                </form:select>
                                <form:errors id="erros" path="orientador" cssClass="error"/><br/>
                            </div>


                            <div class="form-group">
                                <form:label id="labelCostumizada" path="titulo">Titulo</form:label>
                                <form:errors id="erros" path="titulo" cssClass="error"/><br/>
                                <form:input path="titulo" label="titulo" id="tituloForm"  class="form-control" placeholder="Introduza o Titulo" />
                            </div>
                            <div class="form-group">
                                <form:label id="labelCostumizada" path="descricao" for="descricao">Descrição do TFC (máximo 500 caractéres):</form:label>
                                <form:errors id="erros" path="descricao" cssClass="error"/><br/>
                                <form:textarea placeholder="Escreva aqui a descrição da sua proposta....." path="descricao" class="form-control" name="descricao" rows="5" id="descricaoForm"></form:textarea>
                                <small id="count" class="form-text text-muted">Ainda tem 500 caractéres.</small>
                            </div>

                            <div class="form-group">
                                <label class="form-check-label" for="checkboxEntidade">Ligação a um Empresa ou Entidade Externa?</label>
                                <c:choose>
                                    <c:when test="${not empty alunoForm.entidade}">
                                        <input type="checkbox" id="checkboxEntidade" onclick="toogle()" checked><br>
                                        <form:input path="entidade" type="text"  id="entidade" placeholder="Introduza o nome da Entidade"  class="form-control" disabled = "false" ></form:input>
                                    </c:when>
                                    <c:otherwise>
                                        <input type="checkbox" id="checkboxEntidade" onclick="toogle()" ><br>
                                        <form:input path="entidade" type="text" id="entidade" placeholder="Introduza o nome da Entidade"  class="form-control" disabled = "true" ></form:input>
                                    </c:otherwise>
                                </c:choose>
                            </div>

                            <div class="accordion" id="disc" >
                                <label id="labelCostumizada" for="disc">Escolha a(s) disciplina(s) a associar:</label>
                                <form:errors id="erros" path="disciplinas" cssClass="error"/><br/>
                                <c:choose>
                                    <c:when test="${not empty cursos}">
                                        <c:forEach var="curso" items="${cursos}">
                                            <div class="card">


                                                    <div class="card-header" id="${curso.id}" role="tab">
                                                        <h5 class="mb-0">
                                                            <input type="button"
                                                                   value="${curso.nome}"
                                                                   class="btn collapsed"
                                                                   data-toggle="collapse"
                                                                   data-target="#${curso.nome}"
                                                                   aria-controls="#${curso.nome}"
                                                                   id="botaoAccordion">
                                                        </h5>
                                                    </div>

                                                    <div id="${curso.nome}" class="collapse"
                                                         aria-labelledby="${curso.nome}" data-parent="#accordion">
                                                        <div class="card-body">
                                                            <table class="table table-striped" border="0">
                                                                <thead>
                                                                <tr>
                                                                    <th>Nome</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <c:forEach var="disciplina" items="${listadisciplinas}" >
                                                                    <c:if test="${(disciplina.cursoAssociado == curso.id)}">
                                                                        <tr>

                                                                            <td>${disciplina.nome}</td>
                                                                            <td><form:checkbox path="disciplinas"
                                                                                               value="${disciplina.id}"/></td>
                                                                        </tr>
                                                                    </c:if>

                                                                </c:forEach>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                            </div>
                                        </c:forEach>
                                    </c:when>
                                </c:choose>
                            </div>
                            <br>

                            <div class="form-group">
                                <label id="labelCostumizada" for="tecno">Adicione as tecnologias a associar:</label>
                                <form:errors id="erros" path="tecnologias" cssClass="error"/><br/>
                                <input list="tecnologias" name="tecno" id="tecno"/>
                                <datalist id="tecnologias">
                                    <c:choose>
                                    <c:when test="${not empty listatecnologias}">
                                    <c:forEach var="tecnologia" items="${listatecnologias}">
                                    <option value="${tecnologia.nome}">
                                        </c:forEach>
                                        </c:when>
                                        </c:choose>
                                </datalist>
                                <button type="button" id="adicionaDisciplina" class="btn btn-light" disabled><i
                                        class="fas fa-plus"></i></button>
                            </div>

                            <div class="form-group" id="conjuntoTecnologogias">
                                <c:choose>
                                    <c:when test="${not empty alunoForm.tecnologias}">
                                        <c:forEach items="${alunoForm.tecnologias}" var="nomeTecnologia">
                                            <c:choose>
                                                <c:when test="${erroBinding == true}">

                                                    <form:button id="butaoDisciplina" class="btn btn-success btn-sm"
                                                                 value="${nomeTecnologia}" path="tecnologias" type="button"
                                                                 onclick ="deleteElementBD(this,'${nomeTecnologia}')">
                                                        ${nomeTecnologia}
                                                    </form:button>
                                                    <input id="${nomeTecnologia}" type="hidden" name="tecnologias"
                                                           value="${nomeTecnologia}">
                                                </c:when>
                                                <c:otherwise>
                                                    <c:forEach var="tecnologia" items="${listatecnologias}">
                                                        <c:if test="${nomeTecnologia == tecnologia.nome}">
                                                            <form:button id="butaoDisciplina" class="btn btn-success btn-sm"
                                                                         value="${nomeTecnologia}" path="tecnologias" type="button"
                                                                         onclick ="deleteElementBD(this,'${nomeTecnologia}')">
                                                                ${nomeTecnologia}
                                                            </form:button>
                                                            <input id="${nomeTecnologia}" type="hidden" name="tecnologias"
                                                                   value="${nomeTecnologia}">
                                                        </c:if>
                                                    </c:forEach>
                                                </c:otherwise>
                                            </c:choose>

                                        </c:forEach>
                                    </c:when>
                                </c:choose>
                            </div>

                            <button type="submit" name="gravar" id="gravar" class="btn btn-primary btn-xl">Submeter</button>

                        </form>
                    </div>


                </div>
            </div>
        </form:form>


    </tiles:putAttribute>
</tiles:insertDefinition>
