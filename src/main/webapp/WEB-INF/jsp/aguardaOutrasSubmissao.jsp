    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


    <tiles:insertDefinition name="defaultTemplate">

        <tiles:putAttribute name="body">
           <div class="container" id="container">
                <div class="bg-light text-center p-5 border border-success">
                    <h3><i class="fas fa-check" style="color:#006400;"></i>   Avaliação submetida com sucesso</h3>
                    <h5 class="bg-primary text-light p-3">Aguarde a submissão dos outros júris...</54>
                </div

                 <!-- Ainda não funciona-->
                <div class="spinner-grow text-primary" role="status">
                  <span class="sr-only">Loading...</span>
                </div>

                <c:url var="post_url"  value="/verificarSubmissoes/${id}" />
                <form:form method="POST" action="${post_url}">
                    <input type="hidden" value="${idTFC}" name="idTFC"/>
                    <button id="btn_" type="submit" class="btn btn-primary float-right mt-2" style="width: 10em; margin-bottom: 5em;"><i class="fas fa-sync-alt"></i> Atualizar</button>
                </form:form>
           </div>
        </tiles:putAttribute>
    </tiles:insertDefinition>
