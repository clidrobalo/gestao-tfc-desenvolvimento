<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tiles:insertDefinition name="defaultTemplate">

    <tiles:putAttribute name="body">

        <div class="container">
            <h1 id="titulo" style="margin-top: 100px;">Visualização do TFC ${tfc.idtfc}</h1>
            <c:if test="${tfc.avaliacaoProposta != null}">
                <div class="content">
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        <c:choose>
                            <c:when test="${(tfc.avaliacaoProposta == 'Aceite') or (tfc.avaliacaoProposta  == 'Aceite Com Modificações') or (tfc.avaliacaoProposta  == 'Recusado')}">
                                <h4 class="alert-heading">Aviso de Avaliação:</h4>
                                <p>Esta proposta de TFC já foi avaliada pela Coordenação.</p>
                                <p>Resultado da avaliação: <strong>${tfc.avaliacaoProposta}</strong></p>
                                <c:if test="${tfc.avaliacaoProposta == 'Recusado'}">
                                    <p>Motivo da proposta ter sido recusada: <strong>${tfc.motivoRecusa}</strong></p>
                                </c:if>
                                <c:if test="${tfc.avaliacaoProposta  == 'Aceite Com Modificações'}">
                                    <p>Alterações à proposta: <strong>${tfc.motivoRecusa}</strong></p>
                                </c:if>
                            </c:when>
                        </c:choose>

                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </c:if>

            <div class="row">
                <div class="col-md-12">
                    <c:if test="${tipoUtilizador == 'aluno'}">
                        <div class="form-group">
                            <form>
                                <c:choose>
                                    <c:when test="${not empty form.nome2Aluno}">
                                        <label class="form-check-label" for="checkboxGrupo">Pretende fazer o TFC em Grupo?</label>
                                        <input type="checkbox" id="checkboxGrupo" onclick="return false;" checked><br>
                                        <div class="row">
                                            <div class="col-2">
                                                <label path="numero2Aluno">Número do 2 Aluno</label>
                                                <input  type="text"  id="numero2Aluno" value="${form.numero2Aluno}"  class="form-control" disabled = "false"/>
                                            </div>
                                            <div class="col">
                                                <label path="nome2Aluno">Nome do 2 Aluno</label>
                                                <input type="text"  id="nome2Aluno" value="${form.nome2Aluno}"  class="form-control" disabled = "false" />
                                            </div>
                                        </div>
                                    </c:when>
                                </c:choose>
                            </form>
                        </div>
                    </c:if>

                    <form>
                        <div class="form-group">
                            <c:if test="${not empty form.entidade}">
                                <label path="empresa">Empresa ou Entidade Externa associada</label>
                                <input readonly id="empresa" value="${form.entidade}"  class="form-control" />
                            </c:if>
                        </div>
                    </form>

                    <form>
                        <c:if test="${not empty tfc.orientadorProposto}">
                            <c:forEach items="${orientadores}" var="orientador">
                                <c:if test="${(orientador.numeroProfessor == form.orientador)}">
                                    <div class="form-group">
                                        <label path="orientador">Orientador Proposto</label>
                                        <input readonly id="orientador" value="${orientador.nome}"  class="form-control" />
                                    </div>
                                </c:if>
                            </c:forEach>
                        </c:if>
                    </form>

                    <form>
                        <div class="form-group">
                            <label path="titulo">Titulo</label>
                            <input  readonly label="titulo" id="tituloForm" value="${tfc.titulo}"  class="form-control"/>
                            <label path="descricao">Descricao</label>
                            <textarea readonly class="form-control" name="descricao" rows="5" id="descricaoForm">${tfc.descricao}</textarea>
                        </div>
                    </form>

                    <form>
                        <div class="form-group">
                            <label for="estado">Estado</label>
                            <input  readonly label="estado" id="estado" value="${tfc.estado}"  class="form-control"/>
                        </div>
                        <c:if test="${tfc.estado == 'Atribuido'}">
                            <c:if test="${not empty aluno}">
                                <label for="estado">Aluno atribuido:</label>
                                <div class="form-group">
                                    <label for="numeroAluno">Número do Aluno</label>
                                    <input  readonly label="estado" id="numeroAluno" value="${aluno.numeroAluno}"  class="form-control"/>
                                    <label for="nomeAluno">Nome do Aluno</label>
                                    <input  readonly label="estado" id="nomeAluno" value="${aluno.nome}"  class="form-control"/>
                                </div>
                            </c:if>
                            <c:if test="${not empty aluno1Grupo}">
                                <label for="estado">Atribuido a um grupo:</label>
                                <div class="form-group">
                                    <label for="numeroAluno1grupo">Número do Aluno 1 do Grupo</label>
                                    <input  readonly label="estado" id="numeroAluno1grupo" value="${aluno1Grupo.numeroAluno}"  class="form-control"/>
                                    <label for="nomeAluno1Grupo">Nome do Aluno 1 do Grupo</label>
                                    <input  readonly label="estado" id="nomeAluno1Grupo" value="${aluno1Grupo.nome}"  class="form-control"/>
                                    <label for="numeroAluno2grupo">Número do Aluno 2 do Grupo</label>
                                    <input  readonly label="estado" id="numeroAluno2grupo" value="${aluno2Grupo.numeroAluno}"  class="form-control"/>
                                    <label for="nomeAluno2Grupo">Nome do Aluno 2 do Grupo</label>
                                    <input  readonly label="estado" id="nomeAluno2Grupo" value="${aluno2Grupo.nome}"  class="form-control"/>
                                </div>
                            </c:if>
                        </c:if>

                    </form>

                    <form>
                        <div class="form-group">
                            <c:if test="${not empty form.cursoAssociado}">
                                <c:forEach var="curso" items="${cursos}">
                                    <c:if test="${curso.id == form.cursoAssociado}">
                                        <label for="cursoAssociado">Curso Associado</label>
                                        <input  readonly id="cursoAssociado" value="${curso.nome}"  class="form-control"/>
                                    </c:if>
                                </c:forEach>
                            </c:if>
                        </div>
                    </form>

                    <form>
                        <div class="form-group">
                            <c:if test="${not empty form.disciplinas}">
                                <c:choose>
                                    <c:when test="${not empty form.disciplinas}">
                                        <table class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th>Nome da(s) Disciplina(s) Associada(s)</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <c:forEach items="${form.disciplinas}" var="discAssociada">
                                                <c:forEach var="disciplina" items="${listadisciplinas}">
                                                    <c:if test="${discAssociada == disciplina.id}">
                                                        <tr>
                                                            <td>${disciplina.nome}</td>
                                                        </tr>
                                                    </c:if>
                                                </c:forEach>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                    </c:when>
                                </c:choose>
                            </c:if>
                        </div>
                    </form>

                    <form>
                        <div class="form-group">
                            <c:if test="${not empty form.tecnologias}">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Nome da(s) Tecnologia(s) Associada(s)</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="tecnoAssociada" items="${form.tecnologias}">
                                        <tr>
                                            <td>${tecnoAssociada}</td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </c:if>
                        </div>
                    </form>

                    <button name="listar" id="listar" class="btn btn-primary">Voltar à Listagem</button>
                </div>

            </div>
        </div>

        <script>
            var btnEdit = document.getElementById("listar");
            btnEdit.onclick = function (ev) {
                urlFinal = "<c:url value="/listar"/>";
                window.location = urlFinal;
            }
        </script>

    </tiles:putAttribute>
</tiles:insertDefinition>

