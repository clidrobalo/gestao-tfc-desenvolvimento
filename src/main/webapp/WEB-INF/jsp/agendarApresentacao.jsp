<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">
            <div class="container">
                <h1 id="titulo">Sessão Apresentação</h1>

                <table class="table table-striped table-hover table-responsive-sm">
                    <thead>
                        <tr>
                            <th scope="col" class="text-center">Data</th>
                            <th scope="col" class="text-center">Hora de Inicio</th>
                            <th scope="col" class="text-center">Hora de Fim</th>
                            <th scope="col" class="text-center">Sala</th>
                            <th scope="col" class="text-center">---</th>
                        </tr>
                    </thead>

                    <c:url var="post_url"  value="/formSessao" />
                    <form:form method="POST" modelAttribute="sessaoForm" action="${post_url}" >
                    <tbody>
                        <tr>
                            <td>
                                <input type="text"  name="data" class="form-control text-center" id="dataApresentacao" placeholder="mm/dd/yyyy" required>
                            </td>
                            <td>
                                <input type="text"  name="horaInicio" class="form-control text-center" id="horaInicioApresentacao"  placeholder="HH:mm" required>
                            </td>
                            <td>
                                <input type="text" name="horaFim" class="form-control text-center" id="horaFimApresentacao" placeholder="HH:mm" required>
                            </td>
                             <td>
                                <input type="text" name="sala" class="form-control text-center" id="salaApresentacao" placeholder="Sala apresentação">
                            </td>
                            <td>
                                <button type="submit" class="btn btn btn-primary"><i class="fa fa-plus-circle"></i> Nova</button>
                            </td>
                        </tr>
                    </tbody>
                    </form:form>
                </table>

                <c:choose>
                    <c:when test="${not empty listaSessao}">
                        <table class="table table-striped table-hover table-responsive-sm" id="tabelaListaSessao">
                            <thead  class="thead-dark">
                                <tr>
                                    <th class="text-center">ID Sessão</th>
                                    <th class="text-center">Data</th>
                                    <th class="text-center">Hora de Inicio</th>
                                    <th class="text-center">Hora de Fim</th>
                                    <th class="text-center">Sala</th>
                                </tr>
                            </thead>
                            <tbody class="p-3 mb-2 bg-light text-dark">
                                 <c:forEach var="sessao" items="${listaSessao}">
                                    <tr>
                                        <td class="text-center">${sessao.getId()}</td>
                                        <td class="text-center">${sessao.getData()}</td>
                                        <td class="text-center">${sessao.getHoraInicio()}</td>
                                        <td class="text-center">${sessao.getHoraFim()}</td>
                                        <td class="text-center">${sessao.getSala()}</td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </c:when>
                    <c:otherwise>
                    <h2 id="titulo2" class="bg-light text-dark">Vazio</h2>
                    </c:otherwise>
                </c:choose>
            </div>
    </tiles:putAttribute>
</tiles:insertDefinition>
