    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    <tiles:insertDefinition name="defaultTemplate">

        <tiles:putAttribute name="body">
           <div class="container">

                <div class="bg-light text-center p-4 border">
                    <i class="fas fa-exclamation-triangle fa-3x" style="color: #8B0000;"></i>
                    <h4>Atenção: Foi detectada discrepância(s) no(s) seguinte(s) elemento(s) de avaliação.</h4>
                    <h5 class="mt-4 text-primary">Por favor justifique a(s) nota(s) atribuida(s).</54>
                </div

                <div>
                     <h3 id="titulo" class="mt-4">Criterio(s)</h3>
                </div>

                <c:set value="0" var="groupIdBtn"/> <!-- Variavel para referenciar o grupo que cada id do botao de nota pertence-->
                <c:forEach var="criterio" items="${criterios}">
                    <span id="${groupIdBtn= groupIdBtn+1}"></span> <!-- incremmentar id do grupo ids -->
                    <div class="bg-light text-dark mt-3">
                        <h5 class="p-3 col-12 bg-dark text-white">${criterio.getDescricao()} (${criterio.getPeso()}%)</h5>
                        <c:forEach var="itemCriterioAvaliacao" items="${ItensCriterios}">
                            <c:if test="${itemCriterioAvaliacao.getIdICriterioAvaliacao() == criterio.getId()}">
                                <p class="pl-2 pr-2">${itemCriterioAvaliacao.getDescricao()}</p>
                                <hr/>
                            </c:if>
                        </c:forEach>
                        <div class="row justify-content-md-center pb-3">
                            <c:forEach var = "i" begin = "1" end = "${avaliacao.getEscalaAvaliacao()}">
                                <!-- Teste para atribuir cor verde a nota atribuida anteriormente na avaliação -->
                                <c:choose>
                                    <c:when test="${criterio.getNotaCriterio(notaCriterios) == i}">
                                        <!-- Sintaxe do id de cada Botão -> ${i}_${groupIdBtn} -->
                                       <button id="${i}_${groupIdBtn}" class="btn col-1 border border-light text-center text-light p-2" onclick="toogleColorBtnNota(this, ${i}, ${groupIdBtn}, ${criterio.getId()})" style="background-color: rgb(21, 142, 18);">
                                            ${i}
                                       </button>
                                    </c:when>
                                    <c:otherwise>
                                        <!-- ${i}_${groupIdBtn} -->
                                       <button id="${i}_${groupIdBtn}" class="btn col-1 border border-light text-center text-light p-2" onclick="toogleColorBtnNota(this, ${i}, ${groupIdBtn}, ${criterio.getId()})" style="background-color: rgb(0, 108, 255);">
                                            ${i}
                                       </button>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </div>
                        <div class"container">
                            <div class="row">
                                 <div class="col-10">
                                     <textarea  id="${criterio.getId()}"  class="form-control" rows="2" placeholder="Degite aqui a sua justificação..."></textarea>
                                 </div>
                                 <div class="col-2">
                                    <input class="btn btn-secondary" type="button" style="margin: 0px; width: 80px;" value="Salvar" onclick="salvarJustificacao(${criterio.getId()})">
                                </div>
                            </div>

                        </div>

                    </div>
                </c:forEach>

                <c:url var="post_url"  value="/atualizarNotaAvaliacao/${avaliacao.getId()}" />
                <form:form class="mt-4" method="POST" modelAttribute="avaliacaoForm" action="${post_url}">
                    <input type="hidden" value="${avaliacao.getIdTfcAtribuido()}" name="idTFC"/>
                    <input type="hidden" name="notasEscala" id="hiddenNotas"/>
                     <input type="hidden" name="justificacoesDiscrepancia" id="hiddenJustificacoes"/>
                    <button type="submit" class="btn btn-primary float-right" style="width: 10em; margin-bottom: 5em;" onclick="loadDados()">Submeter</button>
                </form:form>

           </div>
        </tiles:putAttribute>
    </tiles:insertDefinition>
