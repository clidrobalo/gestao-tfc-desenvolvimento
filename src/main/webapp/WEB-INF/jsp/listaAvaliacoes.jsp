    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    <tiles:insertDefinition name="defaultTemplate">

        <tiles:putAttribute name="body">
           <div class="container">

               <h1 id="titulo">Lista Avaliações</h1>

                <ol class="breadcrumb">
                   <li class="breadcrumb-item">
                        <a href="<c:url value=""/>">Apresentação Intermédia</a>
                   </li>
                   <li class="breadcrumb-item active"> Apresentação 1º Época</li>
               </ol>

               <div class="text-right mb-1">
                    <button class="btn btn bg-success text-white ml-3"><i class="fas fa-file-excel"></i> Exportar notas para Excel</button>
               </div>

               <hr>

                <c:choose>
                   <c:when test="${not empty listaAvaliacoes}">
                       <table class="table table-striped table-hover table-responsive-sm" id="tabelaListaTFCsAvaliados">
                           <thead  class="thead-dark">
                               <tr>
                                    <th class="text-center">ID TFC</th>
                                    <th>Descrição TFC</th>
                                    <th>Nota TFC</th>
                                    <th class="text-center">Detalhe TFC</th>
                                    <th class="text-center">Detalhe Avaliação</th>
                                    <th class="text-center">
                                        <button class="btn btn btn-primary"><i class="fas fa-share-square"></i> Publicar Notas</button>
                                    </th>
                               </tr>
                           </thead>
                           <tbody class="p-3 mb-2 bg-light text-dark ">
                                <c:forEach var="tfc" items="${listaAvaliacoes}">
                                   <tr>
                                       <td class="text-center">${tfc.getId()}</td>
                                       <td>${tfc.getTitulo()}</td>
                                       <td class="text-center">${tfc.getAvaliacaoApresentacao()}</td>
                                       <td class="text-center">
                                            <a href="<c:url value="/ver/${tfc.getId()}"/>" >
                                                <button class="btn btn bg-primary text-white"><i class="fas fa-eye"></i></button>
                                            </a>
                                       </td>
                                       <td class="text-center">
                                           <c:url var="post_url"  value="/detalheAvaliacao/${tfc.getId()}" />
                                           <form:form method="POST" modelAttribute="detalheAvaliacao" action="${post_url}">
                                               <input type="hidden" value="${0}" name="numMomentoAvaliacao"/>
                                               <button class="btn btn bg-primary text-white"><i class="fas fa-eye"></i></button>
                                           </form:form>
                                       </td>
                                       <td class="text-center">
                                           <button class="btn btn bg-dark text-white"><i class="fas fa-share-square"></i> Publicar Nota</button>
                                       </td>
                                   </tr>
                               </c:forEach>
                           </tbody>
                       </table>
                   </c:when>
                   <c:otherwise>
                   <h2 id="titulo2">Vazio</h2>
                   </c:otherwise>
                </c:choose>
           </div>
        </tiles:putAttribute>
    </tiles:insertDefinition>
