    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    <tiles:insertDefinition name="defaultTemplate">

        <tiles:putAttribute name="body">

          <div class="p-3 mb-2 bg-dark text-white" id="titulo">Avaliação ${avaliacoes.get(0).getMomentoAvaliacao()}</div>

          <div class="container">
              <table class="table table-striped table-hover" id="tabelaDetalheAvaliacao">
                 <thead  class="">
                     <tr class="row">
                        <td class="col text-center  border-bottom border-primary font-weight-bold">Júri</td>
                        <c:forEach var="criterio" items="${criterioAvaliacoes}">
                            <td class="col text-center  border-bottom border-primary font-weight-bold">${criterio.getDescricao()}</td>
                        </c:forEach>
                        <td class="col text-center  border-bottom border-primary font-weight-bold">Média (0-20)</td>
                    </tr>
                 </thead>

                 <tbody class="">
                    <c:forEach var="avaliacao" items="${avaliacoes}">
                        <tr class="row mt-1">
                            <td class="col">${avaliacao.getIdJuri()}</td>
                            <c:forEach var="nota" items="${notaCriterios}">
                                <c:if test="${nota.getIdUser().equals(avaliacao.getIdJuri())}">
                                    <td class="col text-center">${nota.getNota()}</td>
                                </c:if>
                            </c:forEach>
                            <td class="col text-center bg-primary text-white">${avaliacao.getNotaIndividualJuri()}</td>
                        </tr>
                    </c:forEach>
                 </tbody>
                 <tfoot class="">
                    <tr class="row mt-1">
                        <td class="col bg-secondary"></td>
                        <td class="col text-center bg-secondary text-white font-weight-bold">Média Nota</td>
                        <td class="col text-center bg-info text-white font-weight-bold"><h5>${tfc.getAvaliacaoApresentacao()}</h5></td>
                    </tr>
                 </tfoot>
              </table>
           </div>

        </tiles:putAttribute>
    </tiles:insertDefinition>
