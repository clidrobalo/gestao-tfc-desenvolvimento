<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">
        <div class="container">
            <h1 id="titulo" style="margin-top: 100px;">Atribuição do TFC com id ${tfc.idtfc}</h1>
            <c:url var="post_url"  value="/atribuirtfc" />
            <form:form method="POST" modelAttribute="atribuicaoForm" action="${post_url}">
                <form:hidden path="idTFC"/>
                <%--TODO: Apresentar titulo e descrição do TFC--%>

                    <div class="form-group">
                        <label path="titulo">Titulo</label>
                        <input  readonly label="titulo" id="tituloForm" value="${tfc.titulo}"  class="form-control"/>
                        <label path="descricao">Descricao</label>
                        <textarea readonly class="form-control" name="descricao" rows="5" id="descricaoForm">${tfc.descricao}</textarea>
                    </div>

                <table class="table table-striped table-hover table-responsive-sm" id="tabelaAlunosInscricoes">
                    <thead>
                    <tr>
                        <th>Nº Aluno</th>
                        <th>Ordem de Escolha</th>
                        <th>Hora de Registo</th>
                        <c:forEach var="disciplinaAssociada" items="${atribuicaoForm.disciplinas}">
                            <c:forEach items="${listadisciplinas}" var="disciplina">
                                <c:if test="${disciplina.id == disciplinaAssociada}">
                                    <th>${disciplina.nome}</th>
                                </c:if>
                            </c:forEach>
                        </c:forEach>
                    </tr>
                    </thead>

                    <tbody>
                    <c:forEach var="numeroInscricao" items="${atribuicaoForm.listaInscricoes}">
                        <tr>
                            <c:forEach items="${listainscricao}" var="inscricao">
                                <c:if test="${numeroInscricao == inscricao.id}">
                                    <c:choose>
                                        <c:when test="${not empty inscricao.numeroAluno}">
                                            <td>${inscricao.numeroAluno}</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td>G${inscricao.idNumeroGrupo}</td>
                                        </c:otherwise>
                                    </c:choose>
                                    <td>${inscricao.ordemEscolha}ª Escolha</td>
                                    <td>${inscricao.registoDeInscricao}</td>
                                </c:if>
                            </c:forEach>
                            <c:forEach var="disciplinaAssociada" items="${atribuicaoForm.disciplinas}">
                                <c:forEach items="${listadisciplinas}" var="disciplina">
                                    <c:if test="${disciplina.id == disciplinaAssociada}">
                                        <%--Quando existir a possibilidade de aceder as notas dos alunos nas
                                        disciplinas este campo irá ser preenchido com essas notas--%>
                                        <td></td>
                                    </c:if>
                                </c:forEach>
                            </c:forEach>
                            <td><form:button value="${numeroInscricao}" type="submit" name="numeroAlunoAtribuido" id="atribuir" class="btn btn-primary">Atribuir</form:button></td>
                        </tr>
                    </c:forEach>
                    </tbody>

                </table>
            </form:form>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>


