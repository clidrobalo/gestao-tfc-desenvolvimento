    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    <tiles:insertDefinition name="defaultTemplate">

        <tiles:putAttribute name="body">

          <div class="p-3 mb-2 bg-dark text-white" id="titulo">Avaliaçao ${momentoAvaliacao} </div>
          <div class="container">

          <table class="table table-striped table-hover table-responsive-sm" id="tabelaListaTFCsAvaliados">
                 <thead  class="">
                     <tr class="row">
                        <td class="col-5 text-center  border-bottom border-primary font-weight-bold">Critério</td>
                        <td class="col-1 text-center  border-bottom border-primary font-weight-bold">Nota</td>
                        <td class="col-6 text-center  border-bottom border-primary font-weight-bold">Comentario</td>
                    </tr>
                 </thead>

                 <tbody class="">
                    <c:forEach var="criterio" items="${criterioAvaliacoes}">
                        <tr class="row mt-1">
                            <td class="col-5">${criterio.getDescricao()} (${criterio.getPeso()})</td>
                            <c:forEach var="nota" items="${notaCriterios}">
                                <c:if test="${criterio.getId() == nota.getIdCriterio()}">
                                <td class="col-1 text-center bg-primary text-white">${nota.getNota()}</td>
                                </c:if>
                            </c:forEach>
                            <td class="col-6 text-center">
                                <textarea class="form-control" rows="1" disabled>${0}</textarea>
                            </td>
                        </tr>
                    </c:forEach>
                 </tbody>
                 <tfoot class="">
                    <tr class="row mt-1">
                        <td class="col-5 text-center bg-secondary text-white font-weight-bold">Média Nota</td>
                        <td class="col-1 text-center bg-dark text-white font-weight-bold"><h5><strong>${tfc.getAvaliacaoApresentacaoIntermedia()}</strong></h5></td>
                        <td class="col-6 bg-secondary"></td>
                    </tr>
                 </tfoot>
             </table>

              <div class="row">
                  <a href="<c:url value="http://localhost:8080"/>" >
                    <button type="button" class="btn btn-primary" style="width: 120px;">Voltar</button>
                  </a>
              </div>


        </tiles:putAttribute>
    </tiles:insertDefinition>
