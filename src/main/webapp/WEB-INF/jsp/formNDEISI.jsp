<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">

<c:url var="post_url"  value="/formNDEISI" />
<form:form id="myform" method="POST" modelAttribute="ndeisiForm" action="${post_url}">
    <form:hidden path="id"/>
    <form:hidden path="cursoTFC"/>
    <form:hidden path="disciplinasTFC"/>
    <form:hidden path="tecnologiasTFC"/>

        <div class="container" id="formulario">
            <h1>Submissão de Proposta de TFC</h1>
            <div class="content">
                <div class="alert alert-info alert-dismissible fade show" role="alert">
                    <c:choose>
                        <%--Local para programar as mensagens que o utilizador vai receber ao longo dos vários momentos de
                        mudança de estado. Cada when representa uma mensagem--%>
                        <c:when test="${(tfc.estado == 'A Aguardar Aprovação') or (tfc.id == null)}">
                            <h4 class="alert-heading">Instruções para submissão:</h4>
                            <p>O DEISI - Departamento de Engenharia Informática e Sistemas de Informação da ECATI, com o objetivo de criar um papel de cooperação dentro da Universidade, inicia este semestre o convite às várias Escolas da ULHT, e seus departamentos, para submissão de candidaturas de projetos que envolvam o desenvolvimento de soluções tecnológicas capazes de apoiar necessidades que existam.
                                Estes projetos serão desenvolvidos no âmbito da disciplina de Trabalho Final de Curso dos cursos de Engenharia Informática ou de Informática de Gestão em cada semestre.
                                Os projetos terão um orientador do DEISI e um co-orientador, que será indicado pela Escola/Departamento da ULHT.
                                Obrigado pela colaboração e lançamento do desafio. Procuraremos garantir a alocação de algum aluno a esse desafio.
                                Entraremos em contacto no início do semestre, para vos dar nota do arranque do projeto.</p>
                        </c:when>
                        <c:when test="${(tfc.avaliacaoProposta == 'Aceite') or (tfc.avaliacaoProposta == 'Aceite Com Modificações') or (tfc.avaliacaoProposta == 'Recusado')}">
                            <h4 class="alert-heading">Aviso de Avaliação:</h4>
                            <p>Esta proposta de TFC já foi avaliada pela Coordenação. Por isso nenhum dos elementos pode ser editado.
                                Qualquer alteração realizada, não irá ser gravada</p>
                            <p>Resultado da avaliação: <strong>${tfc.avaliacaoProposta}</strong></p>
                            <c:if test="${tfc.motivoRecusa != null}">
                                <p>Motivo da proposta ter sido recusada: <strong>${tfc.motivoRecusa}</strong></p>
                            </c:if>
                        </c:when>

                    </c:choose>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <form>
                        <div class="form-group">

                            <label id="labelCostumizada">Nome do Preponente do TFC</label>
                            <form:errors id="erros" path="preponente" cssClass="error"/><br/>
                            <form:input path="preponente" label="titulo" id="preponente"  class="form-control" placeholder="Introduza o nome do Preponente" />

                            <label id="labelCostumizada">Nome do Departamento</label>
                            <form:errors id="erros" path="nomeDepartamento" cssClass="error"/><br/>
                            <form:input path="nomeDepartamento" label="titulo" id="nomeDepartamento"  class="form-control" placeholder="Introduza o nome do Departamento" />

                        </div>

                        <div class="form-group">


                                    <label id="labelCostumizada">Email de Contato</label>
                                    <form:errors id="erros" path="email" cssClass="error"/><br/>
                                    <form:input type="email" path="email" label="email" id="email"  class="form-control" placeholder="Introduza o email de contato" />


                                    <label id="labelCostumizada">Número de Contato</label>
                                    <form:errors id="erros" path="contato" cssClass="error"/><br/>
                                    <form:input path="contato" label="contato" id="contato"  class="form-control" placeholder="Introduza o telefone de contato" />


                        </div>

                        <div class="form-group">
                            <form:label id="labelCostumizada" path="titulo">Titulo</form:label>
                            <form:errors id="erros" path="titulo" cssClass="error"/><br/>
                            <form:input path="titulo" label="titulo" id="tituloForm"  class="form-control" placeholder="Introduza o Titulo" />
                        </div>
                        <div class="form-group">
                            <form:label id="labelCostumizada" path="descricao" for="descricao">Descrição do TFC (máximo 500 caractéres):</form:label>
                            <form:errors id="erros" path="descricao" cssClass="error"/><br/>
                            <form:textarea placeholder="Escreva aqui a descrição da sua proposta....." path="descricao" class="form-control" name="descricao" rows="5" id="descricaoForm"></form:textarea>
                            <small id="count" class="form-text text-muted">Ainda tem 500 caractéres.</small>
                        </div>

                        <div class="form-group">
                            <label class="form-check-label" for="checkboxEntidade">Ligação a um Empresa ou Entidade Externa?</label>
                            <c:choose>
                                <c:when test="${not empty ndeisiForm.entidade}">
                                    <input type="checkbox" id="checkboxEntidade" onclick="toogle()" checked><br>
                                    <form:input path="entidade" type="text"  id="entidade" placeholder="Introduza o nome da Entidade"  class="form-control" disabled = "false" ></form:input>
                                </c:when>
                                <c:otherwise>
                                    <input type="checkbox" id="checkboxEntidade" onclick="toogle()" ><br>
                                    <form:input path="entidade" type="text" id="entidade" placeholder="Introduza o nome da Entidade"  class="form-control" disabled = "true" ></form:input>
                                </c:otherwise>
                            </c:choose>
                        </div>

                        <div class="form-group">
                            <label id="labelCostumizada" for="cursos">Escolha um curso:</label>
                            <form:select id="cursoForm" path="cursoAssociado" name="cursos">
                                <c:choose>
                                    <c:when test="${not empty cursos}">
                                        <form:option value="0">Nenhum</form:option>
                                        <c:forEach var="curso" items="${cursos}">
                                            <form:option value="${curso.id}">${curso.nome}</form:option>
                                        </c:forEach>
                                    </c:when>
                                </c:choose>
                            </form:select>
                            <form:errors id="erros" path="cursoAssociado" cssClass="error"/><br/>
                        </div>

                        <div class="accordion" id="disc" >
                            <label id="labelCostumizada" for="disc">Escolha a(s) disciplina(s) a associar:</label>
                            <form:errors id="erros" path="disciplinas" cssClass="error"/><br/>
                            <c:choose>
                                <c:when test="${not empty cursos}">
                                    <c:forEach var="curso" items="${cursos}">
                                        <div class="card">


                                            <div class="card-header" id="${curso.id}" role="tab">
                                                <h5 class="mb-0">
                                                    <input type="button"
                                                           value="${curso.nome}"
                                                           class="btn collapsed"
                                                           data-toggle="collapse"
                                                           data-target="#${curso.nome}"
                                                           aria-controls="#${curso.nome}"
                                                           id="botaoAccordion">
                                                </h5>
                                            </div>

                                            <div id="${curso.nome}" class="collapse"
                                                 aria-labelledby="${curso.nome}" data-parent="#accordion">
                                                <div class="card-body">
                                                    <table class="table table-striped" border="0">
                                                        <thead>
                                                        <tr>
                                                            <th>Nome</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <c:forEach var="disciplina" items="${listadisciplinas}" >
                                                            <c:if test="${(disciplina.cursoAssociado == curso.id)}">
                                                                <tr>

                                                                    <td>${disciplina.nome}</td>
                                                                    <td><form:checkbox path="disciplinas"
                                                                                       value="${disciplina.id}"/></td>
                                                                </tr>
                                                            </c:if>

                                                        </c:forEach>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                        </div>
                                    </c:forEach>
                                </c:when>
                            </c:choose>
                        </div>
                        <br>


                        <div class="form-group">
                            <label id="labelCostumizada" for="tecno">Adicione as tecnologias a associar:</label>
                            <form:errors id="erros" path="tecnologias" cssClass="error"/><br/>
                            <input list="tecnologias" name="tecno" id="tecno"/>
                            <datalist id="tecnologias">
                                <c:choose>
                                <c:when test="${not empty listatecnologias}">
                                <c:forEach var="tecnologia" items="${listatecnologias}">
                                <option value="${tecnologia.nome}">
                                    </c:forEach>
                                    </c:when>
                                    </c:choose>
                            </datalist>
                            <button type="button" id="adicionaDisciplina" class="btn btn-light" disabled><i
                                    class="fas fa-plus"></i></button>
                        </div>

                        <div class="form-group" id="conjuntoTecnologogias">
                            <c:choose>
                                <c:when test="${not empty ndeisiForm.tecnologias}">
                                    <c:forEach items="${ndeisiForm.tecnologias}" var="nomeTecnologia">
                                        <c:choose>
                                            <c:when test="${erroBinding == true}">

                                                <form:button id="butaoDisciplina" class="btn btn-success btn-sm"
                                                             value="${nomeTecnologia}" path="tecnologias" type="button"
                                                             onclick ="deleteElementBD(this,'${nomeTecnologia}')">
                                                    ${nomeTecnologia}
                                                </form:button>
                                                <input id="${nomeTecnologia}" type="hidden" name="tecnologias"
                                                       value="${nomeTecnologia}">
                                            </c:when>
                                            <c:otherwise>
                                                <c:forEach var="tecnologia" items="${listatecnologias}">
                                                    <c:if test="${nomeTecnologia == tecnologia.nome}">
                                                        <form:button id="butaoDisciplina" class="btn btn-success btn-sm"
                                                                     value="${nomeTecnologia}" path="tecnologias" type="button"
                                                                     onclick ="deleteElementBD(this,'${nomeTecnologia}')">
                                                            ${nomeTecnologia}
                                                        </form:button>
                                                        <input id="${nomeTecnologia}" type="hidden" name="tecnologias"
                                                               value="${nomeTecnologia}">
                                                    </c:if>
                                                </c:forEach>
                                            </c:otherwise>
                                        </c:choose>

                                    </c:forEach>
                                </c:when>
                            </c:choose>
                        </div>

                        <%--<button type="submit" name="gravar" id="btGravar" class="btn btn-primary ">Gravar</button>--%>


                        <c:if test="${(ndeisiForm.id != null) and (tfc.estado=='A Aguardar Aprovação')}">
                            <a href="/delete/${ndeisiForm.id}"><button type="button" name="apagar" id="btApagar" class="btn btn-danger btn-xl">Apagar</button></a>
                        </c:if>
                        <c:choose>
                            <c:when test="${(tfc.estado=='Aceite') or (tfc.estado=='Aceite com Modificações') or (tfc.estado=='Recusado')}">
                               <button type="button" name="dash" id="btDash" class="btn btn-primary">Voltar ao seu Dashboard</button>
                            </c:when>
                            <c:otherwise>
                                <button type="submit" name="gravar" id="btGravar" class="btn btn-primary btn-xl">Submeter</button>
                            </c:otherwise>
                        </c:choose>

                    </form>

                </div>
            </div>
        </div>
</form:form>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>


<script type="text/javascript">
    document.getElementById('descricaoForm').onkeyup = function () {
        var conta = 500 - this.value.length;
        if (conta  > 0){
            document.getElementById('count').innerHTML = "Ainda tem: " + (500 - this.value.length + " caractéres.");
        }else{
            document.getElementById('count').innerHTML = "Já não tem mais caractéres para escrever";
        }

    };

    function toogle() {
        var input = document.getElementById("entidade");
        if(input.disabled){
            input.disabled = false;
        } else{
            input.disabled = true;
        }
    }

    var el = document.getElementById('adicionaDisciplina');
    el.onclick = function() {
        var disciplina = document.getElementById("tecno").value;
        var node = document.getElementById("conjuntoTecnologogias");
        var butao = document.createElement("form:button");
        butao.setAttribute("id", "butaoDisciplina");
        butao.setAttribute("class", "btn btn-success btn-sm");
        butao.setAttribute("value", disciplina);
        butao.setAttribute("path", "tecnologias");
        butao.setAttribute("onclick", "deleteElement(this, " + disciplina+")");
        butao.innerHTML = disciplina;
        node.append(butao);
        var inputElem = document.createElement('input');
        inputElem.setAttribute("id", disciplina);
        inputElem.setAttribute("type", "hidden");
        inputElem.setAttribute("name", "tecnologias");
        inputElem.setAttribute("value", disciplina);
        node.append(inputElem);
        document.getElementById("tecno").value = '';
        document.getElementById('adicionaDisciplina').disabled = true;
    };

    function deleteElement(id, elem){
        /* var elementos = document.getElementsByName("butaoDisciplina");
         $(".classe").remove();*/
      /*  var elementoHidden = document.getElementById(elem);
        elementoHidden.remove();
         var elementos = document.getElementById("butaoDisciplina");*/
        elem.remove();
        id.remove();
    }

    /*Funcao que nao permite a introdução de uma tecnologia vazia*/
    document.getElementById('tecno').onkeyup = function () {
        if(document.getElementById('tecno').value == ""){
            document.getElementById('adicionaDisciplina').disabled = true;
        }else{
            document.getElementById('adicionaDisciplina').disabled = false;
        }
    }

    $("#tecno").select(function() {
        document.getElementById('adicionaDisciplina').disabled = false;
    });

    function deleteElementBD(id, elem){
        /* var elementos = document.getElementsByName("butaoDisciplina");
         $(".classe").remove();*/
        var elementoHidden = document.getElementById(elem);
        elementoHidden.remove();
        /* var elementos = document.getElementById("butaoDisciplina");
         elementos.remove();*/
        id.remove();
    }

    var btnDash = document.getElementById("btDash");
    var url = "/dashboardNDEISI";

    btnDash.onclick = function (ev) {
        window.location =url;
    }
</script>


    </tiles:putAttribute>
</tiles:insertDefinition>
