    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    <tiles:insertDefinition name="defaultTemplate">

        <tiles:putAttribute name="body">

            <div class="p-3 mb-2 bg-dark text-white" id="titulo">Avaliações</div>

            <div class="container bg-light border border-info mb-4 p-2">
                <div class="row">
                    <c:forEach var="avaliacao" items="${avaliacoes}">
                        <div class="col">
                            <div class="p-3 mb-2 bg-secondary text-white" id="titulo">Avaliação ${avaliacao.getMomentoAvaliacao()}</div>

                                <div class="row d-flex justify-content-center">
                                    <c:url var="post_url"  value="/detalheAvaliacaoDiscente/${tfc.getId()}" />
                                    <form:form method="GET" modelAttribute="detalheAvaliacao" action="${post_url}">
                                        <input type="hidden" value="${0}" name="numMomentoAvaliacao"/>
                                        <button class="btn btn bg-primary text-white"><i class="fas fa-align-justify"></i> Detalhe Nota</i></button>
                                    </form:form>
                                </div>

                            <div class="mb-2 bg-primary text-white font-weight-bold">
                                <div class="row m-0">
                                    <div class="col d-flex justify-content-center p-2">Nota Final</div>
                                    <div class="col d-flex justify-content-center p-2">${tfc.getAvaliacaoApresentacaoIntermedia()}</div>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>

        </tiles:putAttribute>
    </tiles:insertDefinition>
