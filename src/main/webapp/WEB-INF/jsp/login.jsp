<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<html>
<head>
    <meta charset="UTF-8" />
    <title>Gestão de TFC</title>
    <%--<img src = "http://www.ulusofona.pt/media/logotipo-lusofona.svg">--%>
    <link rel="icon" href="data:;base64,iVBORw0KGgo="> <!-- to prevent favicon requests -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
    <link href="jsp/stylesheet/styles.css" rel="stylesheet">
    <%--<link href="sticky-footer.css" rel="stylesheet">--%>
    <style>
        body{

            margin-top: 10%;
            margin-left: 25%;
            max-width: 50%;
            background-color: #cccccc;
            background-image: url("jsp/images/jumboTransparente.png");
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            color: white;
            /*opacity: 0.5;
            filter: alpha(opacity=50); !* For IE8 and earlier *!*/
        }

        h1{
            width:30px;
            margin-top:-50px;
            margin-left:5px;
            background:white;
            text-align: center;
        }

        #botaoLogin{
            width: 100%;
            font-family: 'Signika', sans-serif;
            font-weight: bold;
            background-color: #F4FFFA;
            color: black;
        }

        .labelCostum{
            font-family: 'Signika', sans-serif;
            font-weight: bold;
        }

        #outros{
            margin-top: 100px;
            padding: 1em 1em;
            border: 2px solid #F4FFFA;
        }

        .botaoOutros{
            width: 100%;
            font-family: 'Signika', sans-serif;
            background-color: #F4FFFA;
            font-weight: bold;
            color: black;
            margin: 5px;

        }

        form{
            margin: 0;
            padding: 1em 1em;
            border: 2px solid #F4FFFA;
        }

    </style>
</head>
<body>
<form action="<c:url value='/login' />" method="post">

    <c:if test="${param.error != null}"><div>Erro na autenticação</div><br/></c:if>
    <c:if test="${param.logout != null}"><div>Logout com sucesso</div><br/></c:if>

    <form class="labelCostum" id="DEISI">
        <h2>Espaço dedicado a Docentes e Discentes DEISI</h2>
        <div class="form-group">
            <label class="labelCostum" for="username">Introduza o seu número de Professor ou de Aluno</label>
            <input type="text" name="username" class="form-control labelCostum" id="username" aria-describedby="textHelp" placeholder="Introduza o seu nº de Professor ou de Aluno">
        </div>
        <div class="form-group user">
            <label class="labelCostum" for="password">Password</label>
            <input type="password" class="form-control labelCostum" name="password" id="password" placeholder="Password">
        </div>

        <button type="submit" id="botaoLogin" name="submit" class="btn">Entrar</button>
    </form>

    <div class="container" id="outros">
        <a href="/dashboardNDEISI" class="btn botaoOutros ">Outros Departamentos</a>
        <a href="/dashboardEmpresa" class="btn botaoOutros ">Empresas/Entidades Externas</a>
    </div>



    <%--<label for="username">User:</label>
    <input type="text" id="username" name="username"/><br/>


    <label for="password">Password:</label>
    <input type="password" id="password" name="password"><br/>

    <div>
        <input name="submit" type="submit"/>
    </div>--%>
</form>

</body>
</html>

