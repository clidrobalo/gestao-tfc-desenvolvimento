 <%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">
        <h1 id="titulo">Lista de TFCs com Inscrições</h1>
        <div class="container">
            <c:choose>
                <c:when test="${not empty listaTFC}">
                    <p><button type="button" class="btn btn-primary" onclick="sortDecrescente()">Ordenar a tabela por ordem decrescente de Inscrições</button>
                        <button type="button" class="btn btn-primary" onclick="sortCrescente()">Ordenar a tabela por ordem crescente de Inscrições</button></p>
                    <table class="table table-striped table-hover table-responsive-sm" id="tabelaInscricoes">
                        <thead>
                        <tr>
                            <th>ID TFC</th>
                            <th>Titulo TFC</th>
                            <th>Número Inscrições</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="tfc" items="${listaTFC}">
                            <tr>
                                <td>${tfc.idtfc}</td>
                                <td>${tfc.titulo}</td>
                                <td>${tfc.numeroInscricoes}</td>
                                <td><a href="<c:url value="/atribuirtfc/${tfc.id}"/>" ><i class="fas fa-eye"></i></a></td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise>
                    <h2 id="titulo2">Não existe nenhum TFC para atribuir.</h2>
                </c:otherwise>
            </c:choose>
        </div>

        <script>
            function sortDecrescente() {
                var table, rows, switching, i, x, y, shouldSwitch;
                table = document.getElementById("tabelaInscricoes");
                switching = true;
                while (switching) {
                    switching = false;
                    rows = table.getElementsByTagName("TR");
                    for (i = 1; i < (rows.length - 1); i++) {
                        shouldSwitch = false;
                        x = rows[i].getElementsByTagName("td")[2];
                        y = rows[i + 1].getElementsByTagName("td")[2];
                        if (x.innerHTML < y.innerHTML) {
                            shouldSwitch = true;
                            break;
                        }
                    }
                    if (shouldSwitch) {
                        rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                        switching = true;
                    }
                }
            }
            function sortCrescente() {
                var table, rows, switching, i, x, y, shouldSwitch;
                table = document.getElementById("tabelaInscricoes");
                switching = true;
                while (switching) {
                    switching = false;
                    rows = table.getElementsByTagName("TR");
                    for (i = 1; i < (rows.length - 1); i++) {
                        shouldSwitch = false;
                        x = rows[i].getElementsByTagName("td")[2];
                        y = rows[i + 1].getElementsByTagName("td")[2];
                        if (x.innerHTML > y.innerHTML) {
                            shouldSwitch = true;
                            break;
                        }
                    }
                    if (shouldSwitch) {
                        rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                        switching = true;
                    }
                }
            }
        </script>

    </tiles:putAttribute>
</tiles:insertDefinition>

