<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">
        <c:url var="post_url"  value="/inscricaotfc" />
        <form:form method="POST" action="${post_url}">
            <div class="container">
                <h1>Lista das suas escolhas de TFCs</h1>
                <c:choose>
                    <c:when test="${not empty listainscricao}">
                        <table class="table table-striped table-hover table-responsive-sm">

                            <thead>
                            <tr>
                                <th>ID TFC</th>
                                <th>Titulo</th>
                                <th>Orientador</th>
                                <th>Ordem de Preferência</th>
                                <th>Estado</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach var="inscricao" items="${listainscricao}">
                                <tr>
                                    <td>${inscricao.idTFC}</td>
                                    <c:forEach items="${listatfc}" var="tfc">
                                        <c:if test="${tfc.idtfc == inscricao.idTFC}">
                                            <td>${tfc.titulo}</td>
                                            <c:forEach var="orientador" items="${orientadores}">
                                                <c:if test="${orientador.numeroProfessor == tfc.orientador}">
                                                    <td>${orientador.nome}</td>
                                                </c:if>
                                            </c:forEach>
                                            <td>${inscricao.ordemEscolha}ª Escolha</td>
                                            <td>${inscricao.estado}</td>
                                        </c:if>
                                    </c:forEach>
                                </tr>

                            </c:forEach>
                            </tbody>
                        </table>

                    </c:when>
                    <c:otherwise>
                        <h2 id="titulo2">Ainda não se inscreveu em nenhum TFC.</h2>
                    </c:otherwise>
                </c:choose>

            </div>
        </form:form>

    </tiles:putAttribute>
</tiles:insertDefinition>
