<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">

        <div class="container">

            <br>
            <h2>Bem vindo à plataforma de Gestão de TFCs do DEISI</h2>
            <br>
            <h4>Modo de funcionamento:</h4>
            <ul>
            <security:authorize access="hasRole('ROLE_STUDENT')">
                <%--TODO: Necessário fazer verificação se o aluno tem propostas, em caso de ter verificar se Existe já uma avaliação e apresentar--%>
                <li>Ler o regulamento, disponível no moodle ter em atenção às datas disponíveis no regulamento para a realização de cada uma das tarefa seguintes nesta plataforma; </li>
                <li>Para submeter uma nova proposta carregue: <a href="<c:url value="/formaluno"/>">Submeter Proposta</a> ou em alternativa pode ir pelo menu.</li>
                <li>Poderá consultar todas as suas propostas através do link: <a href="<c:url value="/listar"/>">Listagem de Propostas efetuadas</a>, ou através do menú</li>

                <div class="content pt-4">
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        <h4 class="alert-heading text-danger">Alerta - Agenda avaliação</h4>
                        <p>A sua apresentação foi  agendada para o dia 10 de Maio as 10 Horas. A sala será confirmada nos proximos dias</p>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </security:authorize>

            <security:authorize access="hasRole('ROLE_COORDENACAO')">
                <li>Ler o regulamento, disponível no moodle ter em atenção às datas disponíveis no regulamento para a realização de cada uma das tarefa seguintes nesta plataforma; </li>
                <li>Para submeter uma nova proposta carregue: <a href="<c:url value="/formprofessor"/>">Submeter Proposta</a> ou em alternativa pode ir pelo menu.</li>
                <li>Poderá consultar todas as suas propostas através do link: <a href="<c:url value="/listar"/>">Listagem de Propostas efetuadas</a>, ou através do menú</li>
            </security:authorize>

            <security:authorize access="hasRole('ROLE_TEACHER')">
                <li>Ler o regulamento, disponível no moodle ter em atenção às datas disponíveis no regulamento para a realização de cada uma das tarefa seguintes nesta plataforma; </li>
                <li>Para submeter uma nova proposta carregue: <a href="<c:url value="/formprofessor"/>">Submeter Proposta</a> ou em alternativa pode ir pelo menu.</li>
                <li>Poderá consultar todas as suas propostas através do link: <a href="<c:url value="/listar"/>">Listagem de Propostas efetuadas</a>, ou através do menú</li>
            </security:authorize>
            </ul>
        </div>

    </tiles:putAttribute>
</tiles:insertDefinition>
