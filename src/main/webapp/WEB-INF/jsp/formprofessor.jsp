<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tiles:insertDefinition name="defaultTemplate">

    <tiles:putAttribute name="body">
        <c:url var="post_url"  value="/formprofessor" />
        <form:form method="POST" modelAttribute="profForm" action="${post_url}">
            <form:hidden path="id"/>
            <form:hidden path="cursoTFC"/>
            <form:hidden path="disciplinasTFC"/>
            <form:hidden path="tecnologiasTFC"/>
            <form:hidden path="preponente"/>
            <div class="container">
                <h1>Submissão de Proposta de TFC</h1>
                <div class="row">

                    <div class="col-md-12">
                        <form>
                            <div class="form-group">
                                <form:label id="labelCostumizada" path="titulo">Novo</form:label>
                                <form:errors id="erros" path="titulo" cssClass="error"/><br/>
                                <form:input path="titulo" label="titulo" id="tituloForm"  class="form-control" placeholder="Introduza o Titulo" />
                            </div>
                            <div class="form-group">
                                <form:label id="labelCostumizada" path="descricao" for="descricao">Descrição do TFC (máximo 500 caractéres):</form:label>
                                <form:errors id="erros" path="descricao" cssClass="error"/><br/>
                                <form:textarea placeholder="Escreva aqui a descrição da sua proposta....." path="descricao" class="form-control" name="descricao" rows="5" id="descricaoForm"></form:textarea>
                                <small id="count" class="form-text text-muted">Ainda tem 500 caractéres.</small>
                            </div>

                            <div class="form-group">
                                <label class="form-check-label" for="checkboxEntidade">Ligação a um Empresa ou Entidade Externa?</label>
                                <c:choose>
                                    <c:when test="${not empty profForm.entidade}">
                                        <input type="checkbox" id="checkboxEntidade" onclick="toogle()" checked><br>
                                        <form:input path="entidade" type="text"  id="entidade" placeholder="Introduza o nome da Entidade"  class="form-control" disabled = "false" ></form:input>
                                    </c:when>
                                    <c:otherwise>
                                        <input type="checkbox" id="checkboxEntidade" onclick="toogle()" ><br>
                                        <form:input path="entidade" type="text" id="entidade" placeholder="Introduza o nome da Entidade"  class="form-control" disabled = "true" ></form:input>
                                    </c:otherwise>
                                </c:choose></div>

                            <div class="form-group">
                                <label id="labelCostumizada" for="cursos">Escolha um curso:</label>
                                <form:errors id="erros" path="cursoAssociado" cssClass="error"/><br/>
                                <form:select id="cursoForm" path="cursoAssociado" name="cursos">
                                    <c:choose>
                                        <c:when test="${not empty cursos}">
                                            <form:option value="0">Nenhum</form:option>
                                            <c:forEach var="curso" items="${cursos}">
                                                <form:option value="${curso.id}">${curso.nome}</form:option>
                                            </c:forEach>
                                        </c:when>
                                    </c:choose>
                                </form:select>
                                <form:errors id="erros" path="cursoAssociado" cssClass="error"/><br/>
                            </div>

                            <div class="accordion" id="disc" >
                                <label id="labelCostumizada" for="disc">Escolha a(s) disciplina(s) a associar:</label>
                                <form:errors id="erros" path="disciplinas" cssClass="error"/><br/>
                                <c:choose>
                                    <c:when test="${not empty cursos}">
                                        <c:forEach var="curso" items="${cursos}">
                                            <div class="card">


                                                <div class="card-header" id="${curso.id}" role="tab">
                                                    <h5 class="mb-0">
                                                        <input type="button"
                                                               value="${curso.nome}"
                                                               class="btn collapsed"
                                                               data-toggle="collapse"
                                                               data-target="#${curso.nome}"
                                                               aria-controls="#${curso.nome}"
                                                               id="botaoAccordion">
                                                    </h5>
                                                </div>

                                                <div id="${curso.nome}" class="collapse"
                                                     aria-labelledby="${curso.nome}" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <table class="table table-striped" border="0">
                                                            <thead>
                                                            <tr>
                                                                <th>Nome</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <c:forEach var="disciplina" items="${listadisciplinas}" >
                                                                <c:if test="${(disciplina.cursoAssociado == curso.id)}">
                                                                    <tr>

                                                                        <td>${disciplina.nome}</td>
                                                                        <td><form:checkbox path="disciplinas"
                                                                                           value="${disciplina.id}"/></td>
                                                                    </tr>
                                                                </c:if>

                                                            </c:forEach>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                            </div>
                                        </c:forEach>
                                    </c:when>
                                </c:choose>
                            </div>
                            <br>

                            <div class="form-group">
                                <label id="labelCostumizada" for="tecno">Adicione as tecnologias a associar:</label>
                                <form:errors id="erros" path="tecnologias" cssClass="error"/><br/>
                                <input list="tecnologias" name="tecno" id="tecno"/>

                                <datalist id="tecnologias">
                                    <c:choose>
                                        <c:when test="${not empty listatecnologias}">
                                            <c:forEach var="tecnologia" items="${listatecnologias}">
                                                <option value="${tecnologia.nome}">
                                            </c:forEach>
                                        </c:when>
                                    </c:choose>
                                </datalist>
                                <button type="button" id="adicionaDisciplina" class="btn btn-light" disabled><i
                                        class="fas fa-plus"></i></button>
                            </div>

                            <div class="form-group" id="conjuntoTecnologogias">
                                <c:choose>
                                    <c:when test="${not empty profForm.tecnologias}">
                                        <c:forEach items="${profForm.tecnologias}" var="nomeTecnologia">
                                            <c:choose>
                                                <c:when test="${erroBinding == true}">

                                                    <form:button id="butaoDisciplina" class="btn btn-success btn-sm"
                                                                 value="${nomeTecnologia}" path="tecnologias" type="button"
                                                                 onclick ="deleteElementBD(this,'${nomeTecnologia}')">
                                                        ${nomeTecnologia}
                                                    </form:button>
                                                    <input id="${nomeTecnologia}" type="hidden" name="tecnologias"
                                                           value="${nomeTecnologia}">
                                                </c:when>
                                                <c:otherwise>
                                                    <c:forEach var="tecnologia" items="${listatecnologias}">
                                                        <c:if test="${nomeTecnologia == tecnologia.nome}">
                                                            <form:button id="butaoDisciplina" class="btn btn-success btn-sm"
                                                                         value="${nomeTecnologia}" path="tecnologias" type="button"
                                                                         onclick ="deleteElementBD(this,'${nomeTecnologia}')">
                                                                ${nomeTecnologia}
                                                            </form:button>
                                                            <input id="${nomeTecnologia}" type="hidden" name="tecnologias"
                                                                   value="${nomeTecnologia}">
                                                        </c:if>
                                                    </c:forEach>
                                                </c:otherwise>
                                            </c:choose>

                                        </c:forEach>
                                    </c:when>
                                </c:choose>
                            </div>

                            <button type="submit" name="gravar" id="gravar" class="btn btn-primary btn-xl">Submeter</button>
                        </form>
                    </div>


                </div>
            </div>


        </form:form>

    </tiles:putAttribute>
</tiles:insertDefinition>
