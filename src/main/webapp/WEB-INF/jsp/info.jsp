<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">

        <h1>Utilizador ${utilizador.id}</h1>
        <p>Nome: ${utilizador.name}</p>
        <p>Morada: ${utilizador.address}</p>
        <a href="/edit/${utilizador.id}">Editar</a>
        <form action="/delete/${utilizador.id}" method="post">
            <input type="submit" value="Apagar" />
        </form>

    </tiles:putAttribute>
</tiles:insertDefinition>