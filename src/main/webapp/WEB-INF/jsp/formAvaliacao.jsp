    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    <tiles:insertDefinition name="defaultTemplate">

        <tiles:putAttribute name="body">
               <div class="container">
                    <h1 id="titulo">Avaliação ${avaliacao.getMomentoAvaliacao()}</h1>

                    <div class="row mt-4">
                        <div class="col-3 bg-primary text-light text-center p-2">Titulo</div>
                        <div class="col-9 text-dark text-center p-2" style="background-color: #DCDCDC;">${tfc.getTitulo()}</div>
                    </div>

                     <div class="row mt-1">
                        <div class="col-3 bg-dark text-light text-center p-2">Descrição</div>
                        <div class="col-9 text-dark text-center p-2" style="background-color: #DCDCDC;">${tfc.getDescricao()}</div>
                    </div>

                     <hr>

                    <form>
                        <c:if test="${not empty aluno}">
                            <h3>Aluno atribuido:</h3>
                            <div class="row">
                                <div class="col-3 bg-primary text-light text-center p-2">Número do Aluno</div>
                                <div class="col-9 text-dark text-center p-2" style="background-color: #DCDCDC;">${aluno.numeroAluno}</div>
                            </div>
                             <div class="row mt-1">
                                <div class="col-3 bg-dark text-light text-center p-2">Nome do Aluno</div>
                                <div class="col-9  text-dark text-center p-2" style="background-color: #DCDCDC;">${aluno.nome}</div>
                            </div>
                        </c:if>
                        <c:if test="${not empty aluno1Grupo}">
                            <h3>Atribuido a um grupo:</h3>
                            <div class="row">
                                <div class="col-3 bg-primary text-light text-center p-2">Número do Aluno 1 do Grupo</div>
                                <div class="col-9 bg-light text-dark text-center p-2">${aluno1Grupo.numeroAluno}</div>
                            </div>
                             <div class="row mt-1">
                                <div class="col-3 bg-dark text-light text-center p-2">Nome do Aluno 1 do Grupo</div>
                                <div class="col-9 bg-light text-dark text-center p-2">${aluno1Grupo.nome}</div>
                            </div>
                            <div class="row">
                                <div class="col-3 bg-primary text-light text-center p-2">Número do Aluno 2 do Grupo</div>
                                <div class="col-9 bg-light text-dark text-center p-2">${aluno2Grupo.numeroAluno}</div>
                            </div>
                             <div class="row mt-1">
                                <div class="col-3 bg-dark text-light text-center p-2">Nome do Aluno 2 do Grupo</div>
                                <div class="col-9 bg-light text-dark text-center p-2">${aluno2Grupo.nome}</div>
                            </div>
                        </c:if>
                    </form>

                    <div class="row p-4 border bg-light">
                        <div class="col-6 bg-dark text-light text-center p-2 ">Nota Individual Júri: <strong>${avaliacao.getNotaIndividualJuri()}</strong></div>
                        <div class="col-6 bg-primary text-light text-center p-2">Nota Global: <strong>${avaliacao.getNotaGlobalJuri()}</strong></div>
                    </div>

                    <h3 id="titulo" class="mt-4">Criterios</h3>

                    <c:set value="0" var="groupIdBtn"/> <!-- Variavel para referenciar o grupo que cada id do botao de nota pertence-->
                     <c:choose>
                        <c:when test="${not empty avaliacao}">
                            <c:forEach var="criterio" items="${criterios}">
                                <span id="${groupIdBtn= groupIdBtn+1}"></span> <!-- incremmentar id do grupo ids -->
                                <div class="bg-light text-dark mt-3">
                                    <h5 class="p-3 col-12 bg-dark text-white">${criterio.getDescricao()} (${criterio.getPeso()}%)</h5>
                                    <c:forEach var="itemCriterioAvaliacao" items="${ItensCriterios}">
                                        <c:if test="${itemCriterioAvaliacao.getIdICriterioAvaliacao() == criterio.getId()}">
                                            <p class="pl-2 pr-2">${itemCriterioAvaliacao.getDescricao()}</p>
                                            <hr/>
                                        </c:if>
                                    </c:forEach>
                                    <div class="row justify-content-md-center pb-3">
                                        <c:forEach var = "i" begin = "1" end = "${avaliacao.getEscalaAvaliacao()}">
                                            <c:choose>
                                                <c:when test="${tfcAvaliado == false}">
                                                        <!-- ${i}_${groupIdBtn} -->
                                                       <button id="${i}_${groupIdBtn}" class="btn col-1 border border-light text-center text-light p-2" onclick="toogleColorBtnNota(this, ${i}, ${groupIdBtn}, ${criterio.getId()})" style="background-color: rgb(0, 108, 255);">
                                                            ${i}
                                                       </button>
                                                </c:when>
                                                <c:otherwise>
                                                    <!-- Teste para atribuir cor verde a nota atribuida anteriormente na avaliação -->
                                                   <c:choose>
                                                       <c:when test="${criterio.getNotaCriterio(notaCriterios) == i}">
                                                           <!-- Sintaxe do id de cada Botão -> ${i}_${groupIdBtn} -->
                                                          <button id="${i}_${groupIdBtn}" class="btn col-1 border border-light text-center text-light p-2" onclick="toogleColorBtnNota(this, ${i}, ${groupIdBtn}, ${criterio.getId()})" style="background-color: rgb(21, 142, 18);" disabled>
                                                               ${i}
                                                          </button>
                                                       </c:when>
                                                       <c:otherwise>
                                                           <!-- ${i}_${groupIdBtn} -->
                                                          <button id="${i}_${groupIdBtn}" class="btn col-1 border border-light text-center text-light p-2" onclick="toogleColorBtnNota(this, ${i}, ${groupIdBtn}, ${criterio.getId()})" style="background-color: rgb(0, 108, 255);" disabled>
                                                               ${i}
                                                          </button>
                                                       </c:otherwise>
                                                   </c:choose>
                                                </c:otherwise>
                                            </c:choose>

                                        </c:forEach>
                                    </div>
                                </div>
                            </c:forEach>
                            <hr>

                            <c:url var="post_url"  value="/saveAvaliacao/${avaliacao.getId()}" />

                            <c:choose>
                                <c:when test="${tfcAvaliado == false}">
                                    <form:form method="POST" modelAttribute="avaliacaoForm" action="${post_url}" onsubmit="return validateForm(${criterios.size()})">
                                        <input type="hidden" name="notasEscala" id="hiddenNotas"/>
                                        <input type="hidden" value="${tfc.getId()}" name="idTFC"/>
                                        <button type="submit" class="btn btn-primary float-right" style="width: 10em; margin-bottom: 5em;" onclick="loadDados()">Submeter</button>
                                    </form:form>
                                </c:when>
                            </c:choose>

                        </c:when>
                        <c:otherwise>
                            <h2 id="" class="text-center pt-5 pb-5">Ainda não foi submetido nenhum Criterio de Avaliação</h2>
                        </c:otherwise>
                    </c:choose>
                   </div>
        </tiles:putAttribute>
    </tiles:insertDefinition>
