﻿<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">
        <form:form method="POST" modelAttribute="userForm" action="/form">

            <form:label path="name">Nome</form:label>
            <form:input path="name" label="nome" />
            <form:errors path="name" cssClass="error"/><br/>

            <form:label path="address">Morada</form:label>
            <form:input path="address" label="morada" />
            <form:errors path="address" cssClass="error"/><br/>

            <form:label path="telefone">Telefone</form:label>
            <form:input path="telefone" label="telefone" />
            <form:errors path="telefone" cssClass="error"/><br/>

            <form:label path="estadoCivil">Estado civil</form:label>
            <form:select path="estadoCivil" label="estadoCivil">
                <form:option value="" label="--- Select ---"/>
                <form:options items="${estadosCivisMap}" />
            </form:select>
            <form:errors path="estadoCivil" cssClass="error"/><br/>

            <input type="submit" name="Gravar"/>
        </form:form>
    </tiles:putAttribute>
</tiles:insertDefinition>
