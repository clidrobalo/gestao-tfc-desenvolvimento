<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">

        <div class="container">

            <c:choose>
                <c:when test="${not empty listainscricoes}">
                    <h3>Lista de Inscrições existentes</h3>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>idTFC</th>
                            <th>numero aluno</th>
                            <th>numero grupo</th>
                            <th>ordem escolha</th>
                            <th>registo inscrição</th>
                            <th>estado</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="inscricao" items="${listainscricoes}">
                            <tr>
                                <td>${inscricao.idTFC}</td>
                                <td>${inscricao.numeroAluno}</td>
                                <td>${inscricao.idNumeroGrupo}</td>
                                <td>${inscricao.ordemEscolha}</td>
                                <td>${inscricao.registoDeInscricao}</td>
                                <td>${inscricao.estado}</td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise>
                    <p>Não tem inscricoes</p>
                </c:otherwise>
            </c:choose>
        </div>

        <div class="container">
            <c:choose>
                <c:when test="${not empty utilizadores}">
                    <h3>Lista de Utilizadores existentes</h3>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>id Identificação</th>
                            <th>Coordenador</th>
                            <th>Tipo Utilizador</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="utilizador" items="${utilizadores}">
                            <tr>
                                <td>${utilizador.id}</td>
                                <td>${utilizador.idIdentificacao}</td>
                                <td>${utilizador.coordenador}</td>
                                <td>${utilizador.tipoUtilizador}</td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise>
                    <p>Não tem utilizadores</p>
                </c:otherwise>
            </c:choose>
        </div>

        <div class="container">
            <c:choose>
                <c:when test="${not empty tfcs}">
                    <h3>Lista de TFCS existentes</h3>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>idTFC</th>
                            <th>Titulo</th>
                            <th>Descricao</th>
                            <th>Preponente</th>
                            <th>Estado</th>
                            <th>Data Estado</th>
                            <th>Data da Proposta</th>
                            <th>Empresa</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="tfc" items="${tfcs}">
                            <tr>
                                <td>${tfc.id}</td>
                                <td>${tfc.idtfc}</td>
                                <td>${tfc.titulo}</td>
                                <td>${tfc.descricao}</td>
                                <td>${tfc.preponente}</td>
                                <td>${tfc.estado}</td>
                                <td>${tfc.dataEstado}</td>
                                <td>${tfc.dataProposta}</td>
                                <td>${tfc.entidade}</td>
                            </tr>

                        </c:forEach>
                    </tbody>
                    </table>
                </c:when>
                <c:otherwise>
                    <p>Não tem tfcs</p>
                </c:otherwise>
            </c:choose>
        </div>

        <div class="container">
            <c:choose>
                <c:when test="${not empty tfccurso}">
                    <h3>Lista de TFCS ligados a Cursos existentes</h3>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>idtfc</th>
                                <th>nome curso</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="tfccurso" items="${tfccurso}">
                                <tr>
                                    <td>${tfccurso.id}</td>
                                    <td>${tfccurso.idTFC}</td>
                                    <td>${tfccurso.idCurso}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise>
                    <p>Não tem cursos</p>
                </c:otherwise>
            </c:choose>
        </div>

        <div class="container">
            <c:choose>
                <c:when test="${not empty tfcdisc}">
                    <h3>Lista de TFCS ligados a Disciplinas existentes</h3>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>idTFC</th>
                                <th>idDisciplina</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="tfcdis" items="${tfcdisc}">
                                <tr>
                                    <td>${tfcdis.id}</td>
                                    <td>${tfcdis.numeroTFC}</td>
                                    <td>${tfcdis.idNumeroDisciplina}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise>
                    <p>Não tem tfc com disciplinas</p>
                </c:otherwise>
            </c:choose>
        </div>

        <div class="container">
            <c:choose>
                <c:when test="${not empty tfctecnologias}">
                    <h3>Lista de TFCS ligados a Tecnologias existentes</h3>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>idTFC</th>
                            <th>Nome</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="tfctecno" items="${tfctecnologias}">
                            <tr>
                                <td>${tfctecno.id}</td>
                                <td>${tfctecno.idTFC}</td>
                                <c:forEach var="tecnologia" items="${tecnologias}">
                                    <c:if test="${tfctecno.idTecnologia == tecnologia.id}">
                                        <td>${tecnologia.nome}</td>
                                    </c:if>
                                </c:forEach>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise>
                    <p>Não tem tfc com tecnologias</p>
                </c:otherwise>
            </c:choose>
        </div>

        <div class="container">
            <c:choose>
                <c:when test="${not empty tecnologias}">
                    <h3>Lista de Tecnologias existentes</h3>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>nome</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="tecno" items="${tecnologias}">
                            <tr>
                                <td>${tecno.id}</td>
                                <td>${tecno.nome}</td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise>
                    <p>Não tem tecnologias</p>
                </c:otherwise>
            </c:choose>
        </div>

        <div class="container">
            <c:choose>
                <c:when test="${not empty emp}">
                    <h3>Lista de Empresas existentes</h3>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>nome</th>
                            <th>email</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="ee" items="${emp}">
                            <tr>
                                <td>${ee.id}</td>
                                <td>${ee.nome}</td>
                                <td>${ee.email}</td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise>
                    <p>Não tem empresas</p>
                </c:otherwise>
            </c:choose>
        </div>

        <div class="container">
            <c:choose>
                <c:when test="${not empty grupos}">
                    <h3>Lista de Grupos existentes</h3>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>ID Grupo</th>
                            <th>idNumero1</th>
                            <th>Validação 1</th>
                            <th>idNumero2</th>
                            <th>Validação 2</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="grupo" items="${grupos}">
                            <tr>
                                <td>${grupo.id}</td>
                                <td>${grupo.idNumeroAluno1}</td>
                                <td>${grupo.confirmaAluno1}</td>
                                <td>${grupo.idNumeroAluno2}</td>
                                <td>${grupo.confirmaAluno2}</td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise>
                    <p>Não tem grupos</p>
                </c:otherwise>
            </c:choose>
        </div>

        <div class="container">
            <c:choose>
                <c:when test="${not empty ndeisi}">
                    <h3>Lista de Professores N DEISI</h3>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>ID Professor</th>
                            <th>nome</th>
                            <th>email</th>
                            <th>numero contato</th>
                            <th>departamento</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="ndeisi" items="${ndeisi}">
                            <tr>
                                <td>${ndeisi.id}</td>
                                <td>${ndeisi.idProfessor}</td>
                                <td>${ndeisi.nome}</td>
                                <td>${ndeisi.email}</td>
                                <td>${ndeisi.numeroContato}</td>
                                <td>${ndeisi.departamentoAfeto}</td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise>
                    <p>Não tem outros professores n deisi</p>
                </c:otherwise>
            </c:choose>
        </div>

    </tiles:putAttribute>
</tiles:insertDefinition>
