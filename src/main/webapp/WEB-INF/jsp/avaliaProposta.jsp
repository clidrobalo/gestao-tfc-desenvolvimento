<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tiles:insertDefinition name="defaultTemplate">

    <tiles:putAttribute name="body">

        <div class="container">
            <h1 style="margin-top: 100px;">Avaliação do TFC ${tfc.idtfc}</h1>

            <c:url var="post_url"  value="/avaliaProposta" />
            <form:form method="POST" modelAttribute="avaliaPropostaForm" action="${post_url}">

                <c:choose>
                    <c:when test="${tipoUtilizador == 'empresa'}">
                        <div class="form-group">
                            <div class="form-group">
                                <label id="labelCostumizada" path="nomeEmpresa">Nome da Empresa</label>
                                <form:input path="nomeEmpresa" label="labelCostumizada" id="tituloForm"  class="form-control" disabled="true"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <form:label id="labelCostumizada" path="morada">Morada da Empresa</form:label>

                                <form:input path="morada" label="labelCostumizada" id="tituloForm"  class="form-control" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <form:label id="labelCostumizada" path="interlocutor">Nome do Interlocutor</form:label>

                                <form:input path="interlocutor" label="labelCostumizada" id="tituloForm"  class="form-control" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">

                            <label id="labelCostumizada">Email de Contato</label>

                            <form:input type="email" path="email" label="email" id="email"  class="form-control" disabled="true" />

                            <label id="labelCostumizada">Número de Contato</label>

                            <form:input path="contato" label="contato" id="contato"  class="form-control" disabled="true" />

                        </div>
                    </c:when>
                    <c:when test="${tipoUtilizador == 'ndeisi'}">
                        <div class="form-group">

                            <label id="labelCostumizada">Nome do Preponente do TFC</label>

                            <form:input path="preponente" label="titulo" id="preponente"  class="form-control" disabled="true"/>

                            <label id="labelCostumizada">Nome do Departamento</label>

                            <form:input path="nomeDepartamento" label="titulo" id="nomeDepartamento"  class="form-control" disabled="true" />

                            <label id="labelCostumizada">Email de Contato</label>

                            <form:input type="email" path="email" label="email" id="email"  class="form-control" disabled="true" />

                            <label id="labelCostumizada">Número de Contato</label>

                            <form:input path="contato" label="contato" id="contato"  class="form-control" disabled="true" />

                        </div>
                    </c:when>
                    <c:when test="${tipoUtilizador == 'aluno'}">
                        <div class="form-group">
                                <c:choose>
                                    <c:when test="${not empty avaliaPropostaForm.nome2Aluno}">
                                        <label class="form-check-label" for="checkboxGrupo">Pretende fazer o TFC em Grupo?</label>
                                        <input type="checkbox" id="checkboxGrupo" onclick="toogleGrupo()" checked disabled><br>
                                        <div class="row">
                                            <div class="col-4">
                                                <form:input path="numero2Aluno" type="text"  id="numero2Aluno" placeholder="Introduza o numero do segundo aluno"  class="form-control" disabled = "true"/>
                                            </div>
                                            <div class="col">
                                                <form:input path="nome2Aluno" type="text"  id="nome2Aluno" placeholder="Introduza o nome do segundo aluno"  class="form-control" disabled = "true" />
                                            </div>
                                        </div>
                                    </c:when>
                                </c:choose>

                        </div>

                    </c:when>
                </c:choose>

                <form:hidden path="titulo" />
                <form:hidden path="tipoUtilizador" />
                <form:hidden path="id"/>
                <form:hidden path="cursoTFC"/>
                <form:hidden path="disciplinasTFC"/>
                <form:hidden path="tecnologiasTFC"/>
                <form:hidden path="preponente"/>
                <form:hidden path="entidadeTFC"/>
                <form:hidden path="entidade"/>
                <form:hidden path="nomeEmpresa"/>
                <form:hidden path="morada"/>
                <form:hidden path="interlocutor"/>
                <form:hidden path="email"/>
                <form:hidden path="contato"/>
                <form:hidden path="nomeDepartamento"/>

                <div class="form-group">
                    <label path="titulo">Titulo</label>
                    <input  readonly label="titulo" id="tituloForm" value="${tfc.titulo}"  class="form-control"/>
                    <label path="descricao">Descricao</label>
                    <textarea readonly class="form-control" name="descricao" rows="5" id="descricaoForm">${tfc.descricao}</textarea>
                </div>

                <c:if test="${not empty avaliaPropostaForm.entidade}">
                    <div class="form-group">
                        <label class="form-check-label" for="checkboxEntidade">Ligação a um Empresa ou Entidade Externa?</label>
                        <c:choose>
                            <c:when test="${not empty avaliaPropostaForm.entidade}">
                                <input type="checkbox" id="checkboxEntidade" onclick="toogle()" checked disabled><br>
                                <form:input path="entidade" type="text"  id="entidade" placeholder="Introduza o nome da Entidade"  class="form-control" disabled = "true" ></form:input>
                            </c:when>
                        </c:choose>
                    </div>
                </c:if>

                <div class="form-group">
                    <label id="labelCostumizada" for="profOrientador">Orientador:</label>
                    <form:errors id="erros" path="orientador" cssClass="error"/><br/>
                    <form:select id="profOriForm" path="orientador" name="profOrientador">
                        <c:choose>
                            <c:when test="${not empty orientadores}">

                                <form:option value="0">Selecione um professor Orientador</form:option>
                                <c:forEach var="orienta" items="${orientadores}">
                                    <form:option value="${orienta.numeroProfessor}">${orienta.nome}</form:option>
                                </c:forEach>
                            </c:when>
                        </c:choose>
                    </form:select>
                </div>

                <div class="form-group">
                    <label id="labelCostumizada" for="cursos">Escolha um curso:</label>
                    <form:errors id="erros" path="cursoAssociado" cssClass="error"/><br/>
                    <form:select id="cursoForm" path="cursoAssociado" name="cursos">
                        <c:choose>
                            <c:when test="${not empty cursos}">
                                <form:option value="0">Nenhum</form:option>
                                <c:forEach var="curso" items="${cursos}">
                                    <form:option value="${curso.id}">${curso.nome}</form:option>
                                </c:forEach>
                            </c:when>
                        </c:choose>
                    </form:select>
                </div>

                <div class="accordion" id="disc" >
                    <label id="labelCostumizada" for="disc">Escolha a(s) disciplina(s) a associar:</label>
                    <form:errors id="erros" path="disciplinas" cssClass="error"/><br/>
                    <c:choose>
                        <c:when test="${not empty cursos}">
                            <c:forEach var="curso" items="${cursos}">
                                <div class="card">


                                    <div class="card-header" id="${curso.id}" role="tab">
                                        <h5 class="mb-0">
                                            <input type="button"
                                                   value="${curso.nome}"
                                                   class="btn collapsed"
                                                   data-toggle="collapse"
                                                   data-target="#${curso.nome}"
                                                   aria-controls="#${curso.nome}"
                                                   id="botaoAccordion">
                                        </h5>
                                    </div>

                                    <div id="${curso.nome}" class="collapse"
                                         aria-labelledby="${curso.nome}" data-parent="#accordion">
                                        <div class="card-body">
                                            <table class="table table-striped" border="0">
                                                <thead>
                                                <tr>
                                                    <th>Nome</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <c:forEach var="disciplina" items="${listadisciplinas}" >
                                                    <c:if test="${(disciplina.cursoAssociado == curso.id)}">
                                                        <tr>

                                                            <td>${disciplina.nome}</td>
                                                            <td><form:checkbox path="disciplinas"
                                                                               value="${disciplina.id}"/></td>
                                                        </tr>
                                                    </c:if>

                                                </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                            </c:forEach>
                        </c:when>
                    </c:choose>
                </div>
                <br>

                <div class="form-group">
                    <label id="labelCostumizada" for="tecno">Adicione as tecnologias a associar:</label>
                    <form:errors id="erros" path="tecnologias" cssClass="error"/><br/>
                    <input list="tecnologias" name="tecno" id="tecno"/>
                    <datalist id="tecnologias">
                        <c:choose>
                        <c:when test="${not empty listatecnologias}">
                        <c:forEach var="tecnologia" items="${listatecnologias}">
                        <option value="${tecnologia.nome}">
                            </c:forEach>
                            </c:when>
                            </c:choose>
                    </datalist>
                    <button type="button" id="adicionaDisciplina" class="btn btn-light"><i
                            class="fas fa-plus"></i></button>
                </div>

                <div class="form-group" id="conjuntoTecnologogias">
                    <c:choose>
                        <c:when test="${not empty avaliaPropostaForm.tecnologias}">
                            <c:forEach items="${avaliaPropostaForm.tecnologias}" var="nomeTecnologia">
                                <c:choose>
                                    <c:when test="${erroBinding == true}">

                                        <form:button id="butaoDisciplina" class="btn btn-primary btn-sm"
                                                     value="${nomeTecnologia}" path="tecnologias" type="button"
                                                     onclick ="deleteElementBD(this,'${nomeTecnologia}')">
                                            ${nomeTecnologia}
                                        </form:button>
                                        <input id="${nomeTecnologia}" type="hidden" name="tecnologias"
                                               value="${nomeTecnologia}">
                                    </c:when>
                                    <c:otherwise>
                                        <c:forEach var="tecnologia" items="${listatecnologias}">
                                            <c:if test="${nomeTecnologia == tecnologia.nome}">
                                                <form:button id="butaoDisciplina" class="btn btn-primary btn-sm"
                                                             value="${nomeTecnologia}" path="tecnologias" type="button"
                                                             onclick ="deleteElementBD(this,'${nomeTecnologia}')">
                                                    ${nomeTecnologia}
                                                </form:button>
                                                <input id="${nomeTecnologia}" type="hidden" name="tecnologias"
                                                       value="${nomeTecnologia}">
                                            </c:if>
                                        </c:forEach>
                                    </c:otherwise>
                                </c:choose>

                            </c:forEach>
                        </c:when>
                    </c:choose>
                </div>

                <div class="form-group">
                    <label for="avaliacao">Avaliação:</label>
                    <form:select onchange="validate()" path="avaliacao" class="form-control" id="Avaliacao">
                        <option>Escolha uma avaliação</option>
                        <option>Aceite</option>
                        <option>Aceite Com Modificações</option>
                        <option>Recusado</option>
                    </form:select>
                </div>

                <div class="form-group">
                    <label id="labelCostumizada" for="recusa">Descrição da razão que levou à recusa/modificação da Proposta de TFC (esta informação será apresentada ao preponente):</label>
                    <form:textarea path="motivoRecusa" placeholder="Escreva aqui a razão que levou à recusa/modificação da Proposta....." class="form-control" name="recusa" rows="5" id="recusa" disabled="true"></form:textarea>
                </div>


                <button type="submit" name="gravar" id="gravar" class="btn btn-primary btn-xl">Gravar</button>
            </form:form>
            <button onclick="vaiListagem()" name="listarPropostas" id="listarPropostas" class="btn btn-danger btn-xl">Voltar à Listagem</button>
        </div>

        <script>
            function vaiListagem() {
                var btnEdit = document.getElementById("listarPropostas");
                urlFinal = "/tfc/listarpropostas";
                window.location = urlFinal;
            }

        </script>

    </tiles:putAttribute>
</tiles:insertDefinition>


