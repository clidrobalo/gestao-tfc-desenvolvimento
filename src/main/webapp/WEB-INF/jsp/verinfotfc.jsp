<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<tiles:insertDefinition name="defaultTemplate">

    <tiles:putAttribute name="body">
        <c:url var="post_url"  value="/verinfotfc" />
        <form:form method="POST" modelAttribute="inscricaoForm" action="${post_url}">
                <%--Local onde irá ser apresentada a opção de adicionar o TFC à lista de inscrições.
                Caso a List de Inscrições já tenha 5 opções de escolha, botão disable.
                Fornecer a informação que já existem 5 escolhas feitas--%>
            <form:hidden path="idTFC"/>
            <div class="container" style="margin-top: 100px;">
                <div class="content">
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        <h4 class="alert-heading">Inscrição em TFC:</h4>
                        <c:choose>
                            <c:when test="${fn:length(listainscricoes) < 5}">
                                Cara(o) aluna(o),<br>
                                caso pretenda realizar uma candidatura em grupo (máximo 2 alunos), deverá selecionar a opção Realizar em Grupo,<br>
                                indicando o nome e número do segundo aluno.
                                Caso este TFC seja realizado em Grupo o TFC deve ter um esforço expectável superior às propostas individuais.<br>
                                Para se conseguir inscrever neste TFC, deve escolher uma prioridade para o mesmo. <br>

                                <div class="form-group">
                                    <form:hidden path="numeroAluno"/>
                                    <form:hidden path="id"/>
                                    <form>
                                        <br>
                                        <label class="form-check-label" for="checkboxGrupo">Pretende fazer o TFC em Grupo?</label>
                                        <c:choose>
                                            <c:when test="${not empty inscricaoForm.nome2Aluno}">
                                                <input type="checkbox" id="checkboxGrupo" onclick="toogleGrupo()" checked><br>
                                                <div class="row">
                                                    <div class="col-4">
                                                        <form:input path="numero2Aluno" type="text"  id="numero2Aluno" placeholder="Introduza o numero do segundo aluno"  class="form-control" disabled = "false"/>
                                                    </div>
                                                    <div class="col">
                                                        <form:input path="nome2Aluno" type="text"  id="nome2Aluno" placeholder="Introduza o nome do segundo aluno"  class="form-control" disabled = "false" />
                                                    </div>
                                                </div>
                                            </c:when>
                                            <c:otherwise>
                                                <input type="checkbox" id="checkboxGrupo" onclick="toogleGrupo()"><br>
                                                <div class="row">
                                                    <div class="col-4">
                                                        <form:input path="numero2Aluno" type="text"  id="numero2Aluno" placeholder="Introduza o numero do segundo aluno"  class="form-control" disabled = "true"/>
                                                    </div>
                                                    <div class="col">
                                                        <form:input path="nome2Aluno" type="text"  id="nome2Aluno" placeholder="Introduza o nome do segundo aluno"  class="form-control" disabled = "true" />
                                                    </div>
                                                </div>
                                            </c:otherwise>
                                        </c:choose>
                                        <br>

                                        <c:if test="${inscricaoForm.id != null}">
                                            Este TFC já se encontra na sua lista de escolhas. Caso pretenda pode mudar a ordem de preferência, ou,
                                            apagar esta inscrição.
                                        </c:if>

                                        <label for="dropPrioridade">Escolha uma prioridade:</label>
                                        <form:select onchange="validar()" path="ordemEscolha"  id="dropPrioridade" name="tfc">
                                            <form:option value="0">Escolha uma opção</form:option>
                                            <form:option value="1">1ª Escolha</form:option>
                                            <form:option value="2">2ª Escolha</form:option>
                                            <form:option value="3">3ª Escolha</form:option>
                                            <form:option value="4">4ª Escolha</form:option>
                                            <form:option value="5">5ª Escolha</form:option>
                                        </form:select>
                                    </form>
                                </div>

                                <c:choose>
                                    <c:when test="${inscricaoForm.id == null}">
                                        <button type="submit" name="inscrever" id="inscrever" class="btn btn-primary" disabled>Inscrever</button><br>
                                    </c:when>
                                    <c:otherwise>
                                        <a href="<c:url value="/deleteinscricao/${inscricaoForm.id}"/>" ><button type="button" name="apagar" id="btApagar" class="btn btn-danger">Apagar</button></a>
                                        <button type="submit" name="inscrever" id="inscrever" class="btn btn-primary">Editar</button><br>
                                    </c:otherwise>
                                </c:choose>

                            </c:when>
                            <c:otherwise>
                                <c:choose>
                                    <c:when test="${inscricaoForm.id != null}">
                                        Este TFC já se encontra na sua lista de escolhas. Caso pretenda pode mudar a ordem de preferência, ou,
                                        apagar esta inscrição.

                                        <div class="form-group">
                                            <form:hidden path="numeroAluno"/>
                                            <form:hidden path="id"/>
                                            <form>
                                                <br>
                                                <label class="form-check-label" for="checkboxGrupo">Pretende fazer o TFC em Grupo?</label>
                                                <c:choose>
                                                    <c:when test="${not empty inscricaoForm.nome2Aluno}">
                                                        <input type="checkbox" id="checkboxGrupo" onclick="toogleGrupo()" checked><br>
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <form:input path="numero2Aluno" type="text"  id="numero2Aluno" placeholder="Introduza o numero do segundo aluno"  class="form-control" disabled = "false"/>
                                                            </div>
                                                            <div class="col">
                                                                <form:input path="nome2Aluno" type="text"  id="nome2Aluno" placeholder="Introduza o nome do segundo aluno"  class="form-control" disabled = "false" />
                                                            </div>
                                                        </div>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <input type="checkbox" id="checkboxGrupo" onclick="toogleGrupo()"><br>
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <form:input path="numero2Aluno" type="text"  id="numero2Aluno" placeholder="Introduza o numero do segundo aluno"  class="form-control" disabled = "true"/>
                                                            </div>
                                                            <div class="col">
                                                                <form:input path="nome2Aluno" type="text"  id="nome2Aluno" placeholder="Introduza o nome do segundo aluno"  class="form-control" disabled = "true" />
                                                            </div>
                                                        </div>
                                                    </c:otherwise>
                                                </c:choose>
                                                <br>

                                                <label for="dropPrioridade">Escolha uma prioridade:</label>
                                                <form:select onchange="validar()" path="ordemEscolha"  id="dropPrioridade" name="tfc">
                                                    <form:option value="0">Escolha uma opção</form:option>
                                                    <form:option value="1">1ª Escolha</form:option>
                                                    <form:option value="2">2ª Escolha</form:option>
                                                    <form:option value="3">3ª Escolha</form:option>
                                                    <form:option value="4">4ª Escolha</form:option>
                                                    <form:option value="5">5ª Escolha</form:option>
                                                </form:select>
                                            </form>
                                        </div>

                                        <c:choose>
                                            <c:when test="${inscricaoForm.id == null}">
                                                <button type="submit" name="inscrever" id="inscrever" class="btn btn-primary" disabled>Inscrever</button>
                                            </c:when>
                                            <c:otherwise>
                                                <a href="<c:url value="/deleteinscricao/${inscricaoForm.id}"/>" ><button type="button" name="apagar" id="btApagar" class="btn btn-danger">Apagar</button></a>
                                                <button type="submit" name="inscrever" id="inscrever" class="btn btn-primary">Editar</button><br>
                                            </c:otherwise>
                                        </c:choose>

                                    </c:when>

                                    <c:otherwise>
                                        Neste momento já se encontra inscrito em 5 TFCs.<br>
                                        Caso pretenda se inscrever neste TFC, retire uma inscrição de um outro TFC.<br>
                                        <button type="submit" name="inscrever" id="inscrever" class="btn btn-primary" disabled>Inscrever</button>
                                    </c:otherwise>
                                </c:choose>
                            </c:otherwise>
                        </c:choose>

                        <%--<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>--%>
                    </div>
                </div>
            </div>
        </form:form>
        <div class="container">
            <h1>Visualização do TFC ${tfc.idtfc}</h1>
            <div class="row">
                <div class="col-md-12">
                    <form>
                        <div class="form-group">
                            <c:if test="${not empty avaliaPropostaForm.entidade}">
                                <label path="empresa">Empresa ou Entidade Externa associada</label>
                                <input readonly id="empresa" value="${avaliaPropostaForm.entidade}"  class="form-control" />
                            </c:if>
                        </div>
                    </form>

                    <form>
                        <c:if test="${not empty avaliaPropostaForm.orientador}">
                            <c:forEach items="${orientadores}" var="orientador">
                                <c:if test="${(orientador.numeroProfessor == avaliaPropostaForm.orientador)}">
                                    <div class="form-group">
                                        <label path="orientador">Orientador</label>
                                        <input readonly id="orientador" value="${orientador.nome}"  class="form-control" />
                                    </div>
                                </c:if>
                            </c:forEach>
                        </c:if>
                    </form>

                    <form>
                        <div class="form-group">
                            <label path="titulo">Titulo</label>
                            <input  readonly label="titulo" id="tituloForm" value="${tfc.titulo}"  class="form-control"/>
                            <label path="descricao">Descricao</label>
                            <textarea readonly class="form-control" name="descricao" rows="5" id="descricaoForm">${tfc.descricao}</textarea>
                        </div>
                    </form>

                    <form>
                        <div class="form-group">
                            <label for="estado">Estado</label>
                            <input  readonly label="estado" id="estado" value="${tfc.estado}"  class="form-control"/>
                        </div>
                    </form>

                    <form>
                        <div class="form-group">
                            <c:if test="${not empty avaliaPropostaForm.cursoAssociado}">
                                <c:forEach var="curso" items="${cursos}">
                                    <c:if test="${curso.id == avaliaPropostaForm.cursoAssociado}">
                                        <label for="cursoAssociado">Curso Associado</label>
                                        <input  readonly id="cursoAssociado" value="${curso.nome}"  class="form-control"/>
                                    </c:if>
                                </c:forEach>
                            </c:if>
                        </div>
                    </form>

                    <form>
                        <div class="form-group">
                            <c:if test="${not empty avaliaPropostaForm.disciplinas}">
                                <c:choose>
                                    <c:when test="${not empty avaliaPropostaForm.disciplinas}">
                                        <table class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th>Nome da(s) Disciplina(s) Associada(s)</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <c:forEach items="${avaliaPropostaForm.disciplinas}" var="discAssociada">
                                                <c:forEach var="disciplina" items="${listadisciplinas}">
                                                    <c:if test="${discAssociada == disciplina.id}">
                                                        <tr>
                                                            <td>${disciplina.nome}</td>
                                                        </tr>
                                                    </c:if>
                                                </c:forEach>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                    </c:when>
                                </c:choose>
                            </c:if>
                        </div>
                    </form>

                    <form>
                        <div class="form-group">
                            <c:if test="${not empty avaliaPropostaForm.tecnologias}">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Nome da(s) Tecnologia(s) Associada(s)</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="tecnoAssociada" items="${avaliaPropostaForm.tecnologias}">
                                        <tr>
                                            <td>${tecnoAssociada}</td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </c:if>
                        </div>
                    </form>

                    <button name="listar" id="listar" class="btn btn-primary">Voltar à Listagem</button>
                </div>

            </div>
        </div>

        <script>
            var btnEdit = document.getElementById("listar");
            btnEdit.onclick = function (ev) {
                urlFinal = "/listatfc";
                window.location = urlFinal;
            }

            function validar() {
                {
                    var dropbox = document.getElementById("dropPrioridade");
                    var selectedValue = dropbox.options[dropbox.selectedIndex].value;
                    var botaoInscricao = document.getElementById("inscrever");
                    if (document.getElementById("dropPrioridade").value != "0"){
                        if(botaoInscricao.disabled){
                            botaoInscricao.disabled = false;
                        }
                    }else{
                        botaoInscricao.disabled = true
                    }
                }
            }

        </script>



    </tiles:putAttribute>
</tiles:insertDefinition>
