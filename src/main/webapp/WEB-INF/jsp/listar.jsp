<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">
        <h1 id="titulo">Lista de Propostas de TFCs submetidos</h1>
        <div class="container">
            <c:choose>
            <c:when test="${not empty listaPreponente}">
                <table class="table table-striped table-hover table-responsive-sm">
                    <thead>
                    <tr>
                        <th>ID TFC</th>
                        <th>Titulo TFC</th>
                        <th>Estado</th>
                        <th>Data Estado</th>
                        <th>Data da Proposta</th>

                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="tfc" items="${listaPreponente}">
                        <tr>
                            <td>${tfc.idtfc}</td>
                            <td>${tfc.titulo}</td>
                            <td>${tfc.estado}</td>
                            <td>${tfc.dataEstado}</td>
                            <td>${tfc.dataProposta}</td>
                            <c:if test="${tfc.estado == 'A Aguardar Aprovação'}">
                                <td><a href="<c:url value="/editar/${tfc.id}"/>" ><i class="far fa-edit"></i></a> </td>
                                <td><a href="<c:url value="/delete/${tfc.id}"/>" ><i class="far fa-trash-alt"></i></a></td>
                            </c:if>
                            <c:if test="${tipoUtilizador == 'ProfessorDEISI'}">
                                <td><a href="<c:url value="/editar/${tfc.id}"/>" ><i class="far fa-edit"></i></a> </td>
                                <td><a href="<c:url value="/delete/${tfc.id}"/>" ><i class="far fa-trash-alt"></i></a></td>
                            </c:if>
                            <td><a href="<c:url value="/ver/${tfc.id}"/>" ><i class="fas fa-eye"></i></a></td>
                        </tr>

                    </c:forEach>
                    </tbody>
                </table>
            </c:when>
            <c:otherwise>
                <h2 id="titulo2">Ainda não submeteu nenhum tema para TFC</h2>
            </c:otherwise>
        </c:choose>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>

