    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    <tiles:insertDefinition name="defaultTemplate">

        <tiles:putAttribute name="body">
           <div class="container conteudo">
                           <h1 id="titulo">Agenda das Apresentaçoes</h1>
                           <c:choose>
                               <c:when test="${not empty listaTfcAgenda}">
                                   <table class="table table-striped table-hover table-responsive-sm" id="tabelaListaAgenda">
                                       <thead  class="thead-dark">
                                           <tr>
                                                <th class="text-center">ID</th>
                                                <th class="text-center">Nome</th>
                                                <th class="text-center">Data</th>
                                                 <th class="text-center">Hora</th>
                                                 <th class="text-center">Sala</th>
                                                 <th class="text-center">Detalhe-tfc</th>
                                           </tr>
                                       </thead>
                                       <tbody class="p-3 mb-2 bg-light text-dark ">
                                            <c:forEach var="tfc" items="${listaTfcAgenda}">
                                                <c:forEach var="sessao" items="${listaSessao}">
                                                   <tr>
                                                        <td class="text-center">${tfc.getId()}</td>
                                                        <td class="text-center">${tfc.getTitulo()}</td>

                                                        <td class="text-center">${sessao.getData()}</td>
                                                        <td class="text-center">${sessao.getHoraInicio()}</td>
                                                        <td class="text-center">${sessao.getSala()}</td>
                                                        <td class="text-center">
                                                        <a href="<c:url value="#"/>" ><i class="fas fa-eye"></i></a>
                                                        </td>
                                                   </tr>
                                               </c:forEach>
                                           </c:forEach>
                                       </tbody>
                                   </table>
                               </c:when>
                               <c:otherwise>
                               <h2 id="titulo2">Vazio</h2>
                               </c:otherwise>
                           </c:choose>
                       </div>
        </tiles:putAttribute>
    </tiles:insertDefinition>
