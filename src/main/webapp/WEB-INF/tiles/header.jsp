<%@ page import="pt.ulusofona.deisi.gestaotfcs.data.Utilizador" %>
<%@ page import="javax.persistence.EntityManager" %>
<%@ page import="pt.ulusofona.deisi.gestaotfcs.services.MenuBuilder.Menu" %>
<%@ page import="pt.ulusofona.deisi.gestaotfcs.controller.FormController" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="pt.ulusofona.deisi.gestaotfcs.services.MenuBuilder.SubMenu" %>
<%@ page import="org.springframework.ui.ModelMap" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="header">
    <%--<div class="jumbo jumbotron jumbotron-fluid">

    </div>--%>


    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark" id="navbar">
        <a class="navbar-brand" href="<c:url value="/"/>">
            <img src="<c:url value="/static/images/logo.jpg"/>" width="50" height="50" alt="DEISI">
        </a>
        <%--Não apagar, menu hamburguer--%>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <%--Aqui começar o menu dinamico--%>
        <c:choose>
                <c:when test="${not empty menus}">

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">

                                 <c:forEach items="${menus}" var="menu">
                                     <c:choose>
                                         <c:when test="${not empty menu.submenu}">
                                             <li class="nav-item dropdown dropMenu" >
                                                 <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                         ${menu.nome}
                                                 </a>

                                                 <div class="dropdown-menu " aria-labelledby="navbarDropdown">
                                                     <c:forEach items="${menu.submenu}" var="submenu">
                                                         <a class="dropdown-item" href="<c:url value="${submenu.acao}"/>">${submenu.nome}</a>
                                                     </c:forEach>
                                                 </div>
                                             </li>
                                         </c:when>
                                         <c:otherwise>
                                             <li class="nav-item dropdown dropMenu" >
                                                 <a class="nav-link" href="<c:url value="${menu.accao}"/>" role="button">${menu.nome}</a>
                                                 <%--<a class="nav-link" href="${menu.accao}" role="button">
                                                         ${menu.nome}
                                                 </a>--%>
                                             </li>
                                         </c:otherwise>
                                     </c:choose>

                                </c:forEach>
                            </ul>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="nav-item dropdown dropMenu" id="util">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-user"></i> ${utilizador}
                                    </a>
                                    <div class="dropdown-menu " aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="<c:url value="/logout"/>">Sair</a>
                                    </div>
                                </li>
                            </ul>

                        </div>


                </c:when>
        </c:choose>


    </nav>

</div>



<c:if test="${!isPublicPage}">
<security:authorize access="!isAuthenticated()">
        <div class="container">
            <h2>Por favor realize login para poder aceder a esta página.</h2>
            <a href="/login">Página de Login</a>
        </div>
</security:authorize>
</c:if>

