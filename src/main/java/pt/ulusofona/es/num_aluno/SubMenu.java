package pt.ulusofona.es.num_aluno;

public class SubMenu {
    private String nome;
    private String acao;

    public SubMenu(String nome, String acao) {
        this.nome = nome;
        this.acao = acao;
    }

    public SubMenu(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getAcao() {
        return acao;
    }

    public void setAcao(String acao) {
        this.acao = acao;
    }
}
