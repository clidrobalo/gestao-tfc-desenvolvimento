package pt.ulusofona.es.num_aluno;

import java.util.ArrayList;
import java.util.List;

public class Menu {
    private String nome;
    private List<SubMenu> submenu = new ArrayList<>();
    private String accao;

    public Menu(String nome, List<SubMenu> submenu) {
        this.nome = nome;
        this.submenu = submenu;
    }

    public Menu(String nome) {
        this.nome = nome;
    }

    public Menu(String nome, String accao) {
        this.nome = nome;
        this.accao = accao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<SubMenu> getSubmenu() {
        return submenu;
    }

    public void setSubmenu(List<SubMenu> submenu) {
        this.submenu = submenu;
    }

    public String getAccao() {
        return accao;
    }

    public void setAccao(String accao) {
        this.accao = accao;
    }
}
