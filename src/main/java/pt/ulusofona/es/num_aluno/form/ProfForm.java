package pt.ulusofona.es.num_aluno.form;

import org.hibernate.validator.constraints.NotEmpty;
import pt.ulusofona.es.num_aluno.data.Curso;
import pt.ulusofona.es.num_aluno.data.Disciplina;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ProfForm {

    /*Apenas preenchido na edição*/
    private Long id;

    //Inforação do número de professor DEISI
    private String preponente;

    private String entidade;

    /*Informação preenchida quando se pretende editar um TFC*/
    private long entidadeTFC;
    private long cursoTFC;
    private List<Long> disciplinasTFC;
    private List<Long> tecnologiasTFC;
    /*Fim da informação da edição*/

    @NotEmpty(message = "O titulo necessita estar preenchido")
    private String titulo;

    @NotEmpty(message = "A descricao necessita estar preenchida")
    @Size(max = 250, message = "A descrição só pode ter no máximo 250 caracteres")
    private String descricao;

    @NotNull(message = "Tem que escolher pelo menos um curso")
    @DecimalMin(value = "1",  message = "Deve escolher pelo menos um curso")
    private Long cursoAssociado;

    @NotEmpty(message = "Tem que escolher pelo menos uma disciplina")
    private List<Long> disciplinas;

    @NotEmpty(message = "Necessita pelo menos preencher uma tecnologia")
    private List<String> tecnologias;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPreponente() {
        return preponente;
    }

    public void setPreponente(String preponente) {
        this.preponente = preponente;
    }

    public String getEntidade() {
        return entidade;
    }

    public void setEntidade(String entidade) {
        this.entidade = entidade;
    }

    public long getEntidadeTFC() {
        return entidadeTFC;
    }

    public void setEntidadeTFC(long entidadeTFC) {
        this.entidadeTFC = entidadeTFC;
    }

    public long getCursoTFC() {
        return cursoTFC;
    }

    public void setCursoTFC(long cursoTFC) {
        this.cursoTFC = cursoTFC;
    }

    public List<Long> getDisciplinasTFC() {
        return disciplinasTFC;
    }

    public void setDisciplinasTFC(List<Long> disciplinasTFC) {
        this.disciplinasTFC = disciplinasTFC;
    }

    public List<Long> getTecnologiasTFC() {
        return tecnologiasTFC;
    }

    public void setTecnologiasTFC(List<Long> tecnologiasTFC) {
        this.tecnologiasTFC = tecnologiasTFC;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Long getCursoAssociado() {
        return cursoAssociado;
    }

    public void setCursoAssociado(Long cursoAssociado) {
        this.cursoAssociado = cursoAssociado;
    }

    public List<Long> getDisciplinas() {
        return disciplinas;
    }

    public void setDisciplinas(List<Long> disciplinas) {
        this.disciplinas = disciplinas;
    }

    public List<String> getTecnologias() {
        return tecnologias;
    }

    public void setTecnologias(List<String> tecnologias) {
        this.tecnologias = tecnologias;
    }
}
