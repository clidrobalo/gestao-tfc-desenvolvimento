package pt.ulusofona.es.num_aluno.form;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class NdeisiForm {

    /*Apenas preenchido na edição*/
    private Long id;

    /*Inicio da informação sobre o professor e o departamento que está afeto*/
    @NotEmpty(message = "O nome do Departamento necessita estar preenchido")
    private String nomeDepartamento;

    @NotEmpty(message = "O nome do Preponente necessita estar preenchido")
    private String preponente;

    private String email;

    @NotNull(message="O telefone tem que estar preenchido")
    @Min(value = 100000000, message = "O telefone tem que ter 9 dígitos")
    @Max(value = 999999999, message = "O telefone tem que ter 9 dígitos")
    private Integer contato;

    /*Fim da informação pessoal do professor e do departamento*/

    /*Informação preenchida quando se pretende editar um TFC*/
    private long entidadeTFC;
    private long cursoTFC;
    private List<Long> disciplinasTFC;
    private List<Long> tecnologiasTFC;
    /*Fim da informação da edição*/

    /*Inicio da informação de um TFC*/
    @NotEmpty(message = "O titulo necessita estar preenchido")
    private String titulo;

    @NotEmpty(message = "A descricao necessita estar preenchida")
    @Size(max = 250, message = "A descrição só pode ter no máximo 250 caracteres")
    private String descricao;

    private String entidade;

    private Long cursoAssociado;

    private List<Long> disciplinas;

    private List<String> tecnologias;

    private String orientador;

    public NdeisiForm() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPreponente() {
        return preponente;
    }

    public void setPreponente(String preponente) {
        this.preponente = preponente;
    }

    public String getEntidade() {
        return entidade;
    }

    public void setEntidade(String entidade) {
        this.entidade = entidade;
    }

    public long getEntidadeTFC() {
        return entidadeTFC;
    }

    public void setEntidadeTFC(long entidadeTFC) {
        this.entidadeTFC = entidadeTFC;
    }

    public long getCursoTFC() {
        return cursoTFC;
    }

    public void setCursoTFC(long cursoTFC) {
        this.cursoTFC = cursoTFC;
    }

    public List<Long> getDisciplinasTFC() {
        return disciplinasTFC;
    }

    public void setDisciplinasTFC(List<Long> disciplinasTFC) {
        this.disciplinasTFC = disciplinasTFC;
    }

    public List<Long> getTecnologiasTFC() {
        return tecnologiasTFC;
    }

    public void setTecnologiasTFC(List<Long> tecnologiasTFC) {
        this.tecnologiasTFC = tecnologiasTFC;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Long getCursoAssociado() {
        return cursoAssociado;
    }

    public void setCursoAssociado(Long cursoAssociado) {
        this.cursoAssociado = cursoAssociado;
    }

    public List<Long> getDisciplinas() {
        return disciplinas;
    }

    public void setDisciplinas(List<Long> disciplinas) {
        this.disciplinas = disciplinas;
    }

    public List<String> getTecnologias() {
        return tecnologias;
    }

    public void setTecnologias(List<String> tecnologias) {
        this.tecnologias = tecnologias;
    }

    public String getOrientador() {
        return orientador;
    }

    public void setOrientador(String orientador) {
        this.orientador = orientador;
    }

    public String getNomeDepartamento() {
        return nomeDepartamento;
    }

    public void setNomeDepartamento(String nomeDepartamento) {
        this.nomeDepartamento = nomeDepartamento;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getContato() {
        return contato;
    }

    public void setContato(Integer contato) {
        this.contato = contato;
    }
}
