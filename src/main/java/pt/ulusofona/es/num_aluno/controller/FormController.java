package pt.ulusofona.es.num_aluno.controller;

import com.sun.org.apache.xpath.internal.operations.Bool;
import javassist.NotFoundException;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.NestedServletException;
import pt.ulusofona.es.num_aluno.Menu;
import pt.ulusofona.es.num_aluno.SubMenu;
import pt.ulusofona.es.num_aluno.data.*;
import pt.ulusofona.es.num_aluno.form.*;
import pt.ulusofona.es.num_aluno.services.ServiceTFC;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.rmi.CORBA.Util;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.nio.file.NotDirectoryException;
import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@Transactional
public class FormController {

    @PersistenceContext
    private EntityManager em;


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(ModelMap model, Principal user) {

        Utilizador utilizador = em.createQuery("select u from Utilizador u where u.idIdentificacao=:user",Utilizador.class)
                .setParameter("user", user.getName()).getSingleResult();

        if(utilizador.getTipoUtilizador().equalsIgnoreCase("aluno")){
            model.put("menus", constroiMenuAluno());
        }
        if(utilizador.isCoordenador()){
            model.put("menus", constroiMenuCoordenacao());
        }

        String nomeUtilizador = nomeUtilizador(user.getName());
        model.put("utilizador", nomeUtilizador);
        return "home";
    }

    /**
     * Método que funciona apenas paa testes
     * @param model
     * @param user
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String getList(ModelMap model, Principal user) {

        List<Utilizador> utilizadores = new ArrayList<>();
        utilizadores = em.createQuery("select u from Utilizador u", Utilizador.class).getResultList();
        model.put("utilizadores", utilizadores);

        List<TFC> tfcs = new ArrayList<>();
        tfcs = em.createQuery("select t from TFC t", TFC.class).getResultList();
        model.put("tfcs", tfcs);

        List<TFCDisciplina> tfcdisc = new ArrayList<>();
        tfcdisc = em.createQuery("select t from TFCDisciplina t", TFCDisciplina.class).getResultList();
        model.put("tfcdisc", tfcdisc);

        List<TFCCurso> tfcCursos = new ArrayList<>();
        tfcCursos = em.createQuery("select tc from TFCCurso tc", TFCCurso.class).getResultList();
        model.put("tfccurso", tfcCursos);

        List<TFCTecnologia> tfcTecnologias = new ArrayList<>();
        tfcTecnologias = em.createQuery("select tt from TFCTecnologia tt", TFCTecnologia.class).getResultList();
        model.put("tfctecnologias", tfcTecnologias);

        List<Tecnologia> tecnologias = new ArrayList<>();
        tecnologias = em.createQuery("select te from Tecnologia te", Tecnologia.class).getResultList();
        model.put("tecnologias",tecnologias);

        List<Empresa_EntidadeExterna> emp = new ArrayList<>();
        emp = em.createQuery("select e from Empresa_EntidadeExterna e", Empresa_EntidadeExterna.class).getResultList();
        model.put("emp",emp);
        String nomeUtilizador = nomeUtilizador(user.getName());
        model.put("menus", constroiMenuCoordenacao());
        model.put("utilizador", nomeUtilizador);
        model.put("tipoUtilizador", "professor");

        List<Grupo> grupos= new ArrayList<>();
        grupos = em.createQuery("select g from Grupo g", Grupo.class).getResultList();
        model.put("grupos", grupos);

        return "list";
    }

    @RequestMapping(value = "/listar", method = RequestMethod.GET)
    public String listarPropostas(ModelMap model, Principal user){
        List<TFC> listaTFC = new ArrayList<>();
        List<TFC> listaTFCGrupo = new ArrayList<>();
        try{
            List<Grupo> grupos = em.createQuery("select g from Grupo g", Grupo.class).getResultList();
            for (Grupo grupo: grupos) {
                if(grupo.getIdNumeroAluno1().equalsIgnoreCase(user.getName()) ||
                        grupo.getIdNumeroAluno1().equalsIgnoreCase(user.getName())){
                    listaTFCGrupo = em.createQuery("select tfc from TFC tfc where tfc.preponente=:preponente", TFC.class)
                            .setParameter("preponente", "G"+grupo.getId()).getResultList();
                }
            }
        }catch (NoResultException e){

        }
        listaTFC = em.createQuery("select tfc from TFC tfc where tfc.preponente=:preponente", TFC.class)
                .setParameter("preponente", user.getName()).getResultList();
        if (listaTFCGrupo.size()>0){
            listaTFC.addAll(listaTFCGrupo);
        }
        Utilizador utilizador = em.createQuery("select u from Utilizador u where u.idIdentificacao=:preponente",
                Utilizador.class).setParameter("preponente", user.getName()).getSingleResult();
        if(utilizador.getTipoUtilizador().equalsIgnoreCase("ProfessorDEISI") && utilizador.isCoordenador()){
            model.put("menus", constroiMenuCoordenacao());
        }
        if(utilizador.getTipoUtilizador().equalsIgnoreCase("Aluno")){
            model.put("menus", constroiMenuAluno());
        }
        model.put("listaPreponente", listaTFC);
        String nomeUtilizador = nomeUtilizador(user.getName());
        model.put("utilizador", nomeUtilizador);
        return "listar";
    }

    @RequestMapping(value = "/historico", method = RequestMethod.GET)
    public String listarHistorico(ModelMap model, Principal user){
        List<HistoricoTFC> historicoTFCS = em.createQuery("select h from HistoricoTFC h", HistoricoTFC.class)
                .getResultList();
        model.put("listahistorico", historicoTFCS);
        model.put("menus", constroiMenuCoordenacao());
        return "historico";
    }


    @RequestMapping(value = "/editar/{id}", method = RequestMethod.GET)
    public String editarProposta(ModelMap model, @PathVariable("id") long id, Principal user){

        ServiceTFC serviceTFC = new ServiceTFC();
        HashMap<String, Object> resultado = serviceTFC.vaiBuscarInfoTFC(id, em, user.getName());

        String tipoUtilizador = (String) resultado.get("tipoUtilizador");

        if(tipoUtilizador.equalsIgnoreCase("aluno")){
            model.put("menus", constroiMenuAluno());
            AlunoForm alunoForm = (AlunoForm) resultado.get("form");
            model.put("alunoForm", alunoForm);
        }
        if(tipoUtilizador.equalsIgnoreCase("coordenador")){
            model.put("menus", constroiMenuCoordenacao());
            ProfForm profForm = (ProfForm) resultado.get("form");
            model.put("profForm", profForm);
        }

        TFC tfc = (TFC) resultado.get("tfc");
        model.put("tfc", tfc);

        model.put("tipoUtilizador", tipoUtilizador);

        model.put("orientadores", getProfessores());
        model.put("utilizador", nomeUtilizador(user.getName()));
        model.put("cursos", getCursos());
        model.put("listadisciplinas", getDisciplinas());
        model.put("listatecnologias", getTecnologias());

        String destino = (String) resultado.get("destino");

        return destino;
    }

    @RequestMapping(value = "/ver/{id}", method = RequestMethod.GET)
    public String verTFC(ModelMap model, @PathVariable("id") Long id, Principal user){

        ServiceTFC serviceTFC = new ServiceTFC();
        HashMap<String, Object> resultado = serviceTFC.vaiBuscarInfoTFC(id, em, user.getName());

        String tipoUtilizador = (String) resultado.get("tipoUtilizador");

        if(tipoUtilizador.equalsIgnoreCase("aluno")){
            model.put("menus", constroiMenuAluno());
            AlunoForm alunoForm = (AlunoForm) resultado.get("form");
            model.put("form", alunoForm);
        }
        if(tipoUtilizador.equalsIgnoreCase("coordenador")){
            model.put("menus", constroiMenuCoordenacao());
            ProfForm profForm = (ProfForm) resultado.get("form");
            model.put("form", profForm);
        }

        TFC tfc = (TFC) resultado.get("tfc");
        model.put("tfc", tfc);

        model.put("tipoUtilizador", tipoUtilizador);


        model.put("orientadores", getProfessores());
        model.put("utilizador", nomeUtilizador(user.getName()));
        model.put("cursos", getCursos());
        model.put("listadisciplinas", getDisciplinas());
        model.put("listatecnologias", getTecnologias());

        return "ver";
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String delete(ModelMap model, @PathVariable("id") Long id, RedirectAttributes ra){

        TFC tfc = em.find(TFC.class, id);
        String compara = tfc.getIdtfc().replaceAll("\\d","");

        String destino = "";
        if(compara.equals("Aluno") || compara.equals("DEISI")){
            destino = "redirect:/listar";
        }else if(compara.equals("Empresa")){
            destino = "redirect:/dashboardEmpresa";
        }else{
            destino = "redirect:/dashboardNDEISI";
        }
        em.remove(tfc);

        ra.addFlashAttribute("flashInfo", "O TFC com id " + id + " foi removido.");
        return destino;
    }


    @RequestMapping(value = "/formprofessor", method = RequestMethod.GET)
    public String formprofessor(ModelMap model, Principal user){

        ProfForm profForm = new ProfForm();
       /* String nomeUtilizador = nomeUtilizador(user.getName());*/
        model.put("profForm", profForm);
        model.put("utilizador", nomeUtilizador(user.getName()));
        model.put("cursos", getCursos());
        model.put("listadisciplinas", getDisciplinas());
        model.put("listatecnologias", getTecnologias());
        model.put("menus", constroiMenuCoordenacao());
        return ("formprofessor");
    }

    @RequestMapping(value = "/formprofessor", method = RequestMethod.POST)
    public String formprofessor(@Valid @ModelAttribute("profForm") ProfForm profForm, BindingResult bindingResult,
                                ModelMap model, RedirectAttributes ra, Principal user){

        if (bindingResult.hasErrors()){
            model.put("cursos", getCursos());
            model.put("listadisciplinas", getDisciplinas());
            model.put("listatecnologias", getTecnologias());
            return "formprofessor";
        }

        HashMap<String, Object> tfc = new HashMap<>();

        if(profForm.getId() == null){
            tfc.put("idTFC", null);
        }else{
            tfc.put("idTFC", profForm.getId());
        }

        tfc.put("preponente", profForm.getPreponente());
        tfc.put("entidade", profForm.getEntidade());
        tfc.put("entidadeTFC", profForm.getEntidadeTFC());
        tfc.put("cursoTFC", profForm.getCursoTFC());
        tfc.put("disciplinasTFC", profForm.getDisciplinasTFC());
        tfc.put("tecnologiasTFC", profForm.getTecnologiasTFC());
        tfc.put("titulo", profForm.getTitulo());
        tfc.put("descricao", profForm.getDescricao());
        tfc.put("cursoAssociado", profForm.getCursoAssociado());
        tfc.put("disciplinas", profForm.getDisciplinas());
        tfc.put("tecnologias", profForm.getTecnologias());
        tfc.put("orientador", user.getName());
        tfc.put("preponente", user.getName());
        tfc.put("tipoTFC", "DEISI");
        tfc.put("id", profForm.getId());
        tfc.put("em", em);

        ServiceTFC tfcGenerico = new ServiceTFC();
        String resposta = tfcGenerico.setTfcGenerico(tfc);

        ra.addFlashAttribute("flashInfo", resposta);
        return "redirect:/listar";

    }

    @RequestMapping(value = "/formaluno", method = RequestMethod.GET)
    public String formaluno(ModelMap model, Principal user){

        AlunoForm alunoForm = new AlunoForm();
        model.put("alunoForm", alunoForm);
        model.put("utilizador", nomeUtilizador(user.getName()));
        model.put("cursos", getCursos());
        model.put("listadisciplinas", getDisciplinas());
        model.put("listatecnologias", getTecnologias());
        model.put("orientadores", getProfessores());
        model.put("menus", constroiMenuAluno());
        return ("formaluno");
    }

    @RequestMapping(value = "/formaluno", method = RequestMethod.POST)
    public String formaluno(@Valid @ModelAttribute("alunoForm") AlunoForm alunoForm, BindingResult bindingResult,
                                ModelMap model, RedirectAttributes ra, Principal user){

        if (bindingResult.hasErrors()){
            model.put("alunoForm", alunoForm);
            model.put("utilizador", nomeUtilizador(user.getName()));
            model.put("cursos", getCursos());
            model.put("listadisciplinas", getDisciplinas());
            model.put("listatecnologias", getTecnologias());
            model.put("orientadores", getProfessores());
            model.put("menus", constroiMenuAluno());
            return "formaluno";
        }

        HashMap<String, Object> tfc = new HashMap<>();

        if(alunoForm.getId() == null){
            tfc.put("idTFC", null);
        }else{
            tfc.put("idTFC", alunoForm.getId());
        }

        tfc.put("preponente", alunoForm.getPreponente());
        tfc.put("entidade", alunoForm.getEntidade());
        tfc.put("entidadeTFC", alunoForm.getEntidadeTFC());
        tfc.put("cursoTFC", alunoForm.getCursoTFC());
        tfc.put("disciplinasTFC", alunoForm.getDisciplinasTFC());
        tfc.put("tecnologiasTFC", alunoForm.getTecnologiasTFC());
        tfc.put("numero2aluno", alunoForm.getNumero2Aluno());
        tfc.put("nome2Aluno", alunoForm.getNome2Aluno());
        tfc.put("titulo", alunoForm.getTitulo());
        tfc.put("descricao", alunoForm.getDescricao());
        /*tfc.put("cursoAssociado", alunoForm.getCursoAssociado());*/
        tfc.put("disciplinas", alunoForm.getDisciplinas());
        tfc.put("tecnologias", alunoForm.getTecnologias());
        tfc.put("orientadorProposto", alunoForm.getOrientador());
        tfc.put("preponente", user.getName());
        tfc.put("tipoTFC", "Aluno");
        tfc.put("id", alunoForm.getId());
        tfc.put("em", em);

        ServiceTFC tfcGenerico = new ServiceTFC();
        String resposta = tfcGenerico.setTfcGenerico(tfc);

        ra.addFlashAttribute("flashInfo", resposta);
        return "redirect:/listar";
    }

    @RequestMapping(value = "/formNDEISI", method = RequestMethod.GET)
    public String formNDEISI(ModelMap model){
        NdeisiForm ndeisiForm = new NdeisiForm();

        model.put("ndeisiForm", ndeisiForm);
        model.put("cursos", getCursos());
        model.put("listadisciplinas", getDisciplinas());
        model.put("listatecnologias", getTecnologias());
        return ("formNDEISI");
    }

    @RequestMapping(value = "/formNDEISI", method = RequestMethod.POST)
    public String formNDEISI(@Valid @ModelAttribute("ndeisiForm") NdeisiForm ndeisiForm, BindingResult bindingResult,
                             ModelMap model, RedirectAttributes ra){

        if (bindingResult.hasErrors()){
            model.put("cursos", getCursos());
            model.put("listadisciplinas", getDisciplinas());
            model.put("listatecnologias", getTecnologias());
            model.put("orientadores", getProfessores());
            return "formNDEISI";
        }

        HashMap<String, Object> tfc = new HashMap<>();
        if(ndeisiForm.getId() == null){
            tfc.put("idTFC", null);
        }else{
            tfc.put("idTFC", ndeisiForm.getId());
        }
        tfc.put("nomeDepartamento", ndeisiForm.getNomeDepartamento());
        tfc.put("email", ndeisiForm.getEmail());
        tfc.put("contato", ndeisiForm.getContato());
        tfc.put("preponente", ndeisiForm.getPreponente());
        tfc.put("entidade", ndeisiForm.getEntidade());
        tfc.put("entidadeTFC", ndeisiForm.getEntidadeTFC());
        tfc.put("cursoTFC", ndeisiForm.getCursoTFC());
        tfc.put("disciplinasTFC", ndeisiForm.getDisciplinasTFC());
        tfc.put("tecnologiasTFC", ndeisiForm.getTecnologiasTFC());
        tfc.put("titulo", ndeisiForm.getTitulo());
        tfc.put("descricao", ndeisiForm.getDescricao());
        tfc.put("cursoAssociado", ndeisiForm.getCursoAssociado());
        tfc.put("disciplinas", ndeisiForm.getDisciplinas());
        tfc.put("tecnologias", ndeisiForm.getTecnologias());
        tfc.put("orientador", ndeisiForm.getOrientador());
        tfc.put("tipoTFC", "NDEISI");
        tfc.put("id", ndeisiForm.getId());
        tfc.put("em", em);

        ServiceTFC tfcGenerico = new ServiceTFC();
        String resposta = tfcGenerico.setTfcGenerico(tfc);

        ra.addFlashAttribute("flashInfo", resposta + " Guarde este id de forma a conseguir editar/apagar o TFC criado.");
        return "redirect:/dashboardNDEISI";
    }

    @RequestMapping(value = "/dashboardNDEISI", method = RequestMethod.GET)
    public String dashboardNDEISI(ModelMap model){
        return "dashboardNDEISI";
    }

    @RequestMapping(value = "/editNDEISI/{id}", method = RequestMethod.GET)
    public String editNDEISI(ModelMap model, @PathVariable("id") String id, RedirectAttributes ra, Principal user){

        /*
        Para se utilizar o recolher info genérico do ServiceTFC é necessário providenciar o id do TFC
        que irá ser editado. No dashboard do Professor NDEISI este irá fornecer uma String contendo o idTFC.
        Função trabalha apenas com long, log é necessário extrair o ID do tfc (campo idTFC guarda tipo (NDEISI) +
        id do tfc no momento de criação.
        Variável idTFC será o resultado final da extração do id do TFC a ser editado.
         */
        try{
            String idString = id.replaceAll("[^0-9]", "");
            Long idTFC = Long.parseLong(idString);

            TFC tfcEscolhido = em.find(TFC.class, idTFC);
            ServiceTFC serviceTFC = new ServiceTFC();
            HashMap<String, Object> resultado = serviceTFC.vaiBuscarInfoTFC(idTFC, em, tfcEscolhido.getPreponente());

            String tipoUtilizador = (String) resultado.get("tipoUtilizador");
            NdeisiForm ndeisiForm = (NdeisiForm) resultado.get("form");
            TFC tfc = (TFC) resultado.get("tfc");

            model.put("ndeisiForm", ndeisiForm);
            model.put("tfc", tfc);
            model.put("tipoUtilizador", tipoUtilizador);
            model.put("orientadores", getProfessores());
            model.put("cursos", getCursos());
            model.put("listadisciplinas", getDisciplinas());
            model.put("listatecnologias", getTecnologias());

            return "formNDEISI";
        }catch (Exception e){
            ra.addFlashAttribute("flashInfo", "Não exite um TFC com ID = " + id);
            return "redirect:/dashboardNDEISI";
        }

    }

    @RequestMapping(value = "/dashboardEmpresa", method = RequestMethod.GET)
    public String dashboardEmpresa(ModelMap model){
        return "dashboardEmpresa";
    }

    @RequestMapping(value = "/formEmpresa", method = RequestMethod.GET)
    public String formEmpresa(ModelMap model){
        EmpresaForm empresaForm = new EmpresaForm();

        model.put("empresaForm", empresaForm);
        model.put("cursos", getCursos());
        model.put("listadisciplinas", getDisciplinas());
        model.put("listatecnologias", getTecnologias());
        return ("formEmpresa");
    }

    @RequestMapping(value = "/formEmpresa", method = RequestMethod.POST)
    public String formEmpresa(@Valid @ModelAttribute("empresaForm")  EmpresaForm empresaForm, BindingResult bindingResult,
                             ModelMap model, RedirectAttributes ra){

        if (bindingResult.hasErrors()){
            model.put("cursos", getCursos());
            model.put("listadisciplinas", getDisciplinas());
            model.put("listatecnologias", getTecnologias());
            return "formEmpresa";
        }

        HashMap<String, Object> tfc = new HashMap<>();

        if(empresaForm.getId() == null){
            tfc.put("idTFC", null);
        }else{
            tfc.put("idTFC", empresaForm.getId());
        }

        tfc.put("email", empresaForm.getEmail());
        tfc.put("contato", empresaForm.getContato());
        tfc.put("interlocutor", empresaForm.getInterlocutor());
        tfc.put("entidade", empresaForm.getNomeEmpresa());
        tfc.put("preponente", empresaForm.getPreponente());
        tfc.put("entidadeTFC", empresaForm.getEntidadeTFC());
        tfc.put("cursoTFC", empresaForm.getCursoTFC());
        tfc.put("disciplinasTFC", empresaForm.getDisciplinasTFC());
        tfc.put("tecnologiasTFC", empresaForm.getTecnologiasTFC());
        tfc.put("titulo", empresaForm.getTitulo());
        tfc.put("descricao", empresaForm.getDescricao());
        tfc.put("cursoAssociado", empresaForm.getCursoAssociado());
        tfc.put("disciplinas", empresaForm.getDisciplinas());
        tfc.put("tecnologias", empresaForm.getTecnologias());
        tfc.put("orientador", empresaForm.getOrientador());
        tfc.put("tipoTFC", "Empresa");
        tfc.put("id", empresaForm.getId());
        tfc.put("em", em);

        ServiceTFC tfcGenerico = new ServiceTFC();
        String resposta = tfcGenerico.setTfcGenerico(tfc);

        ra.addFlashAttribute("flashInfo", resposta + " Guarde este id de forma a conseguir editar/apagar o TFC criado.");
        return "redirect:/dashboardEmpresa";
    }

    @RequestMapping(value = "/editEmpresa/{id}", method = RequestMethod.GET)
    public String editEmpresa(ModelMap model, @PathVariable("id") String id, RedirectAttributes ra, Principal user){

         /*
        Para se utilizar o recolher info genérico do ServiceTFC é necessário providenciar o id do TFC
        que irá ser editado. No dashboard do Professor NDEISI e Empresa este irá fornecer uma String contendo o idTFC.
        Função trabalha apenas com long, logo é necessário extrair o ID do tfc (campo idTFC guarda tipo (NDEISI) +
        id do tfc no momento de criação.
        Variável idTFC será o resultado final da extração do id do TFC a ser editado.
         */
         try{
             String idString = id.replaceAll("[^0-9]", "");
             Long idTFC = Long.parseLong(idString);

             TFC tfcEscolhido = em.find(TFC.class, idTFC);

             ServiceTFC serviceTFC = new ServiceTFC();
             HashMap<String, Object> resultado = serviceTFC.vaiBuscarInfoTFC(idTFC, em, tfcEscolhido.getPreponente());

             String tipoUtilizador = (String) resultado.get("tipoUtilizador");

             EmpresaForm empresaForm = (EmpresaForm)  resultado.get("form");

             model.put("empresaForm", empresaForm);

             TFC tfc = (TFC) resultado.get("tfc");
             model.put("tfc", tfc);

             model.put("tipoUtilizador", tipoUtilizador);

             model.put("orientadores", getProfessores());
             model.put("cursos", getCursos());
             model.put("listadisciplinas", getDisciplinas());
             model.put("listatecnologias", getTecnologias());

             return "formEmpresa";
         }catch (Exception e){
             ra.addFlashAttribute("flashInfo", "Não exite um TFC com ID = " + id);
             return "redirect:/dashboardEmpresa";
         }

    }

    /**
     * Função que permite recolher todas as tecnologias
     * @return - faz o return das Tecnologias presentes em BD
     */
    private List<Tecnologia> getTecnologias(){
        List<Tecnologia> tecnologias = em.createQuery("select t from Tecnologia t", Tecnologia.class).
                getResultList();
        return tecnologias;
    }

    /**
     * Função que permite recolher todos os cursos
     * @return - faz o return dos Cursos presentes em BD
     */
    private List<Curso> getCursos(){

        List<Curso> cursos = em.createQuery("select c from Curso c", Curso.class).getResultList();
        return cursos;
    }

    private List<ProfessorDEISI> getProfessores(){
        List<ProfessorDEISI> listaProfessores = em.createQuery("select p from ProfessorDEISI p", ProfessorDEISI.class)
                .getResultList();
        return listaProfessores;
    }

    /**
     * Função que permite recolher todas as disciplinas
     * @return - faz o return das Disciplinas presentes em BD
     */
    private List<Disciplina> getDisciplinas(){
        List<Disciplina> listadisciplinas = em.createQuery("select d from Disciplina d", Disciplina.class).getResultList();
        return listadisciplinas;
    }

    /**
     * Função que permite se contruir os Menus e Submenus de navegação que irão aparecer no NavBar
     * @return - faz o return de uma Lista de Menus para todos os users do tipo Coordenação
     */
    private List<Menu> constroiMenuCoordenacao(){
        List<Menu> menuProfessor = new ArrayList<>();
        SubMenu subMenu = new SubMenu("Adicionar Proposta", "/formprofessor");
        SubMenu subMenu2 = new SubMenu("Listar Propostas", "/listar");
        List<SubMenu> novoSubmenu = new ArrayList<>();
        novoSubmenu.add(subMenu);
        novoSubmenu.add(subMenu2);
        Menu propostas = new Menu("Propostas de TFCs", novoSubmenu);

        List<String> orientacaoSM = new ArrayList<>();
        Menu orientacao = new Menu("Orientação", "/list");

        Menu historico = new Menu("Histórico", "/historico");

        menuProfessor.add(propostas);
        menuProfessor.add(orientacao);
        menuProfessor.add(historico);
        return menuProfessor;
    }

    /**
     * Função que permite se construir os Menus e Submenus de navegação que irão aparecer no NavBar
     * @return - faz o return de uma Lista de Menus para todos os users do tipo Aluno
     */
    public static List<Menu> constroiMenuAluno(){
        List<Menu> menuAluno = new ArrayList<>();
        SubMenu subMenu = new SubMenu("Adicionar Proposta", "/formaluno");
        SubMenu subMenu2 = new SubMenu("Listar Propostas", "/listar");
        List<SubMenu> novoSubmenu = new ArrayList<>();
        novoSubmenu.add(subMenu);
        novoSubmenu.add(subMenu2);
        Menu propostas = new Menu("Propostas de TFCs", novoSubmenu);

        menuAluno.add(propostas);
        return menuAluno;
    }

    /**
     * Função que devolve o nome do user
     * @param user - recebe o id do utilizador
     * @return - devolve o nome do Utilizador
     */
    private String nomeUtilizador(String user){
        String nomeUtilizador = "";
        List<ProfessorDEISI> utilizador = em.createQuery("select p from ProfessorDEISI p", ProfessorDEISI.class).
                getResultList();
        for (ProfessorDEISI pd: utilizador) {
            if(pd.getNumeroProfessor().equalsIgnoreCase(user)){
                nomeUtilizador = pd.getNome();
                return nomeUtilizador;
            }
        }
        List<Aluno> listaAluno = em.createQuery("select  a from Aluno a", Aluno.class).getResultList();
        for (Aluno aluno: listaAluno) {
            if(aluno.getNumeroAluno().equalsIgnoreCase(user)){
                nomeUtilizador = aluno.getNome();
                return nomeUtilizador;
            }
        }
        return nomeUtilizador;
    }

}

