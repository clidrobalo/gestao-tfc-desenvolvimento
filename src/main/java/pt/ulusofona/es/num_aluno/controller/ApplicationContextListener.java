package pt.ulusofona.es.num_aluno.controller;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pt.ulusofona.es.num_aluno.data.*;
import sun.nio.ch.Util;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Component
@Transactional
public class ApplicationContextListener implements ApplicationListener<ContextRefreshedEvent> {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        List<EstadoCivil> estadoCivis = em.createQuery("select e from EstadoCivil e", EstadoCivil.class).getResultList();
        if (estadoCivis == null || estadoCivis.isEmpty()) {
            // popular a BD com os estados civis possíveis
            em.persist(new EstadoCivil(1, "Solteiro"));
            em.persist(new EstadoCivil(2, "Casado"));
            em.persist(new EstadoCivil(3, "Divorciado"));
        }

        List<Curso> cursos = em.createQuery("SELECT u FROM Curso u", Curso.class)
                .getResultList();
        if (cursos == null || cursos.isEmpty()){
            em.persist(new Curso( "Lic. Eng. Informática + Lic. Inf. Gestão"));
            em.persist(new Curso( "Lic. Eng. Informática"));
            em.persist(new Curso("Lic. Inf. Gestão"));
        }

        List<TFC> tfc = em.createQuery("SELECT t FROM TFC t", TFC.class)
                .getResultList();
        if (tfc == null || tfc.isEmpty()){
            TFC novotfc = new TFC("TF1","Lic. Eng. Informática", "p111");
            em.persist(novotfc);
            novotfc.setIdtfc("DEISI" + novotfc.getId());
            novotfc.setEstado("Aguarda Escolha");
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = new Date();
            novotfc.setDataEstado(dateFormat.format(date));
            novotfc.setDataProposta(dateFormat.format(date));
            em.persist(novotfc);
            TFC novotfc2 = new TFC("TF1","Lic. Eng. Informática", "p112");
            em.persist(novotfc2);
            novotfc2.setIdtfc("DEISI" + novotfc2.getId());
            novotfc2.setEstado("Aguarda Escolha");
            novotfc2.setDataEstado(dateFormat.format(date));
            novotfc2.setDataProposta(dateFormat.format(date));
            em.persist(novotfc);
        }

        List<Disciplina> disciplinas = em.createQuery("SELECT b FROM Disciplina b", Disciplina.class)
                .getResultList();
        if (disciplinas == null || disciplinas.isEmpty()){
            /*Conjunto de Disciplinas que pertencem aos cursos LEI e LIG*/
            em.persist(new Disciplina("Análise e Conceção de Sistemas", 1));
            em.persist(new Disciplina("Arquitetura de Computadores", 1));
            em.persist(new Disciplina("Arquitetura de Sistemas Empresariais", 1));
            em.persist(new Disciplina("Bases de Dados", 1));
            em.persist(new Disciplina( "Engenharia de Software", 1));
            em.persist(new Disciplina( "Fundamentos de Programação",1));
            em.persist(new Disciplina( "Inteligência Artificial", 1));
            em.persist(new Disciplina( "Interação Humano-Máquina",1));
            em.persist(new Disciplina( "Linguagens de Programação I", 1));
            em.persist(new Disciplina( "Linguagens de Programação II", 1));
            em.persist(new Disciplina( "Matemática I", 1));
            em.persist(new Disciplina( "Matemática II", 1));
            em.persist(new Disciplina( "Redes de Computadores", 1));
            em.persist(new Disciplina( "Sistemas de Informação Multimédia", 1));
            em.persist(new Disciplina( "Sistemas de Suporte à Decisão", 1));
            em.persist(new Disciplina( "Sistemas Operativos", 1));

            /*Conjunto de Disciplinas que pertencem ao curso LEI*/
            em.persist(new Disciplina("Álgebra Linear", 2));
            em.persist(new Disciplina("Algoritmia e Estruturas de Dados", 2));
            em.persist(new Disciplina("Arquiteturas Avançadas de Computadores", 2));
            em.persist(new Disciplina("Competências Comportamentais", 2));
            em.persist(new Disciplina("Compiladores", 2));
            em.persist(new Disciplina("Computação Distribuída", 2));
            em.persist(new Disciplina("Computação Gráfica", 2));
            em.persist(new Disciplina("Computação Móvel", 2));
            em.persist(new Disciplina("Fundamentos de Física", 2));
            em.persist(new Disciplina("Matemática Discreta", 2));
            em.persist(new Disciplina("Programação Web", 2));
            em.persist(new Disciplina("Segurança Informática", 2));
            em.persist(new Disciplina("Sinais e Sistemas", 2));
            em.persist(new Disciplina("Sistemas Digitais", 2));

            /*Conjunto de Disciplinas que pertencem ao curso LIG*/
            em.persist(new Disciplina("Auditoria de Sistemas de Informação", 3));
            em.persist(new Disciplina("Cálculo Financeiro", 3));
            em.persist(new Disciplina("Contabilidade", 3));
            em.persist(new Disciplina("Controlo de Gestão", 3));
            em.persist(new Disciplina("Data Mining", 3));
            em.persist(new Disciplina("Ética SocioProfissional", 3));
            em.persist(new Disciplina("Fundamentos de Sistemas de Informação", 3));
            em.persist(new Disciplina("Gestão Financeira", 3));
            em.persist(new Disciplina("Instrumentos de Gestão", 3));
            em.persist(new Disciplina("Investigação Operacional", 3));
            em.persist(new Disciplina("Métricas Empresariais", 3));
            em.persist(new Disciplina("Motivação e Liderança", 3));
            em.persist(new Disciplina("Sistemas Móveis Empresariais", 3));
            em.persist(new Disciplina("Teoria e Prática de Marketing", 3));


        }

        List<Tecnologia> tecnologias = em.createQuery("SELECT t FROM Tecnologia t", Tecnologia.class)
                .getResultList();
        if (tecnologias == null || tecnologias.isEmpty()){
            em.persist(new Tecnologia("Arduino"));
            em.persist(new Tecnologia("Blockchain"));
            em.persist(new Tecnologia("Bluetooth"));
            em.persist(new Tecnologia("Bootstrap"));
            em.persist(new Tecnologia("C"));
            em.persist(new Tecnologia("C#"));
            em.persist(new Tecnologia("CSS"));
            em.persist(new Tecnologia("ESP8266"));
            em.persist(new Tecnologia("Framework Symphony"));
            em.persist(new Tecnologia("HTML5"));
            em.persist(new Tecnologia("IBM BlueMix"));
            em.persist(new Tecnologia("Java"));
            em.persist(new Tecnologia("JavaScript"));
            em.persist(new Tecnologia("Joomla"));
            em.persist(new Tecnologia("JSON"));
            em.persist(new Tecnologia("Kotlin"));
            em.persist(new Tecnologia("Linux"));
            em.persist(new Tecnologia("Microsoft Azure"));
            em.persist(new Tecnologia("Mobile"));
            em.persist(new Tecnologia("MS-SQL"));
            em.persist(new Tecnologia("MySQL"));
            em.persist(new Tecnologia("NetIQ IDM"));
            em.persist(new Tecnologia("Object Oriented PHP"));
            em.persist(new Tecnologia("Open Source"));
            em.persist(new Tecnologia("OutSystems Mobile"));
            em.persist(new Tecnologia("OutSystems Web"));
            em.persist(new Tecnologia("PHP"));
            em.persist(new Tecnologia("PHPNuke"));
            em.persist(new Tecnologia("PowerDesigner"));
            em.persist(new Tecnologia("Protocolo MQTT"));
            em.persist(new Tecnologia("Python"));
            em.persist(new Tecnologia("Raspberry PI"));
            em.persist(new Tecnologia("Spring MVC"));
            em.persist(new Tecnologia("SQL"));
            em.persist(new Tecnologia("Web"));
            em.persist(new Tecnologia("WiFi"));
            em.persist(new Tecnologia("WordPress"));
        }

        List<Utilizador> utilizador = em.createQuery("SELECT u FROM Utilizador u", Utilizador.class)
                .getResultList();
        if (utilizador == null || utilizador.isEmpty()){
            Aluno novoAluno = new Aluno("a111", "Aluno de Teste", "Lic. Eng. Informática", "email@email.pt", 210);
            em.persist(novoAluno);
            Utilizador utilAluno = new Utilizador();
            utilAluno.setIdIdentificacao(novoAluno.getNumeroAluno());
            utilAluno.setTipoUtilizador("Aluno");
            em.persist(utilAluno);
            Aluno novoAluno2 = new Aluno("a112", "Aluno de Teste2", "Lic. Eng. Informática", "email@email.pt", 210);
            em.persist(novoAluno2);
            Utilizador utilAluno2 = new Utilizador();
            utilAluno2.setIdIdentificacao(novoAluno2.getNumeroAluno());
            utilAluno2.setTipoUtilizador("Aluno");
            em.persist(utilAluno2);
            Empresa_EntidadeExterna noEmpEnt = new Empresa_EntidadeExterna("Nome de Empresa");
            em.persist(noEmpEnt);
            Utilizador utilEmp = new Utilizador();
            utilEmp.setIdIdentificacao(Long.toString(noEmpEnt.getId()));
            utilEmp.setTipoUtilizador("Empresa");
            em.persist(utilEmp);
            ProfessorDEISI novoDEISI = new ProfessorDEISI("p111", "Professor de Teste", "email@professordeisi.pt", 222);
            em.persist(novoDEISI);
            Utilizador utilDEISI = new Utilizador();
            utilDEISI.setIdIdentificacao(novoDEISI.getNumeroProfessor());
            utilDEISI.setTipoUtilizador("ProfessorDEISI");
            utilDEISI.setCoordenador(true);
            em.persist(utilDEISI);
            ProfessorDEISI novoDEISI2 = new ProfessorDEISI("p112", "Professor de Teste2", "email@professordeisi.pt", 222);
            em.persist(novoDEISI2);
            Utilizador utilDEISI2 = new Utilizador();
            utilDEISI2.setIdIdentificacao(novoDEISI2.getNumeroProfessor());
            utilDEISI2.setTipoUtilizador("ProfessorDEISI");
            utilDEISI2.setCoordenador(false);
            em.persist(utilDEISI2);
            ProfessorDEISI novoDEISI3 = new ProfessorDEISI("p113", "Professor de Teste3", "email@professordeisi.pt", 222);
            em.persist(novoDEISI3);
            Utilizador utilDEISI3 = new Utilizador();
            utilDEISI3.setIdIdentificacao(novoDEISI3.getNumeroProfessor());
            utilDEISI3.setTipoUtilizador("ProfessorDEISI");
            utilDEISI3.setCoordenador(false);
            em.persist(utilDEISI3);
            ProfessorNDEISI novoNDEISI = new ProfessorNDEISI("Professor Não DEISI", "email@professorndeisi.pt", 333, "Departamento N DEISI");
            novoNDEISI.setIdProfessor(novoNDEISI.getId());
            em.persist(novoNDEISI);
            Utilizador utilNDEISI = new Utilizador();
            utilNDEISI.setIdIdentificacao(Long.toString(novoNDEISI.getNumeroProfessorNDEISI()));
            utilNDEISI.setTipoUtilizador("ProfessorNDEISI");
            em.persist(utilNDEISI);
        }




    }

}

