package pt.ulusofona.es.num_aluno.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Grupo {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private String idNumeroAluno1;

    @Column(nullable = false)
    private boolean confirmaAluno1;

    @Column(nullable = true)
    private String idNumeroAluno2;

    @Column(nullable = true)
    private boolean confirmaAluno2;

    public Grupo(String idNumeroAluno1, boolean confirmaAluno1, String idNumeroAluno2, boolean confirmaAluno2) {
        this.idNumeroAluno1 = idNumeroAluno1;
        this.confirmaAluno1 = confirmaAluno1;
        this.idNumeroAluno2 = idNumeroAluno2;
        this.confirmaAluno2 = confirmaAluno2;
    }

    public Grupo() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIdNumeroAluno1() {
        return idNumeroAluno1;
    }

    public void setIdNumeroAluno1(String idNumeroAluno1) {
        this.idNumeroAluno1 = idNumeroAluno1;
    }

    public boolean isConfirmaAluno1() {
        return confirmaAluno1;
    }

    public void setConfirmaAluno1(boolean confirmaAluno1) {
        this.confirmaAluno1 = confirmaAluno1;
    }

    public String getIdNumeroAluno2() {
        return idNumeroAluno2;
    }

    public void setIdNumeroAluno2(String idNumeroAluno2) {
        this.idNumeroAluno2 = idNumeroAluno2;
    }

    public boolean isConfirmaAluno2() {
        return confirmaAluno2;
    }

    public void setConfirmaAluno2(boolean confirmaAluno2) {
        this.confirmaAluno2 = confirmaAluno2;
    }
}
