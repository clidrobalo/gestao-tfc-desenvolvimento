package pt.ulusofona.es.num_aluno.data;

import pt.ulusofona.es.num_aluno.form.EmpresaForm;
import pt.ulusofona.es.num_aluno.form.NdeisiForm;

import javax.persistence.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Entity
public class Empresa_EntidadeExterna{

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = true)
    private String idEmpresa;

    @Column(nullable = false)
    private String nome;

    @Column(nullable = true)
    private String morada;

    @Column(nullable = true)
    private String email;

    @Column(nullable = true)
    private int numeroContato;

    @Column(nullable = true)
    private String interlocutor;

    public Empresa_EntidadeExterna() {
    }

    public Empresa_EntidadeExterna(String nome) {
        this.nome = nome;
    }

    public Empresa_EntidadeExterna(String nome, String morada, String email, int numeroContato, String interlocutor) {
        this.nome = nome;
        this.morada = morada;
        this.email = email;
        this.numeroContato = numeroContato;
        this.interlocutor = interlocutor;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getMorada() {
        return morada;
    }

    public void setMorada(String morada) {
        this.morada = morada;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getNumeroContato() {
        return numeroContato;
    }

    public void setNumeroContato(int numeroContato) {
        this.numeroContato = numeroContato;
    }

    public String getInterlocutor() {
        return interlocutor;
    }

    public void setInterlocutor(String interlocutor) {
        this.interlocutor = interlocutor;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Long idEmpresa) {
        this.idEmpresa = "emp" + idEmpresa;
    }

}


