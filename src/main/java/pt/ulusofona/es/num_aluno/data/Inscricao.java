package pt.ulusofona.es.num_aluno.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Inscricao {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private int idTFC;

    @Column(nullable = true)
    private String numeroAluno;

    @Column(nullable = true)
    private int idNumeroGrupo;

    @Column(nullable = false)
    private int ordemEscolha;

    @Column(nullable = false)
    private String estado;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getIdTFC() {
        return idTFC;
    }

    public void setIdTFC(int idTFC) {
        this.idTFC = idTFC;
    }

    public String getNumeroAluno() {
        return numeroAluno;
    }

    public void setNumeroAluno(String numeroAluno) {
        this.numeroAluno = numeroAluno;
    }

    public int getIdNumeroGrupo() {
        return idNumeroGrupo;
    }

    public void setIdNumeroGrupo(int idNumeroGrupo) {
        this.idNumeroGrupo = idNumeroGrupo;
    }

    public int getOrdemEscolha() {
        return ordemEscolha;
    }

    public void setOrdemEscolha(int ordemEscolha) {
        this.ordemEscolha = ordemEscolha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
