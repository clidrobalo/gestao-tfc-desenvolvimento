package pt.ulusofona.es.num_aluno.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by pedroalves on 16/02/17.
 * Esta tabela tem todos os estados civis possíveis e é pré-carregada para a BD na classe
 * ApplicationContextListener
 */
@Entity
public class EstadoCivil {

    @Id
    private Integer id;

    @Column(nullable = false)
    private String name;

    public EstadoCivil() {
    }

    public EstadoCivil(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
