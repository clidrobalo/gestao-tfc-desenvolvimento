package pt.ulusofona.es.num_aluno.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class TFCDisciplina {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private long numeroTFC;

    @Column(nullable = false)
    private long idNumeroDisciplina;

    public TFCDisciplina() {
    }

    public TFCDisciplina(long numeroTFC, long idNumeroDisciplina) {
        this.numeroTFC = numeroTFC;
        this.idNumeroDisciplina = idNumeroDisciplina;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getNumeroTFC() {
        return numeroTFC;
    }

    public void setNumeroTFC(long numeroTFC) {
        this.numeroTFC = numeroTFC;
    }

    public long getIdNumeroDisciplina() {
        return idNumeroDisciplina;
    }

    public void setIdNumeroDisciplina(long idNumeroDisciplina) {
        this.idNumeroDisciplina = idNumeroDisciplina;
    }
}
