package pt.ulusofona.es.num_aluno.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class EpocaAvaliacao {

    @Id
    @GeneratedValue
    private int numeroTipoAvaliacao;

    @Column(nullable = false)
    private String descricao;

    public int getNumeroTipoAvaliacao() {
        return numeroTipoAvaliacao;
    }

    public void setNumeroTipoAvaliacao(int numeroTipoAvaliacao) {
        this.numeroTipoAvaliacao = numeroTipoAvaliacao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
