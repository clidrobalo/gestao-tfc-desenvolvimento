package pt.ulusofona.es.num_aluno.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

@Entity
public class Agendamento implements Serializable {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private int numeroTFC;

    @Column(nullable = false)
    private int numeroTipoAvaliacao;

    @Column(nullable = false)
    private Date data;

    @Column(nullable = false)
    private Time hora;

    @Column(nullable = true)
    private int notaIntermedia;

    @Column(nullable = true)
    private int notaFinal;

    public long getNumeroAgendamento() {
        return id;
    }

    public void setNumeroAgendamento(int numeroAgendamento) {
        this.id = numeroAgendamento;
    }

    public int getNumeroTFC() {
        return numeroTFC;
    }

    public void setNumeroTFC(int numeroTFC) {
        this.numeroTFC = numeroTFC;
    }

    public int getNumeroTipoAvaliacao() {
        return numeroTipoAvaliacao;
    }

    public void setNumeroTipoAvaliacao(int numeroTipoAvaliacao) {
        this.numeroTipoAvaliacao = numeroTipoAvaliacao;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Time getHora() {
        return hora;
    }

    public void setHora(Time hora) {
        this.hora = hora;
    }

    public int getNotaIntermedia() {
        return notaIntermedia;
    }

    public void setNotaIntermedia(int notaIntermedia) {
        this.notaIntermedia = notaIntermedia;
    }

    public int getNotaFinal() {
        return notaFinal;
    }

    public void setNotaFinal(int notaFinal) {
        this.notaFinal = notaFinal;
    }
}
