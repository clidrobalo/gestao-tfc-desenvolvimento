package pt.ulusofona.es.num_aluno.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Entity
public class HistoricoTFC implements Serializable {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private Long idTFC;

    @Column(nullable = false)
    private String estado;

    @Column(nullable = true)
    private String dataMudancaEstado;

    @Column(nullable = false)
    private String utilizador;

    public HistoricoTFC() {
    }

    public HistoricoTFC(Long idTFC, String estado, String dataMudancaEstado, String utilizador) {
        this.idTFC = idTFC;
        this.estado = estado;
        this.dataMudancaEstado = dataMudancaEstado;
        this.utilizador = utilizador;
    }


    public long getId() {
        return id;
    }

    public Long getIdTFC() {
        return idTFC;
    }

    public void setIdTFC(Long idTFC) {
        this.idTFC = idTFC;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDataMudancaEstado() {
        return dataMudancaEstado;
    }

    public void setDataMudancaEstado(String dataMudancaEstado) {
        this.dataMudancaEstado = dataMudancaEstado;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUtilizador() {
        return utilizador;
    }

    public void setUtilizador(String utilizador) {
        this.utilizador = utilizador;
    }
}
