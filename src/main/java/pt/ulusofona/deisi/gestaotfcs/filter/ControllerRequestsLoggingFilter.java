package pt.ulusofona.deisi.gestaotfcs.filter;

import org.springframework.web.filter.AbstractRequestLoggingFilter;

import javax.servlet.http.HttpServletRequest;

public class ControllerRequestsLoggingFilter extends AbstractRequestLoggingFilter {

    public ControllerRequestsLoggingFilter() {
        setIncludeClientInfo(true);
        setAfterMessagePrefix("[REQ] ");
    }

    @Override
    protected void beforeRequest(HttpServletRequest httpServletRequest, String s) {
        // do nothing
    }

    @Override
    protected void afterRequest(HttpServletRequest httpServletRequest, String message) {
        logger.info(message);
    }

    @Override
    protected boolean shouldLog(HttpServletRequest request) {
        return logger.isInfoEnabled() &&
                !request.getRequestURI().endsWith(".css") &&
                !request.getRequestURI().endsWith(".js") &&
                !request.getRequestURI().endsWith(".png") &&
                !request.getRequestURI().endsWith(".jpg");
    }
}

