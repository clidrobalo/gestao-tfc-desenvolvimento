package pt.ulusofona.deisi.gestaotfcs.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Tecnologia {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private String nome;

    public Tecnologia(String nome) {
        this.nome = nome;
    }

    public Tecnologia() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
