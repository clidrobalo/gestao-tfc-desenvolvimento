package pt.ulusofona.deisi.gestaotfcs.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class HistoricoTFC implements Serializable {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private Long idTFCNumerico;

    @Column(nullable = false)
    private String idTFC;

    @Column(nullable = false)
    private String estado;

    @Column(nullable = true)
    private String dataMudancaEstado;

    @Column(nullable = false)
    private String utilizador;

    @Column(nullable = true)
    private String avaliacao;

    public HistoricoTFC() {
    }

    public HistoricoTFC(Long idTFCNumerico, String idTFC, String estado, String dataMudancaEstado, String utilizador) {
        this.idTFCNumerico = idTFCNumerico;
        this.idTFC = idTFC;
        this.estado = estado;
        this.dataMudancaEstado = dataMudancaEstado;
        this.utilizador = utilizador;
    }

    public HistoricoTFC(Long idTFCNumerico, String idTFC, String estado, String dataMudancaEstado, String utilizador, String avaliacao) {
        this.idTFCNumerico = idTFCNumerico;
        this.idTFC = idTFC;
        this.estado = estado;
        this.dataMudancaEstado = dataMudancaEstado;
        this.utilizador = utilizador;
        this.avaliacao = avaliacao;
    }


    public long getId() {
        return id;
    }

    public String getIdTFC() {
        return idTFC;
    }

    public void setIdTFC(String idTFC) {
        this.idTFC = idTFC;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDataMudancaEstado() {
        return dataMudancaEstado;
    }

    public void setDataMudancaEstado(String dataMudancaEstado) {
        this.dataMudancaEstado = dataMudancaEstado;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUtilizador() {
        return utilizador;
    }

    public void setUtilizador(String utilizador) {
        this.utilizador = utilizador;
    }

    public String getAvaliacao() {
        return avaliacao;
    }

    public void setAvaliacao(String avaliacao) {
        this.avaliacao = avaliacao;
    }

    public Long getIdTFCNumerico() {
        return idTFCNumerico;
    }

    public void setIdTFCNumerico(Long idTFCNumerico) {
        this.idTFCNumerico = idTFCNumerico;
    }
}
