package pt.ulusofona.deisi.gestaotfcs.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;


public class MomentoAvaliacao {

    private List<String> descricoesMomentosAvaliacoes;

    private MomentoAvaliacao() {
        init();
    }

    private static class SingletonHolder {
        private static final MomentoAvaliacao INSTANCE = new MomentoAvaliacao();
    }

    private void init() {
        descricoesMomentosAvaliacoes = new ArrayList<>();
        this.descricoesMomentosAvaliacoes.add("Intermédia");
        this.descricoesMomentosAvaliacoes.add("1º Época");
        this.descricoesMomentosAvaliacoes.add("2º Época");
        this.descricoesMomentosAvaliacoes.add("Época Especial");
    }

    public static synchronized MomentoAvaliacao getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public String getMomentoAvaliacao(int idx) {
        return this.descricoesMomentosAvaliacoes.get(idx);
    }
}


