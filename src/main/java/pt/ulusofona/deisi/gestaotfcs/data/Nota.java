package pt.ulusofona.deisi.gestaotfcs.data;

public class Nota {

    private double pertinencia;
    private double inovacao;
    private double aplicabilidade;
    private double metodologia;
    private double estruturaRelatorio;
    private double apresentacao;
    private double notaTotal;
    
    public Nota(){
        notaTotal=0;
        
    }

    public void setAplicabilidade(double aplicabilidade) {
        this.aplicabilidade = aplicabilidade;
    }

    public void setEstruturaRelatorio(double estruturaRelatorio) {
        this.estruturaRelatorio = estruturaRelatorio;
    }

    public void setInovacao(double inovacao) {
        this.inovacao = inovacao;
    }

    public void setMetodologia(double metodologia) {
        this.metodologia = metodologia;
    }

    public void setPertinencia(double pertinencia) {
        this.pertinencia = pertinencia;
    }

    public double getAplicabilidade() {
        return aplicabilidade;
    }

    public double getEstruturaRelatorio() {
        return estruturaRelatorio;
    }

    public double getInovacao() {
        return inovacao;
    }

    public double getMetodologia() {
        return metodologia;
    }

    public double getPertinencia() {
        return pertinencia;
    }

    public double getNotaTotal() {
        return notaTotal;
    }
    public void calculaNotaTotal(){
        notaTotal=(0.2*pertinencia)+(0.1*inovacao)+(0.15*aplicabilidade)+(0.15*estruturaRelatorio)+(0.1*metodologia)+(0.3*apresentacao);

    }

    public void setApresentacao(double apresentacao) {
        this.apresentacao = apresentacao;
    }

    public double getApresentacao() {
        return apresentacao;
    }
}
