package pt.ulusofona.deisi.gestaotfcs.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class TFCTecnologia {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private long idTFC;

    @Column(nullable = false)
    private long idTecnologia;

    public TFCTecnologia() {
    }

    public TFCTecnologia(long idTFC, long idTecnologia) {
        this.idTFC = idTFC;
        this.idTecnologia = idTecnologia;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdTFC() {
        return idTFC;
    }

    public void setIdTFC(long idTFC) {
        this.idTFC = idTFC;
    }

    public long getIdTecnologia() {
        return idTecnologia;
    }

    public void setIdTecnologia(long idTecnologia) {
        this.idTecnologia = idTecnologia;
    }
}
