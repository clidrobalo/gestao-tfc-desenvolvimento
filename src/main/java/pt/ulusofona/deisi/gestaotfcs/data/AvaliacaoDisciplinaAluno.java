package pt.ulusofona.deisi.gestaotfcs.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class AvaliacaoDisciplinaAluno {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private long idNumeroDisciplina;

    @Column(nullable = false)
    private long idNumeroAluno;

    @Column(nullable = false)
    private int nota;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdNumeroDisciplina() {
        return idNumeroDisciplina;
    }

    public void setIdNumeroDisciplina(long idNumeroDisciplina) {
        this.idNumeroDisciplina = idNumeroDisciplina;
    }

    public long getIdNumeroAluno() {
        return idNumeroAluno;
    }

    public void setIdNumeroAluno(long idNumeroAluno) {
        this.idNumeroAluno = idNumeroAluno;
    }

    public int getNota() {
        return nota;
    }

    public void setNota(int nota) {
        this.nota = nota;
    }
}
