package pt.ulusofona.deisi.gestaotfcs.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Sessao {

    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
    private String data;
    @Column(nullable = false)
    private String horaInicio;
    @Column(nullable = false)
    private String horaFim;
    @Column(nullable = true)
    private String sala;

    public Sessao() {

    }

    public Sessao(String data, String horaInicio, String horaFim, String sala) {
        this.data = data;
        this.horaInicio = horaInicio;
        this.horaFim = horaFim;
        this.sala = sala;
    }

    public void setId(Long id) {
        this.id =  id;
    }

    public Long getId() {
        return id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getHoraFim() {
        return horaFim;
    }

    public void setHoraFim(String horaFim) {
        this.horaFim = horaFim;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }

    public String toString()  {
        return this.getData() + " - " + this.getHoraInicio() + " - " + this.getHoraFim();
    }
}
