package pt.ulusofona.deisi.gestaotfcs.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class ItemCriterioAvaliacao {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
    private String descricao;
    @Column(nullable = true)
    private Long idCriterioAvaliacao;

    public ItemCriterioAvaliacao() {

    }

    public ItemCriterioAvaliacao(String descricao) {
        this.descricao = descricao;
    }

    public ItemCriterioAvaliacao(String descricao, Long idICriterioAvaliacao) {
        this.descricao = descricao;
        this.idCriterioAvaliacao = idICriterioAvaliacao;
    }

    public Long getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdICriterioAvaliacao() {
        return idCriterioAvaliacao;
    }

    public void setIdICriterioAvaliacao(Long idICriterioAvaliacao) {
        this.idCriterioAvaliacao = idICriterioAvaliacao;
    }
}
