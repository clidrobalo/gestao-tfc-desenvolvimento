package pt.ulusofona.deisi.gestaotfcs.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.List;

@Entity
public class ProfessorDEISI {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private String numeroProfessor;

    @Column(nullable = false)
    private String nome;

    @Column(nullable = true)
    private String email;

    @Column(nullable = true)
    private int numeroContato;

    public ProfessorDEISI(String numeroProfessor, String nome, String email, int numeroContato) {
        this.numeroProfessor = numeroProfessor;
        this.nome = nome;
        this.email = email;
        this.numeroContato = numeroContato;
    }

    public ProfessorDEISI() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNumeroProfessor() {
        return numeroProfessor;
    }

    public void setNumeroProfessor(String numeroProfessor) {
        this.numeroProfessor = numeroProfessor;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getNumeroContato() {
        return numeroContato;
    }

    public void setNumeroContato(int numeroContato) {
        this.numeroContato = numeroContato;
    }

    public boolean temSessao(List<Agendamento> agendamentos) {
        for (Agendamento a : agendamentos) {
            if(a.getNumeroProfessorDEISI().equals(this.numeroProfessor)) {
                return true;
            }
        }
        return false;
    }

    public Long getIdSessao(List<Agendamento> agendamentos){
        for (Agendamento a : agendamentos) {
            if(a.getNumeroProfessorDEISI().equals(this.numeroProfessor)) {
                return a.getIdSessao();
            }
        }
        return null;
    }
}