package pt.ulusofona.deisi.gestaotfcs.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Entity
public class NotaCriterio {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
    private Long idTFC;
    @Column(nullable = false)
    private Long idCriterio;
    @Column(nullable = false)
    private String idUser;
    @Column(nullable = true)
    private int nota;

    public NotaCriterio() {

    }

    public NotaCriterio(Long idTFC, Long idCriterio, String idUser) {
        this.idTFC = idTFC;
        this.idCriterio = idCriterio;
        this.idUser = idUser;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdTFC() {
        return idTFC;
    }

    public void setIdTFC(Long idTFC) {
        this.idTFC = idTFC;
    }

    public Long getIdCriterio() {
        return idCriterio;
    }

    public void setIdCriterio(Long idCriterio) {
        this.idCriterio = idCriterio;
    }

    public int getNota() {
        return nota;
    }

    public void setNota(int valor) {
        this.nota = valor;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String toString() {
        return getId() + ", " + getIdCriterio() + ", " + getIdTFC() + ", " + getIdUser() + ", " + getNota() + "\n";
    }

    public double roundNota(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
