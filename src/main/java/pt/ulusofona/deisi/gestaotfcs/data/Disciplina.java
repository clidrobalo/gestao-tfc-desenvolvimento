package pt.ulusofona.deisi.gestaotfcs.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Disciplina {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private String nome;

    @Column(nullable = false)
    private long cursoAssociado;

    public Disciplina(){

    }

    public Disciplina(String nome, long cursoAssociado) {
        this.nome = nome;
        this.cursoAssociado = cursoAssociado;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Long getCursoAssociado() {
        return cursoAssociado;
    }

    public void setCursoAssociado(Long cursoAssociado) {
        this.cursoAssociado = cursoAssociado;
    }
}
