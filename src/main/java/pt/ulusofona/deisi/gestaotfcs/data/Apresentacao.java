package pt.ulusofona.deisi.gestaotfcs.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Apresentacao {

    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
    private Long idTFC;
    @Column(nullable = false)
    private int qtdJuris;
    @Column(nullable = true)
    private String horaInicio;
    @Column(nullable = true)
    private String horaFim;
    @Column(nullable = true)
    private String data;
    @Column(nullable = true)
    private int qtdAvaliacoesSubmetidas;

    public Apresentacao() {

    }

    public Apresentacao(Long idTFC, int qtdJuris) {
        this.idTFC = idTFC;
        this.qtdJuris = qtdJuris;
        this.qtdAvaliacoesSubmetidas = 0;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdTFC() {
        return idTFC;
    }

    public void setIdTFC(Long idTFC) {
        this.idTFC = idTFC;
    }

    public int getQtdJuris() {
        return qtdJuris;
    }

    public void setQtdJuris(int qtdJuris) {
        this.qtdJuris = qtdJuris;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getHoraFim() {
        return horaFim;
    }

    public void setHoraFim(String horaFim) {
        this.horaFim = horaFim;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getQtdAvaliacoesSubmetidas() {
        return qtdAvaliacoesSubmetidas;
    }

    public void addSubmissao() {
        this.qtdAvaliacoesSubmetidas++;
    }
}
