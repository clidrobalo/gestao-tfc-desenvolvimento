package pt.ulusofona.deisi.gestaotfcs.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class Artefato {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private Date dataEntrega;

    @Column(nullable = false)
    private String nomeFicheiro;

    @Column(nullable = false)
    private long idTFC;

    @Column(nullable = false)
    private long idEpocaAvaliacao;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDataEntrega() {
        return dataEntrega;
    }

    public void setDataEntrega(Date dataEntrega) {
        this.dataEntrega = dataEntrega;
    }

    public String getNomeFicheiro() {
        return nomeFicheiro;
    }

    public void setNomeFicheiro(String nomeFicheiro) {
        this.nomeFicheiro = nomeFicheiro;
    }

    public long getIdTFC() {
        return idTFC;
    }

    public void setIdTFC(long idTFC) {
        this.idTFC = idTFC;
    }

    public long getIdEpocaAvaliacao() {
        return idEpocaAvaliacao;
    }

    public void setIdEpocaAvaliacao(long idEpocaAvaliacao) {
        this.idEpocaAvaliacao = idEpocaAvaliacao;
    }
}
