package pt.ulusofona.deisi.gestaotfcs.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Discrepancia {

    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
    private Long idTFC;
    @Column(nullable = false)
    private Long idCriterio;
    @Column(nullable = true)
    private int estado;
    @Column(nullable = true)
    private String descricaoEstado;

    public Discrepancia(Long idTFC, Long idCriterio) {
        this.idTFC = idTFC;
        this.idCriterio = idCriterio;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdTFC() {
        return idTFC;
    }

    public void setIdTFC(Long idTFC) {
        this.idTFC = idTFC;
    }

    public Long getIdCriterio() {
        return idCriterio;
    }

    public void setIdCriterio(Long idCriterio) {
        this.idCriterio = idCriterio;
    }

    public String getDescricaoEstadoEstado() {
        return this.descricaoEstado;
    }

    public void setEstado(int estado) {
        switch (estado) {
            case 0: this.descricaoEstado = "Não justificado";
                this.estado = estado;
                break;
            case 1: this.descricaoEstado = "Justificado";
                this.estado = estado;
                break;
            case 2: this.descricaoEstado = "Resolvido";
                this.estado = estado;
            default:
                System.out.println("Estado Invalido!");
        }
    }
}
