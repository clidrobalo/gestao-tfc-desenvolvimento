package pt.ulusofona.deisi.gestaotfcs.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class TFC implements Serializable {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = true)
    private String idtfc;

    @Column(nullable = true)
    private String Titulo;

    @Column(nullable = true, columnDefinition="TEXT")
    private String descricao;

    @Column(nullable = true)
    private Long idGrupo;

    @Column(nullable = true)
    private String preponente;

    @Column(nullable = true)
    private String orientador;

    @Column(nullable = true)
    private String orientadorProposto;

    @Column(nullable = true)
    private String coorientador;

    @Column(nullable = true)
    private String dataProposta;

    @Column(nullable = true)
    private String anoLetivo;

    @Column(nullable = true)
    private int semestre;

    @Column(nullable = true)
    private String estado;

    @Column(nullable = true)
    private String dataEstado;

    @Column(nullable = true)
    private String tecnologias;

    @Column(nullable = true)
    private Long entidade;

    @Column(nullable = true)
    private String motivoRecusa;

    @Column(nullable = true)
    private String avaliacaoProposta;

    @Column(nullable = true)
    private Double avaliacaoApresentacaoIntermedia;
    @Column(nullable = true)
    private Double avaliacaoApresentacao1Epoca;
    @Column(nullable = true)
    private Double avaliacaoApresentacao2Epoca;
    @Column(nullable = true)
    private Double avaliacaoApresentacaoEpocaEspecial;
    @Column(nullable = true)
    private Integer numeroInscricoes;

    public TFC() {
    }

    public TFC(String titulo, String descricao, String preponente) {
        this.Titulo = titulo;
        this.descricao = descricao;
        this.preponente = preponente;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return Titulo;
    }

    public void setTitulo(String titulo) {
        Titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Long getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(Long idGrupo) {
        this.idGrupo = idGrupo;
    }

    public String getPreponente() {
        return preponente;
    }

    public void setPreponente(String preponente) {
        this.preponente = preponente;
    }

    public String getOrientador() {
        return orientador;
    }

    public void setOrientador(String orientador) {
        this.orientador = orientador;
    }

    public String getDataProposta() {
        return dataProposta;
    }

    public void setDataProposta(String dataProposta) {
        this.dataProposta = dataProposta;
    }

    public String getAnoLetivo() {
        return anoLetivo;
    }

    public void setAnoLetivo(String anoLetivo) {
        this.anoLetivo = anoLetivo;
    }

    public int getSemestre() {
        return semestre;
    }

    public void setSemestre(int semestre) {
        this.semestre = semestre;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDataEstado() {
        return dataEstado;
    }

    public void setDataEstado(String dataEstado) {
        this.dataEstado = dataEstado;
    }

    public String getTecnologias() {
        return tecnologias;
    }

    public void setTecnologias(String tecnologias) {
        this.tecnologias = tecnologias;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getIdtfc() {
        return idtfc;
    }

    public void setIdtfc(String idtfc) {
        this.idtfc = idtfc;
    }

    public Long getEntidade() {
        return entidade;
    }

    public void setEntidade(Long entidade) {
        this.entidade = entidade;
    }

    public String getCoorientador() {
        return coorientador;
    }

    public void setCoorientador(String coorientador) {
        this.coorientador = coorientador;
    }

    public String getOrientadorProposto() {
        return orientadorProposto;
    }

    public void setOrientadorProposto(String orientadorProposto) {
        this.orientadorProposto = orientadorProposto;
    }

    public String getMotivoRecusa() {
        return motivoRecusa;
    }

    public void setMotivoRecusa(String motivoRecusa) {
        this.motivoRecusa = motivoRecusa;
    }

    public String getAvaliacaoProposta() {
        return avaliacaoProposta;
    }

    public void setAvaliacaoProposta(String avaliacaoProposta) {
        this.avaliacaoProposta = avaliacaoProposta;
    }

    public Integer getNumeroInscricoes() {
        return numeroInscricoes;
    }

    public void setNumeroInscricoes(Integer numeroInscricoes) {
        this.numeroInscricoes = numeroInscricoes;
    }

    public void setAvaliacaoApresentacaoIntermedia(Double setAvaliacaoApresentacaoIntermedia) {
        this.avaliacaoApresentacaoIntermedia= setAvaliacaoApresentacaoIntermedia;
    }

    public Double getAvaliacaoApresentacaoIntermedia() {
        return avaliacaoApresentacaoIntermedia;
    }

    public Double getAvaliacaoApresentacao1Epoca() {
        return avaliacaoApresentacao1Epoca;
    }

    public void setAvaliacaoApresentacao1Epoca(Double avaliacaoApresentacao1Epoca) {
        this.avaliacaoApresentacao1Epoca = avaliacaoApresentacao1Epoca;
    }

    public Double getAvaliacaoApresentacao2Epoca() {
        return avaliacaoApresentacao2Epoca;
    }

    public void setAvaliacaoApresentacao2Epoca(Double avaliacaoApresentacao2Epoca) {
        this.avaliacaoApresentacao2Epoca = avaliacaoApresentacao2Epoca;
    }

    public Double getAvaliacaoApresentacaoEpocaEspecial() {
        return avaliacaoApresentacaoEpocaEspecial;
    }

    public void setAvaliacaoApresentacaoEpocaEspecial(Double avaliacaoApresentacaoEpocaEspecial) {
        this.avaliacaoApresentacaoEpocaEspecial = avaliacaoApresentacaoEpocaEspecial;
    }
}
