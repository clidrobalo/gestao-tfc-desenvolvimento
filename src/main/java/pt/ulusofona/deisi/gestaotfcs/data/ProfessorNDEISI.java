package pt.ulusofona.deisi.gestaotfcs.data;

import pt.ulusofona.deisi.gestaotfcs.controller.FormController;
import pt.ulusofona.deisi.gestaotfcs.form.NdeisiForm;

import javax.persistence.*;

@Entity
public class ProfessorNDEISI {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = true)
    private String idProfessor;

    @Column(nullable = false)
    private String nome;

    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    private int numeroContato;

    @Column(nullable = false)
    private String departamentoAfeto;

    public ProfessorNDEISI() {
    }

    public ProfessorNDEISI(String nome, String email, int numeroContato, String departamentoAfeto) {
        this.nome = nome;
        this.email = email;
        this.numeroContato = numeroContato;
        this.departamentoAfeto = departamentoAfeto;
    }

    public long getNumeroProfessorNDEISI() {
        return id;
    }

    public void setNumeroProfessorNDEISI(int numeroProfessorNDEISI) {
        this.id = numeroProfessorNDEISI;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getNumeroContato() {
        return numeroContato;
    }

    public void setNumeroContato(int numeroContato) {
        this.numeroContato = numeroContato;
    }

    public String getDepartamentoAfeto() {
        return departamentoAfeto;
    }

    public void setDepartamentoAfeto(String departamentoAfeto) {
        this.departamentoAfeto = departamentoAfeto;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIdProfessor() {
        return idProfessor;
    }

    public void setIdProfessor(Long id) {
        this.idProfessor = "pnd" + id;
    }

}

