package pt.ulusofona.deisi.gestaotfcs.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

@Entity
public class Agendamento implements Serializable {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private Long idSessao;

    @Column(nullable = false)
    private String numeroProfessor;

    @Column(nullable = true)
    private int numeroTipoAvaliacao;


    public Agendamento() {

    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdSessao() {
        return this.idSessao;
    }

    public String getNumeroProfessorDEISI() {
        return this.numeroProfessor;
    }

    public Long getNumeroAgendamento() {
        return id;
    }

    public int getNumeroTipoAvaliacao() {
        return numeroTipoAvaliacao;
    }

    public void setNumeroTipoAvaliacao(int numeroTipoAvaliacao) {
        this.numeroTipoAvaliacao = numeroTipoAvaliacao;
    }

    public void setIdSessao(Long idSessao) {
        this.idSessao = idSessao;
    }

    public void setNumeroProfessorDEISI(String numeroProfessor) {
        this.numeroProfessor = numeroProfessor;
    }
}
