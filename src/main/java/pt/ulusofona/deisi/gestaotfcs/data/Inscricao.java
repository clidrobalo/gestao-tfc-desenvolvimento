package pt.ulusofona.deisi.gestaotfcs.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class Inscricao {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private String idTFC;

    @Column(nullable = true)
    private String numeroAluno;

    @Column(nullable = true)
    private Long idNumeroGrupo;

    @Column(nullable = true)
    private Integer ordemEscolha;

    @Column(nullable = true)
    private String estado;

    @Column(nullable = true)
    private Date registoDeInscricao;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIdTFC() {
        return idTFC;
    }

    public void setIdTFC(String idTFC) {
        this.idTFC = idTFC;
    }

    public String getNumeroAluno() {
        return numeroAluno;
    }

    public void setNumeroAluno(String numeroAluno) {
        this.numeroAluno = numeroAluno;
    }

    public Long getIdNumeroGrupo() {
        return idNumeroGrupo;
    }

    public void setIdNumeroGrupo(Long idNumeroGrupo) {
        this.idNumeroGrupo = idNumeroGrupo;
    }

    public Integer getOrdemEscolha() {
        return ordemEscolha;
    }

    public void setOrdemEscolha(Integer ordemEscolha) {
        this.ordemEscolha = ordemEscolha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getRegistoDeInscricao() {
        return registoDeInscricao;
    }

    public void setRegistoDeInscricao(Date registoDeInscricao) {
        this.registoDeInscricao = registoDeInscricao;
    }

}
