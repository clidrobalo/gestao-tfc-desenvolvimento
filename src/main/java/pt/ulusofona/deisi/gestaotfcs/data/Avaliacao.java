package pt.ulusofona.deisi.gestaotfcs.data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;


@Entity
public class Avaliacao {

    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
    private String momentoAvaliacao;
    @Column(nullable = false)
    private Long  idTfcAtribuido;
    @Column(nullable = false)
    private String idJuri;
    @Column(nullable = false)
    private int escalaAvaliacao;
    @Column(nullable = false)
    private int discrepanciaMinimo;
    @Column(nullable = true)
    private Double mediaNotaEscala;
    @Column(nullable = true)
    private Double notaIndividualJuri;
    @Column(nullable = true)
    private Double notaGlobalJuri;

    public Avaliacao() {

    }

    public Avaliacao(Long idTfcAtribuido, String idJuri, String momentoAvaliacao, int escalaAvaliacao, int discrepanciaMinimo) {
        this.idTfcAtribuido = idTfcAtribuido;
        this.idJuri = idJuri;
        this.momentoAvaliacao = momentoAvaliacao;
        this.escalaAvaliacao = escalaAvaliacao;
        this.discrepanciaMinimo = discrepanciaMinimo;
        this.notaIndividualJuri = 0.0;
        this.notaGlobalJuri = 0.0;
    }

    public Long getId(){
        return this.id;
    }


    public String getMomentoAvaliacao() {
        return momentoAvaliacao;
    }

    public void setMomentoAvaliacao(String momentoAvaliacao) {
        this.momentoAvaliacao = momentoAvaliacao;
    }

    public Long getTfcAtribuido() {
        return idTfcAtribuido;
    }

    public void setTfcAtribuido(Long idTfcAtribuido) {
        this.idTfcAtribuido = idTfcAtribuido;
    }

    public String getIdJuri() {
        return idJuri;
    }

    public void setIdJuri(String idJuri) {
        this.idJuri = idJuri;
    }

    public Double getNotaIndividualJuri() {
        return notaIndividualJuri;
    }

    public void setNotaIndividualJuri(Double notaIndividualJuri) {
        this.notaIndividualJuri = notaIndividualJuri;
    }

    public Double getNotaGlobalJuri() {
        return notaGlobalJuri;
    }

    public void setNotaGlobalJuri(Double notaGlobalJuri) {
        this.notaGlobalJuri = notaGlobalJuri;
    }

    public int getEscalaAvaliacao() {
        return escalaAvaliacao;
    }

    public void setEscalaAvaliacao(int escalaAvaliacao) {
        this.escalaAvaliacao = escalaAvaliacao;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdTfcAtribuido() {
        return idTfcAtribuido;
    }

    public void setIdTfcAtribuido(Long idTfcAtribuido) {
        this.idTfcAtribuido = idTfcAtribuido;
    }

    public int getDiscrepanciaMinimo() {
        return discrepanciaMinimo;
    }

    public void setDiscrepanciaMinimo(int discrepanciaMinimo) {
        this.discrepanciaMinimo = discrepanciaMinimo;
    }

    public Double getMediaNotaEscala() {
        return mediaNotaEscala;
    }

    public void setMediaNotaEscala(Double mediaNotaEscala) {
        this.mediaNotaEscala = mediaNotaEscala;
    }
}
