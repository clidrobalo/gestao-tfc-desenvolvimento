package pt.ulusofona.deisi.gestaotfcs.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class Utilizador implements Serializable {

    @Id
    @GeneratedValue
    private long id;

    /*@Column(nullable = true)
    private long idProfessorDEISI;*/

    @Column(nullable = true)
    private String idIdentificacao;

   /* @Column(nullable = true)
    private long idProfessorNDEISI;

    @Column(nullable = true)
    private long idEmpresaEntidadeExterna;

    @Column(nullable = true)
    private long idAluno;*/

    @Column(nullable = true)
    private boolean coordenador = false;

    @Column(nullable = true)
    private String tipoUtilizador;

    public Utilizador() {
    }

    public Utilizador(String idIdentificacao, boolean coordenador, String tipoUtilizador) {
        this.idIdentificacao = idIdentificacao;
        this.coordenador = coordenador;
        this.tipoUtilizador = tipoUtilizador;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIdIdentificacao() {
        return idIdentificacao;
    }

    public void setIdIdentificacao(String idIdentificacao) {
        this.idIdentificacao = idIdentificacao;
    }

    public String getTipoUtilizador() {
        return tipoUtilizador;
    }

    public void setTipoUtilizador(String tipoUtilizador) {
        this.tipoUtilizador = tipoUtilizador;
    }

    public boolean isCoordenador() {
        return coordenador;
    }

    public void setCoordenador(boolean coordenador) {
        this.coordenador = coordenador;
    }
}


