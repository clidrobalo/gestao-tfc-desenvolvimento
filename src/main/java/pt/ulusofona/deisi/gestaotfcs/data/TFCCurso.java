package pt.ulusofona.deisi.gestaotfcs.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class TFCCurso implements Serializable {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private long idTFC;

    @Column(nullable = false)
    private Long idCurso;

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getIdTFC() {
        return idTFC;
    }

    public void setIdTFC(long idTFC) {
        this.idTFC = idTFC;
    }

    public Long getIdCurso() {
        return idCurso;
    }

    public void setIdCurso(Long idCurso) {
        this.idCurso = idCurso;
    }
}
