package pt.ulusofona.deisi.gestaotfcs.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.List;

@Entity
public class CriterioAvaliacao {

    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
    private String descricao;
    @Column(nullable = false)
    private Double peso;
    @Column(nullable = true)
    private String comentario;
    @Column(nullable = false)
    private int numeroMomentoAvaliacao;

    public CriterioAvaliacao () {

    }

    public CriterioAvaliacao(String descricao, Double peso, int numeroMomentoAvaliacao) {
        this.descricao = descricao;
        this.peso = peso;
        this.setComentario("");
        this.numeroMomentoAvaliacao = numeroMomentoAvaliacao;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getPeso() {
        return peso;
    }

    public void setPeso(Double peso) {
        this.peso = peso;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public int getNumeroMomentoAvaliacao() {
        return numeroMomentoAvaliacao;
    }

    public void setNumeroMomentoAvaliacao(int numeroMomentoAvaliacao) {
        this.numeroMomentoAvaliacao = numeroMomentoAvaliacao;
    }

    public int getNotaCriterio(List<NotaCriterio> notaCriterioList) {
        for (NotaCriterio notaCriterio : notaCriterioList) {
            if(notaCriterio.getIdCriterio() == getId()) {
                System.out.println("Nota Criterio: " + notaCriterio.getNota());
                return notaCriterio.getNota();
            }
        }
        return -1;
    }
}
