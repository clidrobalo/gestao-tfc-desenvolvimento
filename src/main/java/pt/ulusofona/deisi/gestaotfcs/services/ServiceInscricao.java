package pt.ulusofona.deisi.gestaotfcs.services;

import pt.ulusofona.deisi.gestaotfcs.data.Grupo;
import pt.ulusofona.deisi.gestaotfcs.data.Inscricao;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class ServiceInscricao {

    private static final String atribuido = "Atribuido";
    private static final String naoAtribuido = "Não Atribuido";

    /**
     * Função que irá recolher à BD todos os TFCs em que o aluno se tenha inscrito
     * @param em
     * @param user
     * @return
     */
    public List<Inscricao> recolherInscricaoAluno(EntityManager em, String user){
        List<Inscricao> listaInscricoesAluno = new ArrayList<>();

        long idGrupo = 0;
        List<Grupo> listGrupos = em.createQuery("select gr from Grupo gr", Grupo.class).getResultList();
        for (Grupo grupo: listGrupos) {
            if(grupo.getIdNumeroAluno1().equalsIgnoreCase(user) || grupo.getIdNumeroAluno2().equalsIgnoreCase(user)){
                idGrupo = grupo.getId();
            }
        }

        List<Inscricao> listaInscricoes = em.createQuery("select ins from Inscricao ins", Inscricao.class).getResultList();
        for (Inscricao listInsc: listaInscricoes) {
            if(listInsc.getNumeroAluno() != null){
                if(listInsc.getNumeroAluno().equalsIgnoreCase(user)){
                    listaInscricoesAluno.add(listInsc);
                }
            }else{
                if(listInsc.getIdNumeroGrupo() != null){
                    ServiceInformacao serviceInformacao = new ServiceInformacao();
                    Grupo grupo = serviceInformacao.informacaoGrupo(em, listInsc.getIdNumeroGrupo());
                    if(grupo != null){
                        if(user.equalsIgnoreCase(grupo.getIdNumeroAluno1()) ||
                                user.equalsIgnoreCase(grupo.getIdNumeroAluno2())){
                            listaInscricoesAluno.add(listInsc);
                        }
                    }
                }
            }
        }

        return listaInscricoesAluno;
    }

    /**
     * Função que grava uma inscrição num TFC por parte do Aluno.
     * Esta inscrição não é oficializada. Aluno necessita registar a mesma.
     * @param em
     * @param tfcInscricao
     */
    public boolean gravaInscricao(EntityManager em, HashMap<String, Object> tfcInscricao){
        String idTFC = (String) tfcInscricao.get("idTFC");
        Long numeroGrupo = (Long) tfcInscricao.get("idNumeroGrupo");
        String numeroAluno = (String) tfcInscricao.get("numeroAluno");
        int ordemEscolha = (Integer) tfcInscricao.get("ordemEscolha");
        Long idInscricao = (Long) tfcInscricao.get("idInscricao");
        String estado = (String) tfcInscricao.get("estado");

        List<Inscricao> inscricaoList = em.createQuery("select ins from Inscricao ins where ins.numeroAluno=:numeroAluno or" +
                " ins.idNumeroGrupo=:idNumeroGrupo",Inscricao.class).setParameter("numeroAluno", numeroAluno).
                setParameter("idNumeroGrupo", numeroGrupo).getResultList();

        for(Inscricao inscricao: inscricaoList){
            if(inscricao.getOrdemEscolha() == ordemEscolha &&
                    !inscricao.getIdTFC().equalsIgnoreCase(idTFC)){
                return false;
            }
        }

        List<Grupo> grupos = em.createQuery("select g from Grupo g where g.idNumeroAluno1=:numeroAluno1 or" +
                " g.idNumeroAluno2=:numeroAluno2", Grupo.class).setParameter("numeroAluno1", numeroAluno).
                setParameter("numeroAluno2", numeroAluno).getResultList();

        for(Grupo grupo: grupos){
            List<Inscricao> inscricaoListGrupo = em.createQuery("select ins from Inscricao ins where ins.idNumeroGrupo=:nGrupo",
                    Inscricao.class).setParameter("nGrupo", grupo.getId()).getResultList();
            for(Inscricao inscricao:inscricaoListGrupo){
                if(inscricao.getOrdemEscolha() == ordemEscolha &&
                        !inscricao.getIdTFC().equalsIgnoreCase(idTFC)){
                    return false;
                }
            }
        }

        Inscricao inscricao = new Inscricao();

        if(idInscricao != null){
            inscricao = em.find(Inscricao.class, idInscricao);
        }else{
            inscricao = new Inscricao();
        }

        if(estado != null){
            inscricao.setEstado(estado);
        }
        inscricao.setIdNumeroGrupo(null);
        inscricao.setNumeroAluno(null);
        inscricao.setIdTFC(idTFC);
        inscricao.setOrdemEscolha(ordemEscolha);
        if(numeroGrupo != null){
            inscricao.setIdNumeroGrupo(numeroGrupo);
        }else{
            inscricao.setNumeroAluno(numeroAluno);
        }

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        inscricao.setRegistoDeInscricao(date);

        em.persist(inscricao);
        return true;

    }

    public void apagaInscricao(EntityManager em, Long id){
        Inscricao inscricao = em.find(Inscricao.class, id);
        em.remove(inscricao);
    }

    /**Função já não usada (substituida pelo registo em apenas um passo)
     * Função que irá oficializar o registo das Inscrições dos Alunos.
     * Caso o aluno tenha colocado dois ou mais TFCs com a mesma ordem de prioridade, o registo irá falhar
     * e será retornado false. Caso o registo ocorra de forma correta, é registo em BD a nova inscrição e retornado
     * true.
     * @param em
     * @param idUser
     * @return - resultado da operação de registo das inscrições
     */
    public boolean registoInscricao(EntityManager em, String idUser){
        List<Inscricao> inscricoesAluno = recolherInscricaoAluno(em, idUser);
        boolean tudoCorreto = true;
        for(int i= 0; i<inscricoesAluno.size(); i++){
            for(int j = i+1; j<inscricoesAluno.size(); j++){
                if(inscricoesAluno.get(i).getOrdemEscolha() ==
                        inscricoesAluno.get(j).getOrdemEscolha()){
                    tudoCorreto = false;
                    return tudoCorreto;
                }
            }
        }
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        for(Inscricao inscricao: inscricoesAluno){
            inscricao.setRegistoDeInscricao(date);
            em.persist(inscricao);
        }
        return tudoCorreto;
    }

    public List<Inscricao> recolheInscricoes(EntityManager em){
        return em.createQuery("select ins from Inscricao ins", Inscricao.class).getResultList();
    }

    public List<Inscricao> recolheInscricaoTFC(EntityManager em, String idTFC){
        List<Inscricao> listaInscricaoTFC = new ArrayList<>();

        listaInscricaoTFC = em.createQuery("select ins from Inscricao ins where ins.idTFC=:idTFC",
                Inscricao.class).setParameter("idTFC", idTFC).getResultList();

        List<Inscricao> listaARemover = new ArrayList<>();

        for(Inscricao inscricao: listaInscricaoTFC){
            if(inscricao.getNumeroAluno()!=null){
                List<Inscricao> listaInscricoesAluno = em.createQuery("select ins from Inscricao ins where ins.numeroAluno=:numeroAluno",
                        Inscricao.class).setParameter("numeroAluno", inscricao.getNumeroAluno()).getResultList();
                for(Inscricao elementoInscricao: listaInscricoesAluno){
                    if(elementoInscricao.getId()==inscricao.getId()) {
                        if(elementoInscricao.getEstado()!= null){
                            if (elementoInscricao.getEstado().equalsIgnoreCase(naoAtribuido)){
                                listaARemover.add(inscricao);
                            }
                        }
                    }
                }

            }
        }

        listaInscricaoTFC.removeAll(listaARemover);

        return listaInscricaoTFC;
    }

    public List<Long> devolveIDInscricoesTFC(List<Inscricao> inscricaoList){

        List<Long> idsInscricoes = new ArrayList<>();
        for(Inscricao inscricao: inscricaoList){
            idsInscricoes.add(inscricao.getId());
        }

        return idsInscricoes;
    }

    public void registaAtribuicao(EntityManager em, long idInscricao, String coordenador){
        Inscricao inscricao = em.find(Inscricao.class, idInscricao);
        inscricao.setEstado(atribuido);
        ServiceTFC serviceTFC = new ServiceTFC();
        serviceTFC.atribuiTFC(em, inscricao.getIdTFC(), coordenador);
        em.persist(inscricao);

        /*Colocação de todas as outras inscrições como Não Atribuidas*/
        ServiceInformacao serviceInformacao = new ServiceInformacao();
        List<Long> idsGrupos = new ArrayList<>();
        /*Lista que irá guardar todos os grupos onde aluno presente, caso inscrição pertença a um aluno*/
        List<Grupo> grupos = new ArrayList<>();
        /*Variável que irá guardar a informação, caso a inscrição pertença a uma grupo.*/
        Grupo grupo = new Grupo();
        if(inscricao.getNumeroAluno()!=null){
            grupos = serviceInformacao.devolveGruposOndeAlunoPresente(em, inscricao.getNumeroAluno());
        }else{
            grupo = serviceInformacao.informacaoGrupo(em, inscricao.getIdNumeroGrupo());
        }

        if(grupos!=null){
            for(Grupo gr: grupos){
                idsGrupos.add(gr.getId());
            }
        }

        /*Inicio da Situação que a inscricao pertence a um aluno individual, e vai se colocar todas as suas inscrições
        * individuais (exceto a que foi atribuida) como Não Atribuida*/
        if(inscricao.getNumeroAluno()!= null){
            String numeroAluno = inscricao.getNumeroAluno();
            List<Inscricao> listaAlunoInscrito = em.createQuery("select ins from Inscricao ins where ins.numeroAluno=:numeroAluno",
                    Inscricao.class).setParameter("numeroAluno", numeroAluno).getResultList();
            for(Inscricao ins: listaAlunoInscrito){
                if(ins.getId()!= inscricao.getId()){
                    ins.setEstado(naoAtribuido);
                    em.persist(ins);
                }
            }

            /*Aluno presente num grupo, necessário verificar se esse grupo realizou uma inscrição, caso tenha feito necessário
            * colocar essa inscrição como Não Atribuida*/
            if(!idsGrupos.isEmpty()){
                for(Long idGrupo: idsGrupos){
                    try{
                        Inscricao inscricaoAlunoGrupo = em.createQuery("select ins from Inscricao ins where ins.idNumeroGrupo=:idGrupo",
                                Inscricao.class).setParameter("idGrupo", idGrupo).getSingleResult();
                        if(inscricaoAlunoGrupo.getId()!=inscricao.getId()){
                            inscricaoAlunoGrupo.setEstado(naoAtribuido);
                            em.persist(inscricaoAlunoGrupo);
                        }
                    }catch (NoResultException ignored){

                    }

                }
            }
        }else{
            /*Inscrição pertence a um grupo, necessário colocar as inscrições dos alunos do grupo a Não Atribuidos*/
            List<Inscricao> listaAluno1Inscrito = em.createQuery("select ins from Inscricao ins where ins.numeroAluno=:numeroAluno1",
                    Inscricao.class).setParameter("numeroAluno1", grupo.getIdNumeroAluno1()).getResultList();
            List<Inscricao> listaAluno2Inscrito = em.createQuery("select ins from Inscricao ins where ins.numeroAluno=:numeroAluno2",
                    Inscricao.class).setParameter("numeroAluno2", grupo.getIdNumeroAluno2()).getResultList();

            alteraEstadoDeAlunoGrupo(em, inscricao, listaAluno1Inscrito);

            alteraEstadoDeAlunoGrupo(em, inscricao, listaAluno2Inscrito);
        }


    }

    private void alteraEstadoDeAlunoGrupo(EntityManager em, Inscricao inscricao, List<Inscricao> listaAlunoInscrito) {
        if(listaAlunoInscrito!=null){
            for(Inscricao ins: listaAlunoInscrito){
                if(ins.getId() != inscricao.getId()){
                    ins.setEstado(naoAtribuido);
                    em.persist(ins);
                }
            }
        }
    }


}
