package pt.ulusofona.deisi.gestaotfcs.services;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pt.ulusofona.deisi.gestaotfcs.data.*;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe que irá servir para recolher informação que irá permitir realizar todas as tarefas.
 * Por exemplo conjunto de disciplinas, cursos, etc....
 */

@Service
@Transactional
public class ServiceInformacao {

    private final Log logger = LogFactory.getLog(this.getClass());

    private static final String atribuido = "Atribuido";

    public ServiceInformacao() {
    }

    public Long recolheNumeroGrupo( String numero1Aluno,String numero2Aluno,
                                   EntityManager em){

        long numeroGrupo = 0;
        List<Grupo> grupos = em.createQuery("select g from Grupo g", Grupo.class).getResultList();

        for(Grupo grupo: grupos){
            if((grupo.getIdNumeroAluno1().equalsIgnoreCase(numero1Aluno) ||
                    grupo.getIdNumeroAluno2().equalsIgnoreCase(numero1Aluno)) &&
                    (grupo.getIdNumeroAluno1().equalsIgnoreCase(numero2Aluno) ||
                            grupo.getIdNumeroAluno2().equalsIgnoreCase(numero2Aluno))){
                numeroGrupo = grupo.getId();
                break;
            }
        }
        if(numeroGrupo == 0){
            Grupo novoGrupo = new Grupo(numero1Aluno, true, numero2Aluno, false);
            em.persist(novoGrupo);
            numeroGrupo = novoGrupo.getId();
        }

        return numeroGrupo;
    }

    public Grupo informacaoGrupo(EntityManager em, Long idGrupo){
        Grupo grupo = em.find(Grupo.class, idGrupo);
        return grupo;
    }

    public Aluno informacaoAluno(EntityManager em, String idUtilizador) {

        logger.info("informacaoAluno(): " + idUtilizador);
        Utilizador utilizador = em.createQuery("select u from Utilizador u where u.idIdentificacao=:idUtilizador",
                Utilizador.class).setParameter("idUtilizador", idUtilizador).getSingleResult();
        Aluno aluno = em.createQuery("select a from Aluno a where a.numeroAluno=:id",
                Aluno.class).setParameter("id", utilizador.getIdIdentificacao()).getSingleResult();
        return aluno;
    }

    public List<Long> recolheListaDisciplinasTFC(EntityManager em, String idTFC){
        List<Long> listaDisciplinasTFC = new ArrayList<>();
        TFC tfc = em.createQuery("select tfc from  TFC tfc where tfc.idtfc=:idTFC",
                TFC.class).setParameter("idTFC", idTFC).getSingleResult();

        List<TFCDisciplina> tfcDisciplinaList = em.createQuery("select tfcdis from TFCDisciplina tfcdis where tfcdis.numeroTFC=:idTFC",
                TFCDisciplina.class).setParameter("idTFC", tfc.getId()).getResultList();
        for(TFCDisciplina tfcDisciplina: tfcDisciplinaList){
            listaDisciplinasTFC.add(tfcDisciplina.getId());
        }
        return listaDisciplinasTFC;
    }

    public List<Disciplina> getDisciplinas(EntityManager em){
        List<Disciplina> listadisciplinas = em.createQuery("select d from Disciplina d", Disciplina.class).getResultList();
        return listadisciplinas;
    }

    public List<Grupo> devolveGruposOndeAlunoPresente(EntityManager em, String numeroAluno){
        List<Grupo> grupos = em.createQuery("select grupo from Grupo grupo where grupo.idNumeroAluno1=:numeroAluno or" +
        " grupo.idNumeroAluno2=:numeroAluno", Grupo.class).setParameter("numeroAluno", numeroAluno).getResultList();
        return grupos;
    }

    public Utilizador devolveUtilizador(EntityManager em, String id){
        Utilizador utilizador = em.createQuery("select u from Utilizador u where u.idIdentificacao=:id",
                Utilizador.class).setParameter("id", id).getSingleResult();
        return utilizador;
    }

    public void criaUtilizadorAluno(EntityManager em, String nome, String numero){
        try{
            Utilizador utilizador = em.createQuery("select u from Utilizador u where u.idIdentificacao=:id",
                    Utilizador.class).setParameter("id", numero).getSingleResult();
        }catch (NoResultException e){
            Aluno novoReal = new Aluno(numero, nome, null, null, 0);
            em.persist(novoReal);
            Utilizador utilAlunoReal = new Utilizador();
            utilAlunoReal.setIdIdentificacao(novoReal.getNumeroAluno());
            utilAlunoReal.setTipoUtilizador("Aluno");
            em.persist(utilAlunoReal);
        }
    }

    public void criaUtilizadorProfessor(EntityManager em, String nome, String numero){
        try{
            Utilizador utilizador = em.createQuery("select u from Utilizador u where u.idIdentificacao=:id",
                    Utilizador.class).setParameter("id", numero).getSingleResult();
        }catch (NoResultException e){
            ProfessorDEISI novoReal = new ProfessorDEISI(numero, nome, null, 0);
            em.persist(novoReal);
            Utilizador utilReal = new Utilizador();
            utilReal.setIdIdentificacao(novoReal.getNumeroProfessor());
            utilReal.setTipoUtilizador("ProfessorDEISI");
            em.persist(utilReal);
        }
    }

    public Inscricao vaiBuscarNumeroAlunoGrupo(EntityManager em, String idTFC){

        Inscricao inscricao = em.createQuery("select ins from Inscricao ins where ins.idTFC=:idTFC and" +
        " ins.estado=:atribuido", Inscricao.class).setParameter("idTFC", idTFC).setParameter("atribuido", atribuido)
                .getSingleResult();
        return inscricao;
    }

    public Grupo devolveGrupo(EntityManager em, Long idGrupo){
        Grupo grupo = em.createQuery("select g from Grupo g where g.id=:idGrupo",
                Grupo.class).setParameter("idGrupo", idGrupo).getSingleResult();
        return grupo;
    }
}
