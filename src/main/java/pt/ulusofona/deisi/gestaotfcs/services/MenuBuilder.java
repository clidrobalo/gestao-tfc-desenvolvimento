package pt.ulusofona.deisi.gestaotfcs.services;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MenuBuilder {

    public static class Menu {
        private String nome;
        private List<SubMenu> submenu = new ArrayList<>();
        private String accao;

        public Menu(String nome, List<SubMenu> submenu) {
            this.nome = nome;
            this.submenu = submenu;
        }

        public Menu(String nome) {
            this.nome = nome;
        }

        public Menu(String nome, String accao) {
            this.nome = nome;
            this.accao = accao;
        }

        public String getNome() {
            return nome;
        }

        public void setNome(String nome) {
            this.nome = nome;
        }

        public List<SubMenu> getSubmenu() {
            return submenu;
        }

        public void setSubmenu(List<SubMenu> submenu) {
            this.submenu = submenu;
        }

        public String getAccao() {
            return accao;
        }

        public void setAccao(String accao) {
            this.accao = accao;
        }
    }

    public static class SubMenu {
        private String nome;
        private String acao;

        public SubMenu(String nome, String acao) {
            this.nome = nome;
            this.acao = acao;
        }

        public SubMenu(String nome) {
            this.nome = nome;
        }

        public String getNome() {
            return nome;
        }

        public void setNome(String nome) {
            this.nome = nome;
        }

        public String getAcao() {
            return acao;
        }

        public void setAcao(String acao) {
            this.acao = acao;
        }
    }

    /**
     * Função que permite se contruir os Menus e Submenus de navegação que irão aparecer no NavBar
     *
     * @return - faz o return de uma Lista de Menus para todos os users do tipo Coordenação
     */
    public List<Menu> constroiMenuCoordenacao() {
        List<Menu> menuCoordenacao = new ArrayList<>();

        SubMenu subMenu = new SubMenu("Adicionar Proposta", "/formprofessor");
        SubMenu subMenu2 = new SubMenu("Listar Propostas", "/listar");
        List<SubMenu> novoSubmenu = new ArrayList<>();
        novoSubmenu.add(subMenu);
        novoSubmenu.add(subMenu2);
        Menu propostas = new Menu("Propostas de TFCs", novoSubmenu);

        /*List<String> orientacaoSM = new ArrayList<>();
        Menu orientacao = new Menu("Orientação", "");*/

        SubMenu subMenuCoordenacao1 = new SubMenu("Listar Propostas a Avaliar", "/listarpropostas");
        SubMenu subMenuCoordenacao2 = new SubMenu("Atribuir TFCs", "/listarTFCInsc");

        //Add by Clid
        SubMenu agendarApresentacao = new SubMenu("Agendar Apresentação", "/agendarApresentacao");
        SubMenu atribuirJuri = new SubMenu("Atribuir Júri", "/atribuirJuri");
        SubMenu listaAvaliacoes = new SubMenu("Lista Avaliacões", "/listaAvaliacoes");
        //--------------

        List<SubMenu> coordenacaoSM = new ArrayList<>();
        coordenacaoSM.add(subMenuCoordenacao1);
        coordenacaoSM.add(subMenuCoordenacao2);
        coordenacaoSM.add(agendarApresentacao);
        coordenacaoSM.add(atribuirJuri);
        coordenacaoSM.add(listaAvaliacoes);
        Menu coordenacao = new Menu("Coordenação", coordenacaoSM);

        Menu historico = new Menu("Histórico", "/historico");

        menuCoordenacao.add(propostas);
        menuCoordenacao.add(coordenacao);
        menuCoordenacao.add(historico);
        return menuCoordenacao;
    }

    /**
     * Função que permite se contruir os Menus e Submenus de navegação que irão aparecer no NavBar
     *
     * @return - faz o return de uma Lista de Menus para todos os users do tipo Coordenação
     */
    public List<Menu> constroiMenuProfessor() {
        List<Menu> menuProfessor = new ArrayList<>();

        SubMenu subMenu = new SubMenu("Adicionar Proposta", "/formprofessor");
        SubMenu subMenu2 = new SubMenu("Listar Propostas", "/listar");
        List<SubMenu> novoSubmenu = new ArrayList<>();
        novoSubmenu.add(subMenu);
        novoSubmenu.add(subMenu2);
        Menu propostas = new Menu("Propostas de TFCs", novoSubmenu);

        menuProfessor.add(propostas);

        //--- Add by clid
        Menu disponibilidade = new Menu("Agenda apresentações", "/agendaApresentacaoJuri");
        Menu avaliarDiscente = new Menu("Avaliar discente(s)", "/listaTfcsParaAvaliar");
        menuProfessor.add(disponibilidade);
        menuProfessor.add(avaliarDiscente);
        //--- End Add

        return menuProfessor;
    }

    /**
     * Função que permite se construir os Menus e Submenus de navegação que irão aparecer no NavBar
     *
     * @return - faz o return de uma Lista de Menus para todos os users do tipo Aluno
     */
    public List<Menu> constroiMenuAluno() {
        List<Menu> menuAluno = new ArrayList<>();
        SubMenu subMenu = new SubMenu("Adicionar Proposta", "/formaluno");
        SubMenu subMenu2 = new SubMenu("Listar Propostas", "/listar");
        List<SubMenu> novoSubmenu = new ArrayList<>();
        novoSubmenu.add(subMenu);
        novoSubmenu.add(subMenu2);
        Menu propostas = new Menu("Propostas de TFCs", novoSubmenu);

        SubMenu submenuInscricao = new SubMenu("Listar Propostas", "/listatfc");
        SubMenu subMenuListaInscricao = new SubMenu("Lista de Inscrições", "/visualizacaoinscricao");
        List<SubMenu> novoSubmenuInscricao = new ArrayList<>();
        novoSubmenuInscricao.add(submenuInscricao);
        novoSubmenuInscricao.add(subMenuListaInscricao);

        Menu inscrevePropostas = new Menu("Inscrição TFCs", novoSubmenuInscricao);
        Menu consultarNota= new Menu("Consultar  nota ", "/consultarNotaAluno");

        menuAluno.add(propostas);
        menuAluno.add(inscrevePropostas);
        //--- Add by clid-stelio
        Menu agendaApresentacao = new Menu("Agenda apresentações", "/agendaApresentacaoDiscente");
        menuAluno.add(agendaApresentacao);
        menuAluno.add(consultarNota);
        //--- End Add


        return menuAluno;
    }
}
