package pt.ulusofona.deisi.gestaotfcs.services;

import pt.ulusofona.deisi.gestaotfcs.data.*;
import pt.ulusofona.deisi.gestaotfcs.form.*;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class ServiceTFC {
    private static final String estadoAguardar = "A Aguardar Aprovação";
    private static final String estadoAguardaAtribuicao = "A Aguardar Atribuição";
    private static final String atribuido = "Atribuido";
    private static final String recusado = "Recusado";
    private static final String naoAtribuido = "Não Atribuido";

    private HashMap<String, Object> tfcGenerico = new HashMap<>();

    public ServiceTFC() {
    }

    public HashMap<String, Object> getTfcGenerico() {
        return tfcGenerico;
    }

    public String setTfcGenerico(HashMap<String, Object> tfcGenerico) {
        this.tfcGenerico = tfcGenerico;
        String resultado = criaEdita(this.tfcGenerico);
        return resultado;
    }

    private String criaEdita(HashMap<String, Object> tfcGenerico){
        EntityManager em = (EntityManager) tfcGenerico.get("em");
        ServiceInformacao serviceInformacao = new ServiceInformacao();
        TFC tfc = null;
        boolean edicaoTFC = false;

        String avaliador = (String) tfcGenerico.get("avaliador");
        String motivoRecusa = (String) tfcGenerico.get("motivoRecusa");
        String interlocutor = (String) tfcGenerico.get("interlocutor");
        String avaliacao = (String) tfcGenerico.get("avaliacao");
        String morada = (String) tfcGenerico.get("morada");
        String tipoTFC = (String) tfcGenerico.get("tipoTFC");
        String nomeDepartamento = (String) tfcGenerico.get("nomeDepartamento");
        String preponente = (String) tfcGenerico.get("preponente");
        Long id = (Long) tfcGenerico.get("id");
        String email = (String) tfcGenerico.get("email");
        Integer contato = (Integer) tfcGenerico.get("contato");
        Long entidadeTFC = (Long) tfcGenerico.get("entidadeTFC");
        Long cursoTFC = (Long) tfcGenerico.get("cursoTFC");
        List<Long> disciplinasTFC = (List<Long>) tfcGenerico.get("disciplinasTFC");
        List<Long> tecnologiasTFC = (List<Long>) tfcGenerico.get("tecnologiasTFC");
        String titulo = (String) tfcGenerico.get("titulo");
        String descricao = (String) tfcGenerico.get("descricao");
        String entidade = (String) tfcGenerico.get("entidade");
        Long cursoAssociado = (Long) tfcGenerico.get("cursoAssociado");
        List<Long> disciplinas = (List<Long>) tfcGenerico.get("disciplinas");
        List<String> tecnologias = (List<String>) tfcGenerico.get("tecnologias");
        String orientador = (String) tfcGenerico.get("orientador");
        String orientadorProposto = (String) tfcGenerico.get("orientadorProposto");
        String numero2Aluno = (String) tfcGenerico.get("numero2aluno");
        String nome2Aluno = (String) tfcGenerico.get("nome2Aluno");
        String idTFC = "";

        ProfessorNDEISI pNDEISI = new ProfessorNDEISI();
        if(tipoTFC.equalsIgnoreCase("NDEISI")){
            Utilizador utilizador;
            try{
                pNDEISI = em.createQuery("select pND from ProfessorNDEISI pND where pND.nome=:preponente",
                        ProfessorNDEISI.class).setParameter("preponente", preponente).getSingleResult();
                utilizador = em.createQuery("select u from Utilizador u where u.idIdentificacao=:idProfessor"
                        , Utilizador.class).setParameter("idProfessor", pNDEISI.getIdProfessor()).getSingleResult();
            }catch(NoResultException e){
                pNDEISI = new ProfessorNDEISI(preponente, email,
                        contato, nomeDepartamento);
                em.persist(pNDEISI);
                pNDEISI.setIdProfessor(pNDEISI.getId());
                em.persist(pNDEISI);
                utilizador = new Utilizador(pNDEISI.getIdProfessor(), false, "ProfessorNDEISI");
                em.persist(utilizador);
            }
        }

        Empresa_EntidadeExterna empresa = new Empresa_EntidadeExterna();
        if(tipoTFC.equalsIgnoreCase("Empresa")){
            Utilizador utilizador;
            try{
                empresa = em.createQuery("select emp from Empresa_EntidadeExterna emp where emp.nome=:empresa",
                        Empresa_EntidadeExterna.class).setParameter("empresa", entidade)
                        .getSingleResult();
                utilizador = em.createQuery("select u from Utilizador u where u.idIdentificacao=:idEmpresa"
                        , Utilizador.class).setParameter("idEmpresa", empresa.getIdEmpresa()).getSingleResult();
            }catch(NoResultException e){
                empresa = new Empresa_EntidadeExterna(entidade, morada,
                        email, contato, interlocutor);
                em.persist(empresa);
                empresa.setIdEmpresa(empresa.getId());
                em.persist(empresa);
                utilizador = new Utilizador(empresa.getIdEmpresa(), false, "EmpEnt");
                em.persist(utilizador);
            }
        }

        if(id != null){
            tfc = em.find(TFC.class, id);
            edicaoTFC = true;
        }else{
            tfc = new TFC();
        }


        long numeroGrupo = 0;

        if(numero2Aluno != null){
            /*List<Grupo> grupos = em.createQuery("select g from Grupo g", Grupo.class).getResultList();
            for (Grupo grupo: grupos) {
                if(grupo.getIdNumeroAluno1().equalsIgnoreCase(preponente) ||
                        grupo.getIdNumeroAluno2().equalsIgnoreCase(preponente)){
                    numeroGrupo = grupo.getId();
                    break;
                }
            }

            if(numeroGrupo==0){
                Grupo novoGrupo = new Grupo(preponente, true, numero2Aluno, false);
                em.persist(novoGrupo);
                numeroGrupo = novoGrupo.getId();
            }*/
            numeroGrupo = serviceInformacao.recolheNumeroGrupo(preponente, numero2Aluno, em);
            tipoTFC = "G";
            tfc.setIdGrupo(numeroGrupo );
        }

        if(avaliacao != null && !avaliacao.equalsIgnoreCase("Escolha uma avaliação")){
            tfc.setAvaliacaoProposta(avaliacao);
            if(motivoRecusa != null){
                tfc.setMotivoRecusa(motivoRecusa);
                tfc.setEstado(avaliacao);
            }
            if(!avaliacao.equalsIgnoreCase("Recusado")){
                if(tipoTFC.equalsIgnoreCase("Aluno")){
                    tfc.setEstado(atribuido);
                    ServiceInscricao serviceInscricao = new ServiceInscricao();
                    HashMap<String, Object> inscricao = new HashMap<>();
                    inscricao.put("idTFC", tfc.getIdtfc());
                    if(numeroGrupo != 0){
                        inscricao.put("idNumeroGrupo", numeroGrupo);
                    }else{
                        inscricao.put("numeroAluno", preponente);
                    }
                    inscricao.put("estado", tfc.getEstado());
                    inscricao.put("ordemEscolha", 1);
                    serviceInscricao.gravaInscricao(em, inscricao);
                }else{
                    tfc.setEstado(estadoAguardaAtribuicao);
                }
            }
        }else{
            if(tipoTFC != "DEISI"){
                tfc.setEstado(estadoAguardar);
            }else{
                tfc.setEstado(estadoAguardaAtribuicao);
            }
        }

        if(orientador!=null){
            if(orientador.equalsIgnoreCase("0")){
                tfc.setOrientador(null);
            }else{
                tfc.setOrientador(orientador);
            }
        }

        tfc.setTitulo(titulo);
        tfc.setDescricao(descricao);
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        tfc.setDataEstado(dateFormat.format(date));
        /*A data não pode ser modificada após submissão (FALTA)*/
        if(tfc.getDataProposta()== null){
            tfc.setDataProposta(dateFormat.format(date));
        }


        em.persist(tfc);

        if(orientadorProposto!=null){
            if(orientadorProposto.equalsIgnoreCase("0")){
                tfc.setOrientadorProposto(null);
            }else{
                tfc.setOrientadorProposto(orientadorProposto);
            }
        }

        tfc.setIdtfc( tipoTFC + tfc.getId());
        em.persist(tfc);

        if(avaliacao==null){
            if(tipoTFC.equalsIgnoreCase("NDEISI")){
                tfc.setPreponente(pNDEISI.getIdProfessor());
            }else if (tipoTFC.equalsIgnoreCase("Empresa")){
                tfc.setPreponente(empresa.getIdEmpresa());
            }
            else{
                tfc.setPreponente(preponente);
            }
        }

        if (cursoAssociado != null){
            if(cursoAssociado !=0){
                if(cursoTFC != 0){
                    TFCCurso tfcCursoList = em.createQuery("select tfcc from TFCCurso tfcc where tfcc.idCurso=:cursoTFC and" + " tfcc.idTFC=:id", TFCCurso.class)
                            .setParameter("cursoTFC", cursoTFC).setParameter("id",tfc.getId()).getSingleResult();
                    if(tfcCursoList.getIdTFC() == tfc.getId() && tfcCursoList.getIdCurso() == cursoTFC
                            && cursoTFC != cursoAssociado){
                        tfcCursoList.setIdCurso(cursoAssociado);
                        em.persist(tfcCursoList);
                    }
                }else{
                    TFCCurso tfcCurso = new TFCCurso();
                    tfcCurso.setIdCurso(cursoAssociado);
                    tfcCurso.setIdTFC(tfc.getId());
                    em.persist(tfcCurso);
                }

            }
        }

        List<TFCCurso> tfcCursoList = em.createQuery("select tfcc from TFCCurso tfcc", TFCCurso.class).getResultList();
        for (TFCCurso tfcc: tfcCursoList) {
            if(tfcc.getIdTFC() == tfc.getId() && tfcc.getIdCurso() == cursoTFC
                    && cursoTFC != cursoAssociado){
                em.remove(tfcc);
                break;
            }
        }

        /*
         * Inicio do código que irá permitir remover todas as tecnologias que tenham sido removidas da
         * ligação entre tecnologias e tfc
         * */
        if(tecnologias !=null){
            for (Long tfcTecnologia: tecnologiasTFC) {
                List<TFCTecnologia> apagar = em.createQuery("select tec from TFCTecnologia tec where tec.idTecnologia=:idTec",
                        TFCTecnologia.class).setParameter("idTec", tfcTecnologia).getResultList();
                for (TFCTecnologia apagaNaListaTecnologia: apagar) {
                    Tecnologia tecnologiaApagar = em.find(Tecnologia.class, apagaNaListaTecnologia.getIdTecnologia());
                    /*Vai testar para ver se a List de Tecnologias tem elementos escolhidos*/
                    if(tecnologias!= null){
                        if (!tecnologias.contains(tecnologiaApagar.getNome())) {
                            em.remove(apagaNaListaTecnologia);
                        }
                    }else{
                        List<TFCTecnologia> tfcTecnologiaList = em.createQuery("select tt from TFCTecnologia tt where tt.idTFC=:idTFC"
                                , TFCTecnologia.class).setParameter("idTFC", tfc.getId()).getResultList();
                        for (TFCTecnologia tfcTec: tfcTecnologiaList) {
                            em.remove(tfcTec);
                        }
                    }
                }


            }
        }else{
            if(tecnologiasTFC!=null){
                for (Long tecTFC: tecnologiasTFC) {
                    try{
                        List<TFCTecnologia> tfcTecnologia = em.createQuery("select tfcTec from TFCTecnologia tfcTec where tfcTec.idTecnologia=:idTec",
                                TFCTecnologia.class).setParameter("idTec", tecTFC).getResultList();
                        for (TFCTecnologia tfct: tfcTecnologia) {
                            if(tfct.getIdTecnologia() == tecTFC && tfct.getIdTFC() == tfc.getId()){
                                em.remove(tfct);
                            }
                        }
                    }catch (NoResultException e){
                        System.out.println();
                    }

                }
            }
        }

        /*
         * Código que permite perceber se já existe a ligação entre tecnologia e curso.
         * Vai remover todas as tecnologias da List Tecnologias que já tenham uma ligação
         * em BD com os TFCs
         * Desta forma não irá existir duplicados na tabela TFCTecnologia
         * (Situação repetida para Disciplinas)*/
        List<TFCTecnologia> tfcTecnologias = em.createQuery("select tfctec from TFCTecnologia tfctec",
                TFCTecnologia.class).getResultList();
        if(tecnologias !=null){
            for (TFCTecnologia tec: tfcTecnologias) {
                Tecnologia tecnologia = em.find(Tecnologia.class, tec.getIdTecnologia());
                if(tecnologias.contains(tecnologia.getNome()) && tec.getIdTFC()==tfc.getId()){
                    tecnologias.remove(tecnologia.getNome());
                }
            }
        }

        /*Irá criar apenas as ligações que não existam em BD, evita-se duplicados*/
        if(tecnologias != null){
            for (String tfcTec: tecnologias) {
                try{
                    Tecnologia idTec = em.createQuery("select tec from Tecnologia tec where tec.nome=:nomeTec",
                            Tecnologia.class).setParameter("nomeTec", tfcTec).getSingleResult();
                    TFCTecnologia tfcTecnologia = new TFCTecnologia(tfc.getId(), idTec.getId());
                    em.persist(tfcTecnologia);
                }catch (NoResultException e){
                    Tecnologia tecnologia = new Tecnologia(tfcTec);
                    em.persist(tecnologia);
                    TFCTecnologia tfcTecnologia = new TFCTecnologia(tfc.getId(), tecnologia.getId());
                    em.persist(tfcTecnologia);
                }

            }
        }

        if(disciplinas != null){
            for (Long idTFCDisciplinas: disciplinasTFC) {
                TFCDisciplina apagar = em.find(TFCDisciplina.class, idTFCDisciplinas);
                Disciplina disciplinaApagar = em.find(Disciplina.class, apagar.getIdNumeroDisciplina());
                if(disciplinas !=null){
                    if (!disciplinas.contains(disciplinaApagar.getId())) {
                        em.remove(apagar);
                    }
                }else{
                    List<TFCDisciplina> tfcDisciplinasList = em.createQuery("select td from TFCDisciplina td where td.numeroTFC=:idTFC"
                            , TFCDisciplina.class).setParameter("idTFC", tfc.getId()).getResultList();
                    for (TFCDisciplina tfcDis: tfcDisciplinasList) {
                        em.remove(tfcDis);
                    }
                }
            }
        }else{
            if(disciplinasTFC !=null){
                for (Long disTFC: disciplinasTFC) {
                    try{
                        List<TFCDisciplina> tfDis = em.createQuery("select distec from TFCDisciplina distec where distec.idNumeroDisciplina=:idDisciplina",
                                TFCDisciplina.class).setParameter("idDisciplina", disTFC).getResultList();
                        for (TFCDisciplina tfcDisc: tfDis) {
                            if(tfcDisc.getIdNumeroDisciplina() == disTFC && tfcDisc.getNumeroTFC() == tfc.getId()){
                                em.remove(tfcDisc);
                            }
                        }
                    }catch (NoResultException e){
                        System.out.println("Erro: " + e);
                    }
                }
            }
        }

        List<TFCDisciplina> tfcDisciplinas = em.createQuery("select tfcd from TFCDisciplina tfcd",
                TFCDisciplina.class).getResultList();
        if(tfcDisciplinas!=null && disciplinasTFC.size() != 0){
            for (TFCDisciplina tfcdis: tfcDisciplinas) {
                if(disciplinas!=null){
                    if(disciplinas.contains(tfcdis.getIdNumeroDisciplina()) && tfcdis.getNumeroTFC()==tfc.getId()){
                        disciplinas.remove(tfcdis.getIdNumeroDisciplina());
                    }
                }

            }
        }

        if(disciplinas!=null){
            if(disciplinas.size() != 0){
                for (Long disc: disciplinas) {
                    TFCDisciplina tfcDisciplina = new TFCDisciplina(tfc.getId(), disc);
                    em.persist(tfcDisciplina);
                }
            }
        }



        /*Falta analisar se o orientador fica bem registado*/
        if(entidade!= null){
            if(morada!= null){
                criaEmpresa(em, tfc, entidade, morada);
            }else{
                criaEmpresa(em, tfc, entidade, "sem morada");
            }
        }else{
            tfc.setEntidade(null);
        }
        em.persist(tfc);

        if(avaliacao==null){
            if(edicaoTFC){
                return "O TFC com id " + tfc.getIdtfc() + " foi editado com sucesso";
            }else{
                HistoricoTFC historicoTFC = new HistoricoTFC(tfc.getId(), tfc.getIdtfc(), tfc.getEstado(), tfc.getDataEstado(), tfc.getPreponente());
                em.persist(historicoTFC);
                return "O TFC com id " + tfc.getIdtfc() + " foi criado com sucesso";
            }
        }else {
            HistoricoTFC historicoTFC = new HistoricoTFC(tfc.getId(), tfc.getIdtfc(), tfc.getEstado(), tfc.getDataEstado(), avaliador, avaliacao);
            em.persist(historicoTFC);
            return "O TFC com id " + tfc.getIdtfc() + " foi avaliado com sucesso";
        }

    }

    private void criaEmpresa(EntityManager em, TFC tfcEscolhido, String entidade, String morada){
        try{
            Empresa_EntidadeExterna empent = em.createQuery("select e from Empresa_EntidadeExterna e where e.nome=:nomeEmpresa"
                    , Empresa_EntidadeExterna.class).
                    setParameter("nomeEmpresa", entidade).getSingleResult();
            tfcEscolhido.setEntidade(empent.getId());
            em.persist(tfcEscolhido);
        }catch (NoResultException e){
            Empresa_EntidadeExterna noEmpEnt = new Empresa_EntidadeExterna(entidade, morada);
            em.persist(noEmpEnt);
            Utilizador utilEmp = new Utilizador();
            utilEmp.setIdIdentificacao(Long.toString(noEmpEnt.getId()));
            utilEmp.setTipoUtilizador("Empresa");
            em.persist(utilEmp);
            tfcEscolhido.setEntidade(noEmpEnt.getId());
            em.persist(tfcEscolhido);
        }
    }

    public HashMap<String, Object> vaiBuscarInfoTFC(Long id, EntityManager em){
        List<TFC> tfcs = em.createQuery("select t from TFC t", TFC.class).getResultList();
        if(tfcs==null || tfcs.isEmpty()){
            TFC tfc1= new TFC("Serious Game", "Dificil", "sim");
            TFC tfc2= new TFC("Gestao de TFC's", "Dificil", "sim");
            TFC tfc3= new TFC("Mobile Quiz", "Dificil", "sim");
            TFC tfc4= new TFC("Line UP", "Dificil", "sim");
            TFC tfc5= new TFC("Gestao de Turmas", "Dificil", "sim");


            tfcs.add(tfc1);
            tfcs.add(tfc2);
            tfcs.add(tfc3);
            tfcs.add(tfc4);
            tfcs.add(tfc5);
        }


        HashMap<String, Object> resultado = new HashMap<>();
        TFC tfcEscolhido = em.find(TFC.class, id);

        List<TFCDisciplina> listaDisciplinas = em.createQuery("select dtfc from TFCDisciplina dtfc where dtfc.numeroTFC=:numerotfc",
                TFCDisciplina.class).setParameter("numerotfc", tfcEscolhido.getId()).getResultList();
        List<Long> idDisciplinas = new ArrayList<>();
        List<Long> idDisciplinaTFC = new ArrayList<>();
        for (TFCDisciplina lDisciplina: listaDisciplinas) {
            idDisciplinas.add(lDisciplina.getIdNumeroDisciplina());
            idDisciplinaTFC.add(lDisciplina.getId());
        }

        Empresa_EntidadeExterna empEnt = new Empresa_EntidadeExterna();
        try{
            /*Necessito procurar uma forma melhor de encontrar se o TFC se encontra associado ou não
             * a uma entidade*/
            empEnt = em.createQuery("select ee from Empresa_EntidadeExterna ee where ee.id=:entTFC",
                    Empresa_EntidadeExterna.class).setParameter("entTFC", tfcEscolhido.getEntidade()).getSingleResult();
        }catch (Exception e){

        }

        List<TFCTecnologia> listaTecnologias = em.createQuery("select ttfc from TFCTecnologia ttfc where ttfc.idTFC=:numeroTFC",
                TFCTecnologia.class).setParameter("numeroTFC", tfcEscolhido.getId()).getResultList();
        List<String> nomeTecnologias = new ArrayList<>();
        List<Long> tecnologiasTFC = new ArrayList<>();
        for (TFCTecnologia lTFCTec: listaTecnologias) {
            Tecnologia tecnologia = em.find(Tecnologia.class,lTFCTec.getIdTecnologia());
            nomeTecnologias.add(tecnologia.getNome());
            tecnologiasTFC.add(lTFCTec.getIdTecnologia());
        }

        String destino = "";
        Utilizador utilizador = new Utilizador();
        try{
            utilizador = em.createQuery("select u from Utilizador u where u.idIdentificacao=:utilizador",
                    Utilizador.class).setParameter("utilizador", tfcEscolhido.getPreponente()).getSingleResult();
        }catch (NoResultException e){

        }

        if(utilizador.getTipoUtilizador().equalsIgnoreCase("ProfessorDEISI")){
            /*Fazia desta forma antes, passei a verificação para ver se é coordenador apenas na parte dos menus
             * é a única coisa que muda entre professores DEISI*/
            /*if(utilizador.getTipoUtilizador().equalsIgnoreCase("ProfessorDEISI") && utilizador.isCoordenador()){*/
            ProfForm profForm = new ProfForm();
            ProfessorDEISI profDeisi = em.createQuery("select pd from ProfessorDEISI pd where pd.numeroProfessor=:nProf",
                    ProfessorDEISI.class).setParameter("nProf", utilizador.getIdIdentificacao()).getSingleResult();
            profForm.setPreponente(profDeisi.getNumeroProfessor());
            profForm.setId(tfcEscolhido.getId());
            profForm.setTitulo(tfcEscolhido.getTitulo());
            profForm.setDescricao(tfcEscolhido.getDescricao());
            profForm.setDisciplinas(idDisciplinas);
            profForm.setDisciplinasTFC(idDisciplinaTFC);
            TFCCurso curso = em.createQuery("select ctfc from TFCCurso ctfc where ctfc.idTFC=:numerotfc",
                    TFCCurso.class).setParameter("numerotfc", tfcEscolhido.getId()).getSingleResult();
            profForm.setCursoAssociado(curso.getIdCurso());
            profForm.setCursoTFC(curso.getIdCurso());

            if(empEnt!=null){
                profForm.setEntidade(empEnt.getNome());
            }
            profForm.setTecnologias(nomeTecnologias);
            profForm.setTecnologiasTFC(tecnologiasTFC);

            resultado.put("form", profForm);

            if(utilizador.isCoordenador()){
                resultado.put("tipoUtilizador", "coordenador");
            }else{
                resultado.put("tipoUtilizador", "professordeisi");
            }

            destino = "formprofessor";

        }
        if( utilizador.getTipoUtilizador().equalsIgnoreCase("Aluno")){
            AlunoForm alunoForm = new AlunoForm();
            Aluno aluno = em.createQuery("select a from Aluno a where a.numeroAluno=:nAluno",
                    Aluno.class).setParameter("nAluno", utilizador.getIdIdentificacao()).getSingleResult();
            if(tfcEscolhido.getIdtfc().indexOf('G')>=0){
                Grupo grupo = em.createQuery("select g from Grupo g where g.id=:idTFCGrupo", Grupo.class)
                        .setParameter("idTFCGrupo", tfcEscolhido.getIdGrupo()).getSingleResult();
                if(grupo.getIdNumeroAluno1().equalsIgnoreCase(aluno.getNumeroAluno())){
                    alunoForm.setNumero2Aluno(grupo.getIdNumeroAluno2());
                    try{
                        Aluno aluno1 =em.createQuery("select a from Aluno a where a.numeroAluno=:numeroAluno",
                                Aluno.class).setParameter("numeroAluno",grupo.getIdNumeroAluno2()).getSingleResult();
                        alunoForm.setNome2Aluno(aluno1.getNome());
                    }catch (NoResultException e){

                    }
                }else{
                    alunoForm.setNumero2Aluno(grupo.getIdNumeroAluno1());
                }
            }
            alunoForm.setOrientador(tfcEscolhido.getOrientadorProposto());
            alunoForm.setPreponente(aluno.getNumeroAluno());
            alunoForm.setId(tfcEscolhido.getId());
            alunoForm.setTitulo(tfcEscolhido.getTitulo());
            alunoForm.setDescricao(tfcEscolhido.getDescricao());
            alunoForm.setDisciplinas(idDisciplinas);
            alunoForm.setDisciplinasTFC(idDisciplinaTFC);
            try{
                TFCCurso curso = em.createQuery("select ctfc from TFCCurso ctfc where ctfc.idTFC=:numerotfc",
                        TFCCurso.class).setParameter("numerotfc", tfcEscolhido.getId()).getSingleResult();
                alunoForm.setCurso(curso.getIdCurso());
                alunoForm.setCursoTFC(curso.getIdCurso());
            }catch (NoResultException e){
                System.out.println("Erro: " + e);
            }

            if(empEnt!=null){
                alunoForm.setEntidade(empEnt.getNome());
            }
            alunoForm.setTecnologias(nomeTecnologias);
            alunoForm.setTecnologiasTFC(tecnologiasTFC);

            resultado.put("form", alunoForm);
            resultado.put("tipoUtilizador", "aluno");
            destino = "formaluno";

        }

        if(utilizador.getTipoUtilizador().equalsIgnoreCase("ProfessorNDEISI")){
            NdeisiForm ndeisiForm = new NdeisiForm();
            try{
                try{
                    TFCCurso curso = em.createQuery("select ctfc from TFCCurso ctfc where ctfc.idTFC=:numerotfc",
                            TFCCurso.class).setParameter("numerotfc", tfcEscolhido.getId()).getSingleResult();
                    ndeisiForm.setCursoAssociado(curso.getIdCurso());
                    ndeisiForm.setCursoTFC(curso.getIdCurso());
                }catch (NoResultException e){

                }

                ndeisiForm.setId(tfcEscolhido.getId());
                ndeisiForm.setTitulo(tfcEscolhido.getTitulo());
                ndeisiForm.setDescricao(tfcEscolhido.getDescricao());
                ndeisiForm.setDisciplinas(idDisciplinas);
                ndeisiForm.setDisciplinasTFC(idDisciplinaTFC);

                if(empEnt!=null){
                    ndeisiForm.setEntidade(empEnt.getNome());
                    ndeisiForm.setEntidadeTFC(empEnt.getId());
                }
                ndeisiForm.setTecnologias(nomeTecnologias);
                ndeisiForm.setTecnologiasTFC(tecnologiasTFC);

                ndeisiForm.setTitulo(tfcEscolhido.getTitulo());
                ndeisiForm.setDescricao(tfcEscolhido.getDescricao());
                ProfessorNDEISI professorNDEISI = em.createQuery("select pNDEISI from ProfessorNDEISI pNDEISI where pNDEISI.idProfessor=:preponente",
                        ProfessorNDEISI.class).setParameter("preponente", tfcEscolhido.getPreponente()).getSingleResult();
                ndeisiForm.setPreponente(professorNDEISI.getNome());
                ndeisiForm.setNomeDepartamento(professorNDEISI.getDepartamentoAfeto());
                ndeisiForm.setEmail(professorNDEISI.getEmail());
                ndeisiForm.setContato(professorNDEISI.getNumeroContato());
                ndeisiForm.setId(tfcEscolhido.getId());

            }catch(NoResultException e){
                System.out.println("Não existe nenhum TFC com esse " + id);
            }

            resultado.put("form", ndeisiForm);
            resultado.put("tipoUtilizador", "ndeisi");
            destino = "formNDEISI";

        }
        if(utilizador.getTipoUtilizador().equalsIgnoreCase("EmpEnt")){
            EmpresaForm empresaForm = new EmpresaForm();
            try{
                try{
                    TFCCurso curso = em.createQuery("select ctfc from TFCCurso ctfc where ctfc.idTFC=:numerotfc",
                            TFCCurso.class).setParameter("numerotfc", tfcEscolhido.getId()).getSingleResult();
                    empresaForm.setCursoAssociado(curso.getIdCurso());
                    empresaForm.setCursoTFC(curso.getIdCurso());
                }catch (NoResultException e){

                }

                empresaForm.setId(tfcEscolhido.getId());
                empresaForm.setTitulo(tfcEscolhido.getTitulo());
                empresaForm.setDescricao(tfcEscolhido.getDescricao());
                empresaForm.setDisciplinas(idDisciplinas);
                empresaForm.setDisciplinasTFC(idDisciplinaTFC);

                empresaForm.setTecnologias(nomeTecnologias);
                empresaForm.setTecnologiasTFC(tecnologiasTFC);

                empresaForm.setTitulo(tfcEscolhido.getTitulo());
                empresaForm.setDescricao(tfcEscolhido.getDescricao());
                Empresa_EntidadeExterna emp = em.createQuery("select emp from Empresa_EntidadeExterna emp where emp.idEmpresa=:idEmpresa",
                        Empresa_EntidadeExterna.class).setParameter("idEmpresa", tfcEscolhido.getPreponente()).getSingleResult();
                empresaForm.setNomeEmpresa(emp.getNome());
                empresaForm.setMorada(emp.getMorada());
                empresaForm.setInterlocutor(emp.getInterlocutor());
                empresaForm.setPreponente(emp.getIdEmpresa());
                empresaForm.setEmail(emp.getEmail());
                empresaForm.setContato(emp.getNumeroContato());
                empresaForm.setId(tfcEscolhido.getId());

            }catch(NoResultException e){
                System.out.println("Não existe nenhum TFC com esse " + id);
            }
            resultado.put("form", empresaForm);
            resultado.put("tipoUtilizador", "empresa");
            destino = "formEmpresa";

        }

        if(tfcEscolhido.getEstado().equalsIgnoreCase(atribuido)){
            ServiceInformacao serviceInformacao = new ServiceInformacao();
            Inscricao alunoGrupoAtribuido = serviceInformacao.vaiBuscarNumeroAlunoGrupo(em, tfcEscolhido.getIdtfc());
            if(alunoGrupoAtribuido.getNumeroAluno() != null){
                resultado.put("aluno", alunoGrupoAtribuido.getNumeroAluno());
            }else{
                resultado.put("grupo", alunoGrupoAtribuido.getIdNumeroGrupo());
            }

        }

        resultado.put("destino", destino);
        resultado.put("tfc", tfcEscolhido);

        return resultado;
    }

    public List<TFC> recolheTFCS(EntityManager em){
        List<TFC> listaRetornoTFC = new ArrayList<>();

        List<TFC> listaTFC = em.createQuery("select tfc from TFC tfc", TFC.class).getResultList();
        for (TFC tfc: listaTFC) {
            Utilizador utilizador = em.createQuery("select u from Utilizador u where u.idIdentificacao=:preponente",
                    Utilizador.class).setParameter("preponente", tfc.getPreponente()).getSingleResult();
            if(!utilizador.getTipoUtilizador().equals("ProfessorDEISI") &&
                    tfc.getEstado().equalsIgnoreCase(estadoAguardar)){
                listaRetornoTFC.add(tfc);
            }
        }
        return listaRetornoTFC;
    }

    public AvaliaPropostaForm preencheFormAvaliacaoProposta(Object object, String tipoUtilizador){
        AvaliaPropostaForm avaliaPropostaForm = new AvaliaPropostaForm();
        if(tipoUtilizador.equalsIgnoreCase("aluno")){
            AlunoForm alunoForm = (AlunoForm) object;
            avaliaPropostaForm.setId(alunoForm.getId());
            avaliaPropostaForm.setPreponente(alunoForm.getPreponente());
            avaliaPropostaForm.setEntidade(alunoForm.getEntidade());
            avaliaPropostaForm.setEntidadeTFC(alunoForm.getEntidadeTFC());
            avaliaPropostaForm.setCursoTFC(alunoForm.getCursoTFC());
            avaliaPropostaForm.setDisciplinasTFC(alunoForm.getDisciplinasTFC());
            avaliaPropostaForm.setTecnologiasTFC(alunoForm.getTecnologiasTFC());
            avaliaPropostaForm.setNumero2Aluno(alunoForm.getNumero2Aluno());
            avaliaPropostaForm.setNome2Aluno(alunoForm.getNome2Aluno());
            avaliaPropostaForm.setTitulo(alunoForm.getTitulo());
            avaliaPropostaForm.setDescricao(alunoForm.getDescricao());
            avaliaPropostaForm.setCursoAssociado(alunoForm.getCursoAssociado());
            avaliaPropostaForm.setDisciplinas(alunoForm.getDisciplinas());
            avaliaPropostaForm.setTecnologias(alunoForm.getTecnologias());
            avaliaPropostaForm.setOrientador(alunoForm.getOrientador());
        }
        if(tipoUtilizador.equalsIgnoreCase("ndeisi")){
            NdeisiForm ndeisiForm = (NdeisiForm) object;
            avaliaPropostaForm.setId(ndeisiForm.getId());
            avaliaPropostaForm.setPreponente(ndeisiForm.getPreponente());
            avaliaPropostaForm.setEntidade(ndeisiForm.getEntidade());
            avaliaPropostaForm.setEntidadeTFC(ndeisiForm.getEntidadeTFC());
            avaliaPropostaForm.setCursoTFC(ndeisiForm.getCursoTFC());
            avaliaPropostaForm.setDisciplinasTFC(ndeisiForm.getDisciplinasTFC());
            avaliaPropostaForm.setTecnologiasTFC(ndeisiForm.getTecnologiasTFC());
            avaliaPropostaForm.setTitulo(ndeisiForm.getTitulo());
            avaliaPropostaForm.setDescricao(ndeisiForm.getDescricao());
            avaliaPropostaForm.setCursoAssociado(ndeisiForm.getCursoAssociado());
            avaliaPropostaForm.setDisciplinas(ndeisiForm.getDisciplinas());
            avaliaPropostaForm.setTecnologias(ndeisiForm.getTecnologias());
            avaliaPropostaForm.setOrientador(ndeisiForm.getOrientador());
            avaliaPropostaForm.setNomeDepartamento(ndeisiForm.getNomeDepartamento());
            avaliaPropostaForm.setEmail(ndeisiForm.getEmail());
            avaliaPropostaForm.setContato(ndeisiForm.getContato());
        }
        if(tipoUtilizador.equalsIgnoreCase("empresa")){
            EmpresaForm empresaForm = (EmpresaForm) object;
            avaliaPropostaForm.setId(empresaForm.getId());
            avaliaPropostaForm.setPreponente(empresaForm.getPreponente());
            avaliaPropostaForm.setEntidadeTFC(empresaForm.getEntidadeTFC());
            avaliaPropostaForm.setCursoTFC(empresaForm.getCursoTFC());
            avaliaPropostaForm.setDisciplinasTFC(empresaForm.getDisciplinasTFC());
            avaliaPropostaForm.setTecnologiasTFC(empresaForm.getTecnologiasTFC());
            avaliaPropostaForm.setTitulo(empresaForm.getTitulo());
            avaliaPropostaForm.setDescricao(empresaForm.getDescricao());
            avaliaPropostaForm.setCursoAssociado(empresaForm.getCursoAssociado());
            avaliaPropostaForm.setDisciplinas(empresaForm.getDisciplinas());
            avaliaPropostaForm.setTecnologias(empresaForm.getTecnologias());
            avaliaPropostaForm.setOrientador(empresaForm.getOrientador());
            avaliaPropostaForm.setEmail(empresaForm.getEmail());
            avaliaPropostaForm.setContato(empresaForm.getContato());
            avaliaPropostaForm.setNomeEmpresa(empresaForm.getNomeEmpresa());
            avaliaPropostaForm.setInterlocutor(empresaForm.getInterlocutor());
            avaliaPropostaForm.setMorada(empresaForm.getMorada());
        }
        if(tipoUtilizador.equalsIgnoreCase("coordenador") ||
                tipoUtilizador.equalsIgnoreCase("professordeisi")){
            ProfForm profForm = (ProfForm) object;
            avaliaPropostaForm.setId(profForm.getId());
            avaliaPropostaForm.setPreponente(profForm.getPreponente());
            avaliaPropostaForm.setEntidade(profForm.getEntidade());
            avaliaPropostaForm.setEntidadeTFC(profForm.getEntidadeTFC());
            avaliaPropostaForm.setCursoTFC(profForm.getCursoTFC());
            avaliaPropostaForm.setDisciplinasTFC(profForm.getDisciplinasTFC());
            avaliaPropostaForm.setTecnologiasTFC(profForm.getTecnologiasTFC());
            avaliaPropostaForm.setTitulo(profForm.getTitulo());
            avaliaPropostaForm.setDescricao(profForm.getDescricao());
            avaliaPropostaForm.setCursoAssociado(profForm.getCursoAssociado());
            avaliaPropostaForm.setDisciplinas(profForm.getDisciplinas());
            avaliaPropostaForm.setTecnologias(profForm.getTecnologias());
            avaliaPropostaForm.setOrientador(profForm.getPreponente());
        }

        return avaliaPropostaForm;
    }

    /**
     * Recolhe a informação de todos os TFCs que o Aluno se poderá inscrever.
     * Todos os TFCs cujo preponente seja um Aluno, será excluído desta lista.
     * @param em
     * @param user
     * @return - Lista de TFCs que o Aluno se pode inscrever
     */
    public List<TFC> recolheInfoTFCInscricao(EntityManager em, String user){
        List<TFC> listaRetornoTFC = new ArrayList<>();
        List<TFC> listaTFCs = em.createQuery("select tfc from TFC tfc", TFC.class).getResultList();
        for (TFC tfc: listaTFCs) {
            Utilizador utilizador = em.createQuery("select u from Utilizador u where u.idIdentificacao=:user",
                    Utilizador.class).setParameter("user", tfc.getPreponente()).getSingleResult();
            if(!utilizador.getTipoUtilizador().equalsIgnoreCase("Aluno")
                    && tfc.getEstado().equalsIgnoreCase(estadoAguardaAtribuicao)){
                listaRetornoTFC.add(tfc);
            }

        }

        return listaRetornoTFC;
    }

    public List<TFC> recolheTFCs(EntityManager em){
        return em.createQuery("select tfc from TFC tfc", TFC.class).getResultList();
    }

    /**
     * Recolhe todos os TFCs que tenham pelo menos uma inscrição e que não estejam já atribuidos.
     * @param em
     * @return
     */
    public List<TFC> recolheTFCsComInscricao(EntityManager em){
        ServiceInscricao serviceInscricao = new ServiceInscricao();

        List<TFC> listaTFCsComInscricao = new ArrayList<>();

        List<TFC> listaTFCs = em.createQuery("select tfc from TFC tfc", TFC.class).getResultList();
        List<Inscricao> listaInscricoes = serviceInscricao.recolheInscricoes(em);

        /*Só irá contabilizar TFCs que não tenham sido atribuidos.*/
        for(TFC tfc: listaTFCs){
            Integer numeroInscricoes = 0;
            for(Inscricao inscricao: listaInscricoes){
                if((inscricao.getIdTFC().equalsIgnoreCase(tfc.getIdtfc()))&&
                        ( !tfc.getEstado().equalsIgnoreCase(atribuido))){
                    /*Só contabiliza inscrições cujo estado esteja null, quer indicar que essa inscrição não
                    * pertence a um aluno que já tenha tido um tfc atribuido a si.*/
                    if(inscricao.getEstado() == null ){
                        numeroInscricoes++;
                    }

                }
            }
            tfc.setNumeroInscricoes(numeroInscricoes);
            em.persist(tfc);
            if(numeroInscricoes!=0){
                listaTFCsComInscricao.add(tfc);
            }
        }

        return listaTFCsComInscricao;
    }

    public TFC recolheTFC(EntityManager em, String idTFC){
        TFC tfc = em.createQuery("select tfc from TFC tfc where tfc.idtfc=:idTFC",
                TFC.class).setParameter("idTFC", idTFC).getSingleResult();
        return tfc;
    }

    public void atribuiTFC(EntityManager em, String idTFC, String coordenador){

        TFC tfc = em.createQuery("select tfc from TFC tfc where tfc.idtfc=:idTFC",
                TFC.class).setParameter("idTFC", idTFC).getSingleResult();
        tfc.setEstado(atribuido);

        HistoricoTFC historicoTFC = new HistoricoTFC(tfc.getId(), tfc.getIdtfc(), tfc.getEstado(), tfc.getDataEstado(), coordenador);
        em.persist(historicoTFC);

        em.persist(tfc);

    }

}
