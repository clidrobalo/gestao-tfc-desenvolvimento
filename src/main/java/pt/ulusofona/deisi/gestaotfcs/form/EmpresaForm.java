package pt.ulusofona.deisi.gestaotfcs.form;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.*;
import java.util.List;

public class EmpresaForm {

    /*Apenas preenchido na edição*/
    private Long id;

    /*Inicio da informação sobre o professor e o departamento que está afeto*/
    @NotEmpty(message = "O nome da Empresa necessita estar preenchido")
    private String nomeEmpresa;

    @NotEmpty(message = "O nome do Interlocutor necessita estar preenchido")
    private String interlocutor;

    @NotEmpty(message = "O email necessita estar preenchido")
    private String email;

    @NotNull(message="O telefone tem que estar preenchido")
    @Min(value = 100000000, message = "O telefone tem que ter 9 dígitos")
    @Max(value = 999999999, message = "O telefone tem que ter 9 dígitos")
    private Integer contato;

    @NotNull(message = "A Morada necessita estar preenchida")
    private String morada;
    /*Fim da informação pessoal da empresa*/

    /*Informação preenchida quando se pretende editar um TFC*/
    private long entidadeTFC;
    private long cursoTFC;
    private List<Long> disciplinasTFC;
    private List<Long> tecnologiasTFC;
    /*Fim da informação da edição*/

    /*Inicio da informação de um TFC*/
    @NotEmpty(message = "O titulo necessita estar preenchido")
    private String titulo;

    @NotEmpty(message = "A descricao necessita estar preenchida")
    @Size(max = 500, message = "A descrição só pode ter no máximo 500 caracteres")
    private String descricao;

    private Long cursoAssociado;

    private List<Long> disciplinas;

    private List<String> tecnologias;

    private String orientador;

    private String preponente;

    public EmpresaForm() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getEntidadeTFC() {
        return entidadeTFC;
    }

    public void setEntidadeTFC(long entidadeTFC) {
        this.entidadeTFC = entidadeTFC;
    }

    public long getCursoTFC() {
        return cursoTFC;
    }

    public void setCursoTFC(long cursoTFC) {
        this.cursoTFC = cursoTFC;
    }

    public List<Long> getDisciplinasTFC() {
        return disciplinasTFC;
    }

    public void setDisciplinasTFC(List<Long> disciplinasTFC) {
        this.disciplinasTFC = disciplinasTFC;
    }

    public List<Long> getTecnologiasTFC() {
        return tecnologiasTFC;
    }

    public void setTecnologiasTFC(List<Long> tecnologiasTFC) {
        this.tecnologiasTFC = tecnologiasTFC;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Long getCursoAssociado() {
        return cursoAssociado;
    }

    public void setCursoAssociado(Long cursoAssociado) {
        this.cursoAssociado = cursoAssociado;
    }

    public List<Long> getDisciplinas() {
        return disciplinas;
    }

    public void setDisciplinas(List<Long> disciplinas) {
        this.disciplinas = disciplinas;
    }

    public List<String> getTecnologias() {
        return tecnologias;
    }

    public void setTecnologias(List<String> tecnologias) {
        this.tecnologias = tecnologias;
    }

    public String getOrientador() {
        return orientador;
    }

    public void setOrientador(String orientador) {
        this.orientador = orientador;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getContato() {
        return contato;
    }

    public void setContato(Integer contato) {
        this.contato = contato;
    }

    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    public void setNomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }

    public String getInterlocutor() {
        return interlocutor;
    }

    public void setInterlocutor(String interlocutor) {
        this.interlocutor = interlocutor;
    }

    public String getMorada() {
        return morada;
    }

    public void setMorada(String morada) {
        this.morada = morada;
    }

    public String getPreponente() {
        return preponente;
    }

    public void setPreponente(String preponente) {
        this.preponente = preponente;
    }
}
