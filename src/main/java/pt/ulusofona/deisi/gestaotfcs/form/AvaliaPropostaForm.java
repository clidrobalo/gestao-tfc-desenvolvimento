package pt.ulusofona.deisi.gestaotfcs.form;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.*;
import java.util.List;

public class AvaliaPropostaForm {
    /**
     * Form que irá permitir a avaliação por parte da Coordenação da UC.
     * Será preenchido com a informação do TFC que se encontra a ser avaliado.
     * Desta forma facilita a vizualisação do TFC no JSP.
     */

    private String motivoRecusa;

    private String avaliacao;

    private String tipoUtilizador;

    private Long id;

    private String preponente;

    private String entidade;

    private long entidadeTFC;
    private long cursoTFC;
    private List<Long> disciplinasTFC;
    private List<Long> tecnologiasTFC;
    private String numero2Aluno;
    private String nome2Aluno;

    /*@NotEmpty(message = "O titulo necessita estar preenchido")*/
    private String titulo;

    /*@NotEmpty(message = "A descricao necessita estar preenchida")
    @Size(max = 500, message = "A descrição só pode ter no máximo 500 caracteres")*/
    private String descricao;

    @NotNull(message = "Tem que escolher pelo menos um curso")
    @DecimalMin(value = "1",  message = "Deve escolher pelo menos um curso")
    private Long cursoAssociado;

    @NotEmpty(message = "Tem que escolher pelo menos uma disciplina")
    private List<Long> disciplinas;

    @NotEmpty(message = "Necessita pelo menos preencher uma tecnologia")
    private List<String> tecnologias;

    @Size(min = 2,  message = "Deve escolher pelo menos um professor orientador")
    private String orientador;

    /*Inicio da informação sobre o professor e o departamento que está afeto*/
    private String nomeEmpresa;

    private String interlocutor;

    private String email;

    @Min(value = 100000000, message = "O telefone tem que ter 9 dígitos")
    @Max(value = 999999999, message = "O telefone tem que ter 9 dígitos")
    private Integer contato;

    private String morada;
    /*Fim da informação pessoal da empresa*/

    /*Inicio da informação sobre o professor e o departamento que está afeto*/
    private String nomeDepartamento;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPreponente() {
        return preponente;
    }

    public void setPreponente(String preponente) {
        this.preponente = preponente;
    }

    public String getEntidade() {
        return entidade;
    }

    public void setEntidade(String entidade) {
        this.entidade = entidade;
    }

    public long getEntidadeTFC() {
        return entidadeTFC;
    }

    public void setEntidadeTFC(long entidadeTFC) {
        this.entidadeTFC = entidadeTFC;
    }

    public long getCursoTFC() {
        return cursoTFC;
    }

    public void setCursoTFC(long cursoTFC) {
        this.cursoTFC = cursoTFC;
    }

    public List<Long> getDisciplinasTFC() {
        return disciplinasTFC;
    }

    public void setDisciplinasTFC(List<Long> disciplinasTFC) {
        this.disciplinasTFC = disciplinasTFC;
    }

    public List<Long> getTecnologiasTFC() {
        return tecnologiasTFC;
    }

    public void setTecnologiasTFC(List<Long> tecnologiasTFC) {
        this.tecnologiasTFC = tecnologiasTFC;
    }

    public String getNumero2Aluno() {
        return numero2Aluno;
    }

    public void setNumero2Aluno(String numero2Aluno) {
        this.numero2Aluno = numero2Aluno;
    }

    public String getNome2Aluno() {
        return nome2Aluno;
    }

    public void setNome2Aluno(String nome2Aluno) {
        this.nome2Aluno = nome2Aluno;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Long getCursoAssociado() {
        return cursoAssociado;
    }

    public void setCursoAssociado(Long cursoAssociado) {
        this.cursoAssociado = cursoAssociado;
    }

    public List<Long> getDisciplinas() {
        return disciplinas;
    }

    public void setDisciplinas(List<Long> disciplinas) {
        this.disciplinas = disciplinas;
    }

    public List<String> getTecnologias() {
        return tecnologias;
    }

    public void setTecnologias(List<String> tecnologias) {
        this.tecnologias = tecnologias;
    }

    public String getOrientador() {
        return orientador;
    }

    public void setOrientador(String orientador) {
        this.orientador = orientador;
    }

    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    public void setNomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }

    public String getInterlocutor() {
        return interlocutor;
    }

    public void setInterlocutor(String interlocutor) {
        this.interlocutor = interlocutor;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getContato() {
        return contato;
    }

    public void setContato(Integer contato) {
        this.contato = contato;
    }

    public String getMorada() {
        return morada;
    }

    public void setMorada(String morada) {
        this.morada = morada;
    }

    public String getNomeDepartamento() {
        return nomeDepartamento;
    }

    public void setNomeDepartamento(String nomeDepartamento) {
        this.nomeDepartamento = nomeDepartamento;
    }

    public String getAvaliacao() {
        return avaliacao;
    }

    public void setAvaliacao(String avaliacao) {
        this.avaliacao = avaliacao;
    }

    public String getTipoUtilizador() {
        return tipoUtilizador;
    }

    public void setTipoUtilizador(String tipoUtilizador) {
        this.tipoUtilizador = tipoUtilizador;
    }

    public String getMotivoRecusa() {
        return motivoRecusa;
    }

    public void setMotivoRecusa(String motivoRecusa) {
        this.motivoRecusa = motivoRecusa;
    }
}
