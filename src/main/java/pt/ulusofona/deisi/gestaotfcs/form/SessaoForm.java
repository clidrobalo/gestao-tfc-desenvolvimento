package pt.ulusofona.deisi.gestaotfcs.form;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class SessaoForm {
    private Long id;
    @NotEmpty(message = "A data necessita estar preenchido.")
    private String data;
    @NotEmpty(message = "A hora de inicio necessita estar preenchido.")
    private String horaInicio;
    @NotEmpty(message = "A hora de fim necessita estar preenchido.")
    private String horaFim;
    private String sala;


    public void setId(Long id) {
        this.id =  id;
    }

    public Long getId() {
        return id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getHoraFim() {
        return horaFim;
    }

    public void setHoraFim(String horaFim) {
        this.horaFim = horaFim;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }
}
