package pt.ulusofona.deisi.gestaotfcs.form;

import pt.ulusofona.deisi.gestaotfcs.data.Inscricao;

import java.util.List;

public class AtribuicaoForm {

    private String idTFC;

    private List<Long> listaInscricoes;

    private List<Long> disciplinas;

    private long numeroAlunoAtribuido;

    public AtribuicaoForm() {
    }

    public String getIdTFC() {
        return idTFC;
    }

    public void setIdTFC(String idTFC) {
        this.idTFC = idTFC;
    }

    public List<Long> getListaInscricoes() {
        return listaInscricoes;
    }

    public void setListaInscricoes(List<Long> listaInscricoes) {
        this.listaInscricoes = listaInscricoes;
    }

    public List<Long> getDisciplinas() {
        return disciplinas;
    }

    public void setDisciplinas(List<Long> disciplinas) {
        this.disciplinas = disciplinas;
    }

    public long getNumeroAlunoAtribuido() {
        return numeroAlunoAtribuido;
    }

    public void setNumeroAlunoAtribuido(long numeroAlunoAtribuido) {
        this.numeroAlunoAtribuido = numeroAlunoAtribuido;
    }
}
