package pt.ulusofona.deisi.gestaotfcs.form;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Pattern;

public class InscricaoForm {

    private Long id;

    @NotEmpty
    private String idTFC;
    private String numeroAluno;
    private Long numeroGrupo;
    private int ordemEscolha;
    private String estado;

    @Pattern(regexp = "a\\d{8}",
            message = "Numero de aluno invalido")
    private String numero2Aluno;
    private String nome2Aluno;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdTFC() {
        return idTFC;
    }

    public void setIdTFC(String idTFC) {
        this.idTFC = idTFC;
    }

    public String getNumeroAluno() {
        return numeroAluno;
    }

    public void setNumeroAluno(String numeroAluno) {
        this.numeroAluno = numeroAluno;
    }

    public Long getNumeroGrupo() {
        return numeroGrupo;
    }

    public void setNumeroGrupo(Long numeroGrupo) {
        this.numeroGrupo = numeroGrupo;
    }

    public int getOrdemEscolha() {
        return ordemEscolha;
    }

    public void setOrdemEscolha(int ordemEscolha) {
        this.ordemEscolha = ordemEscolha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }


    public String getNumero2Aluno() {
        return numero2Aluno;
    }

    public void setNumero2Aluno(String numero2Aluno) {
        this.numero2Aluno = numero2Aluno;
    }

    public String getNome2Aluno() {
        return nome2Aluno;
    }

    public void setNome2Aluno(String nome2Aluno) {
        this.nome2Aluno = nome2Aluno;
    }
}
