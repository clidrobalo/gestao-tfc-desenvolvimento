package pt.ulusofona.deisi.gestaotfcs.form;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserForm {

    private Long id;

    @NotEmpty(message="O nome tem que estar preenchido")
    @Size(max = 10, message="O nome tem que ter menos de 10 caracteres")
    private String name;

    @NotEmpty(message="A morada tem que estar preenchida")
    private String address;

    @NotNull(message="O telefone tem que estar preenchido")
    @Min(value = 100000000, message = "O telefone tem que ter 9 dígitos")
    @Max(value = 999999999, message = "O telefone tem que ter 9 dígitos")
    private Integer telefone;

    @NotNull(message="O estado civil tem que estar preenchido")
    private Integer estadoCivil;  // 1 = Solteiro, 2 = Casado, etc.

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getTelefone() {
        return telefone;
    }

    public void setTelefone(Integer telefone) {
        this.telefone = telefone;
    }

    public Integer getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(Integer estadoCivil) {
        this.estadoCivil = estadoCivil;
    }
}
