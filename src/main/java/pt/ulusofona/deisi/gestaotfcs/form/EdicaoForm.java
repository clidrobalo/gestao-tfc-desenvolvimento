package pt.ulusofona.deisi.gestaotfcs.form;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class EdicaoForm {

    private long idTFC;

    private String idStringTFC;

    private long id;

    private String preponente;

    private String entidade;

    private long entidadeTFC;

    @NotEmpty(message = "O titulo necessita estar preenchido")
    private String titulo;

    @NotEmpty(message = "A descricao necessita estar preenchida")
    @Size(max = 500, message = "A descrição só pode ter no máximo 500 caracteres")
    private String descricao;

    @NotNull(message = "Tem que escolher pelo menos um curso")
    private int cursoAssociado;

    @NotEmpty(message = "Tem que escolher pelo menos uma disciplina")
    private List<Long> disciplinas;

    @NotEmpty(message = "Necessita pelo menos preencher uma tecnologia")
    private List<String> tecnologias;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getCurso() {
        return cursoAssociado;
    }

    public void setCurso(int curso) {
        this.cursoAssociado = curso;
    }

    public String getPreponente() {
        return preponente;
    }

    public void setPreponente(String preponente) {
        this.preponente = preponente;
    }

    public List<Long> getDisciplinas() {
        return disciplinas;
    }

    public void setDisciplinas(List<Long> disciplinas) {
        this.disciplinas = disciplinas;
    }

    public int getCursoAssociado() {
        return cursoAssociado;
    }

    public void setCursoAssociado(int cursoAssociado) {
        this.cursoAssociado = cursoAssociado;
    }

    public List<String> getTecnologias() {
        return tecnologias;
    }

    public void setTecnologias(List<String> tecnologias) {
        this.tecnologias = tecnologias;
    }

    public String getEntidade() {
        return entidade;
    }

    public void setEntidade(String entidade) {
        this.entidade = entidade;
    }

    public long getIdTFC() {
        return idTFC;
    }

    public void setIdTFC(long idTFC) {
        this.idTFC = idTFC;
    }

    public String getIdStringTFC() {
        return idStringTFC;
    }

    public void setIdStringTFC(String idStringTFC) {
        this.idStringTFC = idStringTFC;
    }

    public long getEntidadeTFC() {
        return entidadeTFC;
    }

    public void setEntidadeTFC(long entidadeTFC) {
        this.entidadeTFC = entidadeTFC;
    }
}
