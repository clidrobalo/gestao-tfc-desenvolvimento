package pt.ulusofona.deisi.gestaotfcs.form;

import org.hibernate.validator.constraints.NotEmpty;
import pt.ulusofona.deisi.gestaotfcs.data.Grupo;
import pt.ulusofona.deisi.gestaotfcs.data.ProfessorDEISI;

import javax.validation.constraints.Size;
import java.util.List;

public class AlunoForm {
    private Long id;

    private String preponente;

    private String entidade;

    private long entidadeTFC;
    private long cursoTFC;
    private List<Long> disciplinasTFC;
    private List<Long> tecnologiasTFC;
    private String numero2Aluno;
    private String nome2Aluno;

    @NotEmpty(message = "O titulo necessita estar preenchido")
    private String titulo;

    @NotEmpty(message = "A descricao necessita estar preenchida")
    @Size(max = 500, message = "A descrição só pode ter no máximo 500 caracteres")
    private String descricao;

    private Long cursoAssociado;

    private List<Long> disciplinas;

    private List<String> tecnologias;

    private String orientador;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Long getCurso() {
        return cursoAssociado;
    }

    public void setCurso(Long curso) {
        this.cursoAssociado = curso;
    }

    public String getPreponente() {
        return preponente;
    }

    public void setPreponente(String preponente) {
        this.preponente = preponente;
    }

    public List<Long> getDisciplinas() {
        return disciplinas;
    }

    public void setDisciplinas(List<Long> disciplinas) {
        this.disciplinas = disciplinas;
    }

    public Long getCursoAssociado() {
        return cursoAssociado;
    }

    public void setCursoAssociado(Long cursoAssociado) {
        this.cursoAssociado = cursoAssociado;
    }

    public List<String> getTecnologias() {
        return tecnologias;
    }

    public void setTecnologias(List<String> tecnologias) {
        this.tecnologias = tecnologias;
    }

    public String getEntidade() {
        return entidade;
    }

    public void setEntidade(String entidade) {
        this.entidade = entidade;
    }

    public long getEntidadeTFC() {
        return entidadeTFC;
    }

    public void setEntidadeTFC(long entidadeTFC) {
        this.entidadeTFC = entidadeTFC;
    }

    public long getCursoTFC() {
        return cursoTFC;
    }

    public void setCursoTFC(long cursoTFC) {
        this.cursoTFC = cursoTFC;
    }

    public List<Long> getDisciplinasTFC() {
        return disciplinasTFC;
    }

    public void setDisciplinasTFC(List<Long> disciplinasTFC) {
        this.disciplinasTFC = disciplinasTFC;
    }

    public List<Long> getTecnologiasTFC() {
        return tecnologiasTFC;
    }

    public void setTecnologiasTFC(List<Long> tecnologiasTFC) {
        this.tecnologiasTFC = tecnologiasTFC;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero2Aluno() {
        return numero2Aluno;
    }

    public void setNumero2Aluno(String numero2Aluno) {
        this.numero2Aluno = numero2Aluno;
    }

    public String getNome2Aluno() {
        return nome2Aluno;
    }

    public void setNome2Aluno(String nome2Aluno) {
        this.nome2Aluno = nome2Aluno;
    }

    public String getOrientador() {
        return orientador;
    }

    public void setOrientador(String orientador) {
        this.orientador = orientador;
    }
}
