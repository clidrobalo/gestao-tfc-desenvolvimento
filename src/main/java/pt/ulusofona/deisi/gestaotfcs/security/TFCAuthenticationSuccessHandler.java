package pt.ulusofona.deisi.gestaotfcs.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pt.ulusofona.deisi.auth.client.DEISIAuthenticationToken;
import pt.ulusofona.deisi.gestaotfcs.controller.FormController;
import pt.ulusofona.deisi.gestaotfcs.services.ServiceInformacao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Service
public class TFCAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private static Logger LOG = LoggerFactory.getLogger(FormController.class);

    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                        Authentication authentication) throws IOException, ServletException {

        String nome = ((DEISIAuthenticationToken) authentication).getDisplayname();
        String numero = ((DEISIAuthenticationToken) authentication).getUsername();

        LOG.info("User coming from DEISI Auth: " + numero + " - " + nome);

        // TODO: remove this after fixing DEISI Auth
        if (nome == null || nome.isEmpty()) {
            nome = numero;
        }

        ServiceInformacao serviceInformacao = new ServiceInformacao();

        if (authentication.getAuthorities().toString().contains("ROLE_STUDENT")) {
            serviceInformacao.criaUtilizadorAluno(em, nome, numero);
        } else if (authentication.getAuthorities().toString().contains("ROLE_TEACHER")) {
            serviceInformacao.criaUtilizadorProfessor(em, nome, numero);
        }

        httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/");
    }
}
