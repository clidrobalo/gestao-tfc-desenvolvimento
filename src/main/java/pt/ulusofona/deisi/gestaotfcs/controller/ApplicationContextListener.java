package pt.ulusofona.deisi.gestaotfcs.controller;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pt.ulusofona.deisi.gestaotfcs.data.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Component
@Transactional
public class ApplicationContextListener implements ApplicationListener<ContextRefreshedEvent> {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        List<Curso> cursos = em.createQuery("SELECT u FROM Curso u", Curso.class)
                .getResultList();
        if (cursos == null || cursos.isEmpty()){
            em.persist(new Curso( "LEI + LIG"));
            em.persist(new Curso( "Lic. Eng. Informática"));
            em.persist(new Curso("Lic. Inf. Gestão"));
        }

        Sessao sessao= new Sessao("10-10-2010","10:00", "12:00", "10");

      /*  List<TFC> tfc = em.createQuery("SELECT t FROM TFC t", TFC.class)
                .getResultList();
        if (tfc == null || tfc.isEmpty()){
            TFC novotfc = new TFC("TF1","Lic. Eng. Informática", "p111");
            em.persist(novotfc);
            novotfc.setIdtfc("DEISI" + novotfc.getId());
            novotfc.setEstado("Aguarda Escolha");
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = new Date();
            novotfc.setDataEstado(dateFormat.format(date));
            novotfc.setDataProposta(dateFormat.format(date));
            em.persist(novotfc);
            TFC novotfc2 = new TFC("TF1","Lic. Eng. Informática", "p112");
            em.persist(novotfc2);
            novotfc2.setIdtfc("DEISI" + novotfc2.getId());
            novotfc2.setEstado("Aguarda Escolha");
            novotfc2.setDataEstado(dateFormat.format(date));
            novotfc2.setDataProposta(dateFormat.format(date));
            em.persist(novotfc);
        }*/



        List<Disciplina> disciplinas = em.createQuery("SELECT b FROM Disciplina b", Disciplina.class)
                .getResultList();
        if (disciplinas == null || disciplinas.isEmpty()){
            /*Conjunto de Disciplinas que pertencem aos cursos LEI e LIG*/
            em.persist(new Disciplina("Análise e Conceção de Sistemas", 1));
            em.persist(new Disciplina("Arquitetura de Computadores", 1));
            em.persist(new Disciplina("Arquitetura de Sistemas Empresariais", 1));
            em.persist(new Disciplina("Bases de Dados", 1));
            em.persist(new Disciplina( "Engenharia de Software", 1));
            em.persist(new Disciplina( "Fundamentos de Programação",1));
            em.persist(new Disciplina( "Inteligência Artificial", 1));
            em.persist(new Disciplina( "Interação Humano-Máquina",1));
            em.persist(new Disciplina( "Linguagens de Programação I", 1));
            em.persist(new Disciplina( "Linguagens de Programação II", 1));
            em.persist(new Disciplina( "Matemática I", 1));
            em.persist(new Disciplina( "Matemática II", 1));
            em.persist(new Disciplina( "Redes de Computadores", 1));
            em.persist(new Disciplina( "Sistemas de Informação Multimédia", 1));
            em.persist(new Disciplina( "Sistemas de Suporte à Decisão", 1));
            em.persist(new Disciplina( "Sistemas Operativos", 1));

            /*Conjunto de Disciplinas que pertencem ao curso LEI*/
            em.persist(new Disciplina("Álgebra Linear", 2));
            em.persist(new Disciplina("Algoritmia e Estruturas de Dados", 2));
            em.persist(new Disciplina("Arquiteturas Avançadas de Computadores", 2));
            em.persist(new Disciplina("Competências Comportamentais", 2));
            em.persist(new Disciplina("Compiladores", 2));
            em.persist(new Disciplina("Computação Distribuída", 2));
            em.persist(new Disciplina("Computação Gráfica", 2));
            em.persist(new Disciplina("Computação Móvel", 2));
            em.persist(new Disciplina("Fundamentos de Física", 2));
            em.persist(new Disciplina("Matemática Discreta", 2));
            em.persist(new Disciplina("Programação Web", 2));
            em.persist(new Disciplina("Segurança Informática", 2));
            em.persist(new Disciplina("Sinais e Sistemas", 2));
            em.persist(new Disciplina("Sistemas Digitais", 2));

            /*Conjunto de Disciplinas que pertencem ao curso LIG*/
            em.persist(new Disciplina("Auditoria de Sistemas de Informação", 3));
            em.persist(new Disciplina("Cálculo Financeiro", 3));
            em.persist(new Disciplina("Contabilidade", 3));
            em.persist(new Disciplina("Controlo de Gestão", 3));
            em.persist(new Disciplina("Data Mining", 3));
            em.persist(new Disciplina("Ética SocioProfissional", 3));
            em.persist(new Disciplina("Fundamentos de Sistemas de Informação", 3));
            em.persist(new Disciplina("Gestão Financeira", 3));
            em.persist(new Disciplina("Instrumentos de Gestão", 3));
            em.persist(new Disciplina("Investigação Operacional", 3));
            em.persist(new Disciplina("Métricas Empresariais", 3));
            em.persist(new Disciplina("Motivação e Liderança", 3));
            em.persist(new Disciplina("Sistemas Móveis Empresariais", 3));
            em.persist(new Disciplina("Teoria e Prática de Marketing", 3));


        }

        List<Tecnologia> tecnologias = em.createQuery("SELECT t FROM Tecnologia t", Tecnologia.class)
                .getResultList();
        if (tecnologias == null || tecnologias.isEmpty()){
            em.persist(new Tecnologia("Arduino"));
            em.persist(new Tecnologia("Blockchain"));
            em.persist(new Tecnologia("Bluetooth"));
            em.persist(new Tecnologia("Bootstrap"));
            em.persist(new Tecnologia("C"));
            em.persist(new Tecnologia("C#"));
            em.persist(new Tecnologia("CSS"));
            em.persist(new Tecnologia("ESP8266"));
            em.persist(new Tecnologia("Framework Symphony"));
            em.persist(new Tecnologia("HTML5"));
            em.persist(new Tecnologia("IBM BlueMix"));
            em.persist(new Tecnologia("Java"));
            em.persist(new Tecnologia("JavaScript"));
            em.persist(new Tecnologia("Joomla"));
            em.persist(new Tecnologia("JSON"));
            em.persist(new Tecnologia("Kotlin"));
            em.persist(new Tecnologia("Linux"));
            em.persist(new Tecnologia("Microsoft Azure"));
            em.persist(new Tecnologia("Mobile"));
            em.persist(new Tecnologia("MS-SQL"));
            em.persist(new Tecnologia("MySQL"));
            em.persist(new Tecnologia("NetIQ IDM"));
            em.persist(new Tecnologia("Object Oriented PHP"));
            em.persist(new Tecnologia("Open Source"));
            em.persist(new Tecnologia("OutSystems Mobile"));
            em.persist(new Tecnologia("OutSystems Web"));
            em.persist(new Tecnologia("PHP"));
            em.persist(new Tecnologia("PHPNuke"));
            em.persist(new Tecnologia("PowerDesigner"));
            em.persist(new Tecnologia("Protocolo MQTT"));
            em.persist(new Tecnologia("Python"));
            em.persist(new Tecnologia("Raspberry PI"));
            em.persist(new Tecnologia("Spring MVC"));
            em.persist(new Tecnologia("SQL"));
            em.persist(new Tecnologia("Web"));
            em.persist(new Tecnologia("WiFi"));
            em.persist(new Tecnologia("WordPress"));
        }

        List<Utilizador> utilizador = em.createQuery("SELECT u FROM Utilizador u", Utilizador.class)
                .getResultList();
        if (utilizador == null || utilizador.isEmpty()){
            ProfessorDEISI pedroAlves = new ProfessorDEISI("p4997", "Pedro Alves", "email@professordeisi.pt", 222);
            em.persist(pedroAlves);
            Utilizador pedroAlvesU = new Utilizador();
            pedroAlvesU.setIdIdentificacao(pedroAlves.getNumeroProfessor());
            pedroAlvesU.setTipoUtilizador("ProfessorDEISI");
            pedroAlvesU.setCoordenador(true);
            em.persist(pedroAlvesU);

            ProfessorDEISI fernandoTeodosio = new ProfessorDEISI("p3478", "Fernando Teodósio", "email@professordeisi.pt", 222);
            em.persist(fernandoTeodosio);
            Utilizador fernandoTeodosioU = new Utilizador();
            fernandoTeodosioU.setIdIdentificacao(fernandoTeodosio.getNumeroProfessor());
            fernandoTeodosioU.setTipoUtilizador("ProfessorDEISI");
            fernandoTeodosioU.setCoordenador(false);
            em.persist(fernandoTeodosioU);

            ProfessorDEISI luisGomes = new ProfessorDEISI("p2703", "Luís Gomes", "email@professordeisi.pt", 222);
            em.persist(luisGomes);
            Utilizador luisGomesU = new Utilizador();
            luisGomesU.setIdIdentificacao(luisGomes.getNumeroProfessor());
            luisGomesU.setTipoUtilizador("ProfessorDEISI");
            luisGomesU.setCoordenador(true);
            em.persist(luisGomesU);

            ProfessorDEISI pedroSaCosta = new ProfessorDEISI("p5404", "Pedro Sá Costa", "email@professordeisi.pt", 222);
            em.persist(pedroSaCosta);
            Utilizador pedroSaCostaU = new Utilizador();
            pedroSaCostaU.setIdIdentificacao(pedroSaCosta.getNumeroProfessor());
            pedroSaCostaU.setTipoUtilizador("ProfessorDEISI");
            pedroSaCostaU.setCoordenador(false);
            em.persist(pedroSaCostaU);

            ProfessorDEISI joseFaisca = new ProfessorDEISI("p3025", "José Faisca", "email@professordeisi.pt", 222);
            em.persist(joseFaisca);
            Utilizador joseFaiscaU = new Utilizador();
            joseFaiscaU.setIdIdentificacao(joseFaisca.getNumeroProfessor());
            joseFaiscaU.setTipoUtilizador("ProfessorDEISI");
            joseFaiscaU.setCoordenador(false);
            em.persist(joseFaiscaU);

            ProfessorDEISI acacioCarmona = new ProfessorDEISI("p2", "Acácio Carmona", "email@professordeisi.pt", 222);
            em.persist(acacioCarmona);
            Utilizador acacioCarmonaU = new Utilizador();
            acacioCarmonaU.setIdIdentificacao(acacioCarmona.getNumeroProfessor());
            acacioCarmonaU.setTipoUtilizador("ProfessorDEISI");
            acacioCarmonaU.setCoordenador(false);
            em.persist(acacioCarmonaU);

            ProfessorDEISI ruiRibeiro = new ProfessorDEISI("p3113", "Rui Ribeiro", "email@professordeisi.pt", 222);
            em.persist(ruiRibeiro);
            Utilizador ruiRibeiroU = new Utilizador();
            ruiRibeiroU.setIdIdentificacao(ruiRibeiro.getNumeroProfessor());
            ruiRibeiroU.setTipoUtilizador("ProfessorDEISI");
            ruiRibeiroU.setCoordenador(true);
            em.persist(ruiRibeiroU);

            ProfessorDEISI manuelMarquesPita = new ProfessorDEISI("p5265", "Manuel Marques Pita", "email@professordeisi.pt", 222);
            em.persist(manuelMarquesPita);
            Utilizador manuelMarquesPitaU = new Utilizador();
            manuelMarquesPitaU.setIdIdentificacao(manuelMarquesPita.getNumeroProfessor());
            manuelMarquesPitaU.setTipoUtilizador("ProfessorDEISI");
            manuelMarquesPitaU.setCoordenador(false);
            em.persist(manuelMarquesPitaU);

            ProfessorDEISI brunoCipriano = new ProfessorDEISI("p4453", "Bruno Cipriano", "email@professordeisi.pt", 222);
            em.persist(brunoCipriano);
            Utilizador brunoCiprianoU = new Utilizador();
            brunoCiprianoU.setIdIdentificacao(brunoCipriano.getNumeroProfessor());
            brunoCiprianoU.setTipoUtilizador("ProfessorDEISI");
            brunoCiprianoU.setCoordenador(false);
            em.persist(brunoCiprianoU);

            ProfessorDEISI pedroSerra = new ProfessorDEISI("p5403", "Pedro Serra", "email@professordeisi.pt", 222);
            em.persist(pedroSerra);
            Utilizador pedroSerraU = new Utilizador();
            pedroSerraU.setIdIdentificacao(pedroSerra.getNumeroProfessor());
            pedroSerraU.setTipoUtilizador("ProfessorDEISI");
            pedroSerraU.setCoordenador(false);
            em.persist(pedroSerraU);

            ProfessorDEISI francescoCostigliola = new ProfessorDEISI("p4855", "Francesco Costigliola", "email@professordeisi.pt", 222);
            em.persist(francescoCostigliola);
            Utilizador francescoCostigliolaU = new Utilizador();
            francescoCostigliolaU.setIdIdentificacao(francescoCostigliola.getNumeroProfessor());
            francescoCostigliolaU.setTipoUtilizador("ProfessorDEISI");
            francescoCostigliolaU.setCoordenador(false);
            em.persist(francescoCostigliolaU);

            ProfessorDEISI manuelCostaLeite = new ProfessorDEISI("p681", "Manuel Costa Leite", "email@professordeisi.pt", 222);
            em.persist(manuelCostaLeite);
            Utilizador manuelCostaLeiteU = new Utilizador();
            manuelCostaLeiteU.setIdIdentificacao(manuelCostaLeite.getNumeroProfessor());
            manuelCostaLeiteU.setTipoUtilizador("ProfessorDEISI");
            manuelCostaLeiteU.setCoordenador(false);
            em.persist(manuelCostaLeiteU);

            ProfessorDEISI pedroHenriques = new ProfessorDEISI("p4671", "Pedro Henriques", "email@professordeisi.pt", 222);
            em.persist(pedroHenriques);
            Utilizador pedroHenriquesU = new Utilizador();
            pedroHenriquesU.setIdIdentificacao(pedroHenriques.getNumeroProfessor());
            pedroHenriquesU.setTipoUtilizador("ProfessorDEISI");
            pedroHenriquesU.setCoordenador(false);
            em.persist(pedroHenriquesU);

            ProfessorDEISI sergioFerreira = new ProfessorDEISI("p1059", "Sérgio Ferreira", "email@professordeisi.pt", 222);
            em.persist(sergioFerreira);
            Utilizador sergioFerreiraU = new Utilizador();
            sergioFerreiraU.setIdIdentificacao(sergioFerreira.getNumeroProfessor());
            sergioFerreiraU.setTipoUtilizador("ProfessorDEISI");
            sergioFerreiraU.setCoordenador(false);
            em.persist(sergioFerreiraU);

            ProfessorDEISI joaoNunoCorreia = new ProfessorDEISI("p5354", "João Nuno Correia", "email@professordeisi.pt", 222);
            em.persist(joaoNunoCorreia);
            Utilizador joaoNunoCorreiaU = new Utilizador();
            joaoNunoCorreiaU.setIdIdentificacao(joaoNunoCorreia.getNumeroProfessor());
            joaoNunoCorreiaU.setTipoUtilizador("ProfessorDEISI");
            joaoNunoCorreiaU.setCoordenador(false);
            em.persist(joaoNunoCorreiaU);

            ProfessorDEISI pedroPerdigao = new ProfessorDEISI("p5617", "Pedro Perdigão", "email@professordeisi.pt", 222);
            em.persist(pedroPerdigao);
            Utilizador pedroPerdigaoU = new Utilizador();
            pedroPerdigaoU.setIdIdentificacao(pedroPerdigao.getNumeroProfessor());
            pedroPerdigaoU.setTipoUtilizador("ProfessorDEISI");
            pedroPerdigaoU.setCoordenador(true);
            em.persist(pedroPerdigaoU);

            // just for tests
            Aluno a111 = new Aluno("a111", "Aluno Teste 1", "LEI", "email@aluno.pt", 222);
            em.persist(a111);
            Utilizador a111U = new Utilizador();
            a111U.setIdIdentificacao(a111.getNumeroAluno());
            a111U.setTipoUtilizador("Aluno");
            em.persist(a111U);

            Aluno a112 = new Aluno("a112", "Aluno Teste 2", "LEI", "email@aluno.pt", 222);
            em.persist(a112);
            Utilizador a112U = new Utilizador();
            a112U.setIdIdentificacao(a112.getNumeroAluno());
            a112U.setTipoUtilizador("Aluno");
            em.persist(a112U);

        }

        //---------- Add by clid-stelio

        List<CriterioAvaliacao> criterioAvaliacoes = em.createQuery("SELECT c FROM CriterioAvaliacao c", CriterioAvaliacao.class)
                .getResultList();
        if (criterioAvaliacoes == null || criterioAvaliacoes.isEmpty()){
            CriterioAvaliacao c1  = new CriterioAvaliacao("Pertinência e relevância do trabalho", 20.0, 0);
            em.persist(c1);
            CriterioAvaliacao c2  = new CriterioAvaliacao("Inovação da solução apresentada", 10.0, 0);
            em.persist(c2);
            CriterioAvaliacao c3  = new CriterioAvaliacao( "Aplicabilidade da solução", 15.0, 0);
            em.persist(c3);
            CriterioAvaliacao c4  = new CriterioAvaliacao( "Metodologia e planeamento do trabalho", 10.0, 0);
            em.persist(c4);
            CriterioAvaliacao c5  = new CriterioAvaliacao("Estrutura e conteúdo do relatório", 15.0, 0);
            em.persist(c5);
            CriterioAvaliacao c6  = new CriterioAvaliacao("Apresentação oral e discussão", 30.0, 0);
            em.persist(c6);
        }

        List<ItemCriterioAvaliacao> itemCriterioAvaliacaos = em.createQuery("SELECT i FROM ItemCriterioAvaliacao i", ItemCriterioAvaliacao.class)
                .getResultList();
        if (itemCriterioAvaliacaos == null || itemCriterioAvaliacaos.isEmpty()){
            ItemCriterioAvaliacao ic1 = new ItemCriterioAvaliacao("O aluno/grupo consegue identificar claramente o problema que se propõem resolver, demonstrar conhecimento do contexto tecnológico, empresarial e concorrencial no qual o trabalho se enquadra?");
            //Conectar ic1 a um criterio de avaliacao
            ic1.setIdICriterioAvaliacao(1L);
            em.persist(ic1);
            ItemCriterioAvaliacao ic2 = new ItemCriterioAvaliacao("O aluno/grupo consegue demonstrar que a sua solução se diferencia de outras soluções já existentes e conhecidas?");
            ic2.setIdICriterioAvaliacao(2L);
            em.persist(ic2);
            ItemCriterioAvaliacao ic3 = new ItemCriterioAvaliacao("aluno/grupo consegue demonstrar a aplicabilidade da solução proposta em contexto real (académico ou empresarial), deforma a que o trabalho não se esgotasse no dia da apresentação final?");
            ic3.setIdICriterioAvaliacao(3L);
            em.persist(ic3);
            ItemCriterioAvaliacao ic4 = new ItemCriterioAvaliacao("O aluno/grupo consegue demonstrar que o planeamento do trabalho é adequado à solução identificada e que o cronograma apresentado permitirá desenvolver os requisitos propostos dentro dos critérios de aceitação definidos?");
            ic4.setIdICriterioAvaliacao(4L);
            em.persist(ic4);
            ItemCriterioAvaliacao ic5 = new ItemCriterioAvaliacao("O aluno/grupo conseguiu entregar um relatório com uma estrutura adequada e de acordo com o regulamento, em que se consegue entender as características do trabalho final e onde todos os conceitos sejam apresentados com clareza e rigor científico?");
            ic5.setIdICriterioAvaliacao(5L);
            em.persist(ic5);
            ItemCriterioAvaliacao ic6 = new ItemCriterioAvaliacao("O aluno/grupo conseguiu apresentar de forma clara e correcta o trabalho que desenvolveram?");
            ic5.setIdICriterioAvaliacao(6L);
            em.persist(ic6);
        }
    }
}

