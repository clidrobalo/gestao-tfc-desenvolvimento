package pt.ulusofona.deisi.gestaotfcs.controller;

import org.hibernate.Criteria;
import org.hibernate.exception.SQLGrammarException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pt.ulusofona.deisi.auth.client.DEISIAuthenticationToken;
import pt.ulusofona.deisi.gestaotfcs.data.*;
import pt.ulusofona.deisi.gestaotfcs.form.*;
import pt.ulusofona.deisi.gestaotfcs.services.MenuBuilder;
import pt.ulusofona.deisi.gestaotfcs.services.ServiceInformacao;
import pt.ulusofona.deisi.gestaotfcs.services.ServiceInscricao;
import pt.ulusofona.deisi.gestaotfcs.services.ServiceTFC;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.DigestInputStream;
import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@Transactional
public class FormController {

    private static Logger LOG = LoggerFactory.getLogger(FormController.class);

    @PersistenceContext
    private EntityManager em;

    private MomentoAvaliacao mA = MomentoAvaliacao.getInstance();

    @Autowired
    MenuBuilder menuBuilder;

    @ModelAttribute("menus")
    public List<MenuBuilder.Menu> getMenus(HttpServletRequest request) {
        if (request.isUserInRole("TFC_COORDINATION")) {
            return menuBuilder.constroiMenuCoordenacao();
        }

        if (request.isUserInRole("STUDENT")) {
            return menuBuilder.constroiMenuAluno();
        }

        if (request.isUserInRole("TEACHER")) {
            return menuBuilder.constroiMenuProfessor();
        }


        return null;
    }

    @ModelAttribute("utilizador")
    public String getUtilizador(Principal user) {
        if (user == null) {
            return null;
        }
        Utilizador utilizador = em.createQuery("select u from Utilizador u where u.idIdentificacao=:user", Utilizador.class)
                .setParameter("user", user.getName()).getSingleResult();

        return nomeUtilizador(user.getName());
    }

    /**
     * Método que funciona apenas paa testes
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String getList(ModelMap model) {

        List<Inscricao> listaInscricoes = new ArrayList<>();
        listaInscricoes = em.createQuery("select ins from Inscricao ins", Inscricao.class).getResultList();
        model.put("listainscricoes", listaInscricoes);

        List<Utilizador> utilizadores = new ArrayList<>();
        utilizadores = em.createQuery("select u from Utilizador u", Utilizador.class).getResultList();
        model.put("utilizadores", utilizadores);

        List<TFC> tfcs = new ArrayList<>();
        tfcs = em.createQuery("select t from TFC t", TFC.class).getResultList();
        model.put("tfcs", tfcs);

        List<TFCDisciplina> tfcdisc = new ArrayList<>();
        tfcdisc = em.createQuery("select t from TFCDisciplina t", TFCDisciplina.class).getResultList();
        model.put("tfcdisc", tfcdisc);

        List<TFCCurso> tfcCursos = new ArrayList<>();
        tfcCursos = em.createQuery("select tc from TFCCurso tc", TFCCurso.class).getResultList();
        model.put("tfccurso", tfcCursos);

        List<TFCTecnologia> tfcTecnologias = new ArrayList<>();
        tfcTecnologias = em.createQuery("select tt from TFCTecnologia tt", TFCTecnologia.class).getResultList();
        model.put("tfctecnologias", tfcTecnologias);

        List<Tecnologia> tecnologias = new ArrayList<>();
        tecnologias = em.createQuery("select te from Tecnologia te", Tecnologia.class).getResultList();
        model.put("tecnologias", tecnologias);

        List<Empresa_EntidadeExterna> emp = new ArrayList<>();
        emp = em.createQuery("select e from Empresa_EntidadeExterna e", Empresa_EntidadeExterna.class).getResultList();
        model.put("emp", emp);
        model.put("tipoUtilizador", "professor");

        List<Grupo> grupos = new ArrayList<>();
        grupos = em.createQuery("select g from Grupo g", Grupo.class).getResultList();
        model.put("grupos", grupos);

        List<ProfessorNDEISI> professorNDEISIS = new ArrayList<>();
        professorNDEISIS = em.createQuery("select pndeisi from ProfessorNDEISI pndeisi", ProfessorNDEISI.class).getResultList();
        model.put("ndeisi", professorNDEISIS);

        return "list";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home() {
        return "home";
    }

    @RequestMapping(value = "/historico", method = RequestMethod.GET)
    public String listarHistorico(ModelMap model) {
        List<HistoricoTFC> historicoTFCS = em.createQuery("select h from HistoricoTFC h", HistoricoTFC.class)
                .getResultList();
        model.put("listahistorico", historicoTFCS);
        return "historico";
    }

    @RequestMapping(value = "/listarpropostas", method = RequestMethod.GET)
    public String listarPropostasCoordenacao(ModelMap model) {
        ServiceTFC serviceTFC = new ServiceTFC();
        model.put("listatfc", serviceTFC.recolheTFCS(em));
        List<Utilizador> utilizadores = em.createQuery("select u from Utilizador u", Utilizador.class).getResultList();
        model.put("listautilizadores", utilizadores);
        return "listarpropostas";
    }

    @RequestMapping(value = "/listar", method = RequestMethod.GET)
    public String listarPropostas(ModelMap model, Principal user) {
        List<TFC> listaTFC = new ArrayList<>();
        List<TFC> listaTFCGrupo = new ArrayList<>();
        try {
            List<Grupo> grupos = em.createQuery("select g from Grupo g", Grupo.class).getResultList();
            for (Grupo grupo : grupos) {
                if (grupo.getIdNumeroAluno1().equalsIgnoreCase(user.getName()) ||
                        grupo.getIdNumeroAluno1().equalsIgnoreCase(user.getName())) {
                    listaTFCGrupo = em.createQuery("select tfc from TFC tfc where tfc.preponente=:preponente", TFC.class)
                            .setParameter("preponente", "G" + grupo.getId()).getResultList();
                }
            }
        } catch (NoResultException e) {

        }
        listaTFC = em.createQuery("select tfc from TFC tfc where tfc.preponente=:preponente", TFC.class)
                .setParameter("preponente", user.getName()).getResultList();
        if (listaTFCGrupo.size() > 0) {
            listaTFC.addAll(listaTFCGrupo);
        }
        Utilizador utilizador = em.createQuery("select u from Utilizador u where u.idIdentificacao=:preponente",
                Utilizador.class).setParameter("preponente", user.getName()).getSingleResult();

        model.put("tipoUtilizador", utilizador.getTipoUtilizador());
        model.put("listaPreponente", listaTFC);
        return "listar";
    }

    @RequestMapping(value = "/listatfc", method = RequestMethod.GET)
    public String listarPropostasInscricao(ModelMap model, Principal user) {
        ServiceTFC serviceTFC = new ServiceTFC();
        ServiceInscricao serviceInscricao = new ServiceInscricao();

        List<TFC> tfc = serviceTFC.recolheInfoTFCInscricao(em, user.getName());
        List<Inscricao> listaInscricoes = serviceInscricao.recolherInscricaoAluno(em, user.getName());

        model.put("listainscricoes", listaInscricoes);
        model.put("listatfc", tfc);
        model.put("orientadores", getProfessores());

        return "listatfc";
    }

    /**
     * Função para que o aluno tenha acesso a toda a informação sobre um TFC e realize a inscrição no mesmo, caso pretenda
     *
     * @param model
     * @param id
     * @param user
     * @return
     */
    @RequestMapping(value = "/vertfc/{id}", method = RequestMethod.GET)
    public String verTFCInscricao(ModelMap model, @PathVariable("id") Long id, Principal user) {
        ServiceTFC serviceTFC = new ServiceTFC();
        ServiceInformacao serviceInformacao = new ServiceInformacao();
        HashMap<String, Object> resultado = serviceTFC.vaiBuscarInfoTFC(id, em);
        TFC tfc = (TFC) resultado.get("tfc");
        String tipoUtilizador = (String) resultado.get("tipoUtilizador");

        AvaliaPropostaForm avaliaPropostaForm = serviceTFC.preencheFormAvaliacaoProposta(resultado.get("form"), tipoUtilizador);
        InscricaoForm inscricaoForm = new InscricaoForm();

        ServiceInscricao serviceInscricao = new ServiceInscricao();
        List<Inscricao> listaInscricoes = serviceInscricao.recolherInscricaoAluno(em, user.getName());

        avaliaPropostaForm.setTipoUtilizador(tipoUtilizador);
        avaliaPropostaForm.setId(tfc.getId());
        avaliaPropostaForm.setMotivoRecusa(tfc.getMotivoRecusa());
        avaliaPropostaForm.setAvaliacao(tfc.getEstado());

        inscricaoForm.setIdTFC(tfc.getIdtfc());
        for (Inscricao inscricao : listaInscricoes) {
            if (inscricao.getIdTFC().equalsIgnoreCase(tfc.getIdtfc())) {
                inscricaoForm.setId(inscricao.getId());
                if (inscricao.getIdTFC().equalsIgnoreCase(inscricaoForm.getIdTFC())) {
                    inscricaoForm.setOrdemEscolha(inscricao.getOrdemEscolha());
                    if (inscricao.getIdNumeroGrupo() != null) {
                        Grupo grupo = serviceInformacao.informacaoGrupo(em, inscricao.getIdNumeroGrupo());
                        inscricaoForm.setNumeroGrupo(grupo.getId());
                        inscricaoForm.setNumero2Aluno(grupo.getIdNumeroAluno2());
                        inscricaoForm.setNome2Aluno(serviceInformacao.informacaoAluno(em, grupo.getIdNumeroAluno2())
                                .getNome());
                    }
                    break;
                }
            }
        }

        model.put("inscricaoForm", inscricaoForm);
        model.put("listainscricoes", listaInscricoes);
        model.put("tipoUtilizador", tipoUtilizador);
        model.put("avaliaPropostaForm", avaliaPropostaForm);
        model.put("tfc", tfc);
        model.put("orientadores", getProfessores());
        model.put("cursos", getCursos());
        model.put("listadisciplinas", getDisciplinas());
        model.put("listatecnologias", getTecnologias());

        return "verinfotfc";
    }

    /**
     * Função para registar as inscrições de um Aluno.
     *
     * @param inscricaoForm
     * @param bindingResult
     * @param model
     * @param ra
     * @param user
     * @return
     */
    @RequestMapping(value = "/verinfotfc", method = RequestMethod.POST)
    public String registaInscricao(@Valid @ModelAttribute("inscricaoForm") InscricaoForm inscricaoForm
            , BindingResult bindingResult, ModelMap model, RedirectAttributes ra, Principal user) {

        if (bindingResult.hasErrors()) {
            model.put("inscricaoForm", inscricaoForm);
            model.put("orientadores", getProfessores());
            model.put("cursos", getCursos());
            model.put("listadisciplinas", getDisciplinas());
            model.put("listatecnologias", getTecnologias());
            return "verinfotfc";
        }

        if (inscricaoForm.getOrdemEscolha() != 0) {
            HashMap<String, Object> novaInscricao = new HashMap<>();
            ServiceInscricao serviceInscricao = new ServiceInscricao();

            novaInscricao.put("idInscricao", inscricaoForm.getId());
            novaInscricao.put("idTFC", inscricaoForm.getIdTFC());
            novaInscricao.put("ordemEscolha", inscricaoForm.getOrdemEscolha());
            novaInscricao.put("numeroAluno", user.getName());
            if (inscricaoForm.getNumero2Aluno() != null) {
                ServiceInformacao serviceInformacao = new ServiceInformacao();

                // aluno 2 pode não existir na tabela de utilizadores
                serviceInformacao.criaUtilizadorAluno(em,
                        inscricaoForm.getNome2Aluno() != null ? inscricaoForm.getNome2Aluno() : inscricaoForm.getNumero2Aluno(),
                        inscricaoForm.getNumero2Aluno());

                Long numeroGrupo = serviceInformacao.recolheNumeroGrupo(user.getName(), inscricaoForm.getNumero2Aluno(), em);
                novaInscricao.put("idNumeroGrupo", numeroGrupo);
            }

            boolean resultado = serviceInscricao.gravaInscricao(em, novaInscricao);

            if (resultado) {
                ra.addFlashAttribute("flashInfo", "Inscrição no TFC feita com sucesso.");
                return "redirect:/listatfc";
            } else {
                ServiceTFC serviceTFC = new ServiceTFC();
                TFC tfc = serviceTFC.recolheTFC(em, inscricaoForm.getIdTFC());
                model.put("inscricaoForm", inscricaoForm);
                model.put("orientadores", getProfessores());
                model.put("cursos", getCursos());
                model.put("listadisciplinas", getDisciplinas());
                model.put("listatecnologias", getTecnologias());
                ra.addFlashAttribute("flashInfo", "Já existe um TFC com prioridade a mesma ordem de preferência. Por favor altere a situação, e realize novo registo.");
                return "redirect:/vertfc/" + tfc.getId();
            }
        }
        return "redirect:/listatfc";
    }

    @RequestMapping(value = "/visualizacaoinscricao", method = RequestMethod.GET)
    public String visualizacaoInscricoes(ModelMap model, Principal user) {

        ServiceInscricao serviceInscricao = new ServiceInscricao();
        ServiceTFC serviceTFC = new ServiceTFC();

        List<TFC> listaTFC = serviceTFC.recolheTFCs(em);
        List<Inscricao> inscricoesAluno = serviceInscricao.recolherInscricaoAluno(em, user.getName());

        model.put("listainscricao", inscricoesAluno);
        model.put("orientadores", getProfessores());
        model.put("listatfc", listaTFC);

        return "visualizacaoinscricao";
    }

    /**
     * Função que irá exibir todos os TFCs que tenham pelo menos uma inscrição
     *
     * @param model
     * @param user
     * @return
     */
    @RequestMapping(value = "/listarTFCInsc", method = RequestMethod.GET)
    public String listarTFCsComInscricao(ModelMap model, Principal user) {

        ServiceTFC serviceTFC = new ServiceTFC();

        List<TFC> listaTFCsComInscricao = serviceTFC.recolheTFCsComInscricao(em);

        model.put("listaTFC", listaTFCsComInscricao);

        return "/listarTFCInsc";
    }

    @RequestMapping(value = "/atribuirtfc/{id}", method = RequestMethod.GET)
    public String atribuirTFC(ModelMap model, @PathVariable("id") long id, Principal user) {

        ServiceTFC serviceTFC = new ServiceTFC();
        ServiceInscricao serviceInscricao = new ServiceInscricao();
        ServiceInformacao serviceInformacao = new ServiceInformacao();

        HashMap<String, Object> resultado = serviceTFC.vaiBuscarInfoTFC(id, em);
        TFC tfc = (TFC) resultado.get("tfc");

        List<Inscricao> listaInscricao = serviceInscricao.recolheInscricaoTFC(em, tfc.getIdtfc());
        List<Long> idsInscricao = serviceInscricao.devolveIDInscricoesTFC(listaInscricao);
        List<Long> idsDisciplinas = serviceInformacao.recolheListaDisciplinasTFC(em, tfc.getIdtfc());

        AtribuicaoForm atribuicaoForm = new AtribuicaoForm();
        atribuicaoForm.setIdTFC(tfc.getIdtfc());
        atribuicaoForm.setListaInscricoes(idsInscricao);
        atribuicaoForm.setDisciplinas(idsDisciplinas);

        /*Neste momento não existe a possibilidade de ter as notas dos alunos nas disciplinas
         * associadas ao TFC. Se mais tarde for possível, será necessário fazer put a esta lista,
         * para depois conseguir apresentar na View.*/
        model.put("atribuicaoForm", atribuicaoForm);
        model.put("tfc", tfc);
        model.put("listadisciplinas", serviceInformacao.getDisciplinas(em));
        model.put("listainscricao", listaInscricao);

        return "atribuirtfc";
    }

    /**
     * Função que irá registar a atribuição do TFC a um determinado aluno.
     *
     * @param atribuicaoForm
     * @param bindingResult
     * @param model
     * @param ra
     * @return
     */
    @RequestMapping(value = "/atribuirtfc", method = RequestMethod.POST)
    public String registaAtribuicao(ModelMap model, @ModelAttribute("atribuicaoForm") AtribuicaoForm atribuicaoForm
            , BindingResult bindingResult, RedirectAttributes ra, Principal user) {

        if (bindingResult.hasErrors()) {
            model.put("atribuicaoForm", atribuicaoForm);
            ServiceInscricao serviceInscricao = new ServiceInscricao();
            ServiceInformacao serviceInformacao = new ServiceInformacao();
            List<Inscricao> listaInscricao = serviceInscricao.recolheInscricaoTFC(em, atribuicaoForm.getIdTFC());
            model.put("listadisciplinas", serviceInformacao.getDisciplinas(em));
            model.put("listainscricao", listaInscricao);
            return "/atribuirtfc";
        }

        ServiceInscricao serviceInscricao = new ServiceInscricao();
        serviceInscricao.registaAtribuicao(em, atribuicaoForm.getNumeroAlunoAtribuido(), getUtilizador(user));

        return "redirect:/listarTFCInsc";

    }

    @RequestMapping(value = "/editar/{id}", method = RequestMethod.GET)
    public String editarProposta(ModelMap model, @PathVariable("id") long id, Principal user) {

        ServiceTFC serviceTFC = new ServiceTFC();
        HashMap<String, Object> resultado = serviceTFC.vaiBuscarInfoTFC(id, em);

        String tipoUtilizador = (String) resultado.get("tipoUtilizador");

        if (tipoUtilizador.equalsIgnoreCase("aluno")) {
            AlunoForm alunoForm = (AlunoForm) resultado.get("form");
            model.put("alunoForm", alunoForm);
        }
        if (tipoUtilizador.equalsIgnoreCase("coordenador")) {
            ProfForm profForm = (ProfForm) resultado.get("form");
            model.put("profForm", profForm);
        }
        if (tipoUtilizador.equalsIgnoreCase("professordeisi")) {
            ProfForm profForm = (ProfForm) resultado.get("form");
            model.put("profForm", profForm);
        }

        TFC tfc = (TFC) resultado.get("tfc");
        model.put("tfc", tfc);
        model.put("tipoUtilizador", tipoUtilizador);
        model.put("orientadores", getProfessores());
        model.put("cursos", getCursos());
        model.put("listadisciplinas", getDisciplinas());
        model.put("listatecnologias", getTecnologias());

        String destino = (String) resultado.get("destino");

        return destino;
    }

    /*Função Get e Post de inscricaoTFC deixa de fazer efeito. Substituido por sistema de registo em apenas um passo*/
    @RequestMapping(value = "/inscricaotfc", method = RequestMethod.GET)
    public String inscreveTFC(ModelMap model, Principal user) {

        ServiceInscricao serviceInscricao = new ServiceInscricao();
        List<Inscricao> listaInscricao = serviceInscricao.recolherInscricaoAluno(em, user.getName());

        ServiceTFC serviceTFC = new ServiceTFC();
        List<TFC> listaTFC = serviceTFC.recolheInfoTFCInscricao(em, user.getName());

        model.put("listainscricao", listaInscricao);
        model.put("orientadores", getProfessores());
        model.put("listatfc", listaTFC);

        return "/inscricaotfc";
    }

    @RequestMapping(value = "/inscricaotfc", method = RequestMethod.POST)
    public String guardaInscricao(ModelMap model, RedirectAttributes ra, Principal user) {

        ServiceInscricao serviceInscricao = new ServiceInscricao();
        boolean resultado = serviceInscricao.registoInscricao(em, user.getName());

        if (!resultado) {
            List<Inscricao> listaInscricao = serviceInscricao.recolherInscricaoAluno(em, user.getName());
            ServiceTFC serviceTFC = new ServiceTFC();
            List<TFC> listaTFC = serviceTFC.recolheInfoTFCInscricao(em, user.getName());
            model.put("listainscricao", listaInscricao);
            model.put("orientadores", getProfessores());
            model.put("listatfc", listaTFC);
            ra.addFlashAttribute("flashInfo", "Existem TFCs com a mesma ordem de preferência. Por favor altere a situação, e realize novo registo.");
            return "/inscricaotfc";
        }

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        ra.addFlashAttribute("flashInfo", "Registo feito com sucesso. Tempo de registo: " + date);
        return "redirect:/listatfc";
    }

    @RequestMapping(value = "/avaliaproposta/{id}", method = RequestMethod.GET)
    public String avaliaProposta(ModelMap model, @PathVariable("id") long id, Principal user) {

        ServiceTFC serviceTFC = new ServiceTFC();
        HashMap<String, Object> resultado = serviceTFC.vaiBuscarInfoTFC(id, em);

        TFC tfc = (TFC) resultado.get("tfc");
        String tipoUtilizador = (String) resultado.get("tipoUtilizador");
        AvaliaPropostaForm avaliaPropostaForm = serviceTFC.preencheFormAvaliacaoProposta(resultado.get("form"), tipoUtilizador);

        avaliaPropostaForm.setTitulo(tfc.getTitulo());
        avaliaPropostaForm.setTipoUtilizador(tipoUtilizador);
        avaliaPropostaForm.setId(tfc.getId());
        avaliaPropostaForm.setMotivoRecusa(tfc.getMotivoRecusa());
        avaliaPropostaForm.setAvaliacao(tfc.getEstado());
        model.put("tipoUtilizador", tipoUtilizador);
        model.put("avaliaPropostaForm", avaliaPropostaForm);
        model.put("tfc", tfc);
        model.put("orientadores", getProfessores());
        model.put("cursos", getCursos());
        model.put("listadisciplinas", getDisciplinas());
        model.put("listatecnologias", getTecnologias());

        return "avaliaProposta";
    }

    @RequestMapping(value = "/avaliaProposta", method = RequestMethod.POST)
    public String guardaAvaliacao(@Valid @ModelAttribute("avaliaPropostaForm") AvaliaPropostaForm avaliaPropostaForm
            , BindingResult bindingResult, ModelMap model, RedirectAttributes ra, Principal user) {

        if (bindingResult.hasErrors()) {
            ServiceTFC serviceTFC = new ServiceTFC();
            HashMap<String, Object> resultado = serviceTFC.vaiBuscarInfoTFC(avaliaPropostaForm.getId(), em);

            TFC tfc = (TFC) resultado.get("tfc");
            String tipoUtilizador = (String) resultado.get("tipoUtilizador");

            model.put("tipoUtilizador", tipoUtilizador);
            model.put("avaliaPropostaForm", avaliaPropostaForm);
            model.put("tfc", tfc);
            model.put("orientadores", getProfessores());
            model.put("cursos", getCursos());
            model.put("listadisciplinas", getDisciplinas());
            model.put("listatecnologias", getTecnologias());
            return "avaliaProposta";
        }

        HashMap<String, Object> tfc = new HashMap<>();

        tfc.put("preponente", avaliaPropostaForm.getPreponente());
        String entidade = avaliaPropostaForm.getEntidade();
        if (entidade != null && !entidade.isEmpty()) {
            tfc.put("entidade", avaliaPropostaForm.getEntidade());
        } else {
            tfc.put("entidade", avaliaPropostaForm.getNomeEmpresa());
        }
        tfc.put("entidadeTFC", avaliaPropostaForm.getEntidadeTFC());
        tfc.put("cursoTFC", avaliaPropostaForm.getCursoTFC());
        tfc.put("disciplinasTFC", avaliaPropostaForm.getDisciplinasTFC());
        tfc.put("tecnologiasTFC", avaliaPropostaForm.getTecnologiasTFC());
        tfc.put("titulo", avaliaPropostaForm.getTitulo());
        tfc.put("descricao", avaliaPropostaForm.getDescricao());
        tfc.put("cursoAssociado", avaliaPropostaForm.getCursoAssociado());
        tfc.put("disciplinas", avaliaPropostaForm.getDisciplinas());
        tfc.put("tecnologias", avaliaPropostaForm.getTecnologias());
        tfc.put("orientador", avaliaPropostaForm.getOrientador());
        tfc.put("id", avaliaPropostaForm.getId());
        tfc.put("numero2aluno", avaliaPropostaForm.getNumero2Aluno());
        tfc.put("nome2Aluno", avaliaPropostaForm.getNome2Aluno());
        tfc.put("orientadorProposto", avaliaPropostaForm.getOrientador());
        tfc.put("nomeDepartamento", avaliaPropostaForm.getNomeDepartamento());
        tfc.put("email", avaliaPropostaForm.getEmail());
        tfc.put("contato", avaliaPropostaForm.getContato());
        tfc.put("interlocutor", avaliaPropostaForm.getInterlocutor());
        tfc.put("motivoRecusa", avaliaPropostaForm.getMotivoRecusa());
        tfc.put("avaliador", ((DEISIAuthenticationToken) user).getUsername());

        if (avaliaPropostaForm.getTipoUtilizador().equalsIgnoreCase("aluno")) {
            tfc.put("tipoTFC", "Aluno");
        }
        if (avaliaPropostaForm.getTipoUtilizador().equalsIgnoreCase("ndeisi")) {
            tfc.put("tipoTFC", "NDEISI");
        }
        if (avaliaPropostaForm.getTipoUtilizador().equalsIgnoreCase("empresa")) {
            tfc.put("tipoTFC", "Empresa");
        }

        tfc.put("em", em);
        tfc.put("avaliacao", avaliaPropostaForm.getAvaliacao());

        ServiceTFC tfcGenerico = new ServiceTFC();
        String resposta = tfcGenerico.setTfcGenerico(tfc);

        ra.addFlashAttribute("flashInfo", "O TFC foi avaliado");

        return "redirect:/listarpropostas";

    }

    @RequestMapping(value = "/ver/{id}", method = RequestMethod.GET)
    public String verTFC(ModelMap model, @PathVariable("id") Long id, Principal user) {

        ServiceTFC serviceTFC = new ServiceTFC();
        HashMap<String, Object> resultado = serviceTFC.vaiBuscarInfoTFC(id, em);

        String tipoUtilizador = (String) resultado.get("tipoUtilizador");

        if (tipoUtilizador.equalsIgnoreCase("aluno")) {
            AlunoForm alunoForm = (AlunoForm) resultado.get("form");
            model.put("form", alunoForm);
        }
        if (tipoUtilizador.equalsIgnoreCase("coordenador")) {
            ProfForm profForm = (ProfForm) resultado.get("form");
            model.put("form", profForm);
        }
        if (tipoUtilizador.equalsIgnoreCase("professordeisi")) {
            ProfForm profForm = (ProfForm) resultado.get("form");
            model.put("form", profForm);
        }

        String aluno = (String) resultado.get("aluno");
        Long numeroGrupo = (Long) resultado.get("grupo");

        ServiceInformacao serviceInformacao = new ServiceInformacao();

        if (aluno != null) {
            Aluno aluno1 = serviceInformacao.informacaoAluno(em, aluno);
            model.put("aluno", aluno1);
        }
        if (numeroGrupo != null) {
            Grupo grupo = serviceInformacao.devolveGrupo(em, numeroGrupo);
            Aluno aluno1Grupo = serviceInformacao.informacaoAluno(em, grupo.getIdNumeroAluno1());
            model.put("aluno1Grupo", aluno1Grupo);
            Aluno aluno2Grupo = serviceInformacao.informacaoAluno(em, grupo.getIdNumeroAluno2());
            model.put("aluno2Grupo", aluno2Grupo);
        }


        TFC tfc = (TFC) resultado.get("tfc");

        Utilizador utilizador = em.createQuery("select u from Utilizador u where u.idIdentificacao=:id",
                Utilizador.class).setParameter("id", user.getName()).getSingleResult();

        if (user.getName().equalsIgnoreCase(tfc.getPreponente()) ||
                utilizador.isCoordenador()) {

            model.put("tfc", tfc);
            model.put("tipoUtilizador", tipoUtilizador);
            model.put("orientadores", getProfessores());
            model.put("cursos", getCursos());
            model.put("listadisciplinas", getDisciplinas());
            model.put("listatecnologias", getTecnologias());

            return "ver";
        } else {
            return "redirect:/listar";
        }


    }

    @RequestMapping(value = "/deleteinscricao/{id}", method = RequestMethod.GET)
    public String deleteInscricao(ModelMap model, @PathVariable("id") Long id, RedirectAttributes ra) {

        ServiceInscricao serviceInscricao = new ServiceInscricao();
        serviceInscricao.apagaInscricao(em, id);

        ra.addFlashAttribute("flashInfo", "A sua inscrição no TFC foi apagada.");
        return "redirect:/listatfc";
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String delete(ModelMap model, @PathVariable("id") Long id, RedirectAttributes ra) {

        TFC tfc = em.find(TFC.class, id);
        String compara = tfc.getIdtfc().replaceAll("\\d", "");

        String destino = "";
        if (compara.equals("Aluno") || compara.equals("DEISI")) {
            destino = "redirect:/listar";
        } else if (compara.equals("Empresa")) {
            destino = "redirect:/dashboardEmpresa";
        } else {
            destino = "redirect:/dashboardNDEISI";
        }
        em.remove(tfc);

        ra.addFlashAttribute("flashInfo", "O TFC com id " + id + " foi removido.");
        return destino;
    }

    @RequestMapping(value = "/formprofessor", method = RequestMethod.GET)
    public String formprofessor(ModelMap model, Principal user) {

        ServiceInformacao serviceInformacao = new ServiceInformacao();
        Utilizador utilizador = serviceInformacao.devolveUtilizador(em, user.getName());

        ProfForm profForm = new ProfForm();
        model.put("erroBinding", true);
        model.put("profForm", profForm);
        model.put("cursos", getCursos());
        model.put("listadisciplinas", getDisciplinas());
        model.put("listatecnologias", getTecnologias());

        return ("formprofessor");
    }


    @RequestMapping(value = "/formprofessor", method = RequestMethod.POST)
    public String formprofessor(@Valid @ModelAttribute("profForm") ProfForm profForm, BindingResult bindingResult,
                                ModelMap model, RedirectAttributes ra, Principal user) {

        if (bindingResult.hasErrors()) {
            ServiceInformacao serviceInformacao = new ServiceInformacao();

            model.put("erroBinding", true);
            model.put("profForm", profForm);
            model.put("cursos", getCursos());
            model.put("listadisciplinas", getDisciplinas());
            model.put("listatecnologias", getTecnologias());
            return "formprofessor";
        }

        HashMap<String, Object> tfc = new HashMap<>();

        if (profForm.getId() == null) {
            tfc.put("idTFC", null);
        } else {
            tfc.put("idTFC", profForm.getId());
        }

        tfc.put("preponente", profForm.getPreponente());
        tfc.put("entidade", profForm.getEntidade());
        tfc.put("entidadeTFC", profForm.getEntidadeTFC());
        tfc.put("cursoTFC", profForm.getCursoTFC());
        tfc.put("disciplinasTFC", profForm.getDisciplinasTFC());
        tfc.put("tecnologiasTFC", profForm.getTecnologiasTFC());
        tfc.put("titulo", profForm.getTitulo());
        tfc.put("descricao", profForm.getDescricao());
        tfc.put("cursoAssociado", profForm.getCursoAssociado());
        tfc.put("disciplinas", profForm.getDisciplinas());
        tfc.put("tecnologias", profForm.getTecnologias());
        tfc.put("orientador", user.getName());
        tfc.put("preponente", user.getName());
        tfc.put("tipoTFC", "DEISI");
        tfc.put("id", profForm.getId());
        tfc.put("em", em);

        ServiceTFC tfcGenerico = new ServiceTFC();
        String resposta = tfcGenerico.setTfcGenerico(tfc);

        ra.addFlashAttribute("flashInfo", resposta);
        return "redirect:/listar";

    }

    @RequestMapping(value = "/formaluno", method = RequestMethod.GET)
    public String formaluno(ModelMap model) {

        AlunoForm alunoForm = new AlunoForm();
        model.put("erroBinding", false);
        model.put("alunoForm", alunoForm);
        model.put("cursos", getCursos());
        model.put("listadisciplinas", getDisciplinas());
        model.put("listatecnologias", getTecnologias());
        model.put("orientadores", getProfessores());
        return "formaluno";
    }

    @RequestMapping(value = "/formaluno", method = RequestMethod.POST)
    public String formaluno(@Valid @ModelAttribute("alunoForm") AlunoForm alunoForm, BindingResult bindingResult,
                            ModelMap model, RedirectAttributes ra, Principal user) {

        if (bindingResult.hasErrors()) {
            model.put("erroBinding", true);
            model.put("alunoForm", alunoForm);
            model.put("cursos", getCursos());
            model.put("listadisciplinas", getDisciplinas());
            model.put("listatecnologias", getTecnologias());
            model.put("orientadores", getProfessores());
            return "formaluno";
        }

        HashMap<String, Object> tfc = new HashMap<>();

        if (alunoForm.getId() == null) {
            tfc.put("idTFC", null);
        } else {
            tfc.put("idTFC", alunoForm.getId());
        }

        tfc.put("preponente", alunoForm.getPreponente());
        tfc.put("entidade", alunoForm.getEntidade());
        tfc.put("entidadeTFC", alunoForm.getEntidadeTFC());
        tfc.put("cursoTFC", alunoForm.getCursoTFC());
        tfc.put("disciplinasTFC", alunoForm.getDisciplinasTFC());
        tfc.put("tecnologiasTFC", alunoForm.getTecnologiasTFC());
        tfc.put("numero2aluno", alunoForm.getNumero2Aluno());
        tfc.put("nome2Aluno", alunoForm.getNome2Aluno());
        tfc.put("titulo", alunoForm.getTitulo());
        tfc.put("descricao", alunoForm.getDescricao());
        /*tfc.put("cursoAssociado", alunoForm.getCursoAssociado());*/
        tfc.put("disciplinas", alunoForm.getDisciplinas());
        tfc.put("tecnologias", alunoForm.getTecnologias());
        tfc.put("orientadorProposto", alunoForm.getOrientador());
        tfc.put("preponente", user.getName());
        tfc.put("tipoTFC", "Aluno");
        tfc.put("id", alunoForm.getId());
        tfc.put("em", em);

        ServiceTFC tfcGenerico = new ServiceTFC();
        String resposta = tfcGenerico.setTfcGenerico(tfc);

        ra.addFlashAttribute("flashInfo", resposta);
        return "redirect:/listar";
    }

    @RequestMapping(value = "/formNDEISI", method = RequestMethod.GET)
    public String formNDEISI(ModelMap model) {
        NdeisiForm ndeisiForm = new NdeisiForm();

        model.put("erroBinding", false);
        model.put("ndeisiForm", ndeisiForm);
        model.put("cursos", getCursos());
        model.put("listadisciplinas", getDisciplinas());
        model.put("listatecnologias", getTecnologias());
        model.put("isPublicPage", true);
        return ("formNDEISI");
    }

    @RequestMapping(value = "/formNDEISI", method = RequestMethod.POST)
    public String formNDEISI(@Valid @ModelAttribute("ndeisiForm") NdeisiForm ndeisiForm, BindingResult bindingResult,
                             ModelMap model, RedirectAttributes ra) {

        if (bindingResult.hasErrors()) {
            model.put("ndeisiForm", ndeisiForm);
            model.put("erroBinding", true);
            model.put("cursos", getCursos());
            model.put("listadisciplinas", getDisciplinas());
            model.put("listatecnologias", getTecnologias());
            model.put("orientadores", getProfessores());
            model.put("isPublicPage", true);
            return "formNDEISI";
        }

        HashMap<String, Object> tfc = new HashMap<>();
        if (ndeisiForm.getId() == null) {
            tfc.put("idTFC", null);
        } else {
            tfc.put("idTFC", ndeisiForm.getId());
        }
        tfc.put("nomeDepartamento", ndeisiForm.getNomeDepartamento());
        tfc.put("email", ndeisiForm.getEmail());
        tfc.put("contato", ndeisiForm.getContato());
        tfc.put("preponente", ndeisiForm.getPreponente());
        tfc.put("entidade", ndeisiForm.getEntidade());
        tfc.put("entidadeTFC", ndeisiForm.getEntidadeTFC());
        tfc.put("cursoTFC", ndeisiForm.getCursoTFC());
        tfc.put("disciplinasTFC", ndeisiForm.getDisciplinasTFC());
        tfc.put("tecnologiasTFC", ndeisiForm.getTecnologiasTFC());
        tfc.put("titulo", ndeisiForm.getTitulo());
        tfc.put("descricao", ndeisiForm.getDescricao());
        tfc.put("cursoAssociado", ndeisiForm.getCursoAssociado());
        tfc.put("disciplinas", ndeisiForm.getDisciplinas());
        tfc.put("tecnologias", ndeisiForm.getTecnologias());
        tfc.put("orientador", ndeisiForm.getOrientador());
        tfc.put("tipoTFC", "NDEISI");
        tfc.put("id", ndeisiForm.getId());
        tfc.put("em", em);

        ServiceTFC tfcGenerico = new ServiceTFC();
        String resposta = tfcGenerico.setTfcGenerico(tfc);

        ra.addFlashAttribute("flashInfo", resposta + " Guarde este id de forma a conseguir editar/apagar o TFC criado.");
        return "redirect:/dashboardNDEISI";
    }

    @RequestMapping(value = "/dashboardNDEISI", method = RequestMethod.GET)
    public String dashboardNDEISI(ModelMap model) {
        model.put("isPublicPage", true);
        return "dashboardNDEISI";
    }

    @RequestMapping(value = "/editNDEISI", method = RequestMethod.GET)
    public String editNDEISI(ModelMap model, @RequestParam("email") String email, @RequestParam("id") String id, RedirectAttributes ra, Principal user) {

        /*
        Para se utilizar o recolher info genérico do ServiceTFC é necessário providenciar o id do TFC
        que irá ser editado. No dashboard do Professor NDEISI este irá fornecer uma String contendo o idTFC.
        Função trabalha apenas com long, log é necessário extrair o ID do tfc (campo idTFC guarda tipo (NDEISI) +
        id do tfc no momento de criação.
        Variável idTFC será o resultado final da extração do id do TFC a ser editado.
         */
        try {
            String idString = id.replaceAll("[^0-9]", "");
            Long idTFC = Long.parseLong(idString);

            TFC tfcEscolhido = em.find(TFC.class, idTFC);

            ProfessorNDEISI professorNDEISI = em.createQuery("select ndeisi from ProfessorNDEISI ndeisi where ndeisi.idProfessor=:idProfessor",
                    ProfessorNDEISI.class).setParameter("idProfessor", tfcEscolhido.getPreponente()).getSingleResult();

            if (!professorNDEISI.getEmail().equalsIgnoreCase(email)) {
                ra.addFlashAttribute("flashInfo", "A combinação email " + email + " e id do TFC " + id + " não corresponde a nenhum email");
                return "redirect:/dashboardNDEISI";
            }

            ServiceTFC serviceTFC = new ServiceTFC();
            HashMap<String, Object> resultado = serviceTFC.vaiBuscarInfoTFC(idTFC, em);

            String tipoUtilizador = (String) resultado.get("tipoUtilizador");
            NdeisiForm ndeisiForm = (NdeisiForm) resultado.get("form");
            TFC tfc = (TFC) resultado.get("tfc");

            model.put("ndeisiForm", ndeisiForm);
            model.put("tfc", tfc);
            model.put("tipoUtilizador", tipoUtilizador);
            model.put("orientadores", getProfessores());
            model.put("cursos", getCursos());
            model.put("listadisciplinas", getDisciplinas());
            model.put("listatecnologias", getTecnologias());
            model.put("isPublicPage", true);

            return "formNDEISI";
        } catch (Exception e) {
            ra.addFlashAttribute("flashInfo", "Não exite um TFC com ID = " + id);
            return "redirect:/dashboardNDEISI";
        }

    }

    @RequestMapping(value = "/dashboardEmpresa", method = RequestMethod.GET)
    public String dashboardEmpresa(ModelMap model) {
        model.put("isPublicPage", true);
        return "dashboardEmpresa";
    }

    @RequestMapping(value = "/formEmpresa", method = RequestMethod.GET)
    public String formEmpresa(ModelMap model) {
        EmpresaForm empresaForm = new EmpresaForm();

        model.put("erroBinding", false);
        model.put("empresaForm", empresaForm);
        model.put("cursos", getCursos());
        model.put("listadisciplinas", getDisciplinas());
        model.put("listatecnologias", getTecnologias());
        model.put("isPublicPage", true);
        return ("formEmpresa");
    }

    @RequestMapping(value = "/formEmpresa", method = RequestMethod.POST)
    public String formEmpresa(@Valid @ModelAttribute("empresaForm") EmpresaForm empresaForm, BindingResult bindingResult,
                              ModelMap model, RedirectAttributes ra) {

        if (bindingResult.hasErrors()) {
            model.put("erroBinding", true);
            model.put("empresaForm", empresaForm);
            model.put("cursos", getCursos());
            model.put("listadisciplinas", getDisciplinas());
            model.put("listatecnologias", getTecnologias());
            model.put("isPublicPage", true);
            return "formEmpresa";
        }

        HashMap<String, Object> tfc = new HashMap<>();

        if (empresaForm.getId() == null) {
            tfc.put("idTFC", null);
        } else {
            tfc.put("idTFC", empresaForm.getId());
        }

        tfc.put("email", empresaForm.getEmail());
        tfc.put("contato", empresaForm.getContato());
        tfc.put("interlocutor", empresaForm.getInterlocutor());
        tfc.put("entidade", empresaForm.getNomeEmpresa());
        tfc.put("preponente", empresaForm.getPreponente());
        tfc.put("entidadeTFC", empresaForm.getEntidadeTFC());
        tfc.put("cursoTFC", empresaForm.getCursoTFC());
        tfc.put("disciplinasTFC", empresaForm.getDisciplinasTFC());
        tfc.put("tecnologiasTFC", empresaForm.getTecnologiasTFC());
        tfc.put("titulo", empresaForm.getTitulo());
        tfc.put("descricao", empresaForm.getDescricao());
        tfc.put("cursoAssociado", empresaForm.getCursoAssociado());
        tfc.put("disciplinas", empresaForm.getDisciplinas());
        tfc.put("tecnologias", empresaForm.getTecnologias());
        tfc.put("orientador", empresaForm.getOrientador());
        tfc.put("morada", empresaForm.getMorada());
        tfc.put("tipoTFC", "Empresa");
        tfc.put("id", empresaForm.getId());
        tfc.put("em", em);

        ServiceTFC tfcGenerico = new ServiceTFC();
        String resposta = tfcGenerico.setTfcGenerico(tfc);

        ra.addFlashAttribute("flashInfo", resposta + " Guarde este id de forma a conseguir editar/apagar o TFC criado.");
        return "redirect:/dashboardEmpresa";
    }

    /**
     * Função que verifica se existe o TFC que Empresa pretende editar
     *
     * @param model
     * @param id
     * @param ra
     * @param user
     * @return
     */
    @RequestMapping(value = "/editEmpresa", method = RequestMethod.GET)
    public String editEmpresa(ModelMap model, @RequestParam("email") String email, @RequestParam("id") String id, RedirectAttributes ra, Principal user) {

         /*
        Para se utilizar o recolher info genérico do ServiceTFC é necessário providenciar o id do TFC
        que irá ser editado. No dashboard do Professor NDEISI e Empresa este irá fornecer uma String contendo o idTFC.
        Função trabalha apenas com long, logo é necessário extrair o ID do tfc (campo idTFC guarda tipo (NDEISI) +
        id do tfc no momento de criação.
        Variável idTFC será o resultado final da extração do id do TFC a ser editado.
         */
        try {
            ServiceInformacao serviceInformacao = new ServiceInformacao();
            String idString = id.replaceAll("[^0-9]", "");
            Long idTFC = Long.parseLong(idString);

            String emailEmpresa = email;

            TFC tfcEscolhido = em.find(TFC.class, idTFC);

            String idEmpresa = tfcEscolhido.getPreponente().replaceAll("[^0-9]", "");
            Long longIdEmpresa = Long.parseLong(idEmpresa);
            Empresa_EntidadeExterna empresa = em.find(Empresa_EntidadeExterna.class, longIdEmpresa);

            if (!empresa.getEmail().equalsIgnoreCase(emailEmpresa)) {
                ra.addFlashAttribute("flashInfo", "O email  " + emailEmpresa + " não corresponde a nenhum TFC.");
                return "redirect:/dashboardEmpresa";
            }

            ServiceTFC serviceTFC = new ServiceTFC();
            HashMap<String, Object> resultado = serviceTFC.vaiBuscarInfoTFC(idTFC, em);

            String tipoUtilizador = (String) resultado.get("tipoUtilizador");

            EmpresaForm empresaForm = (EmpresaForm) resultado.get("form");

            model.put("empresaForm", empresaForm);

            TFC tfc = (TFC) resultado.get("tfc");
            model.put("tfc", tfc);

            model.put("tipoUtilizador", tipoUtilizador);

            model.put("orientadores", getProfessores());
            model.put("cursos", getCursos());
            model.put("listadisciplinas", getDisciplinas());
            model.put("listatecnologias", getTecnologias());
            model.put("isPublicPage", true);

            return "formEmpresa";
        } catch (Exception e) {
            ra.addFlashAttribute("flashInfo", "Não exite um TFC com ID = " + id);
            return "redirect:/dashboardEmpresa";
        }

    }

    //--------Add by Clid-Stelio

    @RequestMapping(value = "/agendarApresentacao", method = RequestMethod.GET)
    public String listarApresentacoes(ModelMap model, Principal user) {

        List<Sessao> sessaos = em.createQuery("select s from Sessao s", Sessao.class).getResultList();

        model.put("listaSessao", sessaos);
        return ("agendarApresentacao");
    }

    @RequestMapping(value = "/formSessao", method = RequestMethod.POST)
    public String agendarApresentacao(ModelMap model, @Valid @ModelAttribute("sessaoForm") SessaoForm sessaoForm, BindingResult result, Principal user) {

        if (result.hasErrors()) {
            return "error";
        }

        Sessao sessao = new Sessao();

        if (sessaoForm.getId() == null) {
            sessao.setId(null);
        } else {
            sessao.setId(sessaoForm.getId());
        }

        String data = sessaoForm.getData();
        String horaInicio = sessaoForm.getHoraInicio();
        String horaFim = sessaoForm.getHoraFim();
        String sala = sessaoForm.getSala();

        System.out.println(data);
        System.out.println(horaInicio);
        System.out.println(horaFim);
        System.out.println(sala);

        sessao.setData(data);
        sessao.setHoraInicio(horaInicio);
        sessao.setHoraFim(horaFim);
        sessao.setSala(sala);

        em.persist(sessao);

        return ("redirect:/agendarApresentacao");
    }

    @RequestMapping(value = "/atribuirJuri", method = RequestMethod.GET)
    public String agendaAprentacao(ModelMap model, Principal user) {

        List<ProfessorDEISI> juris = getProfessores();

        List<Agendamento> agendamentos = getAgendamentos();

        List<Sessao> sessaos = em.createQuery("select s from Sessao s", Sessao.class).getResultList();

        List<Sessao> listaSessaoOriginal = new ArrayList<>();
        listaSessaoOriginal.addAll(sessaos);

        //Elimininar sessaos que ja estao completas
        sessaos = filtrarSessaos(sessaos);

        ProfessorDEISI juri = getProfessor("p4997");
        System.out.println("Nome Juri: " + juri.getNome() + " - " + juri.getNumeroProfessor());
        model.put("juri", juri);
        model.put("agendamentos", agendamentos);
        model.put("listaJuris", juris);
        model.put("listaSessao", sessaos);
        model.put("listaSessaoOriginal", listaSessaoOriginal);

        return ("atribuirJuri");
    }

    @RequestMapping(value = "/formAtribuirJuri", method = RequestMethod.POST)
    public String atribuirJuri(ModelMap model,  @RequestParam("numeroProfessorDEISI") String numeroProfessorDEISI, @RequestParam("idSessao") Float idSessao,  RedirectAttributes ra, Principal user) {


        System.out.println("Numero professor Deisi: "+ numeroProfessorDEISI);
        System.out.println("Id Sessao: " + idSessao);

        Agendamento agendamento = new Agendamento();

        agendamento.setIdSessao(idSessao.longValue());
        agendamento.setNumeroProfessorDEISI(numeroProfessorDEISI);
        //Tipo avaliação: 1 - Intermedia, 2 - 1ºEpoca, 3 - Epoca Especial
        agendamento.setNumeroTipoAvaliacao(1);

        if(faltaJuriNaSessao(idSessao.longValue()) && !juriTemSessao(numeroProfessorDEISI)) {
            em.persist(agendamento);
        } else {
            System.out.println("--> Sessão completa.!..");
        }

        return ("redirect:/atribuirJuri");
    }

    @RequestMapping(value = "/deleteAgenda", method = RequestMethod.POST)
    public String editarAgenda(ModelMap model,  @RequestParam("numeroProfessorDEISI") String numeroProfessorDEISI,  RedirectAttributes ra, Principal user) {


        System.out.println(numeroProfessorDEISI);

        deleteAgenda(numeroProfessorDEISI);

        return ("redirect:/atribuirJuri");
    }

    @RequestMapping(value = "/agendaApresentacaoJuri", method = RequestMethod.GET)
    public String getApresentacaoAgenda(ModelMap model, Principal user) {

        //Temporario(p4997 - numeroProfessor - user.getName())
        ProfessorDEISI professorDEISI = getProfessor("p4997");
        //Obter todos os agendamentos que pertencem ao respectivo professor
        List<Agendamento> agendamentos = getAgendamentosDoJuri(professorDEISI.getNumeroProfessor());

        List<Sessao> sessaos = getSessoesAgendamentos(agendamentos);

        //Obter todos os TFCs que o respectivo professor é o orientador
        //Temporario(p4997 - numeroProfessor - user.getName())
        List<TFC> tfcs = getTFCsSessaoDoJuri("p4997");

        model.put("listaSessao", sessaos);
        model.put("listaTfcAgenda", tfcs);

        return ("agendaApresentacaoJuri");
    }

    @RequestMapping(value = "/agendaApresentacaoDiscente", method = RequestMethod.GET)
    public String getAgendaApresentacao(ModelMap model, Principal user) {

        TFC tfc = getTFCDoDiscente("21704648");
        Sessao sessao = getSessaoDiscente(tfc);

        model.put("sessao", sessao);
        model.put("tfc", tfc);

        return ("agendaApresentacaoDiscente");
    }

    private Sessao getSessaoDiscente(TFC tfcAtribuido){
        String numOrientador = tfcAtribuido.getOrientador();
        Long idSessao = null;
        //Procurar em que sessão o orientador participa
        for (Agendamento agendamento : getAgendamentos()) {
            if(agendamento.getNumeroProfessorDEISI().equals(numOrientador)) {
                System.out.println("Encontrou a sessão do orientador.");
                idSessao = agendamento.getIdSessao();
            }
        }

        //get sessao by id
        return getSessao(idSessao);
    }

    private Sessao getSessao(Long idSessao) {
        return em.createQuery("FROM Sessao s WHERE s.id = :idSessao", Sessao.class).setParameter("idSessao", idSessao).getSingleResult();
    }

    private TFC getTFCDoDiscente(String numeroAluno) {

        //get dos TFCs aceites pela coordenação
        List<TFC> tfcs = em.createQuery("FROM TFC t WHERE t.avaliacaoProposta = :proposta", TFC.class).setParameter("proposta", "Aceite").getResultList();

        for (TFC tfc :  tfcs) {
            //Verificar se o tfc é realizado em grupo
            if(tfc.getIdGrupo() != null) {
                System.out.println("TFC realizado em grupo.");
                Grupo grupo = em.createQuery("select g from Grupo g where g.id = :idTFCGrupo", Grupo.class)
                        .setParameter("idTFCGrupo", tfc.getIdGrupo()).getSingleResult();

                //verificar se o user pertence a este grupo
                if(grupo.getIdNumeroAluno1().equals(numeroAluno) || grupo.getIdNumeroAluno2().equals(numeroAluno)){
                    System.out.println("Encontrou o TFC.");
                    return tfc;
                }
            //Caso o tfc não for em grupo
            } else {
                System.out.println("TFC realizado individualmente.");
                if(tfc.getPreponente().equals(numeroAluno)){
                    System.out.println("Encontrou o TFC.");
                    return tfc;
                }
            }
        }

        return null;
    }

    private List<TFC> getTFCsSessaoDoJuri(String numeroProfessorDEISI) {
        List<Agendamento> agendamentos = getAgendamentos();
        List<TFC> tfcs = new ArrayList<>();

        for(Agendamento a : agendamentos) {
            if(a.getNumeroProfessorDEISI().equals(numeroProfessorDEISI)) {
                Long idSessao = a.getIdSessao();
                //Obter todos os agendamentos que pertencem a essa sessao(idSessao)
                List<Agendamento> agendamentosSessao = getAgendamentosDaSessao(idSessao);
                if(agendamentosSessao != null) {
                    System.out.println("Agendamentos da sessão encontrada na BD.");
                    for (Agendamento agendamento : agendamentosSessao) {
                        String numeroProfessor = agendamento.getNumeroProfessorDEISI();
                        TFC tfc = getTFCDoJuri(numeroProfessor);
                        //caso encontrar o TFC na BD, guardar na lista, Senão deixa a lista vazia
                        if(tfc != null){
                            tfcs.add(tfc);
                        }
                    }
                    return tfcs;
                }
            }
        }
        return null;
    }

    private TFC getTFCDoJuri(String numeroProfessorDEISI) {
        TFC tfc = null;
        //Try para tratar o erro de "No entity found for query"
        try {
            tfc = em.createQuery("FROM TFC t WHERE t.orientador = :numeroOrientador", TFC.class).setParameter("numeroOrientador", numeroProfessorDEISI).getSingleResult();
        } catch (NoResultException e) {
            //--------
        }
        return tfc;
    }


    private List<Agendamento> getAgendamentosDaSessao(Long idSessao) {
        return em.createQuery("FROM Agendamento a WHERE a.idSessao = :idSessaoAgenda", Agendamento.class).setParameter("idSessaoAgenda", idSessao).getResultList();
    }

    private List<Sessao> getSessoesAgendamentos(List<Agendamento> agendamentos) {
        List<Sessao> sessaos = new ArrayList<>();

        for (Agendamento a : agendamentos) {
            Sessao s = em.createQuery("FROM Sessao s WHERE s.id = :idSessao", Sessao.class).setParameter("idSessao", a.getIdSessao()).getSingleResult();
            sessaos.add(s);
        }
        return  sessaos;
    }

    @RequestMapping(value = "/listaAvaliacoes", method = RequestMethod.GET)
    public String listarTfcsAvaliados(ModelMap model, Principal user) {

        //get dos TFCs avaliados (TFCs que ja possuem uma nota)
        List<TFC> tfcs = em.createQuery("FROM TFC t WHERE t.avaliacaoApresentacaoIntermediaIS NOT NULL", TFC.class).getResultList();

        model.put("listaAvaliacoes", tfcs);

        return ("listaAvaliacoes");
    }

    @RequestMapping(value = "/listaTfcsParaAvaliar", method = RequestMethod.GET)
    public String getlistaTFCsAvaliar(ModelMap model, Principal user){

        //Temporario(p4997 - numeroProfessor - user.getName())
        List<TFC> tfcs = getTFCsSessaoDoJuri("p4997");

        model.put("listaTFCsparaavaliar", tfcs);

        return "listaTfcsParaAvaliar";
    }


    @RequestMapping(value = "/formAvaliacao/{id}", method = RequestMethod.GET)
    public String avaliacaoIntermedia(ModelMap model, @PathVariable("id") Long idTFC, Principal user) {

        Apresentacao apresentacao = getApresentacao(idTFC);

        //Criar apresentacao se nao existir
        if(apresentacao == null) {
            System.out.println("Apresentação Não Existe na BD.");
            apresentacao = new Apresentacao(idTFC, 3);
            em.persist(apresentacao);
        }

        ServiceTFC serviceTFC = new ServiceTFC();
        HashMap<String, Object> resultado = serviceTFC.vaiBuscarInfoTFC(idTFC, em);

        TFC tfc = (TFC) resultado.get("tfc");

        //Load Criterios avaliacao
        List<CriterioAvaliacao> criterioAvaliacaos = getCriteriosAvaliacao(0);

        //Load Itens Criterios avaliacao
        List<ItemCriterioAvaliacao> itensCriterioAvaliacaos = getItensCriteriosAvaliacao();

        String aluno = (String) resultado.get("aluno");
        Long numeroGrupo = (Long) resultado.get("grupo");

        ServiceInformacao serviceInformacao = new ServiceInformacao();

        if (aluno != null) {
            Aluno aluno1 = serviceInformacao.informacaoAluno(em, aluno);
            model.put("aluno", aluno1);
        }
        if (numeroGrupo != null) {
            Grupo grupo = serviceInformacao.devolveGrupo(em, numeroGrupo);
            Aluno aluno1Grupo = serviceInformacao.informacaoAluno(em, grupo.getIdNumeroAluno1());
            model.put("aluno1Grupo", aluno1Grupo);
            Aluno aluno2Grupo = serviceInformacao.informacaoAluno(em, grupo.getIdNumeroAluno2());
            model.put("aluno2Grupo", aluno2Grupo);
        }

        //********************************************************
        //vereficar se o tfc ja foi avaliado por todos os juris
        Avaliacao avaliacao = null;
        if(apresentacao.getQtdAvaliacoesSubmetidas() == apresentacao.getQtdJuris()){
            //Load Avaliacao tfc
            avaliacao = getAvaliacao(idTFC, user.getName());

            //Load Notas Criterios do TFC
            List<NotaCriterio> notaCriterios = getNotaAvaliacao(idTFC);

            Double mediaNotaEscala = calcularMediaNotaEscala(notaCriterios, criterioAvaliacaos);
            System.out.println("Media Nota Escala: " + roundNota(mediaNotaEscala, 2));
            //save media na BD
            salvarMediaNotaEscalaNaBD(idTFC, roundNota(mediaNotaEscala, 2));
            //save media no objeto
            avaliacao.setMediaNotaEscala(roundNota(mediaNotaEscala,2));
            //save media da nota dado pelo juri na Escala 20
            Double notaEscala20 = (avaliacao.getMediaNotaEscala() * 20) / avaliacao.getEscalaAvaliacao();
            avaliacao.setNotaIndividualJuri(notaEscala20);

            //------
            List<Avaliacao> avaliacoes = getAvaliacoes();
            //get notaGlobal juri
            Double notaGlobal = getNotaGlobalJuri(avaliacoes);
            //Save nota global no objeto avaliacao
            avaliacao.setNotaGlobalJuri(roundNota(notaGlobal,2));
            //save notaGlobal na BD
            salvarNotaGobalNaBD(idTFC, roundNota(notaGlobal, 2));
            //Save notaGloba no respectivo TFC
            tfc.setAvaliacaoApresentacaoIntermedia(roundNota(notaGlobal, 2));

            model.put("avaliacao", avaliacao);
            model.put("notaCriterios", notaCriterios);
            model.put("tfcAvaliado", true);
        } else {
            model.put("tfcAvaliado", false);
            avaliacao = new Avaliacao(idTFC, user.getName(),mA.getMomentoAvaliacao(0), 7, 2);
            em.persist(avaliacao);
        }

        //model.put("juri", getProfessor(user.getName()));
        model.put("avaliacao", avaliacao);
        model.put("tfc", tfc);
        model.put("criterios", criterioAvaliacaos);
        model.put("ItensCriterios", itensCriterioAvaliacaos);

        return ("formAvaliacao");
    }

    public double roundNota(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    private void salvarNotaGobalNaBD(Long idTFC, Double notaGlobal) {
        Query query = em.createQuery("update Avaliacao set notaGlobalJuri = :notaGlobal where idTfcAtribuido = :idTFC");
        query.setParameter("notaGlobal", notaGlobal);
        query.setParameter("idTFC",idTFC);
        int result = query.executeUpdate();
        System.out.println("Resultado update Nota Global: " + result);
    }

    private Double getNotaGlobalJuri(List<Avaliacao> avaliacoes) {
        Double soma = 0.0;
        for (Avaliacao a:avaliacoes) {
            soma += a.getNotaIndividualJuri();
        }
        return soma / avaliacoes.size();
    }

    private List<Avaliacao> getAvaliacoes() {
        return em.createQuery("SELECT a FROM Avaliacao a", Avaliacao.class).getResultList();
    }

    private Double calcularMediaNotaEscala(List<NotaCriterio> notaCriterios, List<CriterioAvaliacao> criterioAvaliacaos) {
        System.out.println("-----------------------------\nCalculo Nota Criterios: \n----------------------------");
        Map<Long, Double> notas = new HashMap<>();
        Double soma = 0.0;
        for (CriterioAvaliacao c : criterioAvaliacaos) {
            for (NotaCriterio n : notaCriterios) {
                if(n.getIdCriterio() == c.getId()) {
                    Double peso = c.getPeso();
                    Double nota = n.getNota() * (peso / 100);
                    if(!notas.containsKey(c.getId())){
                        notas.put(c.getId(), n.roundNota(nota, 2));
                        System.out.println("Resultado nota Criterio " + c.getId() + ": " + nota);
                    }
                }
            }
        }
        //Calcular media das notas
        for (Double n : notas.values()) {
            soma += n;
        }

        return soma;
    }

    private void salvarMediaNotaEscalaNaBD(Long idTFC, Double mediaNotaEscala) {
        Query query = em.createQuery("update Avaliacao set mediaNotaEscala = :mediaNotaEscala where idTfcAtribuido = :idTFC");
        query.setParameter("mediaNotaEscala", mediaNotaEscala);
        query.setParameter("idTFC",idTFC);
        int result = query.executeUpdate();
        System.out.println("Resultado update Media Escala: " + result);
    }

    @RequestMapping(value = "/saveAvaliacao/{id}", method = RequestMethod.POST)
    public String guardaNotaAvaliacaoIntermedia(ModelMap model, @RequestParam("idTFC") Long idTFC, @RequestParam("notasEscala") String notasEscala, @PathVariable("id") Long idAvaliacao,  RedirectAttributes ra, Principal user) {

        List<Apresentacao> apresentacoes = em.createQuery("select a from Apresentacao a", Apresentacao.class).getResultList();

        for(Apresentacao a : apresentacoes) {
            if(a.getIdTFC() == idTFC) {
                a.addSubmissao();
                Query query = em.createQuery("update Apresentacao set qtdAvaliacoesSubmetidas = :qtdAvaliacoesSubmetidas where idTFC = :idTFC");
                query.setParameter("qtdAvaliacoesSubmetidas", a.getQtdAvaliacoesSubmetidas());
                query.setParameter("idTFC", a.getIdTFC());
                int result = query.executeUpdate();
                System.out.println("Resultado update: " + result);
            }
        }

        //** Metodo para separar e salvar cada nota no respectivo criterio avaliacao
        String idUser = user.getName();
        salvarNotasNaBD(notasEscala, idTFC, idUser);

        model.put("idAvaliacao", idAvaliacao);
        model.put("idTFC", idTFC);

        return "/aguardaOutrasSubmissao";
    }

    //** Metodo para separar e salvar cada nota no respectivo criterio avaliacao
    private void salvarNotasNaBD(String notasEscala, Long idTFC, String idUser) {
        System.out.println("----> Notas");

        String[] notasKeyValue = notasEscala.split(";");

        List<CriterioAvaliacao> criterioAvaliacaos = em.createQuery("select c from CriterioAvaliacao c", CriterioAvaliacao.class).getResultList();

        for(CriterioAvaliacao criterioAvaliacao : criterioAvaliacaos) {
            for(String notaKv : notasKeyValue) {
                String[] dadosNota = notaKv.split(",");
                Long grupoNotaEscala = Long.parseLong(dadosNota[0]);
                String[] notaEscalaPlusIdCriterio = dadosNota[1].split("_");
                int idCriterioEscala = Integer.parseInt(notaEscalaPlusIdCriterio[0]);
                int notaEscala = Integer.parseInt(notaEscalaPlusIdCriterio[1]);
                System.out.println("Criterio: " + idCriterioEscala + ", Nota: " + notaEscala);

                //Criar uma nova nota
                NotaCriterio notaCriterio = new NotaCriterio(idTFC, criterioAvaliacao.getId(), idUser);

                if(criterioAvaliacao.getId() == idCriterioEscala) {
                    //System.out.println("Grupo: " + dadosNota[0] + ", Nota: " + dadosNota[1]);
                    //guardar nota
                    notaCriterio.setNota(notaEscala);

                    //Guardar nota na base de dados
                    em.persist(notaCriterio);
                }
            }
        }
    }

    @RequestMapping(value = "/verificarSubmissoes/{id}", method = RequestMethod.POST)
    public String verificarSubmissoes(ModelMap model, @RequestParam("idTFC") Long idTFC, @PathVariable("id") Long idAvaliacao, Principal user) {

        Apresentacao apresentacao = getApresentacao(idTFC);

        if(apresentacao !=  null) {
           if(apresentacao.getQtdAvaliacoesSubmetidas() == apresentacao.getQtdJuris()) {

               //Load Criterios avaliacao
               List<CriterioAvaliacao> criterioAvaliacaos = getCriteriosAvaliacao(0);
               //Load Notas Criterios do TFC
               List<NotaCriterio> notaCriterios = getNotaAvaliacao(idTFC);

               //Load avaliacao
               Avaliacao avaliacao = getAvaliacao(idAvaliacao, user.getName());

               //*******************************CALCULO DA DISCREPANCIA
               //Verificar se há discrepancia
               List<Discrepancia> discrepancias = verificarDescrepancia(idTFC, notaCriterios, criterioAvaliacaos, avaliacao.getDiscrepanciaMinimo());

               //--- Redirecionar para pagina de Form avaliação caso não houver discrepancia, senão continue...
               if(discrepancias.isEmpty()){
                    return "redirect:/formAvaliacao/" + idTFC;
               }

                //Obter Criterios da discrepancia
               List<CriterioAvaliacao> criteriosDiscrepancia = obterCriteriosDiscrepancia(discrepancias);

               //Load Itens Criterios avaliacao
               List<ItemCriterioAvaliacao> itensCriterioAvaliacaos = getItensCriteriosAvaliacao();

               model.put("avaliacao", avaliacao);
               model.put("criterios", criteriosDiscrepancia);
               model.put("ItensCriterios", itensCriterioAvaliacaos);
               model.put("notaCriterios", notaCriterios);

               return "formDiscrepancia";
           }
        }

        model.put("idAvaliacao", idAvaliacao);
        model.put("idTFC", idTFC);
        return "aguardaOutrasSubmissao";
    }

    private Apresentacao getApresentacao(Long idTFC) {
        Apresentacao apresentacao = null;
        try {
            apresentacao = em.createQuery("FROM Apresentacao a WHERE a.idTFC = :idTFC", Apresentacao.class).setParameter("idTFC", idTFC).getSingleResult();
        } catch (NoResultException  e) {
            //-----------
        }
        return apresentacao;
    }

    private List<Discrepancia> verificarDescrepancia(Long idTFC, List<NotaCriterio> notaCriterios, List<CriterioAvaliacao> criterioAvaliacaos, int valorDiscrepancia) {
        Map<Long, Discrepancia> hashMapDiscrepancias = new HashMap<>();
        List<Discrepancia> listaDiscrepancias = null;

        for (CriterioAvaliacao criterioAvaliacao : criterioAvaliacaos) {
            List<NotaCriterio> notasCriterioAtual = new ArrayList<>();
            //Obter notas do criterio atual
            for (NotaCriterio notaCriterio : notaCriterios) {
                if(notaCriterio.getIdCriterio() == criterioAvaliacao.getId()) {
                    notasCriterioAtual.add(notaCriterio);
                    System.out.println(notaCriterio.toString());
                }
            }

            //calcular discrepancia
            for(int idx = 1; idx < notasCriterioAtual.size(); idx++) {
                for(int idy = 0; idy < idx; idy++) {
                    int notaJuri_x = notasCriterioAtual.get(idx).getNota();
                    int notaJuri_y = notasCriterioAtual.get(idy).getNota();
                    int diferencaNota = notaJuri_x - notaJuri_y;
                    //Garantir que a diferença sempre seja positivo
                    diferencaNota = diferencaNota > 0 ? diferencaNota : diferencaNota * -1;

                    if(diferencaNota > valorDiscrepancia) {
                        Discrepancia discrepancia = new Discrepancia(idTFC, criterioAvaliacao.getId());
                        if(!hashMapDiscrepancias.keySet().contains(discrepancia.getIdCriterio())){
                            hashMapDiscrepancias.put(discrepancia.getIdCriterio(), discrepancia);
                        }
                    }
                }
            }
        }

        return listaDiscrepancias = new ArrayList<Discrepancia>(hashMapDiscrepancias.values());
    }

    private List<CriterioAvaliacao> obterCriteriosDiscrepancia(List<Discrepancia> discrepancias){
        List<CriterioAvaliacao> criterioAvaliacaos = new ArrayList<>();

        for (Discrepancia d : discrepancias) {
            Long idCriterio = d.getIdCriterio();
            criterioAvaliacaos.add(getCriterio(idCriterio));
        }
        return criterioAvaliacaos;
    }

    @RequestMapping(value = "/formDiscrepancia", method = RequestMethod.GET)
    public String apresentarDiscrepancia(ModelMap model, @RequestParam("idTFC") Long idTFC, @PathVariable("id") Long idAvaliacao, Principal user) {

        System.out.println("Id avaliação: " + idAvaliacao);
        System.out.println("Id TFC: " + idTFC);

        //Load Criterios avaliacao
        List<CriterioAvaliacao> criterioAvaliacaos = getCriteriosAvaliacao(0);

        //Load Itens Criterios avaliacao
        List<ItemCriterioAvaliacao> itensCriterioAvaliacaos = getItensCriteriosAvaliacao();

        model.put("criterios", criterioAvaliacaos);
        model.put("ItensCriterios", itensCriterioAvaliacaos);

        return "formDiscrepancia";
    }

    @RequestMapping(value = "/atualizarNotaAvaliacao/{id}", method = RequestMethod.POST)
    public String atualizarNota(ModelMap model, @RequestParam("idTFC") Long idTFC, @RequestParam("notasEscala") String notasEscala, @RequestParam("justificacoesDiscrepancia") String justificacoesDiscrepancia, @PathVariable("id") Long idAvaliacao,  RedirectAttributes ra, Principal user) {

        //Falta validar as justificaçoes e notas


        //caso a nota foi alterada, atualizar database!
        if(notasEscala != null && !notasEscala.isEmpty()) {
            String[] notasKeyValue = notasEscala.split(";");

            for(String notaKv : notasKeyValue) {
                String[] dadosNota = notaKv.split(",");
                Long grupoNotaEscala = Long.parseLong(dadosNota[0]);
                String[] notaEscalaPlusIdCriterio = dadosNota[1].split("_");
                Long idCriterioEscala = Long.parseLong(notaEscalaPlusIdCriterio[0]);
                int notaEscala = Integer.parseInt(notaEscalaPlusIdCriterio[1]);
                System.out.println("Criterio: " + idCriterioEscala + ", Nota: " + notaEscala);


                //Atualizar nota na base de dados
                Query query = em.createQuery("update NotaCriterio set nota = :notaEscala where idTFC = :idTFC AND idCriterio = :idCriterioEscala AND idUser = :idUserEscala");
                query.setParameter("idCriterioEscala", idCriterioEscala);
                query.setParameter("idTFC", idTFC);
                query.setParameter("notaEscala", notaEscala);
                query.setParameter("idUserEscala", user.getName());
                int result = query.executeUpdate();
                System.out.println("Resultado update: " + result);
            }
        }

        model.put("idAvaliacao", idAvaliacao);
        model.put("idTFC", idTFC);

        return "/aguardaOutrasSubmissao";
    }


    @RequestMapping(value = "/publicarNota", method = RequestMethod.GET)
    public String publicarNota(ModelMap model, Principal user) {

        //Errado
        List<TFC> tfcs = getTFCsSessaoDoJuri("p4997");

        model.put("listaAvaliacoes", tfcs);

        return ("publicarNota");
    }
    //------------------------------------------------------------------------------------


    private Avaliacao getAvaliacao(Long idAvaliacao, String idJuri) {
        return em.createQuery("FROM Avaliacao a WHERE a.id = :idAvaliacao AND a.idJuri = :idJuri", Avaliacao.class).setParameter("idAvaliacao", idAvaliacao).setParameter("idJuri", idJuri).getSingleResult();
    }

    private List<Avaliacao> getAvaliacoes(Long idTFC, String momentoAvaliacao) {
        return em.createQuery("FROM Avaliacao a WHERE a.idTfcAtribuido = :idTFC AND a.momentoAvaliacao = :momento", Avaliacao.class).setParameter("idTFC", idTFC).
                setParameter("momento", momentoAvaliacao).getResultList();
    }

    private List<Avaliacao> getAvaliacoes(Long idTFC) {
        Map<String, Avaliacao> mapAvaliacaos = new HashMap();
        List<Avaliacao> avaliacaos = em.createQuery("FROM Avaliacao a WHERE a.idTfcAtribuido = :idTFC", Avaliacao.class).setParameter("idTFC", idTFC).getResultList();

        for(Avaliacao a : avaliacaos) {
            mapAvaliacaos.put(a.getMomentoAvaliacao(), a);
        }

        return new ArrayList<>(mapAvaliacaos.values());
    }

    private CriterioAvaliacao getCriterio(Long idCriterio) {

        return em.createQuery("FROM CriterioAvaliacao c WHERE c.id = :idCriterio", CriterioAvaliacao.class).setParameter("idCriterio", idCriterio).getSingleResult();
    }

    private List<CriterioAvaliacao> getCriteriosAvaliacao(int  numMomentoAvaliacao) {
        return em.createQuery("FROM CriterioAvaliacao c WHERE c.numeroMomentoAvaliacao = :numMomentoAvaliacao", CriterioAvaliacao.class).setParameter("numMomentoAvaliacao", numMomentoAvaliacao).getResultList();
    }

    private List<ItemCriterioAvaliacao> getItensCriteriosAvaliacao() {

        return em.createQuery("select i from ItemCriterioAvaliacao i", ItemCriterioAvaliacao.class).getResultList();
    }

    private List<NotaCriterio> getNotaAvaliacao(Long idTFC) {

        return em.createQuery("FROM NotaCriterio n WHERE n.idTFC = :idTFC", NotaCriterio.class).setParameter("idTFC", idTFC).getResultList();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Função que permite recolher todas as tecnologias
     *
     * @return - faz o return das Tecnologias presentes em BD
     */
    private List<Tecnologia> getTecnologias() {
        List<Tecnologia> tecnologias = em.createQuery("select t from Tecnologia t", Tecnologia.class).
                getResultList();
        return tecnologias;
    }

    /**
     * Função que permite recolher todos os cursos
     *
     * @return - faz o return dos Cursos presentes em BD
     */
    private List<Curso> getCursos() {

        List<Curso> cursos = em.createQuery("select c from Curso c", Curso.class).getResultList();
        return cursos;
    }

    private List<ProfessorDEISI> getProfessores() {
        List<ProfessorDEISI> listaProfessores = em.createQuery("select p from ProfessorDEISI p", ProfessorDEISI.class)
                .getResultList();
        return listaProfessores;
    }

    private ProfessorDEISI getProfessor(String numProfessor) {
        ProfessorDEISI professorDEISI = em.createQuery("FROM ProfessorDEISI p WHERE p.numeroProfessor = :numProfessor", ProfessorDEISI.class).setParameter("numProfessor", numProfessor).getSingleResult();
        if(professorDEISI == null) {
            return null;
        }else  {
            return  professorDEISI;
        }
    }

    /**
     * Função que permite recolher todas as disciplinas
     *
     * @return - faz o return das Disciplinas presentes em BD
     */
    private List<Disciplina> getDisciplinas() {
        List<Disciplina> listadisciplinas = em.createQuery("select d from Disciplina d", Disciplina.class).getResultList();
        return listadisciplinas;
    }


    private List<Agendamento> getAgendamentos() {
        return em.createQuery("select a from Agendamento a", Agendamento.class).getResultList();
    }

    private List<Agendamento> getAgendamentosDoJuri(String numeroProfessor) {
        return em.createQuery("from Agendamento a WHERE a.numeroProfessor = :numeroProfessor", Agendamento.class).setParameter("numeroProfessor", numeroProfessor).getResultList();
    }

    /**
     * Função que devolve o nome do user
     *
     * @param user - recebe o id do utilizador
     * @return - devolve o nome do Utilizador
     */
    private String nomeUtilizador(String user) {
        String nomeUtilizador = "";
        List<ProfessorDEISI> utilizador = em.createQuery("select p from ProfessorDEISI p", ProfessorDEISI.class).
                getResultList();
        for (ProfessorDEISI pd : utilizador) {
            if (pd.getNumeroProfessor().equalsIgnoreCase(user)) {
                nomeUtilizador = pd.getNome();
                return nomeUtilizador;
            }
        }
        List<Aluno> listaAluno = em.createQuery("select  a from Aluno a", Aluno.class).getResultList();
        for (Aluno aluno : listaAluno) {
            if (aluno.getNumeroAluno().equalsIgnoreCase(user)) {
                nomeUtilizador = aluno.getNome();
                return nomeUtilizador;
            }
        }
        return nomeUtilizador;
    }

    private void deleteAgenda(String numeroProfessor) {
        ///Query query = em.createQuery("delete Agendamento where idProfessorDEISI = :idProfessorDEISI AND idSessao = :idSessao");
        Query query = em.createQuery("delete Agendamento where numeroProfessor = :numProfessor");
        query.setParameter("numProfessor", numeroProfessor);
        int result = query.executeUpdate();
        System.out.println("Resultado delete: " + result);
    }

    private boolean faltaJuriNaSessao(Long idSessao) {
        List<Agendamento> agendamentos = em.createQuery("select a from Agendamento a", Agendamento.class).getResultList();

        int count = 0;

        for(Agendamento a : agendamentos) {
            if(a.getIdSessao() == idSessao) {
                count++;
            }
        }

        return count < 3 ? true : false;
    }

    private List<Sessao> filtrarSessaos(List<Sessao> sessaos) {
        List<Sessao> sessaoListAux = new ArrayList<>();
        sessaoListAux.addAll(sessaos);

        //Eliminar sessoes completas
        for (Sessao s : sessaoListAux) {
            if(!faltaJuriNaSessao(s.getId())){
                sessaos.remove(s);
            }
        }

        return sessaos;
    }

    private boolean juriTemSessao(String numeroProfessorDEISI) {
        List<Agendamento> agendamentos = getAgendamentos();

        for (Agendamento a : agendamentos) {
            if(a.getNumeroProfessorDEISI().equals(numeroProfessorDEISI)) {
                return true;
            }
        }
        return false;
    }

    @RequestMapping(value="/detalheAvaliacao/{id}", method =RequestMethod.POST)
    public String verDetalheAvaliacao(ModelMap model, @RequestParam("numMomentoAvaliacao") int numMomentoAvaliacao, @PathVariable("id") Long idTFC, Principal user){

        TFC tfc = em.createQuery("FROM TFC t WHERE t.id = :idTFC", TFC.class).setParameter("idTFC", idTFC).getSingleResult();

        List<Avaliacao> avalicoes = getAvaliacoes(idTFC, mA.getMomentoAvaliacao(numMomentoAvaliacao));
        List<CriterioAvaliacao> criterioAvaliacoes = getCriteriosAvaliacao(numMomentoAvaliacao);
        List<NotaCriterio> notaCriterios = getNotaAvaliacao(idTFC);

        model.put("tfc", tfc);
        model.put("avaliacoes", avalicoes);
        model.put("criterioAvaliacoes", criterioAvaliacoes);
        model.put("notaCriterios", notaCriterios);

        return "detalheAvaliacao";
    }

    @RequestMapping(value="/detalheAvaliacaoDiscente/{id}", method =RequestMethod.GET)
    public String getNotaAluno(ModelMap model, @RequestParam("numMomentoAvaliacao") int numMomentoAvaliacao, @PathVariable("id") Long idTFC){
        TFC tfc = em.createQuery("FROM TFC t WHERE t.id = :idTFC", TFC.class).setParameter("idTFC", idTFC).getSingleResult();

        List<Avaliacao> avalicoes = getAvaliacoes(idTFC, mA.getMomentoAvaliacao(numMomentoAvaliacao));
        List<CriterioAvaliacao> criterioAvaliacoes = getCriteriosAvaliacao(numMomentoAvaliacao);
        List<NotaCriterio> notaCriterios = getNotaAvaliacao(idTFC);

        model.put("tfc", tfc);
        model.put("momentoAvaliacao", mA.getMomentoAvaliacao(numMomentoAvaliacao));
        model.put("criterioAvaliacoes", criterioAvaliacoes);
        model.put("notaCriterios", notaCriterios);

        return "detalheAvaliacaoDiscente";
    }

    @RequestMapping(value = "consultarNotaAluno", method=RequestMethod.GET)
    public String consultarNota(ModelMap model, Principal user){

        TFC tfc = getTFCDoDiscente(user.getName());

        List<Avaliacao> avaliacoes = getAvaliacoes(tfc.getId());

        model.put("avaliacoes", avaliacoes);
        model.put("tfc", tfc);

        return "consultarNota";
    }
}

